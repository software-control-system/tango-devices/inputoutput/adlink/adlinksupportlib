#include <asl/Data.h>

class MyData : public asl::Data
{
	size_t size () const override{
		return 0;
	}
};

int main(int argc, char* argv[]) {
    MyData * mydata = new MyData();
    return 0;
}
