// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    DAQException.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/DAQException.h>

#if !defined (__ASL_INLINE__)
# include <asl/DAQException.i>
#endif // __ASL_INLINE__

namespace asl {

// ============================================================================
// DAQException::DAQException
// ============================================================================
DAQException::DAQException ()
 : Exception()
{

}

// ============================================================================
// DAQException::DAQException
// ============================================================================
DAQException::DAQException (const char *_reason,
					                  const char *_desc,
					                  const char *_origin,
                            int _code, 
                            int _severity)
 : Exception(_reason, _desc, _origin, _code, _severity)
{

}

// ============================================================================
// DAQException::DAQException
// ============================================================================
DAQException::DAQException (const std::string& _reason,
					                  const std::string& _desc,
					                  const std::string& _origin,
                            int _code, 
                            int _severity)
 : Exception(_reason, _desc, _origin, _code, _severity)
{

}
  
// ============================================================================
// DAQException::DAQException
// ============================================================================
DAQException::DAQException (const DAQException& _src)
  : Exception(_src)
{

}

// ============================================================================
// DAQException::~DAQException
// ============================================================================
DAQException::~DAQException ()
{

}

// ============================================================================
// DAQException::DAQException
// ============================================================================
DAQException& DAQException::operator= (const DAQException& _src) 
{
  Exception::operator=(_src);
  return *this;
}

// ============================================================================
// DataLostException::DataLostException
// ============================================================================
DataLostException::DataLostException (int _why)
: DAQException("lost data", 
               "DAQ aborted due data lost", 
               "DAQ internal process", 
               -1, 
               asl::ERR), why_(_why)
{

}

// ============================================================================
// DataLostException::~DataLostException
// ============================================================================
DataLostException::~DataLostException ()
{

}

// ============================================================================
// DeviceBusyException::DeviceBusyException
// ============================================================================
DeviceBusyException::DeviceBusyException (const char *_reason,
					                                const char *_desc,
					                                const char *_origin,
                                          int _code, 
                                          int _severity)
: DAQException(_reason, _desc, _origin, _code, _severity) 
{

}

// ============================================================================
// DeviceBusyException::DeviceBusyException
// ============================================================================
DeviceBusyException::DeviceBusyException (const std::string& _reason,
					                                const std::string& _desc,
					                                const std::string& _origin,
                                          int _code, 
                                          int _severity)
: DAQException(_reason, _desc, _origin, _code, _severity) 
{

}

// ============================================================================
// DeviceBusyException::~DeviceBusyException
// ============================================================================
DeviceBusyException::~DeviceBusyException ()
{

}

} // namespace asl


