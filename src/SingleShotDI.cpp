// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    SingleShotDI.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <iostream>
#include <asl/SingleShotDI.h>
#include <asl/DAQException.h>

#if !defined (__ASL_INLINE__)
# include <asl/SingleShotDI.i>
#endif // __ASL_INLINE__

namespace asl {

// ============================================================================
// SingleShotDI:: SingleShotDI
// ============================================================================
SingleShotDI::SingleShotDI () 
  : SingleShotDAQ(), daq_hw_ (0)
{
  //- std::cout << "SingleShotDI::SingleShotDI <-" << std::endl;
  //- std::cout << "SingleShotDI::SingleShotDI ->" << std::endl;
}

// ============================================================================
// SingleShotDI:: ~SingleShotDI
// ============================================================================
SingleShotDI::~SingleShotDI () 
{
	//- std::cout << "SingleShotDI::~SingleShotDI <-" << std::endl;

	//- release DAQ hardware
	if (this->daq_hw_) {
		this->daq_hw_->release();
		this->daq_hw_ = 0;
	}

	//- std::cout << "SingleShotDI::~SingleShotDI ->" << std::endl;
}

// ============================================================================
// SingleShotDI::init
// ============================================================================
void SingleShotDI::init (unsigned short _type, unsigned short _id)
  throw (asl::DAQException)
{
	try 
	{
		//- instanciate the board - don't get exclusive access
		this->daq_hw_ = adl::ADSingleShotDIBoard::instanciate(_type, _id, false);
	}
	catch (const asl::DAQException&) 
	{
		//- rethrow exception
		throw;
	}
	catch (...) 
	{
		//- throw unknown error
		throw DAQException();
	}

}

// ============================================================================
// SingleShotDI::configure_port
// ============================================================================
void SingleShotDI::configure_port (adl::DIOPort _port) 
  throw (asl::DAQException)
{
	//- std::cout<<"SingleShotDI::configure_port<-"<<std::endl;
	if (this->initialized() == false) {
		throw asl::DAQException("unitialized DAQ",
			"SingleShotDI::init must be called before any other functions",
			"SingleShotDI::configure_port");
	}
	
	try
	{
		this->daq_hw_->configure_di_port(_port);
	} 
	catch (const DAQException&) 
	{
		throw;
	}
	catch (...)
	{
		throw DAQException();
	}
	//- std::cout<<"SingleShotDI::configure_port->"<<std::endl; 
}

// ============================================================================
// SingleShotDI::read_line
// ============================================================================
bool SingleShotDI::read_line (adl::DIOPort _port, int _line) 
throw (asl::DAQException)
{
	if (this->initialized() == false)
	{
		throw asl::DAQException("unitialized DAQ",
			"SingleShotDI::init must be called before any other functions",
			"SingleShotDI::read_line");
	}
	
	try
	{
		return this->daq_hw_->read_line(_port, _line);
	} 
	catch (const DAQException&) 
	{
		throw;
	}
	catch (...)
	{
		throw DAQException();
	}
}

// ============================================================================
// SingleShotDI::read_port
// ============================================================================
unsigned long SingleShotDI::read_port (adl::DIOPort _port) 
throw (asl::DAQException)
{
	if (this->initialized() == false)
	{
		throw asl::DAQException("unitialized DAQ",
			"SingleShotDI::init must be called before any other functions",
			"SingleShotDI::read_port");
	}
	
	try
	{
		return this->daq_hw_->read_port(_port);
	} 
	catch (const DAQException&) 
	{
		throw;
	}
	catch (...) {
		throw DAQException();
	}
}

} // namespace asl




