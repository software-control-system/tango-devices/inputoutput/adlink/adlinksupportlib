// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ContinuousDAQHandler.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <iostream>
#include <asl/ContinuousDAQ.h>
#include <asl/ContinuousDAQHandler.h>

namespace asl {

// ============================================================================
// ContinuousDAQHandler::ContinuousDAQHandler
// ============================================================================
ContinuousDAQHandler::ContinuousDAQHandler ()
: Thread (false)
{

}

// ============================================================================
// ContinuousDAQHandler::~ContinuousDAQHandler
// ============================================================================
ContinuousDAQHandler::~ContinuousDAQHandler ()
{
 
}

// ============================================================================
// ContinuousDAQHandler::call_handle_data_lost_exception 
// ============================================================================
void ContinuousDAQHandler::call_handle_data_lost_exception (ContinuousDAQ * _daq)
{
  if (_daq == 0) return;
  this->action_ = HANDLE_OVERRUN;
  this->run((void*)_daq);
}

// ============================================================================
// ContinuousDAQHandler::call_handle_daq_exception 
// ============================================================================
void ContinuousDAQHandler::call_handle_daq_exception (ContinuousDAQ * _daq)
{
  if (_daq == 0) return;
  this->action_ = HANDLE_DAQ_ERROR;
  this->run((void*)_daq);
}

// ============================================================================
// ContinuousDAQHandler::ccall_handle_hardware_calibration
// ============================================================================
#if defined (USE_ASYNC_CALIBRATION)
void ContinuousDAQHandler::call_handle_hardware_calibration (ContinuousDAQ * _daq)
{
  if (_daq == 0) return;
  this->action_ = HANDLE_CALIBRATION;
  this->run((void*)_daq);
}
#endif

// ============================================================================
// ContinuousDAQHandler::svc
// ============================================================================
ACE_THR_FUNC_RETURN ContinuousDAQHandler::svc (void* _arg)
{
  //- check argument
  if (_arg == 0) {
    return 0;
  }
  
  //- cast <_arg> into a ContinuousDAQ*
  ContinuousDAQ * daq = ACE_reinterpret_cast(ContinuousDAQ*, _arg);

  try 
  {
    switch (this->action_) 
    {
      case HANDLE_OVERRUN:
        //- extern call to <handle_overrun>
        daq->handle_data_lost_exception();
        break;
      case HANDLE_DAQ_ERROR:
        //- extern call to <abort>
        daq->handle_daq_exception();
        break;
#if defined (USE_ASYNC_CALIBRATION)
      case HANDLE_CALIBRATION:
        //- extern call to <calibrate>
        daq->handle_hardware_calibration();
        break;
#endif
    }
  }
  catch (...) 
  {
    //- ignore: nothing else to do
  }

  //- commit suicide
  delete this;

  return 0;
} 

} // namespace asl


