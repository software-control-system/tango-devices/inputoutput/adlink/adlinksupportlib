// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ContinuousDAQ.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/ContinuousDAQ.h>

#if !defined (__ASL_INLINE__)
# include <asl/ContinuousDAQ.i>
#endif // __ASL_INLINE__

namespace asl {

// ============================================================================
// STATICs
// ============================================================================
#if defined(ASL_DEBUG)
ASL_EXPORT AtomicOp ContinuousDAQ::instance_counter_ = 0;
#endif

// ============================================================================
// ContinuousDAQ::ContinuousDAQ
// ============================================================================
ContinuousDAQ::ContinuousDAQ ()
 : exception_status_(HANDLING_NONE),
	 state_ (UNKNOWN)
#if defined (USE_ASYNC_CALIBRATION)
   ,hw_calibration_successfull_(true)
#endif
{
#if defined(ASL_DEBUG)
  //- increment instance counter
  ++ContinuousDAQ::instance_counter_;
#endif  
}

// ============================================================================
// ContinuousDAQ::~ContinuousDAQ
// ============================================================================
ContinuousDAQ::~ContinuousDAQ ()
{
#if defined(ASL_DEBUG)
  //- decrement instance counter
  --ContinuousDAQ::instance_counter_;
#endif  
}

#if defined(ASL_DEBUG)
// ============================================================================
// ContinuousDAQ::instance_counter
// ============================================================================
u_long ContinuousDAQ::instance_counter () 
{
  return instance_counter_.value();
}
#endif  

// ============================================================================
// ContinuousDAQ::calibrate_hardware
// ============================================================================
void ContinuousDAQ::calibrate_hardware ()
 throw (asl::DAQException, asl::DeviceBusyException)
{
#if defined (USE_ASYNC_CALIBRATION)
  this->hw_calibration_successfull_ = true;
#endif
}

#if defined (USE_ASYNC_CALIBRATION)
// ============================================================================
// ContinuousDAQ::check_hardware_calibration
// ============================================================================
void ContinuousDAQ::check_hardware_calibration ()
     throw (asl::DAQException)
{
  if (this->hw_calibration_successfull_ == false) {
    throw this->hw_calibration_exception_;
  }
}

// ============================================================================
// ContinuousDAQ::calibration_completed
// ============================================================================
bool ContinuousDAQ::calibration_completed ()
{
  return this->exception_status_ != HANDLING_CALIBRATION;
}

// ============================================================================
// ContinuousDAQ::handle_hardware_calibration
// ============================================================================
void ContinuousDAQ::handle_hardware_calibration () 
    throw (asl::DAQException)
{
  //- noop
}
#endif //- USE_ASYNC_CALIBRATION

// ============================================================================
// ContinuousDAQ::handle_data_lost_exception
// ============================================================================
void ContinuousDAQ::handle_data_lost_exception () 
    throw (asl::DAQException)
{
  //- noop
}

// ============================================================================
// ContinuousDAQ::daq_end_reason
// ============================================================================
ContinuousDAQ::DaqEndReason ContinuousDAQ::daq_end_reason ()
{
  return ContinuousDAQ::DAQEND_ON_UNKNOWN_EVT;
}

} // namespace asl


