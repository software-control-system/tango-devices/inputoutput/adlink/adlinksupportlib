// ============================================================================
//
// = CONTEXT
//    Data Stream Model 
//
// = FILENAME
//    Work.cpp
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

// ============================================================================
// HEADERs
// ============================================================================
#include <asl/Work.h>

#if !defined (__ACE_INLINE__)
# include <asl/Work.i>
#endif /* __ACE_INLINE__ */

namespace asl { 

// ============================================================================
// STATICs
// ============================================================================
#if defined(ASL_DEBUG)
AtomicOp Work::instance_counter_ = 0;
#endif

/// ============================================================================
// Work::Work
// ============================================================================
Work::Work ()
{
  //-noop
}

// ============================================================================
// Work::~Work
// ============================================================================
Work::~Work()
{
  //-noop
}

#if defined(ASL_DEBUG)
// ============================================================================
// Work::instance_counter
// ============================================================================
u_long Work::instance_counter () 
{
  return instance_counter_.value();
}
#endif  

// ============================================================================
// Work::on_init - default implementation: does nothing
// ============================================================================
int Work::on_init (WorkerError *&err)
{
  return this->default_hook_impl(err);
}

// ============================================================================
// Work::on_start - default implementation: does nothing
// ============================================================================
int Work::on_start (WorkerError *&err)
{
  return this->default_hook_impl(err);
}

// ============================================================================
// Work::on_stop - default implementation: does nothing
// ============================================================================
int Work::on_stop (WorkerError *&err)
{
  return this->default_hook_impl(err);
}

// ============================================================================
// Work::on_abort - default implementation: does nothing
// ============================================================================
int Work::on_abort (WorkerError *&err)
{
  return this->default_hook_impl(err);
}

// ============================================================================
// Work::on_reset - default implementation: does nothing
// ============================================================================
int Work::on_reset (WorkerError *&err)
{
  return this->default_hook_impl(err);
}

// ============================================================================
// Work::on_quit - default implementation: does nothing
// ============================================================================
void Work::on_quit ()
{
  return;
}

// ============================================================================
// Work::on_quit - default implementation: does nothing
// ============================================================================
int Work::process (Message *in, WorkerError *&err)
{
  return this->default_hook_impl(err, in);
}

// ============================================================================
// Work::default_hook_impl
// ============================================================================
int Work::default_hook_impl (asl::WorkerError *&err, Message *in)
{
  err = 0;
  if (in) in->release();
  return 0;
}

} // namespace asl


