// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ContinuousDIConfig.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/ContinuousDIConfig.h>

#if !defined (__ASL_INLINE__)
# include <asl/ContinuousDIConfig.i>
#endif // __ASL_INLINE__

namespace asl {

// ============================================================================
// ContinuousDIConfig::ContinuousDIConfig
// ============================================================================
ContinuousDIConfig::ContinuousDIConfig ()
  : //- default sampling rate: 1000 Hz
    sampling_rate_ (1000.0),
    //- default port width: 16 bits
    port_width_(adl::width_16),
    //- trigger source
    trigger_source_(adl::internal),
    //- start mode
    start_mode_(adl::start_immediatly),
    //- terminator
    terminator_(adl::terminator_off),
    //- request polarity
    request_polarity_(adl::request_di_rising_edge),
    //- ack polarity
    ack_polarity_(adl::ack_di_rising_edge),
    //- trigger polarity
    trigger_polarity_(adl::trigger_di_rising_edge),
    //- default data_lost strategy: ignore
    data_lost_strategy_ (adl::ignore),
    //- default num of scans/buffer: 500 samples (data ready every half-sec)   
    buffer_depth_ (500),
    //- db_event_callback
    db_event_callback_ (0),
    //- di-end_event_callback
    di_end_event_callback_ (0),
    //- default timeout: 3 seconds
    timeout_ (3000)
{

}

// ============================================================================
// ContinuousDIConfig::ContinuousDIConfig
// ============================================================================
ContinuousDIConfig::ContinuousDIConfig (const ContinuousDIConfig& _src)
{
  *this = _src;
}

// ============================================================================
// ContinuousDIConfig::operator=
// ============================================================================
ContinuousDIConfig& ContinuousDIConfig::operator= (const ContinuousDIConfig& _src)
{
  //- no self assign
  if (this == &_src) {
    return *this;
  }
  //- internal sampling rate
  this->sampling_rate_ = _src.sampling_rate_;
  //-port width
  this->port_width_ = _src.port_width_;
  //-trigger source
  this->trigger_source_ = _src.trigger_source_;
  //-start mode
  this->start_mode_ = _src.start_mode_;
  //-terminator
  this->terminator_ =_src.terminator_;
  //-request polarity
  this->request_polarity_= _src.request_polarity_;
  //-ack polarity
  this->ack_polarity_= _src.ack_polarity_;
  //-trigger polarity
  this->trigger_polarity_ = _src.trigger_polarity_;
  //- data_lost strategy
  this->data_lost_strategy_ = _src.data_lost_strategy_;
  //- num of scans per buffer
  this->buffer_depth_ = _src.buffer_depth_;
  //-  db event callback
  this->db_event_callback_ = _src.db_event_callback_;
  //-  di-end event callback
  this->di_end_event_callback_ = _src.di_end_event_callback_;
  //-  trigger event callback
  this->timeout_ = (_src.timeout_ >= 1) ? _src.timeout_ : 1;
  //- return self
  return *this;
}

// ============================================================================
// ContinuousDIConfig::~ContinuousDIConfig
// ============================================================================
ContinuousDIConfig::~ContinuousDIConfig ()
{

}

// ============================================================================
// ContinuousDIConfig::dump
// ============================================================================
void ContinuousDIConfig::dump ()
{
  ACE_DEBUG((LM_DEBUG, "- DI port width......"));
  switch (port_width_) 
  {
    case adl::width_8:
      ACE_DEBUG((LM_DEBUG, "%s\n", "8 bits")); 
      break;
    case adl::width_16:
      ACE_DEBUG((LM_DEBUG, "%s\n", "16 bits")); 
      break;
    case adl::width_32:
      ACE_DEBUG((LM_DEBUG, "%s\n", "32 bits")); 
      break;
    default:
      ACE_DEBUG((LM_DEBUG, "%s\n", "INVALID VALUE")); 
      break;
  }

  ACE_DEBUG((LM_DEBUG, "- buffer depth.......%d\n", buffer_depth_));

  ACE_DEBUG((LM_DEBUG, "- sampling rate......%.2f\n", sampling_rate_));
  
  ACE_DEBUG((LM_DEBUG, "- timeout............%d msec\n", timeout_));

  ACE_DEBUG((LM_DEBUG, "- start mode........."));
  switch (start_mode_) 
  {
    case adl::start_immediatly:
      ACE_DEBUG((LM_DEBUG, "%s\n", "start immediatly")); 
      break;
    case adl::wait_trigger:
      ACE_DEBUG((LM_DEBUG, "%s\n", "wait trigger")); 
      break;
    default:
      ACE_DEBUG((LM_DEBUG, "%s\n", "INVALID VALUE")); 
      break;
  }

  ACE_DEBUG((LM_DEBUG, "- terminator........."));
  switch (terminator_) 
  {
    case adl::terminator_on:
      ACE_DEBUG((LM_DEBUG, "%s\n", "on")); 
      break;
    case adl::terminator_off:
      ACE_DEBUG((LM_DEBUG, "%s\n", "off")); 
      break;
    default:
      ACE_DEBUG((LM_DEBUG, "%s\n", "INVALID VALUE")); 
      break;
  }

  ACE_DEBUG((LM_DEBUG, "- REQ polarity......."));
  switch (request_polarity_) 
  {
    case adl::request_di_rising_edge:
      ACE_DEBUG((LM_DEBUG, "%s\n", "rising edge")); 
      break;
    case adl::request_di_falling_edge:
      ACE_DEBUG((LM_DEBUG, "%s\n", "falling edge")); 
      break;
    default:
      ACE_DEBUG((LM_DEBUG, "%s\n", "INVALID VALUE")); 
      break;
  }

  ACE_DEBUG((LM_DEBUG, "- ACK polarity......."));
  switch (ack_polarity_) 
  {
    case adl::ack_di_rising_edge:
      ACE_DEBUG((LM_DEBUG, "%s\n", "rising edge")); 
      break;
    case adl::ack_di_falling_edge:
      ACE_DEBUG((LM_DEBUG, "%s\n", "falling edge")); 
      break;
    default:
      ACE_DEBUG((LM_DEBUG, "%s\n", "INVALID VALUE")); 
      break;
  }

  ACE_DEBUG((LM_DEBUG, "- trigger polarity..."));
  switch (trigger_polarity_) 
  {
    case adl::trigger_di_rising_edge:
      ACE_DEBUG((LM_DEBUG, "%s\n", "rising edge")); 
      break;
    case adl::trigger_di_falling_edge:
      ACE_DEBUG((LM_DEBUG, "%s\n", "falling edge")); 
      break;
    default:
      ACE_DEBUG((LM_DEBUG, "%s\n", "INVALID VALUE")); 
      break;
  }

  ACE_DEBUG((LM_DEBUG, "- trigger source....."));
  switch (trigger_source_) 
  {
    case adl::internal:
      ACE_DEBUG((LM_DEBUG, "%s\n", "internal")); 
      break;
    case adl::external:
      ACE_DEBUG((LM_DEBUG, "%s\n", "external")); 
      break;
    case adl::handshaking:
      ACE_DEBUG((LM_DEBUG, "%s\n", "handshaking")); 
      break;
    case adl::trigger_clock_10Mhz:
      ACE_DEBUG((LM_DEBUG, "%s\n", "trigger_clock_10Mhz")); 
      break;
    case adl::trigger_clock_20Mhz:
      ACE_DEBUG((LM_DEBUG, "%s\n", "trigger_clock_20Mhz")); 
      break;
    default:
      ACE_DEBUG((LM_DEBUG, "%s\n", "INVALID VALUE")); 
      break;
  }

  ACE_DEBUG((LM_DEBUG, "- overrun strategy..."));
  switch (trigger_source_) 
  {
    case adl::ignore:
      ACE_DEBUG((LM_DEBUG, "%s\n", "ignore")); 
      break;
    case adl::trash:
      ACE_DEBUG((LM_DEBUG, "%s\n", "trash")); 
      break;
    case adl::notify:
      ACE_DEBUG((LM_DEBUG, "%s\n", "notify")); 
      break;
    case adl::restart:
      ACE_DEBUG((LM_DEBUG, "%s\n", "restart")); 
      break;
    case adl::abort:
      ACE_DEBUG((LM_DEBUG, "%s\n", "abort")); 
      break;
    default:
      ACE_DEBUG((LM_DEBUG, "%s\n", "INVALID VALUE")); 
      break;
  }
}

} // namespace asl



