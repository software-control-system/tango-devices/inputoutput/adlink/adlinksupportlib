// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    Message.cpp
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <ace/Singleton.h>
#include <ace/Malloc_Allocator.h>
#include <asl/Data.h>
#include <asl/Message.h>

#if !defined (__ASL_INLINE__)
#include "asl/Message.i"
#endif /* __ASL_INLINE__ */

namespace asl {

// ============================================================================
// STATICs
// ============================================================================
#if defined(ASL_DEBUG)
ASL_EXPORT AtomicOp DataBlock::instance_counter_ = 0;
ASL_EXPORT AtomicOp Message::instance_counter_ = 0;
#endif

// ============================================================================
// CLASS: MessageAllocator
// ============================================================================
class MessageAllocator : public ACE_New_Allocator  
{
public:
  //-"No-op" ctor.
  MessageAllocator () {};

  //-"No-op" dtor.
  ~MessageAllocator () {};

  //- Memory allocation.
  //- This trick allows the >Message> class to have instance data members.
  //- See ACE_Message_Block::duplicate and Message::duplicate.
  virtual void *malloc (size_t nbytes) {
    ACE_UNUSED_ARG(nbytes);
    return ACE_New_Allocator::malloc(sizeof(Message));
  }
};

// ============================================================================
// SINGLETON: MessageAllocator
// ============================================================================
typedef ACE_Singleton<MessageAllocator, ACE_SYNCH_MUTEX> MSG_ALLOCATOR_SINGLETON;
#define MSG_ALLOCATOR (MSG_ALLOCATOR_SINGLETON::instance())
// ----------------------------------------------------------------------------
#if defined (ACE_HAS_EXPLICIT_TEMPLATE_INSTANTIATION)
template class ACE_Singleton<MessageAllocator, ACE_Thread_Mutex>;
#elif defined (ACE_HAS_TEMPLATE_INSTANTIATION_PRAGMA)
#pragma instantiate ACE_Singleton<MessageAllocator, ACE_Thread_Mutex>
#endif 

// ============================================================================
// DataBlock::DataBlock
// ============================================================================
DataBlock::DataBlock (Data * _data, int _flags)
 : ACE_Data_Block((_data != 0) ? _data->total_size() : kDEFAULT_MSG_SIZE,
                   ACE_Message_Block::MB_DATA,
                   ACE_reinterpret_cast(const char*, _data),
                   0,
                   0,
                   ACE_Message_Block::DONT_DELETE,
                   0), 
   flags_ (_flags),
   data_ (_data)
{
#if defined(ASL_DEBUG)
  //- increment instance counter
  ++DataBlock::instance_counter_;
#endif 
}

// ============================================================================
// DataBlock::~DataBlock
// ============================================================================
DataBlock::~DataBlock ()
{
  //- release data if no ownership assumed 
  if (this->flags_== MSG_DELETE_DATA && this->data_ != 0) {
    delete this->data_;
  }

#if defined(ASL_DEBUG)
  //- decrement instance counter
  --DataBlock::instance_counter_;
#endif 
}

// ============================================================================
// DataBlock::data
// ============================================================================
void DataBlock::data (Data * _data)
{
  //- release data if no ownership assumed 
  if (this->flags_== MSG_DELETE_DATA && this->data_ != 0) {
    delete this->data_;
  }
  //- then adopt new data
  this->data_ = _data;
}

// ============================================================================
// Message::Message
// ============================================================================
Message::Message (int _msg_type, bool _force_proc)
  : ACE_Message_Block(),
    force_processing_ (_force_proc)
{
#if defined(ASL_DEBUG)
  //- increment instance counter
  ++Message::instance_counter_;
#endif

  //- set message type
  this->msg_type(_msg_type);
  this->set_priority_i();
}

// ============================================================================
// Message::Message
// ============================================================================
Message::Message (Data * _data, int _msg_type, bool _force_proc, int _flags)
  : ACE_Message_Block (new DataBlock (_data, _flags), 0, MSG_ALLOCATOR),
    force_processing_ (_force_proc)
{
#if defined(ASL_DEBUG)
  //- increment instance counter
  ++Message::instance_counter_;
#endif

  //- set message type
  this->msg_type(_msg_type);
  this->set_priority_i();
}

// ============================================================================
// Message::~Message
// ============================================================================
Message::~Message ()
{
#if defined(ASL_DEBUG)
  //- decrement instance counter
  --Message::instance_counter_;
#endif
}

// ============================================================================
// Message::set_priority_i
// ============================================================================
void Message::set_priority_i ()
{
  switch (this->msg_type()) 
  {    
    case MessageType::MSG_START:
      this->msg_priority(MessagePriority::MSG_PRIO_START);
      break;
    case MessageType::MSG_RESET:
      this->msg_priority(MessagePriority::MSG_PRIO_RESET);
      break;
    case MessageType::MSG_STOP:
      this->msg_priority(MessagePriority::MSG_PRIO_STOP);
      break;
    case MessageType::MSG_ABORT:
      this->msg_priority(MessagePriority::MSG_PRIO_ABORT);
      break;
    case MessageType::MSG_QUIT:
      this->msg_priority(MessagePriority::MSG_PRIO_QUIT);
      break;
    case MessageType::MSG_FATAL_ERROR:
      this->msg_priority(MessagePriority::MSG_PRIO_FATAL_ERROR);
      break;
    case MessageType::MSG_TASK_PRIORITY:
      this->msg_priority(MessagePriority::MSG_PRIO_TASK_PRIO);
      break;
    default:
      this->msg_priority(MessagePriority::MSG_PRIO_DEFAULT);
      break;
  }
}

#if defined(ASL_DEBUG)
// ============================================================================
// Message::instance_counter
// ============================================================================
u_long Message::instance_counter () 
{
  return instance_counter_.value();
}
#endif  

} // namespace asl


