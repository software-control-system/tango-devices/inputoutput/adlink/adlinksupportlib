// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    SingleShotAI.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================

#include <asl/SingleShotAI.h>

#if !defined (__ASL_INLINE__)
# include <asl/SingleShotAI.i>
#endif // __ASL_INLINE__

namespace asl {

// ============================================================================
// SingleShotAI:: SingleShotAI
// ============================================================================
SingleShotAI:: SingleShotAI () 
  : SingleShotDAQ(), daq_hw_ (0)
{
  //- std::cout << "SingleShotAI::SingleShotAI <-" << std::endl;

  //- std::cout << "SingleShotAI::SingleShotAI ->" << std::endl;
}

// ============================================================================
// SingleShotAI:: ~SingleShotAI
// ============================================================================
SingleShotAI::~SingleShotAI () 
{
  //- std::cout << "SingleShotAI::~SingleShotAI <-" << std::endl;
  //- release DAQ hardware
  if (this->daq_hw_) {
    this->daq_hw_->release();
    this->daq_hw_ = 0;
  }
  //- std::cout << "SingleShotAI::~SingleShotAI ->" << std::endl;
}

// ============================================================================
// SingleShotAI::init
// ============================================================================
void SingleShotAI::init (unsigned short _type, unsigned short _id)
throw (asl::DAQException)
{
	try 
	{
		//- instanciate the board - don't get exclusive access
		this->daq_hw_ = adl::ADSingleShotAIBoard::instanciate(_type, _id, false);
	}
	catch (const asl::DAQException&) 
	{
		//- rethrow exception
		throw;
	}
	catch (...) 
	{
		//- throw unknown error
		throw DAQException();
	}
}

// ============================================================================
// SingleShotAI::calibrate_hardware
// ============================================================================
void SingleShotAI::calibrate_hardware ()
throw (asl::DAQException)
{
	try 
	{
		//- calibrate hardware
		this->daq_hw_->auto_calibrate(false);
		//- save calibration dat into default bank
		this->daq_hw_->save_calibration(kDEFAULT_CALIBRATION_BANK);
	}
	catch (const DAQException&)
	{
		throw;
	}
	catch (...) 
	{
		throw DAQException();
	}
}
// ============================================================================
// SingleShotAI::configure_channel
// ============================================================================
void SingleShotAI::configure_channel (adl::ChanId _chan_id, 
									  adl::Range _range, 
									  adl::GroundRef _ref) 
									  throw (asl::DAQException)
{
	if (this->initialized() == false) {
		throw asl::DAQException("unitialized DAQ",
			"SingleShotAI::init must be called before any other functions",
			"SingleShotAI::configure_channel");
	}
	
	try
	{
		this->daq_hw_->configure_ai_channel(_chan_id,  _range, _ref);
	} 
	catch (const DAQException&)
	{
		throw;
	}
	catch (...) {
		throw DAQException();
	}
	
}

// ============================================================================
// SingleShotAI::read_channel
// ============================================================================
unsigned short SingleShotAI::read_channel (adl::ChanId _chan_id) 
throw (asl::DAQException)
{	
	if (this->initialized() == false)
	{
		throw asl::DAQException("unitialized DAQ",
			"SingleShotAI::init must be called before any other functions",
			"SingleShotAI::read_channel");
	}
	
	try
	{
		return this->daq_hw_->read_channel(_chan_id);
	} 
	catch (const DAQException&) 
	{
		throw;
	}
	catch (...) 
	{
		throw DAQException();
	}
}

// ============================================================================
// SingleShotAI::read_channels
// ============================================================================
asl::AIRawData * SingleShotAI::read_channels (adl::ChanId _max_chan_id) 
throw (asl::DAQException)
{
	if (this->initialized() == false) 
	{
		throw asl::DAQException("unitialized DAQ",
			"SingleShotAI::init must be called before any other functions",
			"SingleShotAI::read_channels");
	}
	
	try
	{
		return this->daq_hw_->read_channels(_max_chan_id);
	} 
	catch (const DAQException&) 
	{
		throw;
	}
	catch (...) 
	{
		throw DAQException();
	}
}

// ============================================================================
// SingleShotAI::read_scaled_channel
// ============================================================================
double SingleShotAI::read_scaled_channel (adl::ChanId _chan_id) 
  throw (asl::DAQException)
{
  if (this->initialized() == false) 
  {
    throw asl::DAQException("unitialized DAQ",
                            "SingleShotAI::init must be called before any other functions",
                            "SingleShotAI::read_scaled_channel");
  }

	try
	{
		return this->daq_hw_->read_scaled_channel(_chan_id);
	} 
	catch (const DAQException&) 
	{
		throw;
	}
	catch (...) 
	{
		throw DAQException();
	}
}

// ============================================================================
// SingleShotAI::read_scaled_channels
// ============================================================================
asl::AIScaledData* SingleShotAI::read_scaled_channels (adl::ChanId _max_chan_id) 
throw (asl::DAQException)
{
	if (this->initialized() == false) 
	{
		throw asl::DAQException("unitialized DAQ",
			"SingleShotAI::init must be called before any other functions",
			"SingleShotAI::read_scaled_channels");
	}
	
	try
	{
		return this->daq_hw_->read_scaled_channels(_max_chan_id);
		
	} 
	catch (const DAQException&) 
	{
		throw;
	}
	catch (...) 
	{
		throw DAQException();
	}
}

} //namespace

