// ============================================================================
//
// = CONTEXT
//    TANGO Project - Tango Tools Library
//
// = FILENAME
//    ContinuousAOConfig.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/ContinuousAOConfig.h>
#include <iostream>
#if !defined (__ASL_INLINE__)
# include <asl/ContinuousAOConfig.i>
#endif // __asl_INLINE__

namespace asl {

// ============================================================================
// STATIC MEMBERS
// ============================================================================
ContinuousAOConfig ContinuousAOConfig::default_config;

// ============================================================================
// ContinuousAOConfig::ContinuousAOConfig
// ============================================================================
ContinuousAOConfig::ContinuousAOConfig ()
  : //- no active channels
    channels_(0),
    //- periodic mode status
    periodic_mode_enabled_ (false),
    //- internal timer triggers ADC conversions
    conversion_source_ (adl::ao_internal_timer),
    //- ao group
    group_ (adl::group_a),
    //- delay counter (only valid in delay mode) source
    delay_counter_source_ (adl::ao_delay_internal_timer),
    //- delay break source (delay betwenn 2 consecutives waveform generations)
    delay_break_counter_source_ (adl::ao_delay_brk_internal_timer),
    //- software trigger
    trigger_source_ (adl::internal_software),
    //- post trigger mode
    trigger_mode_ (adl::ao_post),
    //- disable retrigger
    retrigger_status_ (adl::ao_rt_disabled),
    //- number of waveforms after a trigger
    nb_waveforms_(1),
    //- delay break is disabled
    delay_break_mode_(adl::ao_dbrk_disabled),
    //- trigger on rising edge
    trigger_polarity_ (adl::ao_rising_edge),
    //-delay counter is 0
    delay_counter_ (0),
    //- delay break counter is zero
    delay_break_counter_ (0),
    //- stop source
    stop_source_ (adl::software_termination),
    //- stop mode
    stop_mode_ (adl::stop_immediatly),
    //- analog trigger channel
    analog_trigger_channel_ (adl::analog_trigger_chan0),
    //- default analog trigger condition: above_high_level
    analog_trigger_condition_ (adl::above_high_level),
    //- default analog trigger low condition level: 0
    analog_trigger_lo_level_ (0),
    //- default analog trigger high condition level: 0
    analog_trigger_hi_level_ (0),
    //- default sampling rate: 1000 Hz
    output_rate_ (1000.0),
    //- data format is non-scaled
    format_ (adl::raw_data),
    //- default num of scans per buffer
    buffer_depth_ (500),
    //- db_event_callback
    db_event_callback_ (0)
{
}

// ============================================================================
// ContinuousAOConfig::ContinuousAOConfig
// ============================================================================
ContinuousAOConfig::ContinuousAOConfig (const ContinuousAOConfig& _src)
{
  *this = _src;	
}

// ============================================================================
// ContinuousAOConfig::operator=
// ============================================================================
ContinuousAOConfig& ContinuousAOConfig::operator= (const ContinuousAOConfig& _src)
{
	//- no self assignement
	if (this == &_src) {
		return *this;
	}
	//- analog input active channels
	this->channels_ = _src.channels_;
	//- periodic mode
	this->periodic_mode_enabled_ = _src.periodic_mode_enabled_;
	
  //- copy the src config raw channels in this config
  for(size_t i = 0; i < this->channels_.size(); i++)  
  {   
    // find all channels that could have been allocated
    asl::PeriodicRawBuffersIterator this_it = this->periodic_raw_buffers.find(this->channels_[i].id);
    // a channel has already been allocated in this config so remove it
    if(this_it != this->periodic_raw_buffers.end())
    {
      std::cout<<"delete previous raw channel"<<this->channels_[i].id<<std::endl;
      if(this_it->second)
      {
        delete this_it->second;
        this_it->second = 0;
        this->periodic_raw_buffers.erase(this->channels_[i].id);
      }
    }
    // find channels that have been activated.
    asl::ConstPeriodicRawBuffersIterator it = _src.periodic_raw_buffers.find(this->channels_[i].id);
    if(it != _src.periodic_raw_buffers.end())
    {
      asl::AORawData* src_buf = reinterpret_cast<asl::AORawData*>(it->second);
      asl::AORawData* buffer = new asl::AORawData(*(src_buf));
      asl::PeriodicRawBuffersValue pair(this->channels_[i].id, buffer);
      this->periodic_raw_buffers.insert(pair);
    }
  }
  //- copy the src config scaled channels in this config
  for(size_t j = 0; j < this->channels_.size(); j++)  
  {  
    // find all channels that could have been allocated
    asl::PeriodicScaledBuffersIterator this_it = this->periodic_scaled_buffers.find(this->channels_[j].id); 
    // a channel has already been allocated in this config so remove it
    if(this_it != this->periodic_scaled_buffers.end())
    {
     std::cout<<"delete previous scaled "<<this->channels_[j].id<<std::endl;
     if(this_it->second)
      {
        delete this_it->second;
        this_it->second = 0;
        this->periodic_scaled_buffers.erase(this->channels_[j].id);
      }
    }
    // find channels that have been activated.
    asl::ConstPeriodicScaledBuffersIterator it = _src.periodic_scaled_buffers.find(this->channels_[j].id);
    // a channel has been found in src config, copy it in this config
    if(it != _src.periodic_scaled_buffers.end())
    {
      asl::AOScaledData* src_buf = reinterpret_cast<asl::AOScaledData*>(it->second);
      asl::AOScaledData* buffer = new asl::AOScaledData(*(src_buf));
      //asl::PeriodicScaledBuffersValue pair(this->channels_[i].id, buffer);
         std::cout<<"new scaled "<<this->channels_[j].id<<std::endl;
      if(!this->periodic_scaled_buffers.insert(std::make_pair(this->channels_[j].id, buffer)).second)
      {
        std::cout<<"could not insert in map the channel "<<this->channels_[j].id<<std::endl;
      }
      
    }

  }

  //- conversion source
	this->conversion_source_ = _src.conversion_source_;
	//- ao group
	this->group_ = _src.group_;
	//- delay _counter source
	this->delay_counter_source_ = _src.delay_counter_source_;
	//- delay break source
	this->delay_break_counter_source_ = _src.delay_break_counter_source_;
	//- trigger source
	this->trigger_source_ = _src.trigger_source_;
	//- trigger mode
	this->trigger_mode_ = _src.trigger_mode_;
	//- delay break mode
	this->delay_break_mode_ = _src.delay_break_mode_;
	//- trigger mode
	this->trigger_polarity_ = _src.trigger_polarity_;
	this->retrigger_status_ = _src.retrigger_status_;
	this->nb_waveforms_ = _src.nb_waveforms_;
	//- delay counter
	this->delay_counter_ = _src.delay_counter_;
	//- delay break counter
	this->delay_break_counter_ = _src.delay_break_counter_;
	//- stop source
	this->stop_source_ = _src.stop_source_;
	//- stop mode
	this->stop_mode_ =  _src.stop_mode_;
	//- analog trigger channel
	this->analog_trigger_channel_ = _src.analog_trigger_channel_;
	//- analog trigger condition
	this->analog_trigger_condition_ = _src.analog_trigger_condition_;
	//- analog trigger mode low level condition
	this->analog_trigger_lo_level_ = _src.analog_trigger_lo_level_;
	//- analog trigger mode high level condition
	this->analog_trigger_hi_level_ = _src.analog_trigger_hi_level_;
	//- internal sampling rate
	this->output_rate_ = _src.output_rate_;
	//-data format
	this->format_ = _src.format_;
	//- num of scans per buffer
	this->buffer_depth_ = _src.buffer_depth_;
	//-  db event callback
	this->db_event_callback_ = _src.db_event_callback_;
	//- return itself
	return *this;
} 
// ============================================================================
// ContinuousAOConfig::set_channel_periodic_data
// ============================================================================
void
ContinuousAOConfig::set_channel_periodic_data (unsigned int _channel, const asl::AOScaledData& _pdata)
{  
  asl::PeriodicScaledBuffersIterator it = periodic_scaled_buffers.find(_channel);

  if(it != periodic_scaled_buffers.end())
  {
    //- a buffer already exists for this channel, replace it
    asl::AOScaledData* buffer = new asl::AOScaledData(_pdata);
    asl::AOScaledData* temp = it->second;
    it->second = buffer;
    std::cout<<"ContinuousAOConfig::set_channel_periodic_data : delete scaled "<<_channel<<std::endl;
    delete temp;
  }
  else
  {
    //- store periodic data for the new data
    std::cout<<"ContinuousAOConfig::set_channel_periodic_data : new scaled "<<_channel<<std::endl;
    asl::AOScaledData* buffer = new asl::AOScaledData(_pdata);
    asl::PeriodicScaledBuffersValue pair(_channel, buffer);
    periodic_scaled_buffers.insert(pair);
  }

  this->enable_periodic_mode();
  
  this->set_data_format(adl::scaled_data);
}
// ============================================================================
// ContinuousAOConfig::set_channel_periodic_data
// ============================================================================
void
ContinuousAOConfig::set_channel_periodic_data (unsigned int _channel, const asl::AORawData& _pdata)
{  
  asl::PeriodicRawBuffersIterator it = periodic_raw_buffers.find(_channel);
  
  if(it != periodic_raw_buffers.end())
  {
    //- a buffer already exists for this channel, replace it
    asl::AORawData* buffer = new asl::AORawData(_pdata);
    asl::AORawData* temp = it->second;
    it->second = buffer;
    delete temp;
  }
  else
  {
	//- store periodic data for the new data
    asl::AORawData* buffer = new asl::AORawData(_pdata);
    asl::PeriodicRawBuffersValue pair(_channel, buffer);
    periodic_raw_buffers.insert(pair);
  }

  this->enable_periodic_mode();
  
  this->set_data_format(adl::raw_data);
}
// ============================================================================
// ContinuousAOConfig::get_channel_periodic_data
// ============================================================================
const asl::BaseBuffer *
ContinuousAOConfig::get_channel_periodic_data (unsigned int _channel)
{
    if(this->format_ == adl::raw_data)
    {
        PeriodicRawBuffersIterator it = periodic_raw_buffers.find(_channel);
        if(it!= periodic_raw_buffers.end())
            return (it->second);
        else
            return 0;
    }
    else
    {
        PeriodicScaledBuffersIterator it = periodic_scaled_buffers.find(_channel);
        if(it!= periodic_scaled_buffers.end())
            return (it->second);
        else
            return 0;
    }
}
// ============================================================================
// ContinuousAOConfig::~ContinuousAOConfig
// ============================================================================
ContinuousAOConfig::~ContinuousAOConfig ()
{
  
  //- release any existing periodic data
  PeriodicScaledBuffersIterator its = periodic_scaled_buffers.begin();
  PeriodicScaledBuffersIterator ends = periodic_scaled_buffers.end();
  
  for(;its!=ends; its++)
  {
    if(its->second)
    {
      std::cout<<"delete scaled "<<std::endl;
      delete its->second;
      its->second = 0;
    }
  }
  
  periodic_scaled_buffers.clear();
  
  PeriodicRawBuffersIterator itr = periodic_raw_buffers.begin();
  PeriodicRawBuffersIterator endr = periodic_raw_buffers.end();
  
  for(;itr!=endr; itr++)
  {
    if(itr->second)
    {
      std::cout<<"ContinuousAOConfig::delete one buffer"<<std::endl;
      delete itr->second;
      itr->second = 0;
    }
  }
  periodic_raw_buffers.clear();
}

} // namespace asl
