// ============================================================================
//
// = CONTEXT
//    TANGO Project - Tango Tools Library
//
// = FILENAME
//    ContinuousAO.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#if defined(_SIMULATION_)
# include <asl/PulsedTask.h>
#endif
#include <asl/AOData.h>
#include <asl/ContinuousAO.h>
#include <asl/ContinuousDAQHandler.h>

#if !defined (__ASL_INLINE__)
# include <asl/ContinuousAO.i>
#endif // __ASL_INLINE__

namespace asl {

// ============================================================================
// STATICS
// ============================================================================
ContinuousAO * ContinuousAO::instance = 0;

#if defined(_SIMULATION_)
// ============================================================================
// PERIOD BETWEEN TWO CALLS TO THE CALLBACK IN MILLISECONDS
// ============================================================================
#define kCB_PERIOD 5

// ============================================================================
// CallbackCallerAO : a asl::PulsedTask to simulate DAQ activity
// ============================================================================
class CallbackCallerAO : public asl::PulsedTask
{
public:

  CallbackCallerAO () 
  {
    /*noop*/
  };

  virtual ~CallbackCallerAO () 
  {
    /*noop*/
  };

  virtual int pulsed (void* arg) 
  {
    DAQCallback cb = ACE_reinterpret_cast(DAQCallback, arg);
    if (cb) cb();
    return 0;
  }

};
#endif // _SIMULATION_

// ============================================================================
// ContinuousAO::ContinuousAO
// ============================================================================
ContinuousAO::ContinuousAO ()
  : daq_hw_ (0), 
    thread_executing_user_hook_ (0)
#if defined(_SIMULATION_)
    ,cb_caller_ (0)
#endif
{
#if !defined (DAQ_CALLBACK_ACCEPT_ARG)
  ContinuousAO::instance = this;
#endif
//- std::cout << "ContinuousAO::ContinuousAO <-" << std::endl;
//- std::cout << "ContinuousAO::ContinuousAO <-" << std::endl;
}

// ============================================================================
// ContinuousAO::~ContinuousAO
// ============================================================================
ContinuousAO::~ContinuousAO ()
{
  //- need to lock 
  //- let current action complete
  ACE_GUARD_REACTION(ACE_SYNCH_MUTEX, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());

  //- std::cout << "ContinuousAO::~ContinuousAO <-" << std::endl;

  //- try to stop the DAQ
  try 
	{
    if (this->daq_hw_) 
		  this->stop_i();
  }
  catch (...) {
    //- ignore any exception
  }

  //- release DAQ hardware
  if (this->daq_hw_) {
    this->daq_hw_->release();
    this->daq_hw_ = 0;
  }

  //- std::cout << "ContinuousAO::~ContinuousAO ->" << std::endl;
}

// ============================================================================
// ContinuousAO::init
// ============================================================================
void ContinuousAO::init (unsigned short _type, unsigned short _id)
     throw (asl::DAQException)
{
  ACE_GUARD_REACTION(ACE_SYNCH_MUTEX, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());
  
  //- return if already initialized
  if (this->initialized()) 
  {
    return; 
  }

  try {
    //- instanciate the board - get exclusive access
    this->daq_hw_ = adl::ADContinuousAOBoard::instanciate(_type, _id, false);
  }
  catch (const asl::DAQException&) {
    this->state_ = FAULT;
    //- rethrow exception
    throw; 
  }
  catch (...) {
    this->state_ = FAULT;
    //- throw unknown error
    throw DAQException();
  }
  //- change state to STANDBY (ready to run with default DAQ config)
  this->state_ = STANDBY;
}

// ============================================================================
// ContinuousAO::configure
// ============================================================================
void ContinuousAO::configure (const ContinuousAOConfig& _config)
throw (asl::DAQException, asl::DeviceBusyException)
{
  TIMEOUT_GUARD(this->lock_, "ContinuousAO::configure"); 
  
  this->action_allowed("ContinuousAO::configure");
  
  if (this->state() == RUNNING) 
  {
    throw asl::DeviceBusyException(
      "device busy",
      "can't modify configuration while DAQ or hardware calibration is running",
      "ContinuousAO::configure");
  }
  
  CHECK_GUARD_LOCKED("ContinuousAO::configure");
  
  //- configure without locking
  this->configure_i(_config);
  
  //- change state to STANDBY (ready to run with user config)
  this->state_ = STANDBY;
}

// ============================================================================
// ContinuousAO::configure_i
// ============================================================================
void ContinuousAO::configure_i (const ContinuousAOConfig& _config)
throw (asl::DAQException)
{
  //- throw if not initialized
  if (this->initialized() == false) 
  {
    throw asl::DAQException("unitialized DAQ",
                            "ContinuousAO::init must be called before any other functions",
                            "ContinuousAO::configure");
  }

  //- copy config locally
  this->config_ = _config;

  //- set event callback 
  if (this->config_.periodic_mode_enabled() == false)
  {
    this->config_.set_db_event_callback(ContinuousAO::event_callback);
  }
  else
  {
    this->config_.set_db_event_callback(0);
  }
  
}

// ============================================================================
// ContinuousAO::calibrate_hardware
// ============================================================================
void ContinuousAO::calibrate_hardware ()
  throw (asl::DAQException, asl::DeviceBusyException)
{
	TIMEOUT_GUARD(this->lock_, "ContinuousAO::calibrate_hardware"); 

  this->action_allowed("ContinuousAO::calibrate_hardware");
	
  CHECK_GUARD_LOCKED("ContinuousAO::calibrate_hardware");

  this->exception_status_ = HANDLING_CALIBRATION;

#if defined (USE_ASYNC_CALIBRATION)

  //- ask a ContinuousDAQHandler to  call <this::handle_hardware_calibration>.
	ContinuousDAQHandler * r = new ContinuousDAQHandler();
  if (r == 0) {
    throw asl::DAQException("out of memory",
												    "out of memory error",
												    "ContinuousAO::calibrate_hardware");
  }

	//- spawn external handler
	r->call_handle_hardware_calibration(this);

#else

  this->handle_hardware_calibration_i();

#endif
}

#if defined (USE_ASYNC_CALIBRATION)
// ============================================================================
// ContinuousAO::handle_hardware_calibration
// ============================================================================
void ContinuousAO::handle_hardware_calibration ()
     throw (asl::DAQException)
{
	//- lock 
	ACE_GUARD_REACTION(ACE_Thread_Mutex, 
										 ace_mon, 
										 this->lock_, 
										 throw asl::DAQException());

  this->handle_hardware_calibration_i();
}
#endif

// ============================================================================
// ContinuousAO::handle_hardware_calibration_i
// ============================================================================
void ContinuousAO::handle_hardware_calibration_i ()
     throw (asl::DAQException)
{
  std::cout << "ContinuousAO::handle_hardware_calibration_i <-" << std::endl;

  if (this->exception_status_ != HANDLING_CALIBRATION)
	  return;
		
  //- is DAQ running ?
  bool running = (this->state_ == RUNNING);

 
	
	try 
	{
		//- stop DAQ if running
    if (running) this->stop_i();
     //- change state
    this->state_ = STANDBY;
  }
  catch (const DAQException& de)
	{
	  this->state_ = FAULT;
    this->exception_status_ = HANDLING_NONE;
#if defined (USE_ASYNC_CALIBRATION)
    this->hw_calibration_exception_ = de;
    this->hw_calibration_successfull_ = false;
    return;
#else
    throw de;
#endif
	}
	catch (...) 
	{
	  this->state_ = FAULT;
    this->exception_status_ = HANDLING_NONE;
    DAQException unknown("DAQ stop failed",
					               "unknown exception caught while trying to stop DAQ for hardware calibration",
					               "ContinuousAO::handle_hardware_calibration_i");
#if defined (USE_ASYNC_CALIBRATION)
    this->hw_calibration_exception_ = unknown;
    this->hw_calibration_successfull_ = false;
    return;
#else
    throw unknown;
#endif
	}
	
  try 
  {
		//- calibrate hardware
    this->daq_hw_->auto_calibrate(false);
#if defined (USE_ASYNC_CALIBRATION)
    this->hw_calibration_successfull_ = true;
#endif
  }
  catch (const asl::DAQException& de) 
	{
    this->state_ = FAULT;
    this->exception_status_ = HANDLING_NONE;
#if defined (USE_ASYNC_CALIBRATION)
    this->hw_calibration_exception_ = de;
    this->hw_calibration_successfull_ = false;
#else
    throw de;
#endif
  }
	catch (...) 
	{
    this->state_ = FAULT;
    this->exception_status_ = HANDLING_NONE;
    DAQException unknown("hardware calibration failed",
					               "unknown exception caught while trying to calibrate the hardware",
					               "ContinuousAO::handle_hardware_calibration_i");
#if defined (USE_ASYNC_CALIBRATION)
    this->hw_calibration_exception_ = unknown;
    this->hw_calibration_successfull_ = false;
#else
    throw unknown;
#endif
	}

  try 
  {
		//- save calibration data into default bank
		this->daq_hw_->save_calibration(kDEFAULT_CALIBRATION_BANK);
  }
	catch (...) 
	{
    //- ignore error
	}

  try {
    //- reconfigure the DAQ (release/register done is daq_hw_->auto_calibrate)
    this->configure_i(this->config_);
  }
  catch (const DAQException& de)
	{
	  this->state_ = FAULT;
    this->exception_status_ = HANDLING_NONE;
#if defined (USE_ASYNC_CALIBRATION)
    this->hw_calibration_exception_ = de;
    this->hw_calibration_successfull_ = false;
    return;
#else
    throw;
#endif
	}
	catch (...) 
	{
	  this->state_ = FAULT;
    this->exception_status_ = HANDLING_NONE;
    DAQException unknown("DAQ configuration failed",
					               "unknown exception caught while trying to reconfigure DAQ after hardware calibration",
					               "ContinuousAO::handle_hardware_calibration_i");
#if defined (USE_ASYNC_CALIBRATION)

    this->hw_calibration_exception_ = unknown;
    this->hw_calibration_successfull_ = false;
    return;
#else
    throw unknown;
#endif
	}

	try 
	{
		//- restart DAQ if running
    if (running) this->start_i();
  }
  catch (const DAQException& de)
	{
	   this->state_ = FAULT;
    this->exception_status_ = HANDLING_NONE;
#if defined (USE_ASYNC_CALIBRATION)
    this->hw_calibration_exception_ = de;
    this->hw_calibration_successfull_ = false;
    return;
#else
    throw;
#endif
	}
	catch (...) 
	{
	  this->state_ = FAULT;
    this->exception_status_ = HANDLING_NONE;
    DAQException unknown("DAQ start failed",
					               "unknown exception caught while trying to restart DAQ after hardware calibration",
					               "ContinuousAO::handle_hardware_calibration_i");
#if defined (USE_ASYNC_CALIBRATION)
    this->hw_calibration_exception_ = unknown;
    this->hw_calibration_successfull_ = false;
    return;
#else
    throw unknown;
#endif
	}

  #if defined (USE_ASYNC_CALIBRATION)
  this->hw_calibration_successfull_ = true;
#endif

	this->exception_status_ = HANDLING_NONE;
		
  std::cout << "ContinuousAO::handle_hardware_calibration_i ->" << std::endl;
}

// ============================================================================
// ContinuousAO::start
// ============================================================================
void ContinuousAO::start ()
throw (asl::DAQException, asl::DeviceBusyException)
{
  TIMEOUT_GUARD(this->lock_, "ContinuousAO::start"); 
	 
  this->action_allowed("ContinuousAO::start");
  if (this->state() == RUNNING)
    return;
	 
  CHECK_GUARD_LOCKED("ContinuousAO::start");
  
	 //- start without blocking
	 this->start_i();
   
   //- change state to RUNNING
   this->state_ = RUNNING;
}

// ============================================================================
// ContinuousAO::start_i
// ============================================================================
void ContinuousAO::start_i ()
throw (asl::DAQException)
{
	 
   //- throw if not initialized
  if (this->initialized() == false) 
  {
    throw asl::DAQException("unitialized DAQ",
                            "ContinuousAO::init must be called before any other functions",
                            "ContinuousAO::start");
  }

  /*//- start_i called from user hook?
	 if (thread_executing_user_hook_ == ACE_Thread::self()) {
     //-TODO: what to do here: nothing maybe ?
   }*/
   
#if !defined(_SIMULATION_)
	 
	 try 
	 {
		 //- configure hardware
		 this->daq_hw_->configure_continuous_ao(this->config_);
		 //- start daq
		 this->daq_hw_->start_continuous_ao();
	 } 
	 catch (const DAQException&) 
	 {
		 throw;
	 }
	 catch (...) 
	 {
		 throw DAQException();
	 }
	 
#else // _SIMULATION_

  //- configure hardware
  this->daq_hw_->configure_continuous_ao(this->config_);

  //- just to init buffers index 
  this->daq_hw_->start_continuous_ao();

  if (this->config_.get_db_event_callback() != 0)
  {
    //- start CallbackCallerAO
    this->cb_caller_ = new CallbackCallerAO();
    if (this->cb_caller_ == 0) 
    {
      //-TODO: add error details: out of memory
      throw asl::DAQException();
    } 
    this->cb_caller_->start (ContinuousAO::event_callback, kCB_PERIOD);
  }

#endif // _SIMULATION_
}

// ============================================================================
// ContinuousAO::stop
// ============================================================================
void ContinuousAO::stop ()
  throw (asl::DAQException, asl::DeviceBusyException)
{
  TIMEOUT_GUARD(this->lock_, "ContinuousAO::stop");

  this->action_allowed("ContinuousAO::stop");

  CHECK_GUARD_LOCKED("ContinuousAO::stop");

  //- stop without locking
  this->stop_i();

  //- change state to STANDBY (ready to run with user config)
  this->state_ = STANDBY;
}

// ============================================================================
// ContinuousAO::stop_i
// ============================================================================
void ContinuousAO::stop_i ()
throw (asl::DAQException)
{
	 //- throw if not initialized
  if (this->initialized() == false) {
    throw asl::DAQException("unitialized DAQ",
      "ContinuousAO::init must be called before any other functions",
      "ContinuousAO::stop");
  }
  
  
  //- start_i called from user hook?
	 if (thread_executing_user_hook_ == ACE_Thread::self()) {
    
   }
   
#if !defined(_SIMULATION_)
	 
	 //- stop daq
	 try {
		 this->daq_hw_->stop_continuous_ao();
	 } 
	 catch (DAQException) {
		 throw;
	 }
	 catch (...) {
		 throw DAQException();
	 }
	 
#else // _SIMULATION_
	 
	 //- stop CallbackCallerAO
	 if (this->cb_caller_) 
	 {
		 this->cb_caller_->stop();
		 delete this->cb_caller_;
		 this->cb_caller_ = 0;
	 }
	 
#endif // _SIMULATION_
	 
}

// ============================================================================
// ContinuousAO::abort
// ============================================================================
void ContinuousAO::abort ()
  throw (asl::DAQException, asl::DeviceBusyException)
{
  //- forward call to ContinuousAO::stop
  this->stop();
}

// ============================================================================
// ContinuousAO::handle_daq_exception
// ============================================================================
void ContinuousAO::handle_daq_exception ()
  throw (asl::DAQException)
{
	ACE_GUARD_REACTION(ACE_SYNCH_MUTEX, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());
	
	//- check exception status
	if (this->exception_status_ != HANDLING_ERROR) {
		//- nothing to do here
		return;
	}
	
	//- stop without locking
  try {
	  this->stop_i();
    //- change state
    this->state_ = STANDBY;
	}
	catch (...) 
	{
		//- change state
    this->state_ = FAULT;
    //- ignore exception
	}

	//- exception/evt handled
	this->exception_status_ = HANDLING_NONE;
}

// ============================================================================
// ContinuousAO::event_callback
// ============================================================================
#if defined (WIN32)
void ContinuousAO::event_callback ()
#else
void ContinuousAO::event_callback (int sig_num)
#endif
{

#if !defined (WIN32)
	ACE_UNUSED_ARG(sig_num);
#endif

  static unsigned long count = 0;
	
  ::printf("ContinuousAO::event_callback::call::%ld\r", ++count);
	
  ContinuousAO * instance = ContinuousAO::instance;
	
	//- IMPORTANT NOTE:
	//- DO NOT ACQUIRE THE CONTINUOUS-DAQ LOCKING OBJECT IN THIS METHOD.
	//- POTENTIAL DEALOCK! THE STOP MEMBER ACQUIRE THE LOCK THEN CALL 
	//- D2K_AO_ASYNCCLEAR. THE DRIVER THEN WAOT FOR THE CALLBACK TO COMPLETE
	//- BEFORE EXECUTING STOP THE ASYNC OPERATION. IF THE CALLBACK IS WAITING
	//- FOR THE LOCKING OBJECT TO BE FREE, THE PROCESS HANGS (DEADLOCK).
	
	//- try to acquire the DAQ <lock_>
	if (instance->lock_.tryacquire() == -1)
	{
			//- someone else hold the <lock_>. 
			//- means that a stop or abort request is pending or...
			//- an error occurred while trying to acquire the lock_
			//- leave the place ...
			return;
	}
	
	  //- check internal state: DAQ must be running whitout handling exception
  if (instance->state_ != RUNNING || instance->exception_status_ != HANDLING_NONE) 
  {
    //- release the DAQ <lock_>
    instance->lock_.release();
    return;
  }
	
	//- do we get ownership of the data
	bool have_ownership = true;
	
	asl::BaseBuffer* output_data = 0;
		
	try 
	{
    //- get buffer from user defined function 
		if (instance->config_.get_data_format() == adl::raw_data)
    {
			output_data = instance->handle_output((unsigned int)instance->config_.get_active_channels().size(), 
			                                      instance->config_.get_buffer_depth(),
																						have_ownership);
    }
		else
    { 
			output_data = instance->handle_scaled_output((unsigned int)instance->config_.get_active_channels().size(), 
			                                             instance->config_.get_buffer_depth(),
																									 have_ownership);
    }
		//- check result
		if (output_data == 0) { 
			//- user could not allocate his/her buffer
			instance->lock_.release();
			return;
		}
	}
	catch (asl::DAQException& de) 
	{
		//- exception thrown from user hooks is ignored
		if (output_data && have_ownership) 
      delete output_data;
    //- push error into the errors stack
    de.push_error(static_cast<const char*>("asl::DAQException caught"),
					        static_cast<const char*>("asl::DAQException caught from user hook - aborting AO operation"),
					        static_cast<const char*>("ContinuousAO::event_callback"));
    //- handle error
    instance->handle_daq_exception_i(de);
		//- release the DAQ <lock_>
		instance->lock_.release();
    //- skip remaining code
		return;
	}
	catch (...) 
	{
		//- exception thrown from user hooks is ignored
		if (output_data && have_ownership) 
      delete output_data;
    //- convert from unknown to asl::DAQException
    DAQException de(static_cast<const char*>("unknown exception caught"),
					          static_cast<const char*>("unknown exception caught from user hook - aborting AO operation"),
					          static_cast<const char*>("ContinuousAO::event_callback"));
    //- handle error
    instance->handle_daq_exception_i(de);
		//- release the DAQ <lock_>
		instance->lock_.release();
    //- skip remaining code
		return;
	}
	
	try 
	{
		//- send buffer to the hardware
		instance->daq_hw_->ao_db_event_callback(output_data, have_ownership);
	} 
	catch (const DAQException& de) 
	{
    //- handle error
		instance->handle_daq_exception_i(de);
	}
	catch (...) 
	{
    //- convert from unknown to asl::DAQException
    DAQException de(static_cast<const char*>("unknown exception caught"),
					          static_cast<const char*>("unknown exception caught from ADContinuousAOBoard::ao_db_event_callback - aborting AO operation"),
					          static_cast<const char*>("ContinuousAO::event_callback"));
		instance->handle_daq_exception_i(de);
	}
	
	//- release the DAQ <lock_>
	instance->lock_.release();
}

// ============================================================================
// ContinuousAO::handle_daq_exception_i
// ============================================================================
void ContinuousAO::handle_daq_exception_i (const DAQException& _ex)
{
	//- tell the world we are handling an error
	//- note: may be arrived here without posting the error message
	this->state_ = ABORTING;
	this->exception_status_ = HANDLING_ERROR;
	
	//- abort daq
	try {
		this->stop_i();
	}
	catch (const DAQException& de) 
	{
		//- push abort error onto <_ex> error stack
		for (unsigned int i = 0; i < _ex.errors.size(); i++)
			ACE_const_cast(DAQException&,_ex).push_error(de.errors[i]);
	}
	
	try {
		this->handle_error(_ex);
	}
	catch (...) {
		//- ignore user exception
	}
}

// ============================================================================
// ContinuousAO::dump
// ============================================================================
void ContinuousAO::dump () const
{
	ACE_GUARD(ACE_SYNCH_MUTEX, ace_mon, 
		ACE_const_cast(ACE_Thread_Mutex&, this->lock_));
	
	ACE_DEBUG ((LM_DEBUG, "----------------------------\n"));
	ACE_DEBUG ((LM_DEBUG, "-- ContinuousAO             \n"));
	ACE_DEBUG ((LM_DEBUG, "----------------------------\n"));
	//- return if not initialized
	if (! this->initialized()) {
		ACE_DEBUG ((LM_DEBUG, ". not initialized\n"));
		return; 
	}
	
	switch (this->state_) 
	{
  case STANDBY:
      ACE_DEBUG ((LM_DEBUG, ". state: STANDBY\n"));
      break;
    case RUNNING:
      ACE_DEBUG ((LM_DEBUG, ". state: RUNNING\n"));
      break;
    case ABORTING:
      ACE_DEBUG ((LM_DEBUG, ". state: ABORTING\n"));
      break;
    case FAULT:
      ACE_DEBUG ((LM_DEBUG, ". state: FAULT\n"));
      break;
    default:
      ACE_DEBUG ((LM_DEBUG, ". state: UNKNOWN\n"));
      break;
	}
	
	ACE_DEBUG ((LM_DEBUG, "-- DAQ Hardware: \n"));
	
	this->daq_hw_->dump();
	
	ACE_DEBUG ((LM_DEBUG, "----------------------------\n"));
}
 
// ============================================================================
// ContinuousAO::handle_output
// ============================================================================
asl::AORawData* ContinuousAO::handle_output (unsigned int, unsigned long, bool&)
{
	//- should be overloaded !!!!
	throw asl::DAQException("ContinuousAO::handle_output default implementation called",
		                      "ContinuousAO::handle_output must be overloaded",
		                      "ContinuousAO::handle_output");
	return 0;
}
// ============================================================================
// ContinuousAO::handle_scaled_output
// ============================================================================
asl::AOScaledData* ContinuousAO::handle_scaled_output (unsigned int, unsigned long, bool&)
{
	//- should be overloaded !!!!
	throw asl::DAQException("ContinuousAO::handle_scaled_output default implementation called",
		                      "ContinuousAO::handle_scaled_output must be overloaded",
		                      "ContinuousAO::handle_scaled_output");
	return 0;
}

} // namespace asl

