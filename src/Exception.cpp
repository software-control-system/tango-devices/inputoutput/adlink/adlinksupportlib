// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    Exception.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/Exception.h>

#if !defined (__ASL_INLINE__)
# include <asl/Exception.i>
#endif // __ASL_INLINE__

namespace asl {

// ============================================================================
// STATICs
// ============================================================================
#if defined(ASL_DEBUG)
ASL_EXPORT AtomicOp Exception::instance_counter_ = 0;
#endif

// ============================================================================
// Error::Error
// ============================================================================
Error::Error ()
  :  reason ("unknown"),
     desc ("unknown error"),
     origin ("unknown"),
     code (-1),
     severity (asl::ERR)
{

}

// ============================================================================
// Error::Error
// ============================================================================
Error::Error (const char *_reason,
	      const char *_desc,
              const char *_origin,
              int _code, 
              int _severity)
  :  reason (_reason),
     desc (_desc),
     origin (_origin),
     code (_code),
     severity (_severity)
{

}

// ============================================================================
// Error::Error
// ============================================================================
Error::Error (const std::string& _reason,
	      const std::string& _desc,
	      const std::string& _origin,
              int _code, 
              int _severity)
  :  reason (_reason),
     desc (_desc),
     origin (_origin),
     code (_code),
     severity (_severity)
{

}

// ============================================================================
// Error::Error
// ============================================================================
Error::Error (const Error& _src)
  :  reason (_src.reason),
     desc (_src.desc),
     origin (_src.origin),
     code (_src.code),
     severity (_src.severity)
{

}

// ============================================================================
// Error::~Error
// ============================================================================
Error::~Error ()
{

}

// ============================================================================
// Error::operator=
// ============================================================================
Error& Error::operator= (const Error& _src) 
{
  //- no self assign
  if (this == &_src) {
    return *this;
  }

  this->reason = _src.reason;
  this->desc = _src.desc;
  this->origin = _src.origin;
  this->code = _src.code;
  this->severity = _src.severity;

  return *this;
}

// ============================================================================
// Exception::Exception
// ============================================================================
Exception::Exception () 
  : errors(0)
{
#if defined(ASL_DEBUG)
  //- increment instance counter
  ++Exception::instance_counter_;
#endif 
  this->push_error(Error());
}

// ============================================================================
// Exception::Exception
// ============================================================================
Exception::Exception (const char *_reason,
                      const char *_desc,
                      const char *_origin,
                      int _code, 
                      int _severity) 
  : errors(0)
{
#if defined(ASL_DEBUG)
  //- increment instance counter
  ++Exception::instance_counter_;
#endif 

  this->push_error(Error(_reason, _desc, _origin, _code, _severity));
}

// ============================================================================
// Exception::Exception
// ============================================================================
Exception::Exception (const std::string& _reason,
                      const std::string& _desc,
                      const std::string& _origin,
                      int _code, 
                      int _severity) 
  : errors(0)
{
#if defined(ASL_DEBUG)
  //- increment instance counter
  ++Exception::instance_counter_;
#endif 

  this->push_error(_reason, _desc, _origin, _code, _severity);
}

// ============================================================================
// Exception::Exception
// ============================================================================
Exception::Exception (const Exception& _src) 
  : errors(0)
{
#if defined(ASL_DEBUG)
  //- increment instance counter
  ++Exception::instance_counter_;
#endif  

  for (unsigned int i = 0; i < _src.errors.size();  i++) {
    this->push_error(_src.errors[i]);
  }
}

// ============================================================================
// Exception::Exception
// ============================================================================
Exception& Exception::operator= (const Exception& _src) 
{
  //- no self assign
  if (this == &_src) {
    return *this;
  }

  this->errors.clear();

  for (unsigned int i = 0; i < _src.errors.size();  i++) {
    this->push_error(_src.errors[i]);
  }

  return *this;
}

// ============================================================================
// Exception::~Exception
// ============================================================================
Exception::~Exception ()
{
#if defined(ASL_DEBUG)
  //- decrement instance counter
  --Exception::instance_counter_;
#endif  
  this->errors.clear();
}


// ============================================================================
// Exception::push_error
// ============================================================================
void Exception::push_error (const char *_reason,
    					              const char *_desc,
					                  const char *_origin, 
                            int _err_code, 
                            int _severity)
{
  this->errors.push_back(Error(_reason, _desc, _origin, _err_code, _severity));
}

// ============================================================================
// Exception::push_error
// ============================================================================
void Exception::push_error (const std::string& _reason,
    					              const std::string& _desc,
					                  const std::string& _origin, 
                            int _err_code, 
                            int _severity)
{
  this->errors.push_back(Error(_reason, _desc, _origin, _err_code, _severity));
}

// ============================================================================
// Exception::push_error
// ============================================================================
void Exception::push_error (const Error& _error)
{
  this->errors.push_back(_error);
}

#if defined(ASL_DEBUG)
// ============================================================================
// Exception::instance_counter
// ============================================================================
u_long Exception::instance_counter () 
{
  return instance_counter_.value();
}
#endif  

} // namespace asl


