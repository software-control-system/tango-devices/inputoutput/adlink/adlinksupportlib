// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    PulsedTaskManager.cpp
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/PulsedTaskManager.h>

#if !defined (__ASL_INLINE__)
# include <asl/PulsedTaskManager.i>
#endif // __ASL_INLINE__

namespace asl {

// ============================================================================
// STATICS
// ============================================================================
PulsedTaskManager * PulsedTaskManager::manager = 0;

ACE_Recursive_Thread_Mutex PulsedTaskManager::instance_lock;

// ============================================================================
// PulsedTaskManager::PulsedTaskManager [STATIC]
// ============================================================================
PulsedTaskManager * PulsedTaskManager::instance ()
{
  if (PulsedTaskManager::manager == 0)
  {
    //- perform double-checked locking optimization.
    ACE_MT (ACE_GUARD_RETURN (ACE_Recursive_Thread_Mutex, 
                              ace_mon,
                              PulsedTaskManager::instance_lock, 
                              0));
    //- instanciate
    if (PulsedTaskManager::manager == 0)
    {
       ACE_NEW_RETURN (PulsedTaskManager::manager,
                       PulsedTaskManager,
                       0);

       ACE_REGISTER_FRAMEWORK_COMPONENT(PulsedTaskManager, 
                                        PulsedTaskManager::manager);

       PulsedTaskManager::manager->run();
    }
  }

  return PulsedTaskManager::manager;
}

// ============================================================================
// PulsedTaskManager::close_singleton [STATIC]
// ============================================================================
void PulsedTaskManager::close_singleton ()
{
  ACE_MT (ACE_GUARD (ACE_Recursive_Thread_Mutex, 
                     ace_mon,
                     PulsedTaskManager::instance_lock));

  if (PulsedTaskManager::manager) {
    delete PulsedTaskManager::manager;
    PulsedTaskManager::manager = 0;
  }
}

// ============================================================================
// PulsedTaskManager::PulsedTaskManager
// ============================================================================
PulsedTaskManager::PulsedTaskManager ()
{
  this->reactor_ = new ACE_Reactor();
  ACE_ASSERT(this->reactor_ != 0);
}

// ============================================================================
// PulsedTaskManager::~PulsedTaskManager
// ============================================================================
PulsedTaskManager::~PulsedTaskManager ()
{
  this->reactor_->end_reactor_event_loop();
  this->join();
  delete this->reactor_;
}

// ============================================================================
// PulsedTaskManager::svc
// ============================================================================
ACE_THR_FUNC_RETURN PulsedTaskManager::svc (void *arg)
{
  ACE_UNUSED_ARG(arg);
  this->reactor_->owner(this->id());
  this->reactor_->run_reactor_event_loop();  
  return 0;
}

} // namespace asl

