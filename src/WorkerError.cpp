// ============================================================================
//
// = CONTEXT
//    Data Stream Model 
//
// = FILENAME
//    DS_Message.cpp
//
// = AUTHOR
//    Nicolas Leclercq 
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/WorkerError.h>

#if !defined (__ASL_INLINE__)
# include <asl/WorkerError.i>
#endif /* __ASL_INLINE__ */

namespace asl {

// ============================================================================
// STATICs
// ============================================================================
#if defined(ASL_DEBUG)
ASL_EXPORT AtomicOp WorkerError::instance_counter_ = 0;
#endif

// ============================================================================
// WorkerError::WorkerError
// ============================================================================
WorkerError::WorkerError (const char* _txt, long _code, bool _fatal)
  : txt_ (_txt), 
    code_(_code),
    fatal_ (_fatal),
    cont_(0)
{
  //-noop 
}
  
// ============================================================================
// WorkerError::WorkerError
// ============================================================================
WorkerError::WorkerError (const std::string& _txt, long _code, bool _fatal)
  : txt_ (_txt), 
    code_(_code),
    fatal_ (_fatal),
    cont_(0)
{
  //-noop 
}

// ============================================================================
// WorkerError::WorkerError
// ============================================================================
WorkerError::~WorkerError ()
{
  if (this->cont_) {
    delete cont_;
  }
}

#if defined(ASL_DEBUG)
// ============================================================================
// WorkerError::instance_counter
// ============================================================================
u_long WorkerError::instance_counter () 
{
  return instance_counter_.value();
}
#endif  

} // namespace asl

