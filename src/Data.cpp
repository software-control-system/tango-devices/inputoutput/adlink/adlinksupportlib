// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    Data.cpp
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/Data.h>

#if !defined (__ASL_INLINE__)
# include <asl/Data.i>
#endif // __ASL_INLINE__

namespace asl {

// ============================================================================
// STATICs
// ============================================================================
#if defined(ASL_DEBUG)
ASL_EXPORT AtomicOp Data::instance_counter_ = 0;
#endif

// ============================================================================
// Data::Data
// ============================================================================
Data::Data ()
  : cont_ (0)
{
#if defined(ASL_DEBUG)
  //- increment instance counter
  ++Data::instance_counter_;
#endif  
}

// ============================================================================
// Data::~Data
// ============================================================================
Data::~Data ()
{
#if defined(ASL_DEBUG)
  //- decrement instance counter
  --Data::instance_counter_;
#endif   

  if (cont_) {
    delete cont_;
  }
}

#if defined(ASL_DEBUG)
// ============================================================================
// Data::instance_counter
// ============================================================================
u_long Data::instance_counter () 
{
  return instance_counter_.value();
}
#endif  

} // namespace asl


