// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ContinuousAIConfig.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/ContinuousAIConfig.h>

#if !defined (__ASL_INLINE__)
# include <asl/ContinuousAIConfig.i>
#endif // __ASL_INLINE__

namespace asl {

// ============================================================================
// ContinuousAIConfig::ContinuousAIConfig
// ============================================================================
ContinuousAIConfig::ContinuousAIConfig ()
  : //- no active channels
    channels_(0),
    //- internal timer triggers ADC conversions
    conversion_source_ (adl::ai_internal_timer),
    //- software trigger
    trigger_source_ (adl::internal_software),
    //- post trigger mode
    trigger_mode_ (adl::ai_post),
    //- trigger on rising edge 
    trigger_polarity_ (adl::ai_rising_edge),
    //- delay expressed in internal clock ticks
    delay_unit_ (adl::clock_ticks),
    //- no middle or delay scans (post trigger mode)
    middle_or_delay_scans_ (0), 
    //- disable retrigger
    retrigger_status_ (adl::ai_rt_disabled),
    //- disable mCounter
    mcounter_status_ (adl::ai_mc_disabled),
    //- mCounter disabled 
    mcounter_value_ (0),
    //- default analog trigger channel channel: 0
    analog_trigger_channel_ (adl::analog_trigger_chan0),
    //- default analog trigger condition: above_high_level
    analog_trigger_condition_ (adl::above_high_level),
    //- default analog trigger low condition level: 1
    analog_trigger_lo_level_ (1),
    //- default analog trigger high condition level: 1
    analog_trigger_hi_level_ (1),
    //- default sampling rate: 1000 Hz
    sampling_rate_ (1000.0),
    //- default data_lost strategy: ignore
    data_lost_strategy_ (adl::ignore),
    //- default num of scans per buffer: 500 samples (one buffer ready every half second)   
    buffer_depth_ (500),
    //- db_event_callback
    db_event_callback_ (0),
    //- trigger_event_callback
    trigger_event_callback_ (0),
    //- ai_end_event_callback 
    ai_end_event_callback_ (0),
    //- default timeout: 3 seconds
    timeout_ (3000),
    //- default history length: 1sec
    history_len_msec_ (1000),
    //- disable history
    history_enabled_(false),
    //- disable last buffer recovery
    last_buffer_recovery_enabled_(false),
    //- num. total of trig sequences to acquire
    num_retrig_sequences_(0),
    //- user defined data mask 
    data_mask_enabled_(false),
    data_mask_ (0),
    intermediate_buffer_send_enabled_(false),
    intermediate_num_triggers_(0),
    timebase_(adl::int_time_base),
    ext_timebase_val_(DEFAULT_CLOCK_FREQ)
{

}

// ============================================================================
// ContinuousAIConfig::ContinuousAIConfig
// ============================================================================
ContinuousAIConfig::ContinuousAIConfig (const ContinuousAIConfig& _src)
{
  *this = _src;
}

// ============================================================================
// ContinuousAIConfig::operator=
// ============================================================================
ContinuousAIConfig& ContinuousAIConfig::operator= (const ContinuousAIConfig& _src)
{
  //- no self assign
  if (this == &_src) {
    return *this;
  }
  //- analog input active channels
  this->channels_ = _src.channels_;
  //- conversion source
  this->conversion_source_ = _src.conversion_source_;
  //- trigger source
  this->trigger_source_ = _src.trigger_source_;
  //- trigger mode
  this->trigger_mode_ = _src.trigger_mode_;
  //- trigger mode
  this->trigger_polarity_ = _src.trigger_polarity_;
  //- delay unit (internal-clock-ticks or samples)
  this->delay_unit_ = _src.delay_unit_;
  //- # scans for middle or delay trigger mode
  this->middle_or_delay_scans_ = _src.middle_or_delay_scans_;
  //- retrigger on/off option
  this->retrigger_status_ = _src.retrigger_status_;
  //- mCounter on/off option
  this->mcounter_status_ = _src.mcounter_status_;
  //- mCounter value 
  this->mcounter_value_ = _src.mcounter_value_;
  //- analog trigger source
  this->analog_trigger_channel_ = _src.analog_trigger_channel_;
  //- analog trigger condition
  this->analog_trigger_condition_ = _src.analog_trigger_condition_;
  //- analog trigger mode low level condition
  this->analog_trigger_lo_level_ = _src.analog_trigger_lo_level_;
  //- analog trigger mode high level condition
  this->analog_trigger_hi_level_ = _src.analog_trigger_hi_level_;
  //- internal sampling rate
  this->sampling_rate_ = _src.sampling_rate_;
  //- data_lost strategy
  this->data_lost_strategy_ = _src.data_lost_strategy_;
  //- num of scans per buffer
  this->buffer_depth_ = _src.buffer_depth_;
  //- db event callback
  this->db_event_callback_ = _src.db_event_callback_;
  //- trigger event callback
  this->trigger_event_callback_ = _src.trigger_event_callback_;
  //- ai_end_event_callback 
  this->ai_end_event_callback_ = _src.ai_end_event_callback_;
  //- timeout
  this->timeout_ = (_src.timeout_ >= 1) ? _src.timeout_ : 1;
  //- history_len
  this->history_len_msec_  = _src.history_len_msec_; 
  //- history on/off
  this->history_enabled_ = _src.history_enabled_;
  //- last buffer recovery on/off
  this->last_buffer_recovery_enabled_ = _src.last_buffer_recovery_enabled_;
  //- num. total of trig sequences to acquire
  this->num_retrig_sequences_ = _src.num_retrig_sequences_;
  //- user defined data mask 
  this->data_mask_enabled_ = _src.data_mask_enabled_;
  delete this->data_mask_;
  if ( _src.data_mask_ && _src.data_mask_->depth() )
    this->data_mask_ = new DataMask(*(_src.data_mask_));
  else
    this->data_mask_ = 0;

  this->intermediate_buffer_send_enabled_ = _src.intermediate_buffer_send_enabled_;
  this->intermediate_num_triggers_ = _src.intermediate_num_triggers_;
  this->timebase_ = _src.timebase_;
  this->ext_timebase_val_ = _src.ext_timebase_val_;

  //- return self
  return *this;
}

// ============================================================================
// ContinuousAIConfig::~ContinuousAIConfig
// ============================================================================
ContinuousAIConfig::~ContinuousAIConfig ()
{
  delete this->data_mask_;
}

// ============================================================================
// ContinuousAIConfig::active_channels_ordered_from_zero
// ============================================================================
bool ContinuousAIConfig::active_channels_ordered_from_zero () const
{
  //- # of active channels
  unsigned short max = this->channels_.size();

  //- check ...
  for (unsigned short i = 0; i < max; i++) 
  {
    if (i != this->channels_[i].id)
    {
      return false;
    }
  }

  return true;
}

// ============================================================================
// ContinuousAIConfig::dump
// ============================================================================
void ContinuousAIConfig::dump ()
{
  for (unsigned int i = 0; i < channels_.size(); i++) {
    ACE_DEBUG((LM_DEBUG, "- AI channel %d..............activated\n", i));
  }

  ACE_DEBUG((LM_DEBUG, "- conversion source........."));
  switch (conversion_source_) 
  {
    case adl::ai_internal_timer:
      ACE_DEBUG((LM_DEBUG, "%s\n",  "internal_timer")); 
      break;
    case adl::ai_external_afio:
      ACE_DEBUG((LM_DEBUG, "%s\n",  "external_afio")); 
      break;
    default:
      ACE_DEBUG((LM_DEBUG, "%s\n",  "UNKNOWN/INVALID VALUE")); 
      break;
  }

  ACE_DEBUG((LM_DEBUG, "- timebase source..........."));
  switch (timebase_) 
  {
    case adl::int_time_base:
      ACE_DEBUG((LM_DEBUG, "%s\n",  "internal")); 
      break;
    case adl::ext_time_base:
      ACE_DEBUG((LM_DEBUG, "%s\n",  "external")); 
      break;
    case adl::ssi_time_base:
      ACE_DEBUG((LM_DEBUG, "%s\n",  "ssi")); 
      break;
    default:
      ACE_DEBUG((LM_DEBUG, "%s\n",  "UNKNOWN/INVALID VALUE")); 
      break;
  }
  ACE_DEBUG((LM_DEBUG, "- timebase frequency........%f\n", ext_timebase_val_));

  ACE_DEBUG((LM_DEBUG, "- trigger source............"));
  switch (trigger_source_) 
  {
    case adl::internal_software:
      ACE_DEBUG((LM_DEBUG, "%s\n",  "internal_software")); 
      break;
    case adl::external_analog:
      ACE_DEBUG((LM_DEBUG, "%s\n",  "external_analog")); 
      break;
    case adl::external_digital:
      ACE_DEBUG((LM_DEBUG, "%s\n",  "external_digital")); 
      break;
    default:
      ACE_DEBUG((LM_DEBUG, "%s\n",  "UNKNOWN/INVALID VALUE")); 
      break;
  }

  ACE_DEBUG((LM_DEBUG, "- trigger mode.............."));
  switch (trigger_mode_) 
  {
    case adl::ai_post:
      ACE_DEBUG((LM_DEBUG, "%s\n",  "post")); 
      break;
    case adl::ai_delay:
      ACE_DEBUG((LM_DEBUG, "%s\n",  "delay")); 
      break;
    case adl::ai_pre:
      ACE_DEBUG((LM_DEBUG, "%s\n",  "pre")); 
      break;
    case adl::ai_middle:
      ACE_DEBUG((LM_DEBUG, "%s\n",  "middle")); 
      break;
    default:
      ACE_DEBUG((LM_DEBUG, "%s\n",  "UNKNOWN/INVALID VALUE")); 
      break;
  }

  ACE_DEBUG((LM_DEBUG, "- trigger polarity.........."));
  switch (trigger_polarity_) 
  {
    case adl::ai_rising_edge:
      ACE_DEBUG((LM_DEBUG, "%s\n", "rising edge")); 
      break;
    case adl::ai_falling_edge:
      ACE_DEBUG((LM_DEBUG, "%s\n",  "falling edge")); 
      break;
    default:
      ACE_DEBUG((LM_DEBUG, "%s\n",  "UNKNOWN/INVALID VALUE")); 
      break;
  }

  ACE_DEBUG((LM_DEBUG, "- trigger delay unit........"));
  switch (delay_unit_) 
  {
    case adl::clock_ticks:
      ACE_DEBUG((LM_DEBUG, "%s\n", "clock ticks")); 
      break;
    case adl::samples:
      ACE_DEBUG((LM_DEBUG, "%s\n",  "samples")); 
      break;
    default:
      ACE_DEBUG((LM_DEBUG, "%s\n",  "UNKNOWN/INVALID VALUE")); 
      break;
  }

  ACE_DEBUG((LM_DEBUG, "- middle or delay scans.....%d\n", middle_or_delay_scans_));

  ACE_DEBUG((LM_DEBUG, "- retrigger status..........%s\n", (retrigger_status_ == adl::ai_rt_disabled) ? "disabled" : "enabled"));

  ACE_DEBUG((LM_DEBUG, "- mcounter status...........%s\n", (mcounter_status_ == adl::ai_mc_disabled) ? "disabled" : "enabled"));

  ACE_DEBUG((LM_DEBUG, "- mcounter value............%d\n", mcounter_value_));

  ACE_DEBUG((LM_DEBUG, "- analog trigger channel...."));
  switch (analog_trigger_channel_) 
  {
    case adl::analog_trigger_chan0:
      ACE_DEBUG((LM_DEBUG, "%s\n", "chan-0")); 
      break;
    case adl::analog_trigger_chan1:
      ACE_DEBUG((LM_DEBUG, "%s\n", "chan-1")); 
      break;
    case adl::analog_trigger_chan2:
      ACE_DEBUG((LM_DEBUG, "%s\n", "chan-2")); 
      break;
    case adl::analog_trigger_chan3:
      ACE_DEBUG((LM_DEBUG, "%s\n", "chan-3")); 
      break;
    default:
      ACE_DEBUG((LM_DEBUG, "%s\n",  "UNKNOWN/INVALID VALUE")); 
      break;
  }

  ACE_DEBUG((LM_DEBUG, "- analog trigger cond......."));
  switch (analog_trigger_condition_) 
  {
    case adl::below_low_level:
      ACE_DEBUG((LM_DEBUG, "%s\n", "below_low_level")); 
      break;
    case adl::above_high_level:
      ACE_DEBUG((LM_DEBUG, "%s\n", "above high level")); 
      break;
    case adl::inside_region:
      ACE_DEBUG((LM_DEBUG, "%s\n", "inside region")); 
      break;
    case adl::high_hysteresis:
      ACE_DEBUG((LM_DEBUG, "%s\n", "high hysteresis")); 
      break;
    case adl::low_hysteresis:
      ACE_DEBUG((LM_DEBUG, "%s\n", "low hysteresis")); 
      break;
    default:
      ACE_DEBUG((LM_DEBUG, "%s\n",  "UNKNOWN/INVALID VALUE")); 
      break;
  }

  ACE_DEBUG((LM_DEBUG, "- analog trigger lo-level...%d\n", analog_trigger_lo_level_));

  ACE_DEBUG((LM_DEBUG, "- analog trigger hi-level...%d\n", analog_trigger_hi_level_));

  ACE_DEBUG((LM_DEBUG, "- sampling rate.............%f\n", sampling_rate_));

  ACE_DEBUG((LM_DEBUG, "- data lost strategy........"));
  switch (data_lost_strategy_) 
  {
    case adl::ignore:
      ACE_DEBUG((LM_DEBUG, "%s\n", "ignore")); 
      break;
    case adl::notify:
      ACE_DEBUG((LM_DEBUG, "%s\n", "notify")); 
      break;
    case adl::restart:
      ACE_DEBUG((LM_DEBUG, "%s\n", "restart")); 
      break;
    case adl::abort:
      ACE_DEBUG((LM_DEBUG, "%s\n", "abort")); 
      break;
    default:
      ACE_DEBUG((LM_DEBUG, "%s\n",  "UNKNOWN/INVALID VALUE")); 
      break;
  }

  ACE_DEBUG((LM_DEBUG, "- buffer depth..................%d\n", buffer_depth_));

  ACE_DEBUG((LM_DEBUG, "- history enabled...............%s\n", (history_enabled_ == false) ? "disabled" : "enabled"));

  ACE_DEBUG((LM_DEBUG, "- history length................%d ms\n", history_len_msec_));

  ACE_DEBUG((LM_DEBUG, "- last buffer recovery enabled..%s\n", (last_buffer_recovery_enabled_ == false) ? "disabled" : "enabled"));

  ACE_DEBUG((LM_DEBUG, "- db event callback.............%x\n", db_event_callback_));

  ACE_DEBUG((LM_DEBUG, "- trigger event callback........%x\n", trigger_event_callback_));

  ACE_DEBUG((LM_DEBUG, "- ai-end event callback.........%x\n", ai_end_event_callback_));

  ACE_DEBUG((LM_DEBUG, "- timeout.......................%d\n", timeout_));
}

// ============================================================================
// ContinuousAIConfig::set_data_mask
// ============================================================================
void ContinuousAIConfig::set_data_mask (const std::vector<bool> & dm)
{
  if ( ! this->data_mask_ || dm.size() != this->data_mask_->depth() )
  {
    delete this->data_mask_;
    if ( dm.size() )
      this->data_mask_ = new DataMask(dm.size());
  }

  if ( ! dm.size() )
    return;

  for ( size_t i = 0; i < dm.size(); i++ )
    *(this->data_mask_->base() + i) = dm[i] ? 1. : 0.;
}

// ============================================================================
// ContinuousAIConfig::get_data_mask
// ============================================================================
void ContinuousAIConfig::get_data_mask (std::vector<bool> & dm)
{
  if ( ! this->data_mask_ || this->data_mask_->depth() == 0 )
  {
    dm.clear();
    return;
  }
  
  dm.resize(this->data_mask_->depth());
  for ( size_t i = 0; i < this->data_mask_->depth(); i++)
    dm[i] = *(this->data_mask_->base() + i) != 0. ? true : false;
}

// ============================================================================
// ContinuousAIConfig::check
// ============================================================================
void ContinuousAIConfig::check () const
  throw (asl::DAQException)
{
  //- data mask 
  if ( this->data_mask_enabled_ )
  {
    if ( ! this->data_mask_ )
      throw asl::DAQException("invalid DAQ configuration",
                              "the data mask feature is enabled but no data mask specified!",
                              "ContinuousAIConfig::check");
    if ( this->data_mask_->depth() != this->buffer_depth_ )
      throw asl::DAQException("invalid DAQ configuration",
                              "the data mask buffer size must equal the DAQ buffer size!",
                              "ContinuousAIConfig::check");
  }

  //- user data conversion parameters
  for (unsigned int i = 0; i < this->channels_.size(); i++) 
  {
    if (this->channels_[i].user_data_enabled)
    {
      if (this->channels_[i].user_data_gain == 0.)
      {
        throw asl::DAQException("invalid DAQ configuration",
                                "one of the user data 'gain' parameters is null!",
                                "ContinuousAIConfig::check");
      }
    }
  }
}

} // namespace asl


