// ============================================================================
//
// = CONTEXT
//    Data Stream Model 
//
// = FILENAME
//    Worker.cpp
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <iostream>
#include <asl/Data_T.h>
#include <asl/Message.h>
#include <asl/Extractor_T.h>
#include <asl/Worker.h>

#if !defined (__ASL_INLINE__)
# include <asl/Worker.i>
#endif /* __ASL_INLINE__ */

namespace asl { 

// ============================================================================
// TIMEOUT 
// ============================================================================
//- Worker call Work's <process> if no message receive after <timeout_> secs 
#define kDEFAULT_TIMEOUT 10000000 // 10 seconds in microseconds

// ============================================================================
// STATICs
// ============================================================================
#if defined(ASL_DEBUG)
ASL_EXPORT AtomicOp Worker::instance_counter_ = 0;
#endif

// ============================================================================
// Worker::Worker
// ============================================================================
Worker::Worker (const std::string& _name, Work *_work, bool _work_ownership)
  : ACE_Task_Base (0),
    msg_queue_ (0),
    next_msg_ (0),
    prio_ (Worker::DEFAULT_PRIORITY),
    work_ (_work),
    name_ (_name),
    state_ (Worker::DOWN),
    lwm_ (Worker::DEFAULT_LWM),
    hwm_ (Worker::DEFAULT_HWM),
    last_error_ (0),
    work_ownership_(_work_ownership),
    timeout_ (0, kDEFAULT_TIMEOUT),
    sync_event_(0)
#if defined(ASL_DEBUG)
    ,msg_counter_ (0)
    ,ctrl_counter_ (0)
    ,trash_counter_ (0)
    ,data_counter_ (0)
    ,proc_data_ (0)
    ,thread_id_ (0)
    ,max_bytes_ (0)
    ,max_len_ (0)
    ,max_msgs_ (0)
    ,has_been_empty_ (0)
    ,has_been_full_ (0)
#endif
{
  ACE_ASSERT(this->work_ != 0);
  ACE_NEW(this->msg_queue_ , MessageQueue);
  ACE_ASSERT(this->msg_queue_ != 0);
}

// ============================================================================
// Worker::~Worker (destructor)
// ============================================================================
Worker::~Worker ()
{
  //- wakeup any waiter
  sync_event_.signal();

  //- release work
  if (this->work_ownership_ == true && this->work_ != 0) {
    delete this->work_;
    this->work_ = 0;
  }

  //- release msgQ
  if (this->msg_queue_ != 0) {
    delete this->msg_queue_;
    this->msg_queue_ = 0;
  }
}

// ============================================================================
// Worker::init
// ============================================================================
int Worker::init (size_t _lwm, size_t _hwm, long _priority)
{
  ACE_GUARD_RETURN(ACE_SYNCH_MUTEX, ace_mon, this->ctrl_lock_, -1);

  //- store priority 
  //-TODO:: priority range checking 
  prio_ = _priority;

  //- if lwm > hwm then swap values
  if (_hwm < _lwm) {
    size_t tmp = _hwm;
    _hwm = _lwm;
    _lwm = tmp;
  }

  //- worker must be down
  if (this->state() != Worker::DOWN) {
    set_error(new WorkerError("initialization failed"));
    return -1;
  }

  //- init error state
  int ierr = 0;
  do {
    //- call user defined <on_init> method
    WorkerError * err = 0;
    ierr = this->work_->on_init(err);
    if (err) 
      this->set_error(err);
    if (ierr == -1)
      break;
    //- open msgQ (it is safe to open an opened msgQ)
    ierr = this->msg_queue_->open(_hwm, _lwm);
    if (ierr == -1) {
      set_error(new WorkerError("initialization failed"));
      break;
    }
    //- spawn the service thread
    ierr = this->activate();
    if (ierr == -1) {
      set_error(new WorkerError("initialization failed"));
      break;
    }
  } while (0);

  if (ierr == -1) 
  {
    this->state(Worker::UNDEFINED);
    return -1;
  }
  else 
  {
    ACE_OS::thr_yield ();
    Timeout tmo = ACE_OS::gettimeofday() + Timeout(0, 500000);
    ierr = this->sync_event_.wait(&tmo);
    if (ierr == -1 && errno == ETIME) 
    {
      this->state(Worker::UNDEFINED);
      return -1;
    }
    this->sync_event_.reset();
  }

  return ierr;
}

// ============================================================================
// Worker::close
// ============================================================================
int Worker::close (u_long _flags)
{
  ACE_UNUSED_ARG(_flags);

  ACE_GUARD_RETURN(ACE_SYNCH_MUTEX, ace_mon, this->ctrl_lock_, -1);

  //- prevent this method to be called twice
  if (this->state() == Worker::DOWN)
    return 0; 

  //- close the msgQ
  this->msg_queue_->close ();

  //- update state
  this->state(Worker::DOWN);

  return 0;
}

// ============================================================================
// Worker::start
// ============================================================================
int Worker::start (bool _wait, Timeout * _tv, bool * _tv_expired)
{
  ACE_GUARD_RETURN(ACE_SYNCH_MUTEX, ace_mon, this->ctrl_lock_, -1);

  *_tv_expired = false;

  Message *m = new Message(MessageType::MSG_START);
  if (m == 0) 
  {
    set_error(new WorkerError("out of memory", WorkerError::ERR_START));
    return -1;
  }

  int result = this->msg_queue_->enqueue_prio (m, 0);
  if (result == -1) 
  {
    set_error(new WorkerError("internal error", WorkerError::ERR_START));
    return -1;
  }

  if (_wait) 
  {
    Timeout tmo = _tv ? *_tv : ACE_OS::gettimeofday() + Timeout(0, 500000);
    result = this->sync_event_.wait(&tmo);
    if (result == -1 && errno == ETIME) 
    {
      //- wait timeout
      if (_tv_expired) 
      {
        *_tv_expired = true;
      }
    }
    else if (_tv_expired) 
    {
      *_tv_expired = false;
    }
    this->sync_event_.reset();
    if (result == -1) 
      return -1;
  }

  return 0;
}

// ============================================================================
// Worker::reset
// ============================================================================
int Worker::reset (bool _wait, Timeout * _tv, bool * _tv_expired)
{
  ACE_GUARD_RETURN(ACE_SYNCH_MUTEX, ace_mon, this->ctrl_lock_, -1);

  Message *m = new Message(MessageType::MSG_RESET);
  if (m == 0) {
    set_error(new WorkerError("out of memory", WorkerError::ERR_RESET));
    return -1;
  }
  int result = this->msg_queue_->enqueue_prio (m, 0);
  if (result == -1) {
    set_error(new WorkerError("internal error", WorkerError::ERR_RESET));
    return -1;
  }
  if (_wait) {
    Timeout tmo = _tv ? *_tv : ACE_OS::gettimeofday() + Timeout(0, 500000);
    result = this->sync_event_.wait(&tmo);
    if (result == -1 && errno == ETIME) {
      //- wait timeout
      if (_tv_expired) {
        *_tv_expired = true;
      }
    }
    else if (_tv_expired) {
      *_tv_expired = false;
    }
    this->sync_event_.reset();
    if (result == -1) 
      return -1;
  }
  return 0;
}

// ============================================================================
// Worker::stop
// ============================================================================
int Worker::stop (bool _wait, Timeout * _tv, bool * _tv_expired)
{
  ACE_GUARD_RETURN(ACE_SYNCH_MUTEX, ace_mon, this->ctrl_lock_, -1);

  Message *m = new Message(MessageType::MSG_STOP);
  if (m == 0) {
    set_error(new WorkerError("out of memory", WorkerError::ERR_STOP));
    return -1;
  }
  int result = this->msg_queue_->enqueue_prio (m, 0);
  if (result == -1) {
    set_error(new WorkerError("internal error", WorkerError::ERR_STOP));
    return -1;
  }
  if (_wait) {
    Timeout tmo = _tv ? *_tv : ACE_OS::gettimeofday() + Timeout(0, 500000);
    result = this->sync_event_.wait(&tmo);
    if (result == -1 && errno == ETIME) {
      //- wait timeout
      if (_tv_expired) {
        *_tv_expired = true;
      }
    }
    else if (_tv_expired) {
      *_tv_expired = false;
    }
    this->sync_event_.reset();
    if (result == -1) 
      return -1;
  }
  return 0;
}

// ============================================================================
// Worker::abort
// ============================================================================
int Worker::abort (bool _wait, Timeout * _tv, bool * _tv_expired)
{
  ACE_GUARD_RETURN(ACE_SYNCH_MUTEX, ace_mon, this->ctrl_lock_, -1);

  Message *m = new Message(MessageType::MSG_ABORT);
  if (m == 0) {
    set_error(new WorkerError("out of memory", WorkerError::ERR_ABORT));
    return -1;
  }
  int result = this->msg_queue_->enqueue_prio (m, 0);
  if (result == -1) {
    set_error(new WorkerError("internal error", WorkerError::ERR_ABORT));
    return -1;
  }
  if (_wait) {
    Timeout tmo = _tv ? *_tv : ACE_OS::gettimeofday() + Timeout(0, 500000);
    result = this->sync_event_.wait(&tmo);
    if (result == -1 && errno == ETIME) {
      //- wait timeout
      if (_tv_expired) {
        *_tv_expired = true;
      }
    }
    else if (_tv_expired) {
      *_tv_expired = false;
    }
    this->sync_event_.reset();
    if (result == -1) 
      return -1;
  }
  return 0;
}

// ============================================================================
// Worker::quit
// ============================================================================
int Worker::quit ()
{
  {//- enter critical section 
    ACE_GUARD_RETURN(ACE_SYNCH_MUTEX, ace_mon, this->ctrl_lock_, -1);

    Message *m = new Message(MessageType::MSG_QUIT);
    if (m == 0) {
      set_error(new WorkerError("out of memory", WorkerError::ERR_QUIT));
      return -1;
    }
    int result = this->msg_queue_->enqueue_prio (m, 0);
    if (result == -1) {
      set_error(new WorkerError("internal error", WorkerError::ERR_QUIT));
      return -1;
    }
  }//- leave critical section 

  //- do not put the wait call into critical section (or deadlock garantee)
  this->wait();

  return 0;
}

// ============================================================================
// Worker::next_message
// ============================================================================
int Worker::next_message (Message *&_mb)
{ 
  //- std::cout << "Worker::next_message <-" << std::endl;

  //- update statistics
#if defined(ASL_DEBUG)
  if (this->msg_queue_->message_bytes() > this->max_bytes_) {
    this->max_bytes_ = this->msg_queue_->message_bytes();
  }
  if (this->msg_queue_->message_length() > this->max_len_) {
    this->max_len_ = this->msg_queue_->message_length();
  }
  if (this->msg_queue_->message_count() > this->max_msgs_) {
    this->max_msgs_ = this->msg_queue_->message_count();
  }
  if (this->msg_queue_->is_empty()) {
    this->has_been_empty_ = 1;
  }
  if (this->msg_queue_->is_full()) {
    this->has_been_full_ = 1;
  }
#endif

  //- get next message in continuation field
  Timeout tmo = ACE_OS::gettimeofday() + this->timeout_;
  if (  
       this->next_msg_ == 0 
        && 
       this->getq(this->next_msg_, (this->state_ == Worker::WORKING) ? &tmo : 0) == -1
     ) 
  {
    if (errno == EWOULDBLOCK) 
    {
      //- timeout
      this->next_msg_ = new Message(MessageType::MSG_TIMEOUT);
    } 
    else 
    {
      //- error or signal
      //- wakeup waiters or they will block forever
      this->sync_event_.signal();
      return -1;
    }
  }

  //- reset sync. event
  this->sync_event_.reset();

  //- update statistics
#if defined(ASL_DEBUG)
  if (this->next_msg_->msg_type() & MessageType::CONTROL_FLAG) {
    ++this->ctrl_counter_;
  }
  ++this->msg_counter_;      
#endif

  //- set output value
  //- the caller has to release this message
  _mb = this->next_msg_;
  
  //- get next message in continuation field...
  this->next_msg_ = this->next_msg_->cont();

  // ... so remove mb from continuation field.
  _mb->cont(0);

  //- std::cout << "Worker::next_message ->" << std::endl;

  //- done with no error
  return 0;
}

// ============================================================================
// Worker::svc  (service thread entry point)
// ============================================================================
int Worker::svc ()
{
  //- std::cout << "Worker::svc <-" << std::endl;

  //- input message
  Message * in_msg = 0;
  //- input message
  WorkerError * err = 0;
  //- while loop control flag
  int go_on = 1;

#if defined(ASL_DEBUG)
  this->just_a_call_in_svc_thread_context();
#endif

  //- set priority 
  this->priority_level_i(this->prio_);

  //- update state 
  this->state(Worker::STANDBY);

  //- init caller is waiting on <sync_event> 
  this->sync_event_.signal();

  //- enter almost infinite loop...
  while (go_on)
  {
    do {
      //- init input message
      in_msg = 0;
      //- get next message in the msgQ
      if (this->next_message(in_msg) == -1) {
        //- fatal error
        set_error(new WorkerError("internal error", true));
      }
    } while (in_msg == 0);

    // Process message.
    switch (in_msg->msg_type())
    {
      case MessageType::MSG_START: 
        //- start working
        //- init <output> error message
        err = 0;
        //- release message to avoid memory leak
        in_msg->release();
        //- call user's defined <on_start> routine
        if (this->work_->on_start(err) == -1) {
          if (err != 0)
            this->set_error(err);
          else
            this->set_error(new WorkerError("start error", WorkerError::ERR_START)); 
          //- wakeup waiters
          this->sync_event_.signal();
          break;
        }
        //- start <sub-service>  
        if (this->sub_svc() == -1) {
          //- error or quit request from sub_svc
          go_on = 0;
        }
      break;

      case MessageType::MSG_STOP: 
      case MessageType::MSG_ABORT:
      case MessageType::MSG_RESET:
        //- ignore msg (not started)
        //- release message to avoid memory leak.
        in_msg->release(); 
        //- wakeup waiters
        this->sync_event_.signal();
      break;

      case MessageType::MSG_TIMEOUT: 
        //- ignore msg (not started)
        in_msg->release(); 
      break;

      case MessageType::MSG_QUIT: 
        //- release message to avoid memory leak.
        in_msg->release();
        //- call user <on_quit> routine.
        this->work_->on_quit();
        //- exit <while(go_on)> loop.
        go_on = 0;
      break;

      case MessageType::MSG_TASK_PRIORITY:
        //- change priority
        //- get new priority from message.
        Container<long>* pc;
        if (message_data(in_msg, pc) == -1) {
          this->prio_ = pc->content();
          if (this->priority_level_i(this->prio_) == -1) {
            this->set_error(new WorkerError("thread priority could not be changed")); 
          }
        }
        //- release message to avoid memory leak.
        in_msg->release();
      break;

      default: 
        // msg not handle at this level unless its <force_processing>
        // option is set to TRUE, otherwise thrash it!
        // ----------------------------------------------------------
        if (in_msg->force_processing())
        {
          //- init <output> error message
          err = 0;
          //- call user's <process> routine.
          if (this->work_->process(in_msg, err) == -1) {
            if (err != 0)
              this->set_error(err);
            else
              this->set_error(new WorkerError("unknown error"));
          }
        }
        else
        {
          //- inc. trash counter (debug only)
#if defined(ASL_DEBUG)
          ++this->trash_counter_;
#endif
          //- release message to avoid memory leak.
          in_msg->release();
        }
      break;
    } // switch 


  } // while (go_on)

  //- update internal state
  this->state(Worker::UP);

  //- wakeup waiters
  this->sync_event_.signal();  

  //- std::cout << "Worker::svc <-" << std::endl;

  return 0;
}

// ============================================================================
// Worker::sub_svc
// ============================================================================
int Worker::sub_svc ()
{
  //- std::cout << "Worker::sub_svc <-" << std::endl;

  //- input message
  Message * in_msg = 0;
  //- output error message
  WorkerError * err = 0;
  //- while loop control flag
  int go_on = 1;
  //- return value
  int result = 0;

  //- update internal state
  this->state(Worker::WORKING);

  //- wakeup waiters
  this->sync_event_.signal();

  //- enter almost infinite loop...
  while (go_on)
  {
    //- get next message in the task's queue.
    if (this->next_message(in_msg) == -1) {
      //- fatal error
      set_error(new WorkerError("internal error", true));
    }

    //- process message. 
    switch (in_msg->msg_type())
    {
      case MessageType::MSG_START:
        //- ignore msg
        //- release message to avoid memory leak
        in_msg->release();
        //- wakeup waiters
        this->sync_event_.signal();
      break;

      case MessageType::MSG_RESET:
        //- reset requested
        err = 0;
        //- call user <on_reset> routine.
        if (this->work_->on_reset(err) == -1) {
          if (err != 0)
            this->set_error(err);
          else
            this->set_error(new WorkerError("reset error", WorkerError::ERR_RESET));  
          //- set return value
          result = 0;
          //- exit <while(go_on)> loop.
          go_on = 0;
        }
        //- release message to avoid memory leak
        in_msg->release();
      break;

      case MessageType::MSG_STOP:
        //- stop working
        err = 0;
        //- call user <on_stop> routine
        if (this->work_->on_stop(err) == -1) {
          if (err != 0)
            this->set_error(err);
          else
            this->set_error(new WorkerError("stop error", WorkerError::ERR_STOP));   
        }
        //- set return value
        result = 0;
        //- exit <while(go_on)> loop
        go_on = 0;
        //- release message to avoid memory leak
        in_msg->release();
      break;

      case MessageType::MSG_ABORT:
        //- abort
        err = 0;
        // Call user <on_abort> routine.
        if (this->work_->on_abort(err) == -1) {
          if (err != 0)
            this->set_error(err);
          else
            this->set_error(new WorkerError("abort error", WorkerError::ERR_ABORT)); 
        }
        //- set return value
        result = 0;
        //- exit <while(go_on)> loop.
        go_on = 0;
        //- release message to avoid memory leak
        in_msg->release();
      break;

      case MessageType::MSG_QUIT:
        //- quit
        in_msg->release();
        //- call user <on_quit> routine
        this->work_->on_quit();
        //- set return value
        result = -1;
        //- exit loop
        go_on = 0;
      break;

      case MessageType::MSG_TASK_PRIORITY:
        //- change priority
        //- get new priority from message.
        Container<long>* pc;
        if (message_data(in_msg, pc) != -1) {
          this->prio_ = pc->content();
          if (this->priority_level_i(this->prio_) == -1) {
            this->set_error(new WorkerError("thread priority could not be changed")); 
          }
        }
        //- release message to avoid memory leak.
        in_msg->release();
      break;

      case MessageType::MSG_DATA:
      case MessageType::MSG_TIMEOUT: 
      default:
        //- process data
        //- inc. data message counter and compute data size (debug only).
#if defined(ASL_DEBUG)
        ++this->data_counter_;
        this->proc_data_ += in_msg->data_size();
#endif
        //- init <output> error message
        err = 0;
        //- call user's <process> routine.
        if (this->work_->process(in_msg, err) == -1) {
          if (err != 0)
            this->set_error(err); 
          else 
            this->set_error(new WorkerError("unknown error"));  
          //- call user's <on_abort> routine.
          err = 0;
          if (this->work_->on_abort(err) == -1) {
            if (err != 0)
              this->set_error(err);
            else
              this->set_error(new WorkerError("abort error", WorkerError::ERR_ABORT));
          }
          //- exit <while(go_on)> loop.
          go_on = 0;
          //- set return value
          result = 0;
        }
        break;
    } // switch (in_msg->msg_type())
  } // while (go_on)

  //- update internal state
  this->state(Worker::STANDBY);

  //- wakeup waiters
  this->sync_event_.signal();

  //- std::cout << "Worker::sub_svc ->" << std::endl;

  return result;
}

// ============================================================================
// Worker::set_error
// ============================================================================
void Worker::set_error (WorkerError *_err)
{
  ACE_GUARD(ACE_SYNCH_MUTEX, ace_mon, this->lock_);
  //- check input
  if (_err == 0) {
    return;
  }
  //- store error localy
  if (last_error_) {
    _err->cont(last_error_);
  }
  last_error_ = _err;
  //- fatal error
  if (_err->is_fatal()) {
    this->msg_queue_->enqueue_prio(new Message(MessageType::MSG_ABORT));
  }
}

// ============================================================================
// Worker::priority_level
// ============================================================================
int Worker::priority_level (long _prio)
{
  if (this->msg_queue_->deactivated() == 0) {
    Container<long>* lc = new Container<long>(_prio);
    if (lc == 0) {
      return -1;
    }
    Message * msg = new Message(lc, MessageType::MSG_TASK_PRIORITY);
    if (msg == 0) {
      delete lc;
      return -1;
    }
    if (this->put(msg, 0) < 0) {
      msg->release(); 
      return -1;
    }
    return 0;
  }
  return -1;
}

// ============================================================================
// Worker::priority_level_i
// ============================================================================
int Worker::priority_level_i (long _prio)
{
  //- get thread handle.
  ACE_hthread_t ht;
  ACE_Thread::self(ht);

  //- get current priority.
  int current_prio;
  ACE_Thread::getprio(ht, current_prio);
  this->prio_ = current_prio;

  //- change priotity.
  if (ACE_Thread::setprio(ht, _prio) < 0)
    return -1;

  return 0;
}

// ============================================================================
// Worker::dump
// ============================================================================
void Worker::dump (int _indent_level) const
{
  ACE_GUARD(ACE_SYNCH_MUTEX, ace_mon, 
            ACE_const_cast(ACE_Thread_Mutex&, this->lock_));

  int i;
  for (i=0 ; i<_indent_level ; ++i) ACE_DEBUG((LM_DEBUG,"\t"));
  ACE_DEBUG ((LM_DEBUG,  ACE_TEXT (". name...........%s\n"), this->name().c_str()));

  for (i=0 ; i<_indent_level ; ++i) ACE_DEBUG((LM_DEBUG,"\t"));
  switch (this->state())
  { 
    case Worker::UP:
      ACE_DEBUG((LM_DEBUG,". state..........up\n"));
      break;
    case Worker::STANDBY:
      ACE_DEBUG((LM_DEBUG,". state..........standby\n"));
      break;
    case Worker::WORKING:
      ACE_DEBUG((LM_DEBUG,". state..........working\n"));
      break;
    case Worker::DOWN:
      ACE_DEBUG((LM_DEBUG,". state..........down\n"));
      break;
    default:
    case Worker::UNDEFINED:
      ACE_DEBUG((LM_DEBUG,". state..........undefined\n"));
      break;
  }

#if defined(ASL_DEBUG)
  for (i=0 ; i<_indent_level ; ++i) ACE_DEBUG((LM_DEBUG,"\t"));
  ACE_DEBUG ((LM_DEBUG,  ACE_TEXT (". thread id......%d\n"),
      this->thread_id_));

  for (i=0 ; i < _indent_level ; ++i) ACE_DEBUG((LM_DEBUG,"\t"));
  ACE_DEBUG ((LM_DEBUG,  ACE_TEXT (". thread prio....%d\n"),
      this->prio_));

  for (i=0 ; i < _indent_level ; ++i) ACE_DEBUG((LM_DEBUG,"\t"));
  ACE_DEBUG ((LM_DEBUG,  ACE_TEXT (". total msg......%d\n"),     
      this->msg_counter_));

  for (i=0 ; i < _indent_level ; ++i) ACE_DEBUG((LM_DEBUG,"\t"));
  ACE_DEBUG ((LM_DEBUG,  ACE_TEXT (". ctrl msg.......%d\n"),
      this->ctrl_counter_));

  for (i=0 ; i < _indent_level ; ++i) ACE_DEBUG((LM_DEBUG,"\t"));
  ACE_DEBUG ((LM_DEBUG,  ACE_TEXT (". trash msg......%d\n"),
      this->trash_counter_));
  
  for (i=0 ; i < _indent_level ; ++i) ACE_DEBUG((LM_DEBUG,"\t"));
  ACE_DEBUG ((LM_DEBUG,  ACE_TEXT (". mQ::activated..%d\n"), 
    !this->msg_queue_->deactivated()));

  for (i=0 ; i < _indent_level ; ++i) ACE_DEBUG((LM_DEBUG,"\t"));
  ACE_DEBUG ((LM_DEBUG,  ACE_TEXT (". mQ::msg........%d\n"), 
    this->msg_queue_->message_count()));

  for (i=0 ; i < _indent_level ; ++i) ACE_DEBUG((LM_DEBUG,"\t"));
  ACE_DEBUG ((LM_DEBUG,  ACE_TEXT (". mQ::bytes......%d\n"),     
    this->msg_queue_->message_bytes()));

  #if defined(ASL_DEBUG)
    for (i=0 ; i < _indent_level ; ++i) ACE_DEBUG((LM_DEBUG,"\t"));
    ACE_DEBUG ((LM_DEBUG,  ACE_TEXT (". mQ::max bytes..%d\n"),     
      this->max_bytes_));
    for (i=0 ; i < _indent_level ; ++i) ACE_DEBUG((LM_DEBUG,"\t"));
    ACE_DEBUG ((LM_DEBUG,  ACE_TEXT (". mQ::max len....%d\n"),     
      this->max_len_));
    for (i=0 ; i < _indent_level ; ++i) ACE_DEBUG((LM_DEBUG,"\t"));
    ACE_DEBUG ((LM_DEBUG,  ACE_TEXT (". mQ::max msgs...%d\n"),     
      this->max_msgs_));
    for (i=0 ; i < _indent_level ; ++i) ACE_DEBUG((LM_DEBUG,"\t"));
    ACE_DEBUG ((LM_DEBUG,  ACE_TEXT (". mQ::hb-empty...%d\n"),     
      this->has_been_empty_));
    for (i=0 ; i < _indent_level ; ++i) ACE_DEBUG((LM_DEBUG,"\t"));
    ACE_DEBUG ((LM_DEBUG,  ACE_TEXT (". mQ::hb-full....%d\n"),     
      this->has_been_full_));
  #endif

  for (i=0 ; i < _indent_level ; ++i) ACE_DEBUG((LM_DEBUG,"\t"));
  ACE_DEBUG ((LM_DEBUG,  ACE_TEXT (". mQ::lwm........%d\n"),   
    this->msg_queue_->low_water_mark()));

  for (i=0 ; i < _indent_level ; ++i) ACE_DEBUG((LM_DEBUG,"\t"));
  ACE_DEBUG ((LM_DEBUG,  ACE_TEXT (". mQ::hwm........%d\n"),     
    this->msg_queue_->high_water_mark()));
#endif // ASL_DEBUG
}

#if defined(ASL_DEBUG)
// ============================================================================
// Worker::instance_counter
// ============================================================================
u_long Worker::instance_counter () 
{
  return instance_counter_.value();
}
#endif  

} // namespace asl

