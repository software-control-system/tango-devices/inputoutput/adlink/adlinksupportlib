// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    SingleShotDO.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/SingleShotDO.h>
#include <asl/DAQException.h>

#if !defined (__ASL_INLINE__)
# include <asl/SingleShotDO.i>
#endif // __ASL_INLINE__

namespace asl {

// ============================================================================
// SingleShotDO:: SingleShotDO
// ============================================================================
SingleShotDO:: SingleShotDO () 
  : SingleShotDAQ(), daq_hw_ (0) 
{
  //- std::cout << "SingleShotDO::SingleShotDO <-" << std::endl;
  //- std::cout << "SingleShotDO::SingleShotDO ->" << std::endl;
}

// ============================================================================
// SingleShotDO:: ~SingleShotDO
// ============================================================================
SingleShotDO::~SingleShotDO () 
{
  //- std::cout << "SingleShotDO::~SingleShotDO <-" << std::endl;
  
  //- release DAQ hardware
  if (this->daq_hw_) {
    this->daq_hw_->release();
    this->daq_hw_ = 0;
  }

  //- std::cout << "SingleShotDO::~SingleShotDO ->" << std::endl;
}

// ============================================================================
// SingleShotDO::init
// ============================================================================
void SingleShotDO::init (unsigned short _type, unsigned short _id)
throw (asl::DAQException)
{
	try 
	{
		//- instanciate the board - don't get exclusive access
		this->daq_hw_ = adl::ADSingleShotDOBoard::instanciate(_type, _id, false);
	}
	catch (const asl::DAQException&) 
	{
		//- rethrow exception
		throw;
	}
	catch (...) 
	{
		//- throw unknown error
		throw DAQException();
	}
}

// ============================================================================
// SingleShotDO::configure_port
// ============================================================================
void SingleShotDO::configure_port (adl::DIOPort _port) 
throw (asl::DAQException)
{
  //- std::cout << "SingleShotDO::configure_port<-"<<std::endl;

	if (this->initialized() == false) {
		throw asl::DAQException("unitialized DAQ",
			"SingleShotDO::init must be called before any other functions",
			"SingleShotDO::configure_port");
	}
	
	try
	{
		this->daq_hw_->configure_do_port(_port);
	} 
	catch (const DAQException&)
	{
		throw;
	}
	catch (...) {
		throw DAQException();
	}

  //- std::cout << "SingleShotDO::configure_port->"<<std::endl;
}
// ============================================================================
// SingleShotDO::write_line
// ============================================================================
void SingleShotDO::write_line (adl::DIOPort _port, int _line, bool _state) 
throw (asl::DAQException)
{	
	if (this->initialized() == false) {
		throw asl::DAQException("unitialized DAQ",
			"SingleShotDO::init must be called before any other functions",
			"SingleShotDO::write_line");
	}
	
	try
	{
		this->daq_hw_->write_line(_port, _line, _state);
	} 
	catch (const DAQException&) 
	{
		throw;
	}
	catch (...) 
	{
		throw DAQException();
	}
}
// ============================================================================
// SingleShotDO::write_port
// ============================================================================
void SingleShotDO::write_port (adl::DIOPort _port, unsigned long _state) 
throw (asl::DAQException)
{
	if (this->initialized() == false) {
		throw asl::DAQException("unitialized DAQ",
			"SingleShotDO::init must be called before any other functions",
			"SingleShotDO::write_port");
	}
	
	try
	{
		this->daq_hw_->write_port(_port,_state);
	} 
	catch (const DAQException&) 
	{
		throw;
	}
	catch (...) 
	{
		throw DAQException();
	}
}

} //namespace



