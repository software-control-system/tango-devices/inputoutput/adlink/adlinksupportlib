// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ContinuousAI.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <math.h>
#include <iostream>
#if defined(_SIMULATION_)
# include <asl/PulsedTask.h>
#endif
#include <asl/Message.h>
#include <asl/Extractor_T.h>
#include <asl/AIData.h> 
#include <asl/ContinuousAI.h>
#include <asl/adlink/ADCalibrableAIOBoard.h>
#include <asl/ContinuousDAQHandler.h>
#include <asl/DAQException.h>

#if !defined (__ASL_INLINE__)
# include <asl/ContinuousAI.i>
#endif // __ASL_INLINE__

namespace asl {

// ============================================================================
// STATICS
// ============================================================================
ContinuousAI* ContinuousAI::instance = 0;
ACE_Thread_Mutex ContinuousAI::delete_lock;


#if defined(_SIMULATION_)
// ============================================================================
// PERIOD BETWEEN TWO CALLS TO THE CALLBACK IN MILLISECONDS
// ============================================================================
#define kCB_PERIOD 5

// ============================================================================
// CallbackCaller : a asl::PulsedTask to simulate DAQ activity
// ============================================================================
class CallbackCaller : public asl::PulsedTask
{
public:

  CallbackCaller () 
  {
    /*noop*/
  };

  virtual ~CallbackCaller () 
  {
    /*noop*/
  };

  virtual int pulsed (void* arg) 
  {
    DAQCallback cb = ACE_reinterpret_cast(DAQCallback, arg);
    if (cb) cb();
    return 0;
  }

};
#endif // _SIMULATION_

// ============================================================================
// The ContinuousAI underlying Worker's Work
// ============================================================================
class ASL_EXPORT ContinuousAIWork : public asl::Work
{
public:

  //- ctor
  ContinuousAIWork (ContinuousAI * _ai) 
    : daq_end_received_(false), ai_(_ai), thread_executing_hook_(0) 
  {
    /*noop*/
  };

  //- dtor
  virtual ~ContinuousAIWork () 
  {
    /*noop*/
  };

  ACE_thread_t thread_executing_hook () const 
  {
    return this->thread_executing_hook_;
  };

  virtual int on_start (WorkerError *&err) 
  {
    this->daq_end_received_ = false;
    err = 0;
    return 0;
  };
  
  //- process hook overload
  virtual int process (Message *in, WorkerError *&err)
  {
    //- init return value
    int result = 0;
    //- init <out> error message  handle_daq_end () = 0;  
    err = 0;

    //- save id of thread executing user hook
    thread_executing_hook_ = ACE_Thread::self();

    // Filter message... 
    //-------------------------
    switch (in->msg_type())
    {
      // <in> is a <DATA> message.
      //-------------------------
      case MessageType::MSG_DATA:
      {
        //- avoid sending data to user after daq-end evt
        if (this->daq_end_received_ == false) 
        {
          AIRawData* data;
          //- get message content
          if (message_data(in, data) == -1) 
          {
            ASL_NEW(err, WorkerError("unexpected data", 1));
            in->release();
            result = -1;
            break;
          }
          //- detach data (i.e. get ownership)
          in->detach_data();
          //- release input message to avoid memory leaks
          in->release();
          //- call user defined handle_input
          try 
          {
            this->ai_->handle_input(data);
          }
          catch (...) 
          {
            //- ignore exception 
          }
        }
        else 
        {
          in->release();
        }
      }
      break;

      // <in> is a <OVERRUN> message.
      //-----------------------------
      case MessageType::MSG_OVERRUN:
      case MessageType::MSG_OVERLOAD:
      {
        //- release input message to avoid memory leaks
        in->release();
        //- call user defined data_lost
        try 
        {
          this->ai_->handle_data_lost();
        }
        catch (...) 
        {
          //- ignore exception 
        }
      }
      break;

      // <in> is a <TIMEOUT> message.
      //-----------------------------
      case MessageType::MSG_TIMEOUT:
      {
        //- release input message to avoid memory leaks
        in->release();
        //- call user defined overrun
        try 
        {
          this->ai_->handle_timeout();
        }
        catch (...) 
        {
          //- ignore exception 
        }
      }
      break;

      // <in> is a <MSG_DAQEND> message.
      //-----------------------------
      case MessageType::MSG_DAQEND:
      {
        //- mark evt 
        this->daq_end_received_ = true;
        //- expected data
        Container<ContinuousDAQ::DaqEndReason> * why;
        //- get message content
        if (message_data(in, why) == -1) 
        {
          ASL_NEW(err, WorkerError("unexpected data", 1));
          in->release();
          result = -1;
          break;
        }
        //- stop DAQ and get remaining samples
        try 
        {
          //- stop DAQ to ensure DAQ state consistency
          this->ai_->stop();
          //- call user defined handle_daq_end
          this->ai_->handle_daq_end(why->content());
        }
        catch (...) 
        {
          //- ignore exception 
        }
        //- release input message to avoid memory leaks
        in->release();
      }
      break;

      // <in> is a <MSG_FATAL_ERROR> message.
      //-----------------------------
      case MessageType::MSG_FATAL_ERROR:
      {
        //- expected data
        Container<const DAQException>* c = 0;
        //- get message content
        if (message_data(in, c) == -1) 
        {
          ASL_NEW(err, WorkerError("unexpected data", 1));
          in->release();
          result = -1;
          break;
        }
        //- call user defined handle_error
        try 
        {
          this->ai_->handle_error(c->content());
        }
        catch (...) 
        {
          //- ignore exception 
        }
        //- release input message to avoid memory leaks
        in->release();
      }
      break;

      // <in> has a type we do not handle
      //---------------------------------
      default:
      {
        //- release input message to avoid memory leaks
        in->release();
      }  
      break;
    }

    return result;
  }

private:

  //- dad-end evt flag
  bool daq_end_received_; 
  
  //- the associated ContinuousAI
  ContinuousAI* ai_;

  //- id of the thread executing a user hook
  ACE_thread_t thread_executing_hook_; 
};

// ============================================================================
// ContinuousAI::ContinuousAI
// ============================================================================
ContinuousAI::ContinuousAI ()
  : worker_ (0),
    work_ (0),
    daq_hw_ (0),
    cb_(0)
#if defined(_SIMULATION_)
    ,cb_caller_(0)
#endif
{
  //- std::cout << "ContinuousAI::ContinuousAI <-" << std::endl;

  ContinuousAI::instance = this;

  //- std::cout << "ContinuousAI::ContinuousAI ->" << std::endl;
}

// ============================================================================
// ContinuousAI::~ContinuousAI
// ============================================================================
ContinuousAI::~ContinuousAI ()
{
  //- std::cout << "ContinuousAI::~ContinuousAI <-" << std::endl;

  //- need to lock 
  //- let current action complete (e.g. calibration)
  ACE_GUARD_REACTION(ACE_SYNCH_MUTEX, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());

  //- also need to fight against race condition between 
  //- ContinuousAI::ai_end_event_callback and this dtor. 
  //- ContinuousAI::ai_end_event_callback may try to use a dead
  //- ContinuousAI::instance in case the dtor is deleting all.
  ACE_GUARD_REACTION(ACE_SYNCH_MUTEX, 
                     ace_delete_mon, 
                     ContinuousAI::delete_lock, 
                     throw asl::DAQException());

  //- try to stop the DAQ
  try 
  {
    if (this->daq_hw_) 
      this->stop_i();
  }
  catch (...) 
  {
    //- ignore any exception
  }

  //- quit then release worker
  if (this->worker_) 
  {
    this->worker_->abort();
    this->worker_->quit();
    delete this->worker_;
    this->worker_ = 0;
  }

  //- release worker's work
  if (this->work_) 
  {
    delete this->work_;
    this->work_ = 0;
  }

  //- release DAQ hardware
  if (this->daq_hw_) 
  {
    this->daq_hw_->release();
    this->daq_hw_ = 0;
  }

  //- release history buffer
  if (this->cb_) 
  {
    delete this->cb_;
    this->cb_ = 0;
  }

  ContinuousAI::instance = 0;

  //- std::cout << "ContinuousAI::~ContinuousAI ->" << std::endl;
}

// ============================================================================
// ContinuousAI::init
// ============================================================================
void ContinuousAI::init (unsigned short _type, unsigned short _id)
    throw (asl::DAQException)
{
  //- std::cout << "ContinuousAI::init <-" << std::endl;

  ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                    ace_mon, 
                    this->lock_, 
                    throw asl::DAQException());

  //- return if already initialized
  if (this->initialized()) 
  {
    return; 
  }

  //- instanciate the work
  ASL_NEW(this->work_, ContinuousAIWork(this));
  if (this->work_ == 0) 
  {
    this->state_ = FAULT;
    //- out of memory
    throw asl::DAQException("out of memory",
                            "out of memory error",
                            "ContinuousAI::init");
  }

  //- instanciate the worker
  ASL_NEW(this->worker_, Worker("unnamed", this->work_, false));
  if (this->worker_ == 0) 
  {
    this->state_ = FAULT;
    //- out of memory
    throw asl::DAQException("out of memory",
                            "out of memory error",
                            "ContinuousAI::init");
  }

  //- init worker
  if (this->worker_->init() == -1) 
  {
    this->state_ = FAULT;
    throw asl::DAQException("asl internal error",
                            "could start deferred procesing task",
                            "ContinuousAI::init");
  }

  try 
  {
    //- instanciate the board - get exclusive access
    this->daq_hw_ = adl::ADContinuousAIBoard::instanciate(_type, _id, false);
  }
  catch (const asl::DAQException&) 
  {
    this->state_ = FAULT;
    //- rethrow exception
    throw;
  }
  catch (...) 
  {
    this->state_ = FAULT;
    //- throw unknown error
    throw DAQException();
  }
 
  //- change state to STANDBY (ready to run with default DAQ config)
  this->state_ = STANDBY;

  //- std::cout << "ContinuousAI::init ->" << std::endl;
}
    
// ============================================================================
// ContinuousAI::set_user_data_parameters
// ============================================================================
void ContinuousAI::set_user_data_parameters (adl::ChanId id, double gain, double offset1, double offset2)
    throw (asl::DAQException)
{
  ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     return);

  this->config_.set_user_data_parameters(id, gain, offset1, offset2);
}

// ============================================================================
// ContinuousAI::configure
// ============================================================================
void ContinuousAI::configure (const ContinuousAIConfig& _config)
  throw (asl::DAQException, asl::DeviceBusyException)
{
  //- std::cout << "ContinuousAI::configure <-" << std::endl;

  TIMEOUT_GUARD(this->lock_, "ContinuousAI::configure"); 

  this->action_allowed("ContinuousAI::configure");

  if (this->state() == RUNNING)
  {
    throw asl::DeviceBusyException("device busy",
                                   "can't modify configuration while DAQ is running",
                                   "ContinuousAI::configure");
  }

  CHECK_GUARD_LOCKED("ContinuousAI::configure");

  //- configure without blocking nor changing internal state
  this->configure_i(_config);

  //- change state to STANDBY (ready to run with user config)
  this->state_ = STANDBY;

  //- std::cout << "ContinuousAI::configure ->" << std::endl;
}

// ============================================================================
// ContinuousAI::configure_i
// ============================================================================
void ContinuousAI::configure_i (const ContinuousAIConfig& _config)
    throw (asl::DAQException)
{
  //- std::cout << "ContinuousAI::configure_i <-" << std::endl;

  //- throw if not initialized
  if (this->initialized() == false) 
  {
    throw asl::DAQException("unitialized DAQ",
                            "ContinuousAI::init must be called before any other functions",
                            "ContinuousAI::configure");
  }

  //- get <num of scans per buffer>
  unsigned long depth = _config.get_buffer_depth();

  //- actual depth is <num of scans per buffer> * <num of active channels>
  depth *= _config.num_active_channels();

  //- be sure buffer size is even
  if (depth % 2) {
    throw asl::DAQException("invalid DAQ buffer depth",
                            "DAQ buffer depth must be even",
                            "ContinuousAI::configure_i");
  }

  //- copy config locally
  this->config_ = _config;

  //- init the history buffer (i.e. the circular buffer)
  if (this->config_.history_enabled()) 
  {
    //- calculate num of DAQ-buffers in history
    double msec_per_buffer = 1000.0 * (double)this->config_.get_buffer_depth() / this->config_.get_sampling_rate(); 
    
    // Replace ::ceil by ulong cast for cb_ size to be consistent with history_len_msec_
    unsigned long num_buffers_in_history = (unsigned long)floor(((double)this->config_.get_history_length() / msec_per_buffer) + 0.5);
    if (num_buffers_in_history < 2.) 
    {
      this->config_.set_history_length(static_cast<unsigned long>(2. * msec_per_buffer));
      num_buffers_in_history = 2;
    }
    //- allocate the history buffer
    if (this->cb_) 
    {
      delete this->cb_;
      this->cb_ = 0;
    }
    ASL_NEW(this->cb_, AICircularBuffer(this->config_.get_buffer_depth() * this->config_.num_active_channels(), 
                                        num_buffers_in_history));
    if (this->cb_ == 0) 
    {
      throw asl::DAQException("out of memory",
                              "could not allocate history buffer",
                              "ContinuousAI::configure");
    }
  } 

  //- store db event callback in configuration
  this->config_.set_db_event_callback(ContinuousAI::db_event_callback);

  //- store trigger event callback in configuration
  this->config_.set_trigger_event_callback(ContinuousAI::trig_event_callback);

  //- store ai-end event callback in configuration
  this->config_.set_ai_end_event_callback(ContinuousAI::ai_end_event_callback);

  //- adapt worker's message queue lo/hi water mark to DAQ buffer depth
  size_t wm = this->config_.get_buffer_depth() 
            * this->config_.get_active_channels().size()
            * sizeof(adl::AIDataType) 
            * kMAX_MESSAGES;

  //- apply limit 
  if (wm > kMAX_MSGQ_BYTES) 
  {
    wm = kMAX_MSGQ_BYTES;  
  }

  //- set worker's lo water mark
  this->worker_->low_water_mark(wm);

  //- set worker's hi water mark
  this->worker_->high_water_mark(wm);

  //- set worker's timeout
  this->worker_->timeout(this->config_.get_timeout());

  //- hardware configuration is done when daq is started

  //- std::cout << "ContinuousAI::configure_i ->" << std::endl;
}

// ============================================================================
// ContinuousAI::start
// ============================================================================
void ContinuousAI::start ()
  throw (asl::DAQException, asl::DeviceBusyException)
{
  //- std::cout << "ContinuousAI::start <-" << std::endl;

  TIMEOUT_GUARD(this->lock_, "ContinuousAI::start");

  this->action_allowed("ContinuousAI::start");

  if (this->state() == RUNNING)
    return;
  
  CHECK_GUARD_LOCKED("ContinuousAI::start");

  //- start without blocking nor changing internal state 
  this->start_i();
  
  //- change state to RUNNING
  this->state_ = RUNNING;

  //- std::cout << "ContinuousAI::start ->" << std::endl;
}

// ============================================================================
// ContinuousAI::start_i
// ============================================================================
void ContinuousAI::start_i ()
    throw (asl::DAQException)
{
  //- std::cout << "ContinuousAI::start_i <-" << std::endl;

  //- throw if not initialized
  if (this->initialized() == false) 
  {
    throw asl::DAQException("unitialized DAQ",
                            "ContinuousAI::init must be called before any other functions",
                            "ContinuousAI::start");
  }

  //- check the onfiguration
  this->config_.check();

  //- unfreeze and clear history
  if (this->cb_) 
  {
    this->cb_->unfreeze(); 
    this->cb_->clear(); 
  } 
  this->last_sample_index_in_history_buffer_ = 0;

  bool wait = true;
  if (this->work_->thread_executing_hook() == ACE_Thread::self()) 
  {
    //- start called from user hook: don't wait to avoid deadlock!
    wait = false;
  }

  //- start worker
  bool tmo_expired = false;
  Timeout tmo = ACE_OS::gettimeofday() + Timeout(0, kPOST_TIMEOUT);
  int err = this->worker_->start(wait, &tmo, &tmo_expired);
  if (err == -1)
  {
    if (tmo_expired) 
    {
      //- ignore timeout (worker just busy)
      err = 0;
    }
    else
    {
      this->state_ = FAULT;
      throw asl::DAQException("asl internal error",
                              "could not start deferred processing task",
                              "ContinuousAI::start");
    }
  }

  try 
  {
    //- configure hardware
    this->daq_hw_->configure_continuous_ai(this->config_);
    //- start daq
    this->daq_hw_->start_continuous_ai();
  } 
  catch (const DAQException&) 
  {
    //- abort worker
    this->worker_->abort(wait);
    throw;
  }
  catch (...) 
  {
    //- abort worker
    this->worker_->abort(wait);
    throw DAQException();
  }

#if defined(_SIMULATION_)

  //- start CallbackCaller
  ASL_NEW(this->cb_caller_, CallbackCaller);
  if (this->cb_caller_ == 0) 
  {
    //- abort worker
    this->worker_->abort(wait);
    throw asl::DAQException();
  }

// TIME BEFORE END CALLBACK CALL IN MILLISECONDS FOR SAFE MODE
#define kEND_TIME 3000
  
  // safe mode
  if (this->config_.retrigger_enabled() && 
      (this->config_.num_trigger_sequences() != 0))
  {
    if (this->config_.num_intermediate_trigger() == 0)
    {
      std::cout << "SIMULATED SAFE MODE started !!!" << std::endl;
      this->cb_caller_->start(ContinuousAI::ai_end_event_callback, kEND_TIME, 1);
    }
    else
    {
      size_t nb = this->config_.num_trigger_sequences() / this->config_.num_intermediate_trigger();
      std::cout << "SIMULATED SAFE INTERMEDIATE BUFFER MODE started for " << nb << " sequences!!" << std::endl;
      this->cb_caller_->start(ContinuousAI::ai_end_event_callback, kCB_PERIOD, nb);
    }
  }
  else
  {
    this->cb_caller_->start(ContinuousAI::db_event_callback, kCB_PERIOD);
  }

#endif // _SIMULATION_

  //- std::cout << "ContinuousAI::start_i ->" << std::endl;
}

// ============================================================================
// ContinuousAI::stop
// ============================================================================
void ContinuousAI::stop ()
  throw (asl::DAQException, asl::DeviceBusyException)
{
  //- std::cout << "ContinuousAI::stop <-" << std::endl;

  TIMEOUT_GUARD(this->lock_, "ContinuousAI::stop");

  this->action_allowed("ContinuousAI::stop");

  if (this->state() == STANDBY || this->state() == ABORTING)
    return;

  CHECK_GUARD_LOCKED("ContinuousAI::stop");

  try
  {
    //- change state to ABORTING
    this->state_ = ABORTING;

    //- stop without blocking nor changing internal state
    this->stop_i();

    //- change state to STANDBY
    this->state_ = STANDBY;
  }
  catch (...)
  {
    //- change state to FAULT
    this->state_ = FAULT;

    //- forward exception to caller
    throw;
  }

  //- std::cout << "ContinuousAI::stop ->" << std::endl;
}

// ============================================================================
// ContinuousAI::stop_i
// ============================================================================
void ContinuousAI::stop_i ()
    throw (asl::DAQException)
{
  std::cout << "ContinuousAI::stop_i <-" << std::endl;

  //- throw if not initialized
  if (this->initialized() == false) {
    throw asl::DAQException("unitialized DAQ",
                            "ContinuousAI::init must be called before any other functions",
                            "ContinuousAI::stop");
  }

  bool wait = true;
  if (this->work_->thread_executing_hook() == ACE_Thread::self()) 
  {
    //- abort called from user hook: don't wait to avoid deadlock!
    wait = false;
  }

  //- stop worker
  bool tmo_expired = false;
  Timeout tmo = ACE_OS::gettimeofday() + Timeout(0, kPOST_TIMEOUT);
  int err = this->worker_->stop(wait, &tmo, &tmo_expired);
  //- worker error will be handled after call to <daq_hw_->stop_continuous_ai> 
  //- because we must try to stop DAQ before leaving the place - here we just 
  //- ignore tmo errors
  if (err == -1 && tmo_expired)
  {
    //- ignore timeout (worker just busy)
    err = 0;
  }

  //- stop daq
  try 
  {
    this->daq_hw_->stop_continuous_ai();
    
    bool finite_retrig_mode  = this->config_.retrigger_enabled() 
                            && this->config_.num_trigger_sequences();

    bool pre_our_middle_trig_mode = this->config_.get_trigger_mode() == adl::ai_pre
                                 || this->config_.get_trigger_mode() == adl::ai_middle;

    if ( ! pre_our_middle_trig_mode && ! finite_retrig_mode ) 
#if defined (WIN32)
      this->ai_end_event_callback();
#else
      this->ai_end_event_callback(0);
#endif
  } 
  catch (const DAQException&) 
  {
    throw;
  }
  catch (...) 
  {
    throw DAQException();
  }

#if defined(_SIMULATION_)

  //- stop CallbackCaller
  if (this->cb_caller_) 
  {
    this->cb_caller_->stop();
    delete this->cb_caller_;
    this->cb_caller_ =0;
  }

#endif // _SIMULATION_

  //- handle worker error
  if (err == -1) 
  {
    //- throw daq error
    throw asl::DAQException("asl internal error",
                            "could not stop deferred processing task",
                            "ContinuousAI::stop");
  }

  std::cout << "ContinuousAI::stop_i ->" << std::endl;
}

// ============================================================================
// ContinuousAI::abort
// ============================================================================
void ContinuousAI::abort ()
  throw (asl::DAQException, asl::DeviceBusyException)
{
  //- std::cout << "ContinuousAI::abort <-" << std::endl;

  TIMEOUT_GUARD(this->lock_, "ContinuousAI::abort");

  this->action_allowed("ContinuousAI::abort");

  if (this->state() == STANDBY)
    return;

  CHECK_GUARD_LOCKED("ContinuousAI::abort");

  try
  {
    //- change state to ABORTING
    this->state_ = ABORTING;
    //- abort without blocking nor changing internal state
    this->abort_i();
    //- change state to STANDBY
    this->state_ = STANDBY;
  }
  catch (...)
  {
    //- change state to FAULT
    this->state_ = FAULT;
    //- forward exception to caller
    throw;
  }

  //- std::cout << "ContinuousAI::abort ->" << std::endl;
}

// ============================================================================
// ContinuousAI::abort_i
// ============================================================================
void ContinuousAI::abort_i ()
    throw (asl::DAQException)
{
  //- std::cout << "ContinuousAI::abort_i <-" << std::endl;

  //- throw if not initialized
  if (this->initialized() == false) 
  {
    throw asl::DAQException("unitialized DAQ",
                            "ContinuousAI::init must be called before any other functions",
                            "ContinuousAI::abort");
  }

  bool wait = true;
  if (this->work_->thread_executing_hook() == ACE_Thread::self()) 
  {
    //- abort called from user hook: don't wait to avoid deadlock!
    wait = false;
  }

  //- abort worker
  bool tmo_expired;
  Timeout tmo = ACE_OS::gettimeofday() + Timeout(0, kPOST_TIMEOUT);
  int err = this->worker_->abort(wait, &tmo, &tmo_expired);
  //- worker error will be handled after call to <daq_hw_->stop_continuous_ai> 
  //- because we must try to stop DAQ before leaving the place - here we just 
  //- ignore tmo errors
  if (err == -1 && tmo_expired)
  {
    //- ignore timeout (worker just busy)
    err = 0;
  }
  
  //- stop daq
  try 
  {
    this->daq_hw_->stop_continuous_ai();

    bool finite_retrig_mode  = this->config_.retrigger_enabled() 
                            && this->config_.num_trigger_sequences();

    bool pre_our_middle_trig_mode = this->config_.get_trigger_mode() == adl::ai_pre
                                 || this->config_.get_trigger_mode() == adl::ai_middle;

    if ( ! pre_our_middle_trig_mode && ! finite_retrig_mode ) 
#if defined (WIN32)
      this->ai_end_event_callback();
#else
      this->ai_end_event_callback(0);
#endif
  } 
  catch (const DAQException&) 
  {
    throw;
  }
  catch (...) 
  {
    throw DAQException();
  }

#if defined(_SIMULATION_)

  //- stop CallbackCaller
  if (this->cb_caller_) 
  {
    this->cb_caller_->stop();
    delete this->cb_caller_;
    this->cb_caller_ = 0;
  }

#endif // _SIMULATION_

  //- handle worker error
  if (err == -1) 
  {
    //- throw daq error
    throw asl::DAQException("asl internal error",
                            "could not abort deferred processing task",
                            "ContinuousAI::abort");
  }

  //- std::cout << "ContinuousAI::abort_i ->" << std::endl;
}

// ============================================================================
// ContinuousAI::handle_data_lost_exception
// ============================================================================
void ContinuousAI::handle_data_lost_exception ()
     throw (asl::DAQException)
{
  //- std::cout << "ContinuousAI::handle_data_lost_exception <-" << std::endl;

  //- lock 
  ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                    ace_mon, 
                    this->lock_, 
                    throw asl::DAQException());

  //- check exception status
  if (this->exception_status_ != HANDLING_OVERRUN) 
  {
    //- nothing to do here
    return;
  }

  //- apply overrun strategy
  try 
  {
    adl::DataLostStrategy dls = this->config_.get_data_lost_strategy();
    switch (dls)
    {
      case adl::abort:
        //- abort without locking nor changing internal state
        this->abort_i();
        //- change state
        this->state_ = STANDBY;
        break;
      case adl::restart:
        //- stop without locking nor changing internal state
        //- why stop instead of abort?
        //- if abort, any valid data store in the msg-q will be ignored
        this->stop_i();
        //- change state
        this->state_ = STANDBY;
        //- (re)start without locking nor changing internal state
        this->start_i();
        //- change state
        this->state_ = RUNNING;
        break;
      default:
        break;
    } 
  }
  catch (...) 
  {
    //- change state
    this->state_ = FAULT;
    //- we are running in the continuous DAQ handler's thread so...
    //- ignore exception
  }

  //- exception/evt handled
  this->exception_status_ = HANDLING_NONE;
  
  //- std::cout << "ContinuousAI::handle_data_lost_exception ->" << std::endl;
}

// ============================================================================
// ContinuousAI::handle_daq_exception
// ============================================================================
void ContinuousAI::handle_daq_exception ()
     throw (asl::DAQException)
{
  //- std::cout << "ContinuousAI::handle_daq_exception <-" << std::endl;

  ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());

  //- check exception status
  if (this->exception_status_ != HANDLING_ERROR) 
  {
    //- nothing to do here
    return;
  }

  try 
  {
    //- abort without locking nor changing internal state
    this->abort_i();
    //- change state
    this->state_ = STANDBY;
  }
  catch (...) 
  {
    //- change state
    this->state_ = FAULT;
    //- we are running in the continuous DAQ handler's thread so...
    //- ignore exception
  }

  //- exception/evt handled
  this->exception_status_ = HANDLING_NONE;

  //- std::cout << "ContinuousAI::handle_daq_exception ->" << std::endl;
}

// ============================================================================
// ContinuousAI::calibrate_hardware
// ============================================================================
void ContinuousAI::calibrate_hardware ()
  throw (asl::DAQException, asl::DeviceBusyException)
{
  //- std::cout << "ContinuousAI::calibrate_hardware <-" << std::endl;

  TIMEOUT_GUARD(this->lock_, "ContinuousAI::calibrate_hardware"); 

  this->action_allowed("ContinuousAI::calibrate_hardware");

  CHECK_GUARD_LOCKED("ContinuousAI::calibrate_hardware");

  this->exception_status_ = HANDLING_CALIBRATION;

#if defined (USE_ASYNC_CALIBRATION)

  this->hw_calibration_successfull_ = true;

  //- ask a ContinuousDAQHandler to  call <this::handle_hardware_calibration>.
  ContinuousDAQHandler * r;
  ASL_NEW(r, ContinuousDAQHandler);
  if (r == 0) 
  {
    throw asl::DAQException("out of memory",
                            "out of memory error",
                            "ContinuousAI::calibrate_hardware");
  }

  //- spawn external handler
  r->call_handle_hardware_calibration(this);

#else

  this->handle_hardware_calibration_i();

#endif

  //- std::cout << "ContinuousAI::calibrate_hardware ->" << std::endl;
}

#if defined (USE_ASYNC_CALIBRATION)
// ============================================================================
// ContinuousAI::handle_hardware_calibration
// ============================================================================
void ContinuousAI::handle_hardware_calibration ()
     throw (asl::DAQException)
{
  //- std::cout << "ContinuousAI::handle_hardware_calibration <-" << std::endl;

  //- lock 
  ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());

  this->handle_hardware_calibration_i();

  //- std::cout << "ContinuousAI::handle_hardware_calibration ->" << std::endl;
}
#endif

// ============================================================================
// ContinuousAI::handle_hardware_calibration_i
// ============================================================================
void ContinuousAI::handle_hardware_calibration_i ()
     throw (asl::DAQException)
{
  //- std::cout << "ContinuousAI::handle_hardware_calibration_i <-" << std::endl;

  if (this->exception_status_ != HANDLING_CALIBRATION)
    return;

  //- is DAQ running ?
  bool running = (this->state_ == RUNNING);
  
  try 
  {
    //- stop DAQ if running
    if (running) this->stop_i();
    //- change state
    this->state_ = STANDBY;
  }
  catch (const DAQException& de)
  {
    this->state_ = FAULT;
    this->exception_status_ = HANDLING_NONE;
    //- std::cout << "ContinuousAI::handle_hardware_calibration_i -> [ERROR:stop_i]" << std::endl;
#if defined (USE_ASYNC_CALIBRATION)
    this->hw_calibration_exception_ = de;
    this->hw_calibration_successfull_ = false;
    return;
#else
    throw de;
#endif
  }
  catch (...) 
  {
    this->state_ = FAULT;
    this->exception_status_ = HANDLING_NONE;
    DAQException unknown("DAQ stop failed",
                         "unknown exception caught while trying to stop DAQ for hardware calibration",
                         "ContinuousAI::handle_hardware_calibration_i");
    //- std::cout << "ContinuousAI::handle_hardware_calibration_i -> [ERROR:stop_i]" << std::endl;
#if defined (USE_ASYNC_CALIBRATION)
    this->hw_calibration_exception_ = unknown;
    this->hw_calibration_successfull_ = false;
    return;
#else
    throw unknown;
#endif
  }

  try 
  {
    //- calibrate hardware
    this->daq_hw_->auto_calibrate(false);
  }
  catch (const asl::DAQException& de) 
  {
    this->state_ = FAULT;
    this->exception_status_ = HANDLING_NONE;
    //- std::cout << "ContinuousAI::handle_hardware_calibration_i -> [ERROR:hw:auto_calibrate]" << std::endl;
#if defined (USE_ASYNC_CALIBRATION)
    this->hw_calibration_exception_ = de;
    this->hw_calibration_successfull_ = false;
#else
    throw de;
#endif
  }
  catch (...) 
  {
    this->state_ = FAULT;
    this->exception_status_ = HANDLING_NONE;
    DAQException unknown("hardware calibration failed",
                         "unknown exception caught while trying to calibrate the hardware",
                         "ContinuousAI::handle_hardware_calibration_i");
    //- std::cout << "ContinuousAI::handle_hardware_calibration_i -> [ERROR:hw:auto_calibrate]" << std::endl;
#if defined (USE_ASYNC_CALIBRATION)
    this->hw_calibration_exception_ = unknown;
    this->hw_calibration_successfull_ = false;
#else
    throw unknown;
#endif
  }

  try 
  {
    //- save calibration data into default bank
    this->daq_hw_->save_calibration(kDEFAULT_CALIBRATION_BANK);
  }
  catch (...) 
  {
    //- ignore error
  }

  try 
  {
    //- reconfigure the DAQ (release/register done is daq_hw_->auto_calibrate)
    this->configure_i(this->config_);
  }
  catch (const DAQException& de)
  {
    this->state_ = FAULT;
    this->exception_status_ = HANDLING_NONE;
    //- std::cout << "ContinuousAI::handle_hardware_calibration_i -> [ERROR:configure_i]" << std::endl;
#if defined (USE_ASYNC_CALIBRATION)
    this->hw_calibration_exception_ = de;
    this->hw_calibration_successfull_ = false;
    return;
#else
    throw;
#endif
  }
  catch (...) 
  {
    this->state_ = FAULT;
    this->exception_status_ = HANDLING_NONE;
    DAQException unknown("DAQ configuration failed",
                         "unknown exception caught while trying to reconfigure DAQ after hardware calibration",
                         "ContinuousAI::handle_hardware_calibration_i");
    //- std::cout << "ContinuousAI::handle_hardware_calibration_i -> [ERROR:configure_i]" << std::endl;
#if defined (USE_ASYNC_CALIBRATION)

    this->hw_calibration_exception_ = unknown;
    this->hw_calibration_successfull_ = false;
    return;
#else
    throw unknown;
#endif
  }

  try 
  {
    //- restart DAQ if running
    if (running) this->start_i();
    //- change state
    this->state_ = RUNNING;
  }
  catch (const DAQException& de)
  {
    this->state_ = FAULT;
    this->exception_status_ = HANDLING_NONE;
    //- std::cout << "ContinuousAI::handle_hardware_calibration_i -> [ERROR:start_i]" << std::endl;
#if defined (USE_ASYNC_CALIBRATION)
    this->hw_calibration_exception_ = de;
    this->hw_calibration_successfull_ = false;
    return;
#else
    throw;
#endif
  }
  catch (...) 
  {
    this->state_ = FAULT;
    this->exception_status_ = HANDLING_NONE;
    DAQException unknown("DAQ start failed",
                         "unknown exception caught while trying to restart DAQ after hardware calibration",
                         "ContinuousAI::handle_hardware_calibration_i");
    //- std::cout << "ContinuousAI::handle_hardware_calibration_i -> [ERROR:start_i]" << std::endl;
#if defined (USE_ASYNC_CALIBRATION)
    this->hw_calibration_exception_ = unknown;
    this->hw_calibration_successfull_ = false;
    return;
#else
    throw unknown;
#endif
  }

#if defined (USE_ASYNC_CALIBRATION)
  this->hw_calibration_successfull_ = true;
#endif

  this->exception_status_ = HANDLING_NONE;
    
  //- std::cout << "ContinuousAI::handle_hardware_calibration_i ->" << std::endl;
}

// ============================================================================
// ContinuousAI::db_event_callback
// ============================================================================
#if defined (WIN32)
  void ContinuousAI::db_event_callback ()
#else
  void ContinuousAI::db_event_callback (int sig_num)
#endif
{
#if defined(__linux)
  ACE_UNUSED_ARG(sig_num);
#endif

  //- std::cout << "ContinuousAI::db_event_callback <-" << std::endl;

  ContinuousAI::db_or_trig_event_callback(adl::db_evt);

  //- std::cout << "ContinuousAI::db_event_callback ->" << std::endl;
}

// ============================================================================
// ContinuousAI::trig_event_callback
// ============================================================================
#if defined (WIN32)
  void ContinuousAI::trig_event_callback ()
#else
  void ContinuousAI::trig_event_callback (int sig_num)
#endif
{
#if defined(__linux)
  ACE_UNUSED_ARG(sig_num);
#endif

  //- std::cout << "ContinuousAI::trig_event_callback <-" << std::endl;

  ContinuousAI::db_or_trig_event_callback(adl::trig_evt);

  //- std::cout << "ContinuousAI::trig_event_callback ->" << std::endl;
}

// ============================================================================
// ContinuousAI::db_or_trig_event_callback
// ============================================================================
void ContinuousAI::db_or_trig_event_callback (adl::Event _evt)
{
  ContinuousAI* instance = ContinuousAI::instance;

  //- std::cout << "ContinuousAI::db_or_trig_event_callback <-" << std::endl;

  //- IMPORTANT NOTE:
  //- DO NOT BLOCK TO ACQUIRE THE CONTINUOUS-DAQ LOCKING OBJECT IN THIS METHOD.
  //- POTENTIAL DEALOCK! THE STOP (OR ABORT) MEMBER ACQUIRE THE LOCK AND CALL 
  //- D2K_AI_ASYNCCLEAR. THE DRIVER THEN WAIT FOR THE CALLBACK TO COMPLETE
  //- BEFORE STOPPING THE ASYNC OPERATION. IF THIS CALLBACK IS WAITING FOR THE 
  //- LOCKING OBJECT TO BE FREE, THE PROCESS HANGS (DEADLOCK).

  //- try to acquire the DAQ <lock_>
  if (! instance || instance->lock_.tryacquire() == -1)
  {
    //- someone else hold the <lock_> or could not lock. in the first case: 
    //- means that the DAQ is either handling exception, calibrating the 
    //- hardware or handling an external request (such as stop or abort).
    //- in the second case (i.e. could not lock) this is an error we can't
    //- handle here because we are called by ADLink driver - in both cases 
    //- just we leave the place ...
    return;
  }

  //- check internal state: DAQ must be running whitout handling exception
  if (instance->state_ != RUNNING || instance->exception_status_ != HANDLING_NONE) 
  {
    //- release the DAQ <lock_>
    instance->lock_.release();
    return;
  }

  try 
  {
    //- get data from hardware
    AIRawData * data = 0;
    switch (_evt) 
    {
      case adl::trig_evt:
        //- rettrigger enabled
        data = instance->daq_hw_->ai_trigger_event_callback();
        break;
      case adl::db_evt:
        //- double buffering
        data = instance->daq_hw_->ai_db_event_callback();
        break;
      default:
        break;
    } 
    //- check result - a null pointer may be due to a pre or middle trig event
    //- or an buffer overrun with ignore strategy
    if (data == 0) 
    {
      //- release the DAQ <lock_>
      instance->lock_.release();
      //- just return 
      return;
    }
    //- push data into history buffer (i.e. the circular/post-mortem buffer)
    if (instance->config_.history_enabled() && instance->cb_) 
    { 
      try 
      {
        instance->cb_->push(data);
      }
      catch (...) 
      {
        //- ignore error  
      }
    } 
    //- instanciate data message
    Message * msg;
    ASL_NEW(msg, Message(data));
    if (msg == 0) 
    {
      delete data;
      //- the following exception will be catched locally (see below)
      throw asl::DAQException("out of memory",
                              "out of memory error",
                              "ContinuousAI::db_or_trig_event_callback");
    }
    //- send it to the consumer (i.e. Worker)
    Timeout tmo = ACE_OS::gettimeofday() + Timeout(0, kPOST_TIMEOUT);
    int err = instance->worker_->post(msg, &tmo);
    if (err == -1) 
    {
      msg->release();
      if (errno == EWOULDBLOCK) 
      {
        //- the following exception will be catched locally (see below)
        throw DataLostException(DataLostException::PROC_TASK_OVERLOAD);
      } 
      else 
      {
        //- the following exception will be catched locally (see below)
        throw DAQException("asl internal error",
                           "could not post data to deferred processing task",
                           "ContinuousAI::db_or_trig_event_callback");
      }
    }
  } 
  catch (const DataLostException& dle) 
  {
    if (instance->config_.history_enabled() && instance->cb_) 
    { 
      try 
      {
        instance->cb_->push(0);
      }
      catch (...) 
      {
        //- ignore error  
      }
    } 
    //- apply overrun strategy
    bool aborted_on_local_error = false;
    adl::DataLostStrategy dls = instance->config_.get_data_lost_strategy();
    switch (dls)
    {
      case adl::notify:
      case adl::abort:
      case adl::restart:
        {
          //- post <MSG_OVERRUN> : no timeout
          int msg_type = (dle.why() == DataLostException::PROC_TASK_OVERLOAD) 
                            ?
                          MessageType::MSG_OVERLOAD
                            :
                          MessageType::MSG_OVERRUN;
          Message * msg;
          ASL_NEW(msg, Message(msg_type, true));
          if (msg == 0) 
          {
            aborted_on_local_error = true;
            instance->handle_daq_exception_i(DAQException("out of memory",
                                                          "out of memory error",
                                                          "ContinuousAI::db_or_trig_event_callback"));
          }
          int err = instance->worker_->post(msg);
          if (err == -1) 
          {
            msg->release();
            aborted_on_local_error = true;
            instance->handle_daq_exception_i(DAQException("asl internal error",
                                                          "could not post OVERRUN message to deferred processing task",
                                                          "ContinuousAI::db_or_trig_event_callback"));
          }
          if (! aborted_on_local_error && dls != adl::notify) 
          {
            instance->handle_data_lost_exception_i();
          }
        }
        break;
      case adl::trash:
      default:
        break;
    }
  }
  catch (const DAQException& ex) 
  {
    //- daq error
    instance->handle_daq_exception_i(ex);
  }
  catch (...) 
  {
    //- unknown error
    instance->handle_daq_exception_i(DAQException());
  }

  //- release the DAQ <lock_>
  instance->lock_.release();

  //- std::cout << "ContinuousAI::db_or_trig_event_callback ->" << std::endl;
}

// ============================================================================
// ContinuousAI::ai_end_event_callback
// ============================================================================
#if defined (WIN32)
  void ContinuousAI::ai_end_event_callback ()
#else
  void ContinuousAI::ai_end_event_callback (int sig_num)
#endif
{
#if defined(__linux)
  ACE_UNUSED_ARG(sig_num);
#endif

  ContinuousAI* instance = ContinuousAI::instance;

  //- std::cout << "ContinuousAI::ai_end_event_callback <-" << std::endl;

  //- IMPORTANT NOTE:
  //- DO NOT BLOCK TO ACQUIRE THE CONTINUOUS-DAQ LOCKING OBJECT IN THIS METHOD.
  //- POTENTIAL DEALOCK! THE STOP (OR ABORT) MEMBER ACQUIRE THE LOCK AND CALL 
  //- D2K_AI_ASYNCCLEAR. THE DRIVER THEN WAIT FOR THE CALLBACK TO COMPLETE
  //- BEFORE STOPPING THE ASYNC OPERATION. IF THIS CALLBACK IS WAITING FOR THE 
  //- LOCKING OBJECT TO BE FREE, THE PROCESS HANGS (DEADLOCK).

  //- try to acquire the DAQ <lock_>
  if (! instance || instance->lock_.tryacquire() == -1)
  {
    //- someone else hold the <lock_> or could not lock. in the first case: 
    //- means that the DAQ is either handling exception, calibrating the 
    //- hardware or handling an external request (such as stop or abort).
    //- in the second case (i.e. could not lock) this is an error we can't
    //- handle here because we are called by ADLink driver - in both cases 
    //- just we leave the place ...
    return;
  }

  //- find out why the acquisition stopped
  DaqEndReason why = instance->daq_end_reason();

  //- to not notify user in the following cases:
  //- daq end due to calibration request
  //- daq end due to buffer overrun and overrun strategy is set to restart 
  if (
      (why == DAQEND_ON_CALIB_REQUEST)
        || 
      (why == DAQEND_ON_OVERRUN && instance->config_.get_data_lost_strategy() == adl::restart) 
     )
  {
    //- release the DAQ <lock_>
    instance->lock_.release();
    return;
  }

  //- instanciate the DATA msg
  Message * msg = 0;

  //- in case daq end is due to ext.trigger...
  //- or user request & recovery of last data buffer enabled
  if ( (why == DAQEND_ON_EXTERNAL_TRIGGER) ||
    ((why == DAQEND_ON_USER_REQUEST) && (instance->config_.last_buffer_recovery_enabled())) )
  {    
    try
    {
      //- ...then get remaining samples and...
      asl::AIRawData * data = instance->remaining_samples();
      if (data)
      {
        //- ...push it into history buffer (i.e. the circular buffer)
        if (instance->config_.history_enabled() && instance->cb_) 
        { 
          try 
          {
            instance->cb_->push(data);
            instance->cb_->freeze();
            unsigned long idx = ( instance->cb_->factor() - 1 ) 
                              * instance->cb_->modulo()
                              + instance->last_sample_index_in_last_daq_buffer();
            instance->last_sample_index_in_history_buffer(idx);
          }
          catch (...) 
          {
            //- can't do anything here: we are called by ADLink driver
            //- release the DAQ <lock_>
            instance->lock_.release();
            return;
          }
        } 
        ASL_NEW(msg, Message(data));
        if (msg == 0) 
        {
          //- can't do anything here: we are called by ADLink driver
          delete data;
          //- release the DAQ <lock_>
          instance->lock_.release();
          return;
        }
        //- send it to the consumer (i.e. Worker) : no timeout 
        int err = instance->worker_->post(msg);
        if (err == -1) 
        {
          //- can't do anything here: we are called by ADLink driver
          msg->release();
          //- release the DAQ <lock_>
          instance->lock_.release();
          return;
        }
      }
    }
    catch (...)
    {
      //- can't do anything here: we are called by ADLink driver
      //- release the DAQ <lock_>
      instance->lock_.release();
      return;
    }
  }
  else if (why == DAQEND_ON_FINITE_RETRIGGER_SEQUENCE)
  {    
    try
    {
      asl::AIRawData * buffer_0 = 0; 
      asl::AIRawData * buffer_1 = 0;
      instance->daq_buffers(buffer_0, buffer_1);
      if ( buffer_0 )
      {
        std::cout << "ContinuousAI::ai_end_event_callback::posting buffer_0" << std::endl;
        //- send it to the consumer (i.e. Worker) : no timeout 
        ASL_NEW(msg, Message(buffer_0));
        int err = instance->worker_->post(msg);
        if (err == -1) 
        {
          //- can't do anything here: we are called by ADLink driver
          msg->release();
          //- release the DAQ <lock_>
          instance->lock_.release();
          return;
        }
      }
    }
    catch (...)
    {
      //- can't do anything here: we are called by ADLink driver
      //- release the DAQ <lock_>
      instance->lock_.release();
      return;
    }
  }

  //- encapsulate <why> into an asl::Container
  Container<ContinuousDAQ::DaqEndReason>* msg_data;
  ASL_NEW(msg_data, Container<ContinuousDAQ::DaqEndReason>(why));
  if (msg_data == 0) 
  {
    //- can't do anything here: we are called by ADLink driver
    //- release the DAQ <lock_>
    instance->lock_.release();
    return;
  }
  //- instanciate <MessageType::MSG_DAQEND> : no timeout 
  ASL_NEW(msg, Message(msg_data, MessageType::MSG_DAQEND, true));
  if (msg == 0) 
  {
    //- can't do anything here: we are called by ADLink driver
    delete msg_data;
    //- release the DAQ <lock_>
    instance->lock_.release();
    return;
  }
  //- send it to the consumer (i.e. Worker)
  int err = instance->worker_->post(msg);
  if (err == -1) 
  {
    //- can't do anything here: we are called by ADLink driver
    msg->release();
    //- release the DAQ <lock_>
    instance->lock_.release();
    return;
  }

  //- release the DAQ <lock_>
  instance->lock_.release();

  //- std::cout << "ContinuousAI::ai_end_event_callback ->" << std::endl;
}
  
// ============================================================================
// ContinuousAI::daq_end_reason
// ============================================================================
ContinuousDAQ::DaqEndReason ContinuousAI::daq_end_reason ()
{
  //- find out why the acquisition stopped
  ContinuousDAQ::DaqEndReason why = DAQEND_ON_UNKNOWN_EVT;

  //- std::cout << "ContinuousAI::ai_end_event_callback::state::" << this->state_ << std::endl;
  //- std::cout << "ContinuousAI::ai_end_event_callback::exception_status::" << this->exception_status_ << std::endl;
  
  const adl::AITriggerMode tgm = this->config_.get_trigger_mode();

  switch (this->exception_status_)
  {
    case HANDLING_NONE:
      if (this->state_ == ABORTING || this->state_ == STANDBY) 
      {
        why = DAQEND_ON_USER_REQUEST;
        std::cout << "ContinuousAI::daq_end_reason::DAQ stopped on user request" << std::endl;
      }
      else if (tgm == adl::ai_pre || tgm == adl::ai_middle) 
      {
        why = DAQEND_ON_EXTERNAL_TRIGGER;
        std::cout << "ContinuousAI::daq_end_reason::DAQ stopped on external trigger" << std::endl;
      }
      else if (this->config_.retrigger_enabled() && this->config_.num_trigger_sequences())
      {
        why = DAQEND_ON_FINITE_RETRIGGER_SEQUENCE;
        std::cout << "ContinuousAI::daq_end_reason::DAQ stopped on finite retrigger sequence" << std::endl;
      }
      break;
    case HANDLING_ERROR:
      why = DAQEND_ON_ERROR;
      std::cout << "ContinuousAI::daq_end_reason::DAQ stopped on error" << std::endl;
      break;
    case HANDLING_OVERRUN:
      why = DAQEND_ON_OVERRUN;
      std::cout << "ContinuousAI::daq_end_reason::DAQ stopped on overrun (ignored if strategy==restart)" << std::endl;
      break;
    case HANDLING_CALIBRATION:
      why = DAQEND_ON_CALIB_REQUEST;
      std::cout << "ContinuousAI::daq_end_reason::DAQ stopped on calibration request (ignored)" << std::endl;
      break;
    default:
      why = DAQEND_ON_UNKNOWN_EVT;
      std::cout << "ContinuousAI::daq_end_reason::DAQ stopped on unknwon event" << std::endl;
      break;
  }
  
  return why;
}

// ============================================================================
// ContinuousAI::handle_daq_exception_i
// ============================================================================
void ContinuousAI::handle_daq_exception_i (const DAQException& _ex)
{
  //- std::cout << "ContinuousAI::handle_daq_exception_i <-" << std::endl;

  //- tell the world we are handling an error
  //- note: we have to change state before posting the error message
  this->state_ = ABORTING;
  this->exception_status_ = HANDLING_ERROR;

  do 
  {
    //- encapsulate the exception into a asl::Container
    Container<const DAQException>* msg_data;
    ASL_NEW(msg_data, Container<const DAQException>(_ex));  
    if (msg_data == 0) 
    {
      //- too many errors 
      break;
    }
    //- post <MSG_FATAL_ERROR> : no timeout 
    Message * msg;
    ASL_NEW(msg, Message(msg_data, MessageType::MSG_FATAL_ERROR, true));
    if (msg == 0) 
    {
      //- too many errors 
      delete msg_data;
      break;
    }
    //- post the error message
    if (this->worker_->post(msg) == -1) 
    {
      //- too many errors
      msg->release();
      break;
    }
  } 
  while (0);

  //- std::cout << "ContinuousAI::handle_daq_exception_i::spawn DAQ handler <-" << std::endl;

  //- ask a ContinuousDAQHandler to  call <abort>.
  ContinuousDAQHandler * r;
  ASL_NEW(r, ContinuousDAQHandler); 
  if (r) 
  {
    //- spawn external handler
    r->call_handle_daq_exception(this);
  }
  else 
  {
    //- can't handle such error, panic...
    this->state_ = FAULT;
    this->exception_status_ = HANDLING_NONE;
  }

  //- std::cout << "ContinuousAI::handle_daq_exception_i::spawn DAQ handler ->" << std::endl;

  //- std::cout << "ContinuousAI::handle_daq_exception_i ->" << std::endl;
}

// ============================================================================
// ContinuousAI::handle_data_lost_exception_i
// ============================================================================
void ContinuousAI::handle_data_lost_exception_i ()
{
  //- std::cout << "ContinuousAI::handle_data_lost_exception_i <-" << std::endl;

  //- tell the world we are handling an error  
  //- note: change state and/or exception_status before posting the error message
  this->exception_status_ = HANDLING_OVERRUN;
  if (this->config_.get_data_lost_strategy() == adl::abort)
  {
    this->state_ = ABORTING;
  }
  
  //- ask a ContinuousDAQHandler to  call <this::handle_data_lost>.
  ContinuousDAQHandler * r;
  ASL_NEW(r, ContinuousDAQHandler); 
  //- std::cout << "ContinuousAI::handle_data_lost_exception_i::spawn DAQ handler <-" << std::endl;
  if (r) 
  {
    //- spawn external handler
    r->call_handle_data_lost_exception(this);
  }
  else 
  {
    //- can't handle such error, panic...
    this->state_ = FAULT;
    this->exception_status_ = HANDLING_NONE;
  }

  //- std::cout << "ContinuousAI::handle_data_lost_exception_i::spawn DAQ handler ->" << std::endl;

  //- std::cout << "ContinuousAI::handle_data_lost_exception_i ->" << std::endl;
}

// ============================================================================
// ContinuousAI::clear_history
// ============================================================================
void ContinuousAI::clear_history ()
{
  if (this->cb_) this->cb_->clear();
}

// ============================================================================
// ContinuousAI::freeze_history
// ============================================================================
void ContinuousAI::freeze_history ()
{
  if (this->cb_) this->cb_->freeze();
}

// ============================================================================
// ContinuousAI::scale_data
// ============================================================================
AIScaledData * ContinuousAI::scale_data (AIRawData * raw_data) const 
  throw (asl::DAQException)
{
  if ( ! this->initialized() )
    throw asl::DAQException();

  if ( ! raw_data ) {
    throw asl::DAQException("invalid input argument",
                            "unexpected null pointer to raw data",
                            "ContinuousAI::scale_data");
  }

  return this->daq_hw_->scale_data(raw_data);
}

// ============================================================================
// ContinuousAI::scale_data
// ============================================================================
void ContinuousAI::scale_data (AIRawData * raw_data, AIScaledData *& scaled_data)
  throw (asl::DAQException)
{
  if ( ! this->initialized() )
    throw asl::DAQException();

  this->daq_hw_->scale_data(raw_data, scaled_data);
}

// ============================================================================
// ContinuousAI::convert_to_user_data
// ============================================================================
AIScaledData * ContinuousAI::convert_to_user_data (AIScaledData * scaled_data) const
  throw (asl::DAQException)
{
  if ( ! this->initialized() )
    throw asl::DAQException();

  if ( ! scaled_data ) {
    throw asl::DAQException("invalid input argument",
                            "unexpected null pointer to scaled data",
                            "ContinuousAI::convert_to_user_data");
  }

  asl::AIScaledData * user_data = new asl::AIScaledData(*scaled_data);
  if ( ! user_data ) {
    throw asl::DAQException("data buffer allocation failed",
                            "out of memory error",
                            "ContinuousAI::convert_to_user_data");
  }

  this->scaled_to_user_data(user_data);
    
  return user_data;
}

// ============================================================================
// ContinuousAI::convert_to_user_data
// ============================================================================
void ContinuousAI::convert_to_user_data (AIScaledData * scaled_data, AIScaledData *& user_data) 
 throw (asl::DAQException)
{
  if ( ! this->initialized() )
    throw asl::DAQException();

  if ( ! scaled_data ) {
    throw asl::DAQException("invalid input argument",
                            "invalid (null) pointer to scaled data)",
                            "AContinuousAI::convert_to_user_data");
  }

  if ( ! user_data )
  {
    user_data = this->convert_to_user_data(scaled_data);
  }
  else if ( user_data->depth() != scaled_data->depth() )
  {
    delete user_data;
    user_data = this->convert_to_user_data(scaled_data);
  }
  else
  {
    *user_data = *scaled_data;
    this->scaled_to_user_data(user_data);
  }
}

// ============================================================================
// ContinuousAI::convert_to_user_data
// ============================================================================
void ContinuousAI::apply_user_data_mask (AIScaledData * scaled_or_user_data) const
  throw (asl::DAQException)
{
  this->apply_data_mask(scaled_or_user_data);
}

// ============================================================================
// ContinuousAI::apply_user_data_mask
// ============================================================================
void ContinuousAI::apply_user_data_mask (AIScaledData * scaled_or_user_data, AIScaledData *& masked_data) 
 throw (asl::DAQException)
{
  if ( ! this->initialized() )
    throw asl::DAQException();

  if ( ! scaled_or_user_data ) {
    throw asl::DAQException("invalid input argument",
                            "invalid (null) pointer to scaled or user data)",
                            "AContinuousAI::apply_user_data_mask");
  }

  if ( ! masked_data || masked_data->depth() != scaled_or_user_data->depth() )
  {
    delete masked_data;
    masked_data = new asl::AIScaledData(*scaled_or_user_data);
    if ( ! masked_data ) {
      throw asl::DAQException("data buffer allocation failed",
                              "out of memory error",
                              "ContinuousAI::apply_user_data_mask");
    }
  }
    
  *masked_data = *scaled_or_user_data;

  this->apply_data_mask(masked_data);
}

// ============================================================================
// ContinuousAI::scaled_history
// ============================================================================
AIScaledData * ContinuousAI::scaled_history () const
          throw (asl::DAQException)
{
  //- be sure history is enabled 
  if (this->config_.history_enabled() == false || this->cb_ == 0) 
  {
    throw DAQException("no data available",
                       "history buffer is disabled - check your DAQ configuration",
                       "ContinuousAI::scaled_history");
  } 

  //- get raw history
  AIRawData * raw_history = this->cb_->ordered_data();

  //- scale history data 
  AIScaledData * scaled_history = 0;
  try 
  {
    scaled_history = this->daq_hw_->scale_data(raw_history);
    delete raw_history;
  }
  catch (...) 
  {
    delete raw_history;
    throw; 
  }

  //- (if necessary...) convert scaled to user data and apply the data mask 
  if ( this->config_.user_data_enabled() ||  this->config_.data_mask_enabled() )
  {
    unsigned long daq_buffer_size = this->cb_->modulo();
    unsigned long num_daq_buffers = this->cb_->factor();
    double * p = scaled_history->base();

    for ( size_t i = 0; i < num_daq_buffers; i++ )
    {
      if ( this->config_.user_data_enabled() ) // if user data enabled for at least one channel
        this->scaled_to_user_data(p + i * daq_buffer_size, daq_buffer_size);  

      if ( this->config_.data_mask_enabled() )
        this->apply_data_mask(p + i * daq_buffer_size, daq_buffer_size);  
    }
  }

  return scaled_history; 
}

// ============================================================================
// ContinuousAI::raw_history
// ============================================================================
AIRawData * ContinuousAI::raw_history () const
          throw (asl::DAQException)
{
  //- be sure history is enabled 
  if (this->config_.history_enabled() == false || this->cb_ == 0) 
  {
    throw DAQException("no data available",
                       "history buffer is disabled",
                       "ContinuousAI::raw_history");
  } 

  //- raw data history
  AIRawData * rd = static_cast<AIRawData *>(this->cb_->ordered_data());

  //- apply the data mask if necessary...
  if ( this->config_.data_mask_enabled() )
  {
    unsigned long daq_buffer_size = this->cb_->modulo();
    unsigned long num_daq_buffers = this->cb_->factor();
    unsigned short * p = rd->base();
    for ( size_t i = 0; i < num_daq_buffers; i++ )
      this->apply_data_mask(p + i * daq_buffer_size, daq_buffer_size);  
  }

  return rd;
}


// ============================================================================
// ContinuousAI::remaining_samples
// ============================================================================
AIRawData * ContinuousAI::remaining_samples ()
          throw (asl::DAQException)
{
  if ( ! this->initialized() )
    throw asl::DAQException();

  return this->daq_hw_->remaining_samples();
}

// ============================================================================
// ContinuousAI::daq_buffers
// ============================================================================
void ContinuousAI::daq_buffers (asl::AIRawData*& buffer_0, asl::AIRawData*& buffer_1)
          throw (asl::DAQException)
{
  if ( ! this->initialized() )
    throw asl::DAQException();

  this->daq_hw_->daq_buffers(buffer_0, buffer_1);
}

// ============================================================================
// ContinuousAI::dump
// ============================================================================
void ContinuousAI::dump () const
{
  ACE_GUARD(ACE_Thread_Mutex, ace_mon, 
            ACE_const_cast(ACE_Thread_Mutex&, this->lock_));

  ACE_DEBUG ((LM_DEBUG, "----------------------------\n"));
  ACE_DEBUG ((LM_DEBUG, "-- ContinuousAI             \n"));
  ACE_DEBUG ((LM_DEBUG, "----------------------------\n"));

  //- return if not initialized
  if (! this->initialized()) 
  {
    ACE_DEBUG ((LM_DEBUG, ". not initialized\n"));
    return; 
  }

  switch (this->state()) 
  {
    case STANDBY:
      ACE_DEBUG ((LM_DEBUG, ". state: STANDBY\n"));
      break;
    case RUNNING:
      ACE_DEBUG ((LM_DEBUG, ". state: RUNNING\n"));
      break;
    case ABORTING:
      ACE_DEBUG ((LM_DEBUG, ". state: ABORTING\n"));
      break;
    case FAULT:
      ACE_DEBUG ((LM_DEBUG, ". state: FAULT\n"));
      break;
    default:
      ACE_DEBUG ((LM_DEBUG, ". state: UNKNOWN\n"));
      break;
  }

  ACE_DEBUG ((LM_DEBUG, "-- DAQ Hardware: \n"));

  this->daq_hw_->dump(); 

  ACE_DEBUG ((LM_DEBUG, "-- Async.Task:\n"));

  this->worker_->dump();

  ACE_DEBUG ((LM_DEBUG, "----------------------------\n"));
}

// ============================================================================
// ContinuousAI::apply_data_mask
// ============================================================================
void ContinuousAI::apply_data_mask (asl::AIRawData * raw_data) const
  throw (asl::DAQException)
{
  //- be sure input is valid
  if ( ! raw_data )
    return;

  //- do the job...
  this->apply_data_mask(raw_data->base(), raw_data->depth());
}

// ============================================================================
// ContinuousAI::apply_data_mask
// ============================================================================
void ContinuousAI::apply_data_mask (unsigned short * p, unsigned long s) const
  throw (asl::DAQException)
{
  //- be sure input is valid
  if ( ! p || ! s )
    return;

  //- be sure the data mask is valid
  asl::DataMask * dm = this->config_.data_mask_;
  if ( ! dm ) 
  {
    throw DAQException("invalid data mask",
                       "the user defined data mask is invalid (hint: DAQ and mask buffers must have the same size)",
                       "ContinuousAI::apply_data_mask");
  } 

  //- do the job
  size_t nbac = this->config_.num_active_channels();
  for ( size_t i = 0, j = 0 ; i < s; i++ )
  {
    if ( (i >= nbac) && ( ! ( i % nbac ) ) )
      j++;

    *(p + i) *= ( (*dm)[j] != 0. )
              ? static_cast<unsigned short>(1) 
              : static_cast<unsigned short>(0);
  }
}

// ============================================================================
// ContinuousAI::apply_data_mask
// ============================================================================
void ContinuousAI::apply_data_mask (asl::AIScaledData * data) const
  throw (asl::DAQException)
{
  //- be sure input is valid
  if ( ! data )
    return;

  //- do the job...
  this->apply_data_mask(data->base(), data->depth());
}

// ============================================================================
// ContinuousAI::apply_data_mask
// ============================================================================
void ContinuousAI::apply_data_mask (double * p, unsigned long s) const
  throw (asl::DAQException)
{
  //- be sure input is valid
  if ( ! p || ! s )
    return;

  //- be sure the data mask is valid
  asl::DataMask * dm = this->config_.data_mask_;
  if ( ! dm ) 
  {
    throw DAQException("invalid data mask",
                       "the user defined data mask is invalid (hint: DAQ and mask buffers must have the same size)",
                       "ContinuousAI::apply_data_mask");
  } 

  //- do the job...
  size_t nbac = this->config_.num_active_channels();
  for ( size_t i = 0, j = 0; i < s; i++ )
  {
    if ( (i >= nbac) && ( ! ( i % nbac ) ) )
      j++;

    *(p + i) *= *(dm->base() + j);
  }
}

// ============================================================================
// ContinuousAI::scaled_to_user_data
// ============================================================================
void ContinuousAI::scaled_to_user_data (asl::AIScaledData * data) const
  throw (asl::DAQException)
{
  //- be sure input is valid
  if ( ! data )
    return;

  //- do the job...
  this->scaled_to_user_data(data->base(), data->depth());
}

// ============================================================================
// ContinuousAI::scaled_to_user_data
// ============================================================================
void ContinuousAI::scaled_to_user_data (double * p, unsigned long s) const
  throw (asl::DAQException)
{
  //- be sure input is valid
  if ( ! p || ! s )
    return;

  // apply user data specific formula on each active channel (if enabled)
  const asl::ActiveAIChannels& ac = this->config_.get_active_channels();
  size_t nbac = this->config_.num_active_channels();

  for ( size_t i = 0, j = 0; i < s; i++ )
  {
    if (i >= nbac)
      j = i % nbac;
    else
      j = i;

    if (ac[j].user_data_enabled)
    {
      double g, o1, o2;
      this->config_.get_user_data_parameters(ac[j].id, g, o1, o2);

      *(p + i) = ( ( *(p + i) - o1 ) /  g ) - o2;
    }
  }

}

} // namespace asl


