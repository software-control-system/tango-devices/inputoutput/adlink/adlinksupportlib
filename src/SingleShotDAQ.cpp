// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    SingleShotDAQ.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/SingleShotDAQ.h>

#if !defined (__ASL_INLINE__)
# include <asl/SingleShotDAQ.i>
#endif // __ASL_INLINE__

namespace asl {

// ============================================================================
// STATICs
// ============================================================================
#if defined(ASL_DEBUG)
ASL_EXPORT AtomicOp SingleShotDAQ::instance_counter_ = 0;
#endif

// ============================================================================
// SingleShotDAQ::SingleShotDAQ
// ============================================================================
SingleShotDAQ::SingleShotDAQ ()
{
#if defined(ASL_DEBUG)
  //- increment instance counter
  ++SingleShotDAQ::instance_counter_;
#endif  
}

// ============================================================================
// SingleShotDAQ::~SingleShotDAQ
// ============================================================================
SingleShotDAQ::~SingleShotDAQ ()
{
#if defined(ASL_DEBUG)
  //- decrement instance counter
  --SingleShotDAQ::instance_counter_;
#endif  
}

} // namespace asl


