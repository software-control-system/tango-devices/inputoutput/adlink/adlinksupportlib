// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    DataExtractor_T.cpp
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

#ifndef _DATA_EXTRACTOR_T_CPP_
#define _DATA_EXTRACTOR_T_CPP_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/Message.h>
#include <asl/Extractor_T.h>

#if !defined (__ASL_INLINE__)
# include "asl/Extractor_T.i"
#endif // __ASL_INLINE__

namespace asl {

// ============================================================================
// DataExtractor::DataExtractor
// ============================================================================
template <class T> 
DataExtractor<T>::DataExtractor()
{
  //- noop ctor
}

// ============================================================================
// DataExtractor::~DataExtractor
// ============================================================================
template <class T> 
DataExtractor<T>::~DataExtractor()
{
  //- noop dtor
}

} // namespace asl

#endif // _DATA_EXTRACTOR_T_CPP_


