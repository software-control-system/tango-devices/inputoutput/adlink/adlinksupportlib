// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    AD2010.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/adlink/AD2010.h>

namespace adl { 

// ============================================================================
// STATICS
// ============================================================================
AD2010::Repository AD2010::repository;
ACE_Recursive_Thread_Mutex AD2010::repository_lock;

// ============================================================================
// AD2010::instanciate
// ============================================================================
AD2010* AD2010::instanciate (unsigned short _id, bool _exclusive_access)
  throw (asl::DAQException)
{
  //- lock
  ACE_GUARD_REACTION(ACE_Recursive_Thread_Mutex, 
                     ace_mon, 
                     AD2010::repository_lock, 
                     throw asl::DAQException());

  //- search requested board in the repository
  RepositoryIterator it = AD2010::repository.find(_id);

  //- already instnciated?
  if (it != AD2010::repository.end()) {
    //- can't get ownership if _exclusive_access requested
    if (_exclusive_access || it->second->exclusive_access_) {
      //- can't get exclusive access to hardware
      throw asl::DAQException("hardware already in use",
                              "could not get exclusive access to the hardware",
                              "AD2010::instanciate");
    }
    //- share the board
    else {
      return it->second->duplicate();
    }
  }

  //- instanciate new board
  AD2010* b = new AD2010(_id);
  if (b == 0) {
    //- out of memory
    throw asl::DAQException("out of memory",
                            "out of memory error",
                            "AD2010::instanciate");
  }

  //- update board's <exclusive_access> flag
  b->exclusive_access_ = _exclusive_access;

  int err = b->register_hardware();
  if (err < 0) {
    //- release board
    b->release();
    //- reset returned pointer
    b = 0;
    //- hardware registration failed
    throw asl::DAQException("hardware registration failed",
                            adl::d2kdask_error_text(err),
                            "AD2010::instanciate",
                            err);
  }

  return b;
}

// ============================================================================
// AD2010::duplicate
// ============================================================================
AD2010 * AD2010::duplicate ()
{
  return dynamic_cast<AD2010*>(this->SharedObject::duplicate()); 
}
 
// ============================================================================
// AD2010::AD2010
// ============================================================================
AD2010::AD2010(unsigned short _id) 
  : ADBoard(adl::DAQ2010, _id),
    ADCalibrableAIOBoard(adl::DAQ2010, _id),
    ADSingleShotAIBoard(adl::DAQ2010, _id, AD2010_NUM_CHANNELS),
    ADSingleShotAOBoard(adl::DAQ2010, _id, AD2010_NUM_CHANNELS),
    ADSingleShotDIBoard(adl::DAQ2010, _id),
    ADSingleShotDOBoard(adl::DAQ2010, _id),
    ADContinuousAIBoard(adl::DAQ2010, _id, AD2010_NUM_CHANNELS)
{
  //- store board in the repository
  RepositoryValue pair(_id, this);
  AD2010::repository.insert(pair);
   //- initialize dio ports 
  for (int i = adl::port_1a; i <= adl::port_1ch; i++) {
    DIOPortDirValue pair((adl::DIOPort)i, adl::unknown);
    this->dio_port_dir_.insert(pair);
  }
}

// ============================================================================
// AD2010::~AD2010
// ============================================================================
AD2010::~AD2010()
{
  //- lock
  ACE_GUARD_REACTION(ACE_Recursive_Thread_Mutex, 
                     ace_mon, 
                     AD2010::repository_lock, 
                     throw asl::DAQException());
										 
  //- release hardware
  this->release_hardware();

  //- remove board from repository
  RepositoryIterator it = AD2010::repository.find(this->id_);
  if (it != AD2010::repository.end()) {
    AD2010::repository.erase(it);    
  }
}


// ============================================================================
// AD2010::register_hardware
// ============================================================================
int AD2010::register_hardware ()
{
#if !defined(_SIMULATION_) 

  int bid = ::D2K_Register_Card(DAQ_2010, this->id_);
  if (bid < 0) {
	  this->idid_ = kIMPOSSIBLE_IID;
		return bid;
	}
  this->idid_ = bid;
  return 0;

#else // _SIMULATION_
 
  return this->id_;

#endif // _SIMULATION_  
}

// ============================================================================
// AD2010::release_hardware
// ============================================================================
int AD2010::release_hardware ()
{
  //- be sure the board is registered
  if (this->idid_ == kIMPOSSIBLE_IID) {
    return 0;
  }

#if !defined(_SIMULATION_) 

  return ::D2K_Release_Card(this->idid_);

#else // _SIMULATION_

  return 0;
  
#endif // _SIMULATION_ 
} 

// ============================================================================
// AD2010::clock_frequency
// ============================================================================
double AD2010::clock_frequency () const
{
  return (double)AD2010_CLOCK_FREQ;
}

// ============================================================================
// AD2010::nsec_per_clock_tick
// ============================================================================
double AD2010::nsec_per_clock_tick () const
{
  return (double)AD2010_NSEC_TICK;
}
// ============================================================================
// AD2010::read_channel
// ============================================================================
unsigned short AD2010::read_channel (adl::ChanId _chan_id)
  throw (asl::DAQException)
{
  unsigned short value = 0;

#if !defined(_SIMULATION_) 

 /* ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());*/



  int err = ::D2K_AI_ReadChannel(this->idid_, _chan_id, &value);

  if (err < 0) 
  {
    throw asl::DAQException("could not read AI channel",
                            adl::d2kdask_error_text(err),
                            "ADSingleShotAIBoard::read_channel",
                            err);
  }
  
  value = value>>2;
#endif

  return value;
}

// ============================================================================
// AD2010::read_raw_channels
// ============================================================================
asl::AIRawData* AD2010::read_raw_channels (adl::ChanId _max_chan_id)
  throw (asl::DAQException)
{
  asl::AIRawData* values = 0;

#if !defined(_SIMULATION_) 

  ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());

  //- build ::D2K_AI_SimuReadChannel argument
  asl::Buffer<unsigned short> channels(_max_chan_id + 1);
  for (unsigned int i = 0; i <= _max_chan_id; i++) 
  {
    channels[i] = i;
  }

  //- instanciate returned buffer
  values = new asl::AIRawData(_max_chan_id + 1);
  if (values == 0) {
    throw asl::DAQException("out of memory",
                            "out of memory error",
                            "AD2010::read_channels");
  }

  int err = ::D2K_AI_SimuReadChannel (this->idid_, 
                                      static_cast<unsigned short>(channels.depth()),
                                      channels.base(), 
                                      values->base());
  if (err < 0)
  {
    throw asl::DAQException("could not read channels",
                            adl::d2kdask_error_text(err),
                            "AD2010::read_channels",
                            err);
  }
#endif

  return values;
}

// ============================================================================
// AD2010::read_channels
// ============================================================================
asl::AIRawData* AD2010::read_channels (adl::ChanId _max_chan_id)
  throw (asl::DAQException)
{
  asl::AIRawData* values = 0;

#if !defined(_SIMULATION_) 

  values = this->read_raw_channels(_max_chan_id);

  for (adl::ChanId i = 0; i <= _max_chan_id; i++) 
  {
    *(values->base()+i) = *(values->base() + i ) >> 2;
  }

#endif

  return values;
}

// ============================================================================
// AD2010::read_scaled_channels
// ============================================================================
asl::AIScaledData* AD2010::read_scaled_channels (adl::ChanId _max_chan_id)
  throw (asl::DAQException)
{
  asl::AIRawData* values = this->read_raw_channels(_max_chan_id);
    
  //- instanciate returned buffer
  asl::AIScaledData* scaled_values = new asl::AIScaledData(values->depth());
  if (scaled_values == 0) 
  {
    delete values;
    throw asl::DAQException("out of memory",
                            "out of memory error",
                            "AD2010::read_scaled_channels");
  }
  
  for (unsigned int i = 0; i < values->depth(); i++) 
  {
#if !defined(_SIMULATION_) 
    ::D2K_AI_VoltScale (this->idid_, 
                        this->ai_channels[i].range, 
                        (*values)[i], 
                        scaled_values->base() + i);
#else
    (*scaled_values)[i] = (*values)[i]; 
#endif
  }
   
  delete values;

  return scaled_values;
}

// ============================================================================
// AD2010::check_di_port
// ============================================================================
void AD2010::check_di_port (adl::DIOPort _port)
    throw (asl::DAQException)
{
  if (_port < AD2010_MIN_PORT || _port > AD2010_MAX_PORT)
  {
	  throw asl::DAQException("invalid DI port specified",
                            "specified DI port is out of range for board DAQ-2010",
                            "AD2010::check_di_port");
  }
}

// ============================================================================
// AD2010::check_di_line
// ============================================================================
void AD2010::check_di_line (int _line)
    throw (asl::DAQException)
{
  if (this->read_line_port_ == adl::port_1cl || this->read_line_port_ == adl::port_1ch)
  {
    if (_line < AD2010_MIN_LINE || _line >AD2010_MAX_4BP_LINE)
    {
	    throw asl::DAQException("invalid DI line specified",
                            "specified DI port is out of range for board DAQ-2010",
                            "AD2010::check_di_line");
    }
  }
  else
  {
    if (_line < AD2010_MIN_LINE || _line > AD2010_MAX_8BP_LINE)
    {
	    throw asl::DAQException("invalid DI line specified",
                              "specified DI port is out of range for board DAQ-2010",
                              "AD2010::check_di_line");
    }
  }
}
// ============================================================================
// AD2010::check_di_port_config
// ============================================================================
void AD2010::check_di_port_config (adl::DIOPort _port)
    throw (asl::DAQException)
{
  switch (this->dio_port_dir_.find(_port)->second)
  {
    case adl::unknown:
      throw asl::DAQException("the port was not configured",
                              "the port must be configured before performing input acquisition",
                              "AD2010::check_di_port_config");
      break;
    
    case adl::output:
      throw asl::DAQException("the port was configured in output",
                              "the port must be configured in input to perform input acquisition",
                              "AD2010::check_di_port_config");
      break;
    
    case adl::input:
      break;
    
    default:
      throw asl::DAQException();
      break;
  }
}
// ============================================================================
// AD2010::check_do_port_config
// ============================================================================
void AD2010::check_do_port_config (adl::DIOPort _port)
    throw (asl::DAQException)
{
  switch (this->dio_port_dir_.find(_port)->second)
  {
    case adl::unknown:
      throw asl::DAQException("the port was not configured",
                              "the port must be configured before performing output acquisition",
                              "AD2010::check_di_port_config");
      break;
    
    case adl::input:
      throw asl::DAQException("the port was configured in input",
                              "the port must be configured in output to perform output acquisition",
                              "AD2010::check_di_port_config");
      break;
    
    case adl::output:
      break;
    
    default:
        throw asl::DAQException();
      break;

  }
}
// ============================================================================
// AD2010::check_do_port
// ============================================================================
void AD2010::check_do_port (adl::DIOPort _port)
    throw (asl::DAQException)
{
  if (_port < AD2010_MIN_PORT || _port > AD2010_MAX_PORT)
  {
	  throw asl::DAQException("invalid DO port specified",
                            "specified DO port is out of range for board DAQ-2010",
                            "AD2010::check_do_port");
  }
}

// ============================================================================
// AD2010::check_do_line
// ============================================================================
void AD2010::check_do_line (int _line)
    throw (asl::DAQException)
{
   if (this->write_line_port_ == adl::port_1cl || this->write_line_port_ == adl::port_1ch)
  {
    if (_line < AD2010_MIN_LINE || _line >AD2010_MAX_4BP_LINE)
    {
	    throw asl::DAQException("invalid DO line specified",
                            "specified DO port is out of range for board DAQ-2010",
                            "AD2010::check_do_line");
    }
  }
  else
  {
    if (_line < AD2010_MIN_LINE || _line > AD2010_MAX_8BP_LINE)
    {
	    throw asl::DAQException("invalid DO line",
                              "specified DO line is out of range for board DAQ-2010",
                              "AD2010::check_do_line");
    }
  }
}

// ============================================================================
// AD2010::configure_di_port
// ============================================================================
void AD2010::configure_di_port (adl::DIOPort _port)
    throw (asl::DAQException)
{
#if !defined(_SIMULATION_) 

/*  ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());*/

  this->check_di_port(_port);

  int err = ::D2K_DIO_PortConfig(this->idid_ , _port, adl::input);

  if (err < 0) 
  {
	  throw asl::DAQException("port configuration failed",
                            adl::d2kdask_error_text(err),
                            "AD2010::configure_di_port",
						                err);

  }

  this->dio_port_dir_.find(_port)->second = adl::input;

#endif // _SIMULATION_
}

// ============================================================================
// AD2010::configure_do_port
// ============================================================================
void AD2010::configure_do_port (adl::DIOPort _port)
    throw (asl::DAQException)
{
#if !defined(_SIMULATION_) 

 /* ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());*/

  this->check_do_port(_port);

  int err = ::D2K_DIO_PortConfig(this->idid_ , _port, adl::output);

  if (err < 0) 
  {
	  throw asl::DAQException("port configuration failed",
                            adl::d2kdask_error_text(err),
                            "AD2010::configure_do_port",
						                err);

  }

  this->dio_port_dir_.find(_port)->second = adl::output;

#endif // _SIMULATION_
}

// ============================================================================
// AD2010::read_line
// ============================================================================
bool AD2010::read_line (adl::DIOPort _port, int _line)
    throw (asl::DAQException)
{

  this->read_line_port_ = _port; 
  this->check_di_port(_port);

  this->check_di_line(_line);

/*	ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());*/

  unsigned short state = 0;

#if !defined(_SIMULATION_) 

	int err = ::D2K_DI_ReadLine(this->idid_, _port, _line, &state);

	if (err < 0) 
	{
    throw asl::DAQException("could not read DI line",
                            adl::d2kdask_error_text(err),
                            "AD2010::read_line",
                            err);
	}

#endif // _SIMULATION_

 return state ? true : false;
}

// ============================================================================
// AD2010::read_port
// ============================================================================
unsigned long AD2010::read_port (adl::DIOPort _port)
    throw (asl::DAQException)
{

  this->check_di_port(_port);
  this->check_di_port_config(_port);

/*	ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());*/

  unsigned long state = 0;

#if !defined(_SIMULATION_) 	

  int err = ::D2K_DI_ReadPort(this->idid_, _port, &state);

	if (err < 0) 
	{
	  throw asl::DAQException("could not read DI port",
                            adl::d2kdask_error_text(err),
                            "AD2010::read_port",
                            err);
	}

#endif // _SIMULATION_

  return state;
}

// ============================================================================
// AD2010::write_line
// ============================================================================
void AD2010::write_line (adl::DIOPort _port, int _line, bool _state)
    throw (asl::DAQException)
{

  this->write_line_port_ = _port; 

  this->check_do_port(_port);
  this->check_do_port_config(_port);
  this->check_do_line(_line);

/*	ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());*/

#if !defined(_SIMULATION_) 

	unsigned short state = _state ? true : false;

	int err = ::D2K_DO_WriteLine(this->idid_, _port, _line, state);

	if (err < 0) 
	{
		throw asl::DAQException("could not write DO line",
                            adl::d2kdask_error_text(err),
                            "AD2010::write_line",
                            err);
	}

#endif // _SIMULATION_
}

// ============================================================================
// AD2010::write_port
// ============================================================================
void AD2010::write_port (adl::DIOPort _port, unsigned long _state)
    throw (asl::DAQException)
{
  this->check_do_port(_port);
  this->check_do_port_config(_port);

  /*ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());*/

	if (this->write_line_port_ == adl::port_1cl || this->write_line_port_ == adl::port_1ch )
     //- mask unused bits (4 bits port)
        _state &= 0x000F;
  else
    //- mask unused bits (8 bits port)
        _state &= 0x00FF;

#if !defined(_SIMULATION_) 	

	int err = ::D2K_DO_WritePort(this->idid_, _port,  _state);

	if (err < 0) 
	{
    throw asl::DAQException("write port failed",
                            adl::d2kdask_error_text(err),
                            "AD2010::write_port",
                            err);
	}

#endif // _SIMULATION_
}

// ============================================================================
// AD2010::dump
// ============================================================================
void AD2010::dump () const
{
  ACE_DEBUG ((LM_DEBUG, ". manufacturer.......ADLink\n"));
  ACE_DEBUG ((LM_DEBUG, ". type...............DAQ-2010\n"));
  ACE_DEBUG ((LM_DEBUG, ". board id...........%d\n", this->id_));
  ACE_DEBUG ((LM_DEBUG, ". registration id....%d\n", this->idid_));
  ACE_DEBUG ((LM_DEBUG, ". exclusive access...%s\n", this->exclusive_access_ ? "yes" : "no"));
}

} // namespace adl
