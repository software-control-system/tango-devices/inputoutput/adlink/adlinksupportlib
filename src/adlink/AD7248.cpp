// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    AD7248.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/adlink/AD7248.h>

namespace adl { 
// ============================================================================
// STATICS
// ============================================================================
AD7248::Repository AD7248::repository;
ACE_Recursive_Thread_Mutex AD7248::repository_lock;

// ============================================================================
// AD7248::instanciate
// ============================================================================
AD7248* AD7248::instanciate (unsigned short _id, bool _exclusive_access)
    throw (asl::DAQException)
{
  //- lock
  ACE_GUARD_REACTION(ACE_Recursive_Thread_Mutex, 
                     ace_mon, 
                     AD7248::repository_lock, 
                     throw asl::DAQException());

  //- search requested board in the repository
  RepositoryIterator it = AD7248::repository.find(_id);

  //- already instnciated?
  if (it != AD7248::repository.end()) 
  {
    //- can't get ownership if _exclusive_access requested
    if (_exclusive_access || it->second->exclusive_access_) 
	{
      //- can't get exclusive access to hardware
      throw asl::DAQException("hardware already in use",
                              "could not get exclusive access to the hardware",
                              "AD7248::instanciate");
    }
    //- share the board
    else
	{
      return it->second->duplicate();
    }
  }

  //- instanciate new board
  
  AD7248* b = new AD7248(_id);
  if (b == 0) 
  {
    //- out of memory
    throw asl::DAQException("out of memory",
                            "out of memory error",
                            "AD7248::instanciate");
  }

  //- update board's <exclusive_access> flag
  b->exclusive_access_ = _exclusive_access;

  int err = b->register_hardware();
  if (err < 0) 
  {
    //- release board
    b->release();
		//- reset returned pointer
	  b = 0;
    //- hardware registration failed
    throw asl::DAQException("hardware registration failed",
                            adl::pcisdask_error_text(err),
                            "AD7248::instanciate",
                            err);
  }

  return b;
}

// ============================================================================
// AD7248::duplicate
// ============================================================================
AD7248 * AD7248::duplicate ()
{
  return dynamic_cast<AD7248*>(this->SharedObject::duplicate()); 
}
 
// ============================================================================
// AD7248::AD7248
// ============================================================================
AD7248::AD7248 (unsigned short _id) 
  : ADBoard(adl::PCI7248, _id),
    ADSingleShotDIBoard(adl::PCI7248, _id),
    ADSingleShotDOBoard(adl::PCI7248, _id)
{
  //- store board in the repository
  RepositoryValue pair(_id, this);
  AD7248::repository.insert(pair);
  //- initialize dio ports 
  for (int i = adl::port_1a; i <= adl::port_2ch; i++) {
    DIOPortDirValue pair((adl::DIOPort)i, adl::unknown);
    this->dio_port_dir_.insert(pair);
  }
}

// ============================================================================
// AD7248::~AD7248
// ============================================================================
AD7248::~AD7248()
{
  //- lock
  ACE_GUARD_REACTION(ACE_Recursive_Thread_Mutex, 
                     ace_mon, 
                     AD7248::repository_lock, 
                     throw asl::DAQException());
										 
  //- release hardware
  this->release_hardware();

  //- remove board from repository
  RepositoryIterator it = AD7248::repository.find(this->id_);
  if (it != AD7248::repository.end()) 
  {
    AD7248::repository.erase(it);    
  }
}

// ============================================================================
// AD7248::register_hardware
// ============================================================================
int AD7248::register_hardware ()
{
#if !defined(_SIMULATION_) 

  int bid = ::Register_Card(PCI_7248, this->id_);
  if (bid < 0) {
	  this->idid_ = kIMPOSSIBLE_IID;
		return bid;
	}

  this->idid_ = bid;

  return 0;

#else // _SIMULATION_
  
  return this->id_;

#endif // _SIMULATION_
}

// ============================================================================
// AD7248::release_hardware
// ============================================================================
int AD7248::release_hardware ()
{
  //- be sure the board is registered
  if (this->idid_ == kIMPOSSIBLE_IID) {
    return 0;
  }

#if !defined(_SIMULATION_) 

  //- unregister card from driver
  return ::Release_Card(this->idid_);

#else // _SIMULATION_

  return 0;
  
#endif // _SIMULATION_
}

// ============================================================================
// AD7248::check_di_port
// ============================================================================
void AD7248::check_di_port (adl::DIOPort _port)
    throw (asl::DAQException)
{
  if (_port < AD7248_MIN_PORT || _port > AD7248_MAX_PORT) 
  {
	  throw asl::DAQException("invalid DI port specified",
                            "specified DI port is out of range for board PCI-7248",
                            "AD7248::check_di_port");
  }
}

// ============================================================================
// AD7248::check_di_line
// ============================================================================
void AD7248::check_di_line (int _line)
    throw (asl::DAQException)
{
  if (this->read_line_port_ == adl::port_1cl || this->read_line_port_ == adl::port_1ch 
      ||this->read_line_port_ == adl::port_2cl ||this->read_line_port_ == adl::port_2ch )
  {
    if (_line < AD7248_MIN_LINE || _line >AD7248_MAX_4BP_LINE)
    {
	    throw asl::DAQException("invalid DI line specified",
                            "specified DI port is out of range for board PCI-7248",
                            "AD7248::check_di_line");
    }
  }
  else
  {
    if (_line < AD7248_MIN_LINE || _line > AD7248_MAX_8BP_LINE)
    {
	    throw asl::DAQException("invalid DI line specified",
                              "specified DI line is out of range for board PCI-7248",
                              "AD7248::check_di_line");
    }
  }
}

// ============================================================================
// AD7248::check_do_port
// ============================================================================
void AD7248::check_do_port (adl::DIOPort _port)
    throw (asl::DAQException)
{
  if (_port < AD7248_MIN_PORT || _port > AD7248_MAX_PORT)
  {
	  throw asl::DAQException("invalid DO port specified",
                            "specified DO port is out of range for board PCI-7248",
                            "AD7248::check_do_port");
  }
}

// ============================================================================
// AD7248::check_do_line
// ============================================================================
void AD7248::check_do_line (int _line)
    throw (asl::DAQException)
{
  if (this->write_line_port_ == adl::port_1cl || this->write_line_port_ == adl::port_1ch
     ||this->read_line_port_ == adl::port_2cl ||this->read_line_port_ == adl::port_2ch )
  {
    if (_line < AD7248_MIN_LINE || _line >AD7248_MAX_4BP_LINE)
    {
	    throw asl::DAQException("invalid DO line specified",
                            "specified DO port is out of range for board PCI-7248",
                            "AD7248::check_do_line");
    }
  }
  else
  {
    if (_line < AD7248_MIN_LINE || _line > AD7248_MAX_8BP_LINE)
    {
	    throw asl::DAQException("invalid DO line specified",
                              "specified DO line is out of range for board PCI-7248",
                              "AD7248::check_do_line");
    }
  }
}

// ============================================================================
// AD7248::check_di_port_config
// ============================================================================
void AD7248::check_di_port_config (adl::DIOPort _port)
    throw (asl::DAQException)
{
  switch (this->dio_port_dir_.find(_port)->second)
  {
    case adl::unknown:
      throw asl::DAQException("the port was not configured",
                              "the port must be configured before performing input acquisition",
                              "AD7248::check_di_port_config");
      break;
    
    case adl::output:
      throw asl::DAQException("the port was configured in output",
                              "the port must be configured in input to perform input acquisition",
                              "AD7248::check_di_port_config");
      break;
    
    case adl::input:
      break;
    
    default:
      throw asl::DAQException();
      break;
  }
}
// ============================================================================
// AD7248::check_do_port_config
// ============================================================================
void AD7248::check_do_port_config (adl::DIOPort _port)
    throw (asl::DAQException)
{
  switch (this->dio_port_dir_.find(_port)->second)
  {
    case adl::unknown:
      throw asl::DAQException("the port was not configured",
                              "the port must be configured before performing output acquisition",
                              "AD7248::check_di_port_config");
      break;
    
    case adl::input:
      throw asl::DAQException("the port was configured in input",
                              "the port must be configured in output to perform output acquisition",
                              "AD7248::check_di_port_config");
      break;
    
    case adl::output:
      break;
    
    default:
        throw asl::DAQException();
      break;

  }
}
// ============================================================================
// AD7248::configure_di_port
// ============================================================================
void AD7248::configure_di_port (adl::DIOPort _port)
    throw (asl::DAQException)
{
#if !defined(_SIMULATION_) 

 /* ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());*/

  this->check_do_port(_port);

  int err = ::DIO_PortConfig(this->idid_ , _port, adl::input);

  if (err < 0) 
  {
	  throw asl::DAQException("port configuration failed",
                            adl::pcisdask_error_text(err),
                            "AD7248::configure_port",
						                err);

  }

  this->dio_port_dir_.find(_port)->second = adl::input;

#endif // _SIMULATION_
}

// ============================================================================
// AD7248::configure_do_port
// ============================================================================
void AD7248::configure_do_port (adl::DIOPort _port)
    throw (asl::DAQException)
{
#if !defined(_SIMULATION_) 

 /* ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());*/

  this->check_do_port(_port);

  int err = ::DIO_PortConfig(this->idid_ , _port, adl::output);

  if (err < 0) 
  {
	  throw asl::DAQException("port configuration failed",
                            adl::pcisdask_error_text(err),
                            "AD7248::configure_port",
						                err);

  }

  this->dio_port_dir_.find(_port)->second = adl::output;

#endif // _SIMULATION_
}

// ============================================================================
// AD7248::read_line
// ============================================================================
bool AD7248::read_line (adl::DIOPort _port, int _line)
    throw (asl::DAQException)
{
  this->read_line_port_ = _port;

  this->check_di_port(_port);
  this->check_di_port_config(_port);
  this->check_di_line(_line);

/*	ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());*/

  unsigned short state = 0;

#if !defined(_SIMULATION_) 

  int err = ::DI_ReadLine(this->idid_, _port, _line, &state);

	if (err < 0) 
	{
    throw asl::DAQException("could not read DI line",
                            adl::pcisdask_error_text(err),
                            "AD7248::read_line",
                            err);
	}

#endif // _SIMULATION_

 return state ? true : false;
}

// ============================================================================
// AD7248::read_port
// ============================================================================
unsigned long AD7248::read_port (adl::DIOPort _port)
    throw (asl::DAQException)
{
  this->check_di_port(_port);
  this->check_di_port_config(_port);
	
  /*ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());*/

  unsigned long state = 0;

#if !defined(_SIMULATION_) 	

  int err = ::DI_ReadPort(this->idid_, _port, &state);

	if (err < 0) 
	{
	  throw asl::DAQException("could not read DI port",
                            adl::pcisdask_error_text(err),
                            "AD7248::read_port",
                            err);
	}

#endif // _SIMULATION_

  return state;
}

// ============================================================================
// AD7248::write_line
// ============================================================================
void AD7248::write_line (adl::DIOPort _port, int _line, bool _state)
    throw (asl::DAQException)
{
  this->write_line_port_ = _port;

  this->check_do_port(_port);
  this->check_do_port_config(_port);
  this->check_do_line(_line);

	
  /*ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());*/

#if !defined(_SIMULATION_) 

	unsigned short state = _state ? true : false;

	int err = ::DO_WriteLine(this->idid_, _port, _line, state);

	if (err < 0) 
	{
		throw asl::DAQException("could not write DO line",
                            adl::pcisdask_error_text(err),
                            "AD7248::write_line",
                            err);
	}

#endif // _SIMULATION_
}

// ============================================================================
// AD7248::write_port
// ============================================================================
void AD7248::write_port (adl::DIOPort _port, unsigned long _state)
    throw (asl::DAQException)
{
  this->check_do_port(_port);
  this->check_do_port_config(_port);
	
  /*ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());*/

	if (this->write_line_port_ == adl::port_1cl || this->write_line_port_ == adl::port_1ch
     ||this->read_line_port_ == adl::port_2cl ||this->read_line_port_ == adl::port_2ch )
     //- mask unused bits (4 bits port)
        _state &= 0x000F;
  else
    //- mask unused bits (8 bits port)
        _state &= 0x00FF;

#if !defined(_SIMULATION_) 	

	int err = ::DO_WritePort(this->idid_, _port, _state);

	if (err < 0) 
	{
    throw asl::DAQException("write port failed",
                            adl::pcisdask_error_text(err),
                            "AD7248::write_port",
                            err);
	}

#endif // _SIMULATION_
}

// ============================================================================
// AD7248::dump
// ============================================================================
void AD7248::dump () const
{
  ACE_DEBUG ((LM_DEBUG, ". manufacturer.......ADLink\n"));
  ACE_DEBUG ((LM_DEBUG, ". type...............DAQ-7248\n"));
  ACE_DEBUG ((LM_DEBUG, ". board id...........%d\n", this->id_));
  ACE_DEBUG ((LM_DEBUG, ". registration id....%d\n", this->idid_));
  ACE_DEBUG ((LM_DEBUG, ". exclusive access...%s\n", this->exclusive_access_ ? "yes" : "no"));
}

} // namespace adl

