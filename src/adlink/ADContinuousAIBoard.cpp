// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ADContinuousAIBoard.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#ifdef __linux
#  include <pthread.h>
#  include <signal.h> 
#endif
#include <math.h>
#include <iostream>
#include <asl/adlink/ADContinuousAIBoard.h>
#include <asl/adlink/AD2005.h>
#include <asl/adlink/AD2010.h>
#include <asl/adlink/AD2205.h>

namespace adl {

// ============================================================================
// SUPPORTED HARDWARE
// ============================================================================
#define __NAN__ ::sqrt(-1)

// ============================================================================
// SUPPORTED HARDWARE
// ============================================================================
#define CD_HW_SUPPORT "current impl. supports ADLink boards: DAQ-2005, DAQ-2010"

// ============================================================================
// ADLINK SIGNALS
// ============================================================================
#ifdef __linux
#  define kFIRST_AD_SIGNAL 58
//- DB EVENT........Linux signal 58
//- TRIG EVENT......Linux signal 58
//- DAQ-END EVENT...Linux signal 59
#  define kLAST_AD_SIGNAL  59
#endif
 
// ============================================================================
// ADContinuousAIBoard::instanciate
// ============================================================================
ADContinuousAIBoard * ADContinuousAIBoard::instanciate (unsigned short _type, 
                                                        unsigned short _id,
                                                        bool _exclusive_access)
  throw (asl::DAQException)
{
  ADContinuousAIBoard * b = 0;

  //- type of board
  switch (_type)  
  {
    //- 2005
    case adl::DAQ2005:
      b = AD2005::instanciate (_id, _exclusive_access);
      break;

    //- 2010
    case adl::DAQ2010:
      b = AD2010::instanciate (_id, _exclusive_access);
      break;
    
    //- 2205
    case adl::DAQ2205:
      b = AD2205::instanciate (_id, _exclusive_access);
      break;

    //- unsupported hardware
    default:
      throw asl::DAQException("unsupported or invalid hardware type",
                              CD_HW_SUPPORT,
                              "ADContinuousAIBoard::instanciate");
      break;
  }

  return b;
}

// ============================================================================
// ADContinuousAIBoard::ADContinuousAIBoard
// ============================================================================
ADContinuousAIBoard::ADContinuousAIBoard (unsigned short _type, 
                                          unsigned short _id, 
                                          unsigned short _nchans) 
  : ADBoard(_type, _id),
    ADCalibrableAIOBoard(_type, _id),
    daq_buffer_idx_ (0),
    daq_buffer_id_ (0),
    daq_buffer_0_ (0),
    daq_buffer_1_ (0),
    channels_arg_ (_nchans),
    has_been_started_at_least_once_ (false),
    last_sample_index_in_last_daq_buffer_ (0),
    stop_pos_ (0),
    stop_count_ (0),
	first_config_(true)
{
  //- std::cout << "ADContinuousAIBoard::ADContinuousAIBoard <-" << std::endl;
  //- std::cout << "ADContinuousAIBoard::ADContinuousAIBoard ->" << std::endl;
}

// ============================================================================
// ADContinuousAIBoard::~ADContinuousAIBoard
// ============================================================================
ADContinuousAIBoard::~ADContinuousAIBoard ()
{
  //- std::cout << "ADContinuousAIBoard::~ADContinuousAIBoard <-" << std::endl;
  //- release daq buffers
  this->release_daq_buffers();
  //- std::cout << "ADContinuousAIBoard::~ADContinuousAIBoard ->" << std::endl;
}

// ============================================================================
// ADContinuousAIBoard::configure_continuous_ai
// ============================================================================
void ADContinuousAIBoard::configure_continuous_ai (const asl::ContinuousAIConfig& _ai_config)
    throw (asl::DAQException)
{
  // configuration optimization:
  asl::ContinuousAIConfig prev_config; 
  if (!first_config_)
  {
    // store previous config if not first time function is called
    prev_config = this->ai_config_;
  }

  //- store config locally
  this->ai_config_ = _ai_config;

#if !defined(_SIMULATION_) 

  //- check registration id (may throw an exception)
  this->check_registration();

  int err = 0;

  //- channels config --------------------------------------------------------
  //--------------------------------------------------------------------------
  const asl::ActiveAIChannels& ac = this->ai_config_.get_active_channels();

  if (ac.size() == 0) {
    //- no active channels
    throw asl::DAQException("analog input: invalid configuration",
                            "no active AI channels",
                            "ADContinuousAIBoard::configure_continuous_ai");
  }

  const asl::ActiveAIChannels& prev_ac = prev_config.get_active_channels();

  for (unsigned int c = 0;  c < ac.size();  c++) 
  {
	  int err = 0;
	  bool do_config = true;
	  // if not first time function is called, check if config has changed
	  if (!first_config_)
	  {
	    if ((prev_ac[c].id == ac[c].id) && 
			(prev_ac[c].range == ac[c].range) &&
			(prev_ac[c].grd_ref == ac[c].grd_ref))
		{
			// config equals, don't call D2K_AI_CH_Config function
			do_config = false;
		}
		else
		{
			do_config = true;
		}
	  }
	  else
	  {
		  do_config = true;
	  }

	  if (do_config)
	  {
         err = ::D2K_AI_CH_Config(this->idid_, ac[c].id, ac[c].range | ac[c].grd_ref);
	  }

      if (err < 0)
      {
        throw asl::DAQException("AI channel configuration failed",
                                adl::d2kdask_error_text(err),
                                "ADContinuousAIBoard::configure_continuous_ai",
                                err);
      }
  }
  first_config_ = false;

  // get timebase type
  adl::AIOTimeBase timebase_type = this->ai_config_.get_timebase_type();
  
  //- analog trigger config --------------------------------------------------
  //- or external timebase (same D2K function)
  //--------------------------------------------------------------------------
  if ((this->ai_config_.get_trigger_source() == adl::external_analog) ||
      (timebase_type == adl::external_analog))
  {
    //- analog trigger: control
    unsigned short atrig_ctrl = this->ai_config_.get_analog_trigger_source() | 
                     this->ai_config_.get_analog_trigger_condition();

    //- analog trigger: high level
    unsigned short atrig_hi_level = this->ai_config_.get_analog_high_level_condition();

    //- analog trigger: high level
    unsigned short atrig_lo_level = this->ai_config_.get_analog_low_level_condition();

    err = ::D2K_AIO_Config(this->idid_, 
                           timebase_type,
                           atrig_ctrl, 
                           atrig_hi_level, 
                           atrig_lo_level);
    if (err < 0) 
	{
      //- analog trigger configuration failed
      throw asl::DAQException("analog input trigger configuration failed",
                              adl::d2kdask_error_text(err),
                              "ADContinuousAIBoard::configure_continuous_ai",
                              err);
    }
  }

  //- analog input config ----------------------------------------------------
  //--------------------------------------------------------------------------
  //- adc conversion trigger source
  unsigned short conv_src = this->ai_config_.get_conversion_source();

  //- trigger control
  unsigned long trig_ctrl = 0;
  trig_ctrl |= this->ai_config_.get_trigger_source();
  trig_ctrl |= this->ai_config_.get_trigger_mode();
  trig_ctrl |= this->ai_config_.get_delay_unit();
  trig_ctrl |= (this->ai_config_.retrigger_enabled() ? adl::ai_rt_enabled : adl::ai_rt_disabled);
  trig_ctrl |= (this->ai_config_.mcounter_enabled() ? adl::ai_mc_enabled : adl::ai_mc_disabled);
  trig_ctrl |= this->ai_config_.get_trigger_polarity();
  
  //- mid or delay scans count
  U32 mod_scans_cnt = 0;
  if (
      this->ai_config_.get_trigger_mode() == adl::ai_delay
        ||
      this->ai_config_.get_trigger_mode() == adl::ai_middle
     ) 
  {
    //- convert from nano_secs to clock_tiks
    if (this->ai_config_.get_delay_unit() == adl::clock_ticks) 
    {
      //- get user value
      double  sc = this->ai_config_.get_middle_or_delay_scans();
      
      //- convert to clock-ticks
      if (timebase_type == adl::ext_time_base) // external timebase
      {
        double nsec_per_ticks = (double)(NANOSEC_PER_SEC / this->ai_config_.get_ext_timebase_val());
        mod_scans_cnt = (unsigned long)(sc / nsec_per_ticks); // use specified timebase value
      }
      else
      {
        mod_scans_cnt = (unsigned long)(sc / this->nsec_per_clock_tick()); // use default timebase value
      }
    }
    //- get num of scans
    else 
    {
      mod_scans_cnt = (unsigned long)this->ai_config_.get_middle_or_delay_scans();
    }
  }

  //- mcounter value
  unsigned short mc_cnt = 0;
  if (this->ai_config_.mcounter_enabled()) 
  {
    mc_cnt = this->ai_config_.get_mcounter_value();
  }

  //- retrigger count (0 for infinite retrigger)
  unsigned short retrg_cnt = static_cast<unsigned short>(this->ai_config_.num_trigger_sequences());

  //- config
  err = ::D2K_AI_Config(this->idid_, 
                        conv_src,
                        trig_ctrl,
                        mod_scans_cnt,
                        mc_cnt,
                        retrg_cnt,
                        (BOOLEAN)1);

  if (err < 0) {
    //- ai config failed
    throw asl::DAQException("analog input configuration failed",
                            adl::d2kdask_error_text(err),
                            "ADContinuousAIBoard::configure_continuous_ai",
                            err);
  }

  //- double buffering -------------------------------------------------------
  //--------------------------------------------------------------------------
  //- enable db if retrigger disabled
  if (this->ai_config_.retrigger_enabled() == false) 
  {
    //- enable db
    err = ::D2K_AI_AsyncDblBufferMode(this->idid_, (BOOLEAN)1);
    if (err < 0) {
      //- ai db config failed
      throw asl::DAQException("double bufferring configuration failed",
                              adl::d2kdask_error_text(err),
                              "ADContinuousAIBoard::configure_continuous_ai",
                              err);
    }
  }

  //- register event handler -------------------------------------------------
  //--------------------------------------------------------------------------
  //- select event to install
  I16 evt = this->ai_config_.retrigger_enabled() ? adl::trig_evt : adl::db_evt;

  //- select event-handler to install
  asl::DAQCallback cb = 0;
  if (this->ai_config_.retrigger_enabled()) // retrig mode
  {
    if (! this->ai_config_.num_trigger_sequences()) // infinite mode
      cb = this->ai_config_.get_trigger_event_callback();
    else
      if (this->ai_config_.intermediate_buffers_enabled() && 
          this->ai_config_.num_intermediate_trigger()) // finite & intermediate mode
        cb = this->ai_config_.get_trigger_event_callback();
      // no else case cause in finite only mode, don't need trigger callback
  }
  else // internal mode
  {
    cb = this->ai_config_.get_db_event_callback();
  }

  //- install event and event-handler

#if defined (WIN32)
  if (cb)
  {
    err = ::D2K_AI_EventCallBack (this->idid_, 1, evt, (U32)cb);
#else
    err = ::D2K_AI_EventCallBack (this->idid_, 1, evt, cb);
#endif
    if (err < 0) {
      throw asl::DAQException("DAQ event handler registration failed",
                              adl::d2kdask_error_text(err),
                              "ADContinuousAIBoard::configure_continuous_ai",
                              err);
    }
  }

  //- get callback address
  cb = this->ai_config_.get_ai_end_event_callback();
  //- be sure callback address is valid
  if (cb == 0) {
    throw asl::DAQException("invalid configuration",
                            "invalid callback address specified",
                            "ADContinuousAIBoard::configure_continuous_ai");
  }

  bool finite_retrig_mode  = this->ai_config_.retrigger_enabled() 
                          && this->ai_config_.num_trigger_sequences();

  bool pre_our_middle_trig_mode = this->ai_config_.get_trigger_mode() == adl::ai_pre
                               || this->ai_config_.get_trigger_mode() == adl::ai_middle;

  if ( pre_our_middle_trig_mode || finite_retrig_mode ) 
  {
    std::cout << "installing::daq-end-evt handler" << std::endl; 
    //- install event and event-handler
#if defined (WIN32)
    err = ::D2K_AI_EventCallBack (this->idid_, 1, adl::daq_end, (U32)cb);
#else
    err = ::D2K_AI_EventCallBack (this->idid_, 1, adl::daq_end, cb);
#endif
    if (err < 0) {
      throw asl::DAQException("DAQ event handler registration failed",
                              adl::d2kdask_error_text(err),
                              "ADContinuousAIBoard::configure_continuous_ai",
                              err);
    }
  }

  //- register daq buffers ---------------------------------------------------
  //--------------------------------------------------------------------------
  if (this->init_daq_buffers() == -1) {
    //- could not allocate buffers
    throw asl::DAQException("DAQ buffers allocation failed",
                            "out of memory error",
                            "ADContinuousAIBoard::configure_continuous_ai");
  }

  //- register buffer 0 (store buffer id in <daq_buffer_id_>)
  err = ::D2K_AI_ContBufferSetup(this->idid_, 
                                 this->daq_buffer_0_->base(), 
                                 this->daq_buffer_0_->depth(), 
                                 &this->daq_buffer_id_);

  if (err < 0) {
    //- could not register buffer
    throw asl::DAQException("DAQ buffer 0 setup failed - possible driver memory problem",
                            adl::d2kdask_error_text(err),
                            "ADContinuousAIBoard::configure_continuous_ai",
                            err);
  }

  if (this->daq_buffer_1_)
  {
    //- register buffer 1
    unsigned short ignored;
    err = ::D2K_AI_ContBufferSetup(this->idid_, 
                                   this->daq_buffer_1_->base(), 
                                   this->daq_buffer_1_->depth(), 
                                   &ignored);

    if (err < 0) {
      //- could not register buffer
      throw asl::DAQException("DAQ buffer 1 setup failed - possible driver memory problem",
                              adl::d2kdask_error_text(err),
                              "ADContinuousAIBoard::configure_continuous_ai",
                              err);
    }
  }

#else // _SIMULATION_
  
  //- register daq buffers ---------------------------------------------------
  //--------------------------------------------------------------------------
  
  if (this->init_daq_buffers() == -1) {
    //- could not allocate buffers
    throw asl::DAQException("DAQ buffers allocation failed",
                            "out of memory error",
                            "ADContinuousAIBoard::configure_continuous_ai");
  }

#endif // _SIMULATION_ 
}

// ============================================================================
// ADContinuousAIBoard::init_daq_buffers
// ============================================================================
int ADContinuousAIBoard::init_daq_buffers ()
{
  //- release buffers
  this->release_daq_buffers();

  //- get <num of samples per channel>
  unsigned long depth = this->ai_config_.get_buffer_depth();

  //- actual depth is <num of scans per buffer> * <num of active channels>
  depth *= this->ai_config_.num_active_channels();

  //- actual depth is <num of scans per buffer> * <num of active channels> * <retrig-count>
  if (this->ai_config_.retrigger_enabled() && this->ai_config_.num_trigger_sequences())
    depth *= this->ai_config_.num_trigger_sequences();

  /*
  std::cout << "ADContinuousAIBoard::init_daq_buffers::depth: "
            << depth
            << std::endl;
  */

  //- be sure buffer size is even
  if (depth % 2) {
    throw asl::DAQException("invalid DAQ buffer depth",
                            "DAQ buffer depth must be even",
                            "ADContinuousAIBoard::init_daq_buffers");
  }

  //- allocate buffer 0
  this->daq_buffer_0_ = new asl::AIRawData(depth);
  if (this->daq_buffer_0_ == 0) {
    return -1;
  }
  this->daq_buffer_0_->clear();

  //- allocate buffer 1
  if (! this->ai_config_.retrigger_enabled() || ! this->ai_config_.num_trigger_sequences())
  {
    this->daq_buffer_1_ = new asl::AIRawData(depth);
    if (this->daq_buffer_1_ == 0) {
      delete this->daq_buffer_0_;
      this->daq_buffer_0_ = 0;
      return -1;
    }
    this->daq_buffer_1_->clear();
  }

  return 0;
}

// ============================================================================
// ADContinuousAIBoard::release_daq_buffers
// ============================================================================
void ADContinuousAIBoard::release_daq_buffers ()
{
  if (this->daq_buffer_0_) {
    delete daq_buffer_0_;
    daq_buffer_0_ = 0;
  }

  if (this->daq_buffer_1_) {
    delete daq_buffer_1_;
    daq_buffer_1_ = 0;
  }
}

// ============================================================================
// ADContinuousAIBoard::start_continuous_ai
// ============================================================================
void ADContinuousAIBoard::start_continuous_ai ()
    throw (asl::DAQException)
{
  //- std::cout << "ADContinuousAIBoard::start_continuous_ai <-" << std::endl;

#if !defined(_SIMULATION_) 

  //- check registration id (may throw an exception)
  this->check_registration();

  //- reset last sample index
  this->last_sample_index_in_last_daq_buffer_ = 0;

  //- reset buffer index
  this->daq_buffer_idx_ = 0;

  //- get user specified daq rate
  double daq_rate = this->ai_config_.get_sampling_rate();

  //- get timebase type
  adl::AIOTimeBase timebase_type = this->ai_config_.get_timebase_type();

  //- convert from sample rate (in samples/s) to sample interval
  unsigned long scans_interval = 0;
  if (timebase_type == adl::ext_time_base) // external timebase
  {
    scans_interval = (unsigned long)(this->ai_config_.get_ext_timebase_val() / daq_rate); // use specified frequency
  }
  else
  {
    scans_interval = (unsigned long)(this->clock_frequency() / daq_rate); // use default clock frequency
  }

  //- clear overrun flag (workaround)
  ::D2K_AI_AsyncDblBufferOverrun(this->idid_, 1, 0);

  //- interval between ADC conversion of consecutive channels 
  //- is not supported for boards DAQ_2005, DAQ_2006 and DAQ_2010
  unsigned long samples_interval = kIGNORED_PARAM;
  switch ( this->type_ )
  {
    case adl::DAQ2204:
      samples_interval = 14;
      break;
    case adl::DAQ2205:
      samples_interval = 80;
      break;
    case adl::DAQ2206:
      samples_interval = 160;
      break;
    case adl::DAQ2208:
      samples_interval = 14;
      break;
    default:
      break;
  }

  //----------------------------------------------------------------
  //- DAQ optimisation: depends on num of channels and their order -
  //----------------------------------------------------------------

  //- get active channels
  const asl::ActiveAIChannels& ac = this->ai_config_.get_active_channels();

  //- error code
  int err = 0;

  //- about read_scans...
  //- if double-buffered mode is disabled, this is the total number of scans 
  //- to be performed. for double-buffered acquisition, read_scans is the size 
  //- (in samples) allocated for each channel in the circular buffer. range of 
  //- valid values is 2 to 16777215. the value must be a multiple of two.
  unsigned long read_scans = this->ai_config_.get_buffer_depth();

  if (this->ai_config_.retrigger_enabled() && this->ai_config_.num_trigger_sequences())
    read_scans *= this->ai_config_.num_trigger_sequences();

  /*
  std::cout << "ADContinuousAIBoard::start_continuous_ai::read_scans: "
            << read_scans
            << std::endl;
  */

  //- start continous AI
  if (ac.size() == 1) 
  {
    //- only one active channel: use D2K_AI_ContReadChannel
    //-----------------------------------------------------
    err = ::D2K_AI_ContReadChannel(this->idid_,
                                   ac[0].id,
                                   this->daq_buffer_id_,
                                   read_scans,
                                   scans_interval,
                                   samples_interval,
                                   adl::async_op);

  }
  else 
  {
    //- several active channels
    if (this->ai_config_.active_channels_ordered_from_zero()) 
    {
      //- channels "ordered from zero": use D2K_AI_ContScanChannels
      //-----------------------------------------------------------
      err = ::D2K_AI_ContScanChannels(this->idid_,
                                      ac.size() - 1,
                                      this->daq_buffer_id_,
                                      read_scans,
                                      scans_interval,
                                      samples_interval,
                                      adl::async_op);
    }
    else
    {
      //- channels not "ordered from zero": use D2K_AI_ContReadMultiChannels
      //--------------------------------------------------------------------
      //- fill D2K_AI_ContReadMultiChannels "chans" arg
      this->channels_arg_.clear();
      for (unsigned int i = 0; i < ac.size(); i++) {
        this->channels_arg_[i] = ac[i].id;
      }
      //- start DAQ
      err = ::D2K_AI_ContReadMultiChannels(this->idid_,
                                           ac.size(),
                                           this->channels_arg_.base(),
                                           this->daq_buffer_id_,
                                           read_scans,
                                           scans_interval,
                                           samples_interval,
                                           adl::async_op);
    }
  }

  if (err < 0) {
    throw asl::DAQException("DAQ start failed",
                            adl::d2kdask_error_text(err),
                            "ADContinuousAIBoard::start_continuous_ai",
                            err);
  }

#endif // _SIMULATION_

  this->has_been_started_at_least_once_ = true;

  //- std::cout << "ADContinuousAIBoard::start_continuous_ai ->" << std::endl;
}

// ============================================================================
// ADContinuousAIBoard::ai_db_event_callback
// ============================================================================
asl::AIRawData* ADContinuousAIBoard::ai_db_event_callback (void* arg)
    throw (asl::DataLostException, asl::DAQException)
{
  ACE_UNUSED_ARG(arg);

  //- std::cout << "ADContinuousAIBoard::ai_db_event_callback <-" << std::endl;

#if !defined(_SIMULATION_) 

  //- std::cout << "ADContinuousAIBoard::D2K_AI_AsyncDblBufferHalfReady <-" << std::endl;

  //- about the following call to D2K_AI_AsyncDblBufferHalfReady:
  //- the aim is to maintain correct buffer index (ADLink internal cooking)
  //- data is ready since signal is raise when half buffer of data available
  BOOLEAN hb_ready, f_stop;
  int err = ::D2K_AI_AsyncDblBufferHalfReady(this->idid_, &hb_ready, &f_stop);
  if (err < 0) {
    throw asl::DAQException("internal DAQ error",
                            adl::d2kdask_error_text(err),
                            "ADContinuousAIBoard::ai_db_event_callback",
                            err);
  }

  //- operation (i.e. DAQ ) may have been stopped by a (pre or middle) trig event 
  //- in this case we let the DAQ-END event handler retrieve the data
  if (f_stop)
  {
    ACE_DEBUG ((LM_DEBUG, "ADContinuousAIBoard::ai_db_event_callback::fstop detected\n"));
    return 0;
  }
  
  //- std::cout << "ADContinuousAIBoard::D2K_AI_AsyncDblBufferHalfReady ->" << std::endl;

  //- select current buffer
  asl::AIRawData * data_src = this->daq_buffer_0_;
  if (this->daq_buffer_1_ && (this->daq_buffer_idx_ % 2))
    data_src = this->daq_buffer_1_;

  //- copy data from daq buffer to user buffer
  asl::AIRawData * raw_data = new asl::AIRawData(*data_src);
  if (raw_data == 0) {  
    throw asl::DAQException("data buffer allocation failed",
                            "out of memory error",
                            "ADContinuousAIBoard::ai_db_event_callback");
  }

  //- tell the driver data is handled
  ::D2K_AI_AsyncDblBufferHandled(this->idid_);

  //- check overrun status
  unsigned short overrun_status;
  ::D2K_AI_AsyncDblBufferOverrun(this->idid_, 0, &overrun_status);

  //- in case of buffer overrun ...
  if (overrun_status) 
  {
    //- clear overrun flag
    ::D2K_AI_AsyncDblBufferOverrun(this->idid_, 1, 0);

    //- apply overrun strategy
    switch (this->ai_config_.get_data_lost_strategy()) 
    {
      case adl::trash:
      case adl::abort:
      case adl::restart:
      case adl::notify:
        //- release allocated buffer
        delete raw_data;
        //- throw data_lost exception
        throw asl::DataLostException(asl::DataLostException::DAQ_BUFFER_OVERRUN);
        break;
      case adl::ignore:
        //- try to resync with the current buffer 
        this->daq_buffer_idx_ = (++this->daq_buffer_idx_) % 2;
        //- release allocated buffer
        delete raw_data;
        //- just return no data
        return 0;
        break;
      default:
        break;
    }
  }

  //- update daq_buffer_idx
  this->daq_buffer_idx_ = (++this->daq_buffer_idx_) % 2;

  //- std::cout << "ADContinuousAIBoard::ai_db_event_callback ->" << std::endl;

  return raw_data;

#else // _SIMULATION_ 

  //- copy data from daq buffer to user buffer
  asl::AIRawData * raw_data = new asl::AIRawData(this->daq_buffer_0_->depth());
  if (raw_data == 0) {
    throw asl::DAQException("data buffer allocation failed",
                            "out of memory error",
                            "ADContinuousAIBoard::ai_db_event_callback");
  }

  //- fill data with 0
  raw_data->fill(32768 / 2);

  // update daq_buffer_idx for remaining samples option
  this->daq_buffer_idx_ = this->daq_buffer_0_->depth() / 2;

  return raw_data;

#endif // _SIMULATION_ 
}

// ============================================================================
// ADContinuousAIBoard::ai_trigger_event_callback
// ============================================================================
asl::AIRawData* ADContinuousAIBoard::ai_trigger_event_callback (void* arg)
    throw (asl::DataLostException, asl::DAQException)
{
  ACE_UNUSED_ARG(arg);

  // check intermediate buffer mode
  if (!this->ai_config_.intermediate_buffers_enabled())
  {
    // no intermediate buffer mode: read data at each callback call

#if !defined(_SIMULATION_)

  asl::AIRawData * data_src = this->daq_buffer_0_;

  //- get current buffer index
  if (! this->ai_config_.num_trigger_sequences())
  {
    //- dummy parameters
    BOOLEAN trg_ready;
    BOOLEAN stop_flag;

    //- will contain the redy buffer index
    unsigned short trig_cnt_or_buffer_id;

    int err = ::D2K_AI_AsyncReTrigNextReady(this->idid_, &trg_ready, &stop_flag, &trig_cnt_or_buffer_id);
    if (err < 0) {
      throw asl::DAQException("internal DAQ error",
                              adl::d2kdask_error_text(err),
                              "ADContinuousAIBoard::ai_trigger_event_callback",
                              err);
    }

    if (this->daq_buffer_1_ && trig_cnt_or_buffer_id)
      data_src = this->daq_buffer_1_;

    /*
    std::cout << "ADContinuousAIBoard::ai_trigger_event_callback::D2K_AI_AsyncReTrigNextReady::"
              << "trg_ready:" 
              << (trg_ready ? 1 : 0)
              << " - stop_flag:"
              << (stop_flag ? 1 : 0)
              << " - trig_cnt_or_buffer_id: "
              << trig_cnt_or_buffer_id
              << std::endl;
    */

    //- update daq_buffer_idx
    this->daq_buffer_idx_ = (++this->daq_buffer_idx_) % 2;
  }

  //- copy data from daq buffer to user buffer
  asl::AIRawData * raw_data = new asl::AIRawData(*data_src);
  if (raw_data == 0) {
    throw asl::DAQException("data buffer allocation failed",
                            "out of memory error",
                            "ADContinuousAIBoard::ai_trigger_event_callback");
  }

  return raw_data;

#else // _SIMULATION_ 

  //- copy data from daq buffer to user buffer
  asl::AIRawData * raw_data = new asl::AIRawData(this->daq_buffer_0_->depth());
  if (raw_data == 0) {
    throw asl::DAQException("data buffer allocation failed",
                            "out of memory error",
                            "ADContinuousAIBoard::ai_trigger_event_callback");
  }

  //- fill data with 0
  raw_data->fill(32768 / 2);

  return raw_data;

#endif // _SIMULATION_ 

  }
  else
  {
    // intermediate buffer mode: only read data every N callback call

#if !defined(_SIMULATION_)

    asl::AIRawData * data_src = this->daq_buffer_0_;

    //- get trigger number:

    //- dummy parameters
    BOOLEAN trg_ready;
    BOOLEAN stop_flag;
    //- will contain the ready buffer index
    unsigned short trig_cnt;

    int err = ::D2K_AI_AsyncReTrigNextReady(this->idid_, &trg_ready, &stop_flag, &trig_cnt);
    if (err < 0) {
      throw asl::DAQException("internal DAQ error",
                              adl::d2kdask_error_text(err),
                              "ADContinuousAIBoard::ai_trigger_event_callback",
                              err);
    }
/*
    std::cout << "ADContinuousAIBoard::ai_trigger_event_callback::D2K_AI_AsyncReTrigNextReady::"
              << "trg_ready:" 
              << (trg_ready ? 1 : 0)
              << " - stop_flag:"
              << (stop_flag ? 1 : 0)
              << " - trig_cnt: "
              << trig_cnt
              << std::endl;*/

    if (this->ai_config_.num_trigger_sequences() && // finite mode
        this->ai_config_.num_intermediate_trigger()) // intermediate mode properly configured
    {
      // we get buffer if:
      // - num intermediate trig = 1 : get all buffers
      // - or num intermediate trig > 1 and (trg - 1)%num interm trig = 0 : get only multiples
      if ((this->ai_config_.num_trigger_sequences() == 1) ||
          ((this->ai_config_.num_trigger_sequences() > 1) &&
           !((trig_cnt - 1)%this->ai_config_.num_intermediate_trigger()))) // trigger number multiple of intermediate buffer nber
      {
        //- copy data from daq buffer to user buffer
        asl::AIRawData * raw_data = new asl::AIRawData(*data_src);
        if (raw_data == 0) {
          throw asl::DAQException("data buffer allocation failed",
                                  "out of memory error",
                                  "ADContinuousAIBoard::ai_trigger_event_callback");
        }

        return raw_data;
      }
      else
      {
        // do nothing
        return NULL;
      }
    }
    else
    {
      // do nothing
      return NULL;
    }

#else // _SIMULATION_ 

    std::cout << "ADContinuousAIBoard::ai_trigger_event_callback() intermediate bufefr case" << std::endl;

    //- copy data from daq buffer to user buffer
    asl::AIRawData * raw_data = new asl::AIRawData(this->daq_buffer_0_->depth());
    if (raw_data == 0) {
      throw asl::DAQException("data buffer allocation failed",
                              "out of memory error",
                              "ADContinuousAIBoard::ai_trigger_event_callback");
    }

    //- fill data with 0
    raw_data->fill(32768 / 2);

    return raw_data;

#endif // _SIMULATION_ 

  }

}

// ============================================================================
// ADContinuousAIBoard::stop_continuous_ai
// ============================================================================
void ADContinuousAIBoard::stop_continuous_ai ()
    throw (asl::DAQException)
{
  std::cout << "ADContinuousAIBoard::stop_continuous_ai <-" << std::endl;
  
#if !defined(_SIMULATION_)

  //- check registration id (may throw an exception)
  this->check_registration();

  //- IMPORTANT NOTE:
  //- THE DRIVER WAIT FOR THE CALLBACK TO COMPLETE BEFORE STOPPING THE
  //- ASYNC OPERATION. If THE CALLBACK NEVER RETURNS, THE PROCESS HANGS.

  if (this->has_been_started_at_least_once_)
  {

    //- async AI operation already stopped?
    if (! this->ai_config_.retrigger_enabled())
    {
      BOOLEAN hb_ready, f_stop;
      int err = ::D2K_AI_AsyncDblBufferHalfReady(this->idid_, &hb_ready, &f_stop);
      if (! err && f_stop) 
      {
        return;
      }
    }

    //- stop async AI operation
    //- note: D2K_AI_AsyncClear auto remove event handlers 
    unsigned long pos;
    unsigned long count;

    int err = ::D2K_AI_AsyncClear(this->idid_, &pos, &count);
    if (err < 0) 
    {
      throw asl::DAQException("DAQ stop failed",
                              adl::d2kdask_error_text(err),
                              "ADContinuousAIBoard::stop_continuous_ai",
                              err);
    }
    else
    {
      this->stop_count_ = count;
      this->stop_pos_ = pos;
    }

    std::cout << "ADContinuousAIBoard::stop_continuous_ai::pos::" << this->stop_pos_ << std::endl;
    std::cout << "ADContinuousAIBoard::stop_continuous_ai::count::" << this->stop_count_ << std::endl;
  }

#else
  if (daq_buffer_0_)
    this->stop_count_ = this->daq_buffer_0_->depth() / 2;
  else
    this->stop_count_ = 0;
#endif // _SIMULATION_

  this->has_been_started_at_least_once_ = false;
  
  std::cout << "ADContinuousAIBoard::stop_continuous_ai ->" << std::endl;
}

// ============================================================================
// ADContinuousAIBoard::last_sample_index_in_last_daq_buffer 
// ============================================================================
unsigned long ADContinuousAIBoard::last_sample_index_in_last_daq_buffer () const
{
  return this->last_sample_index_in_last_daq_buffer_;
}

// ============================================================================
// ADContinuousAIBoard::remaining_samples 
// ============================================================================
asl::AIRawData* ADContinuousAIBoard::remaining_samples ()
    throw (asl::DAQException)
{
  //- std::cout << "ADContinuousAIBoard::remaining_samples <-" << std::endl; 

  std::cout << "ADContinuousAIBoard::remaining_samples::pos::" << this->stop_pos_ << std::endl;
  std::cout << "ADContinuousAIBoard::remaining_samples::count::" << this->stop_count_ << std::endl;
  

  //- be sure we have at least one sample to return
  if (! this->stop_count_)
    return 0;

  //- get trigger mode
  const adl::AITriggerMode tgm = this->ai_config_.get_trigger_mode();

  //- be sure pre/middle trigger condition is applied
  //- or last data buffer recovery is enabled
  if ( (tgm != adl::ai_pre) && 
       (tgm != adl::ai_middle) && 
       (!this->ai_config_.last_buffer_recovery_enabled()) )
  {
    throw asl::DAQException("invalid configuration",
                            "incompatible trigger mode (pre or middle trigger mode required) OR last buffer recovery option disabled",
                            "ADContinuousAIBoard::remaining_samples");
  }

  //- select data source 
  asl::AIRawData * data_src = (this->daq_buffer_1_ && (this->daq_buffer_idx_ % 2)) 
                               ? 
                               this->daq_buffer_1_ 
                               : 
                               this->daq_buffer_0_;

  /*
  if (data_src == this->daq_buffer_0_)
     std::cout << "ADContinuousAIBoard::remaining_samples::obtaining data from DAQ buffer 0" << std::endl;
  else
     std::cout << "ADContinuousAIBoard::remaining_samples::obtaining data from DAQ buffer 1" << std::endl;
  */

  //- be sure buffer is valid
  if (data_src == 0)
  {
    throw asl::DAQException("invalid data source",
                            "unexpected data null source - invalid buffer",
                            "ADContinuousAIBoard::remaining_samples");
  }

  //- allocate user buffer
  asl::AIRawData * raw_data = new asl::AIRawData(data_src->depth());
  if (raw_data == 0) {
    throw asl::DAQException("data buffer allocation failed",
                            "out of memory error",
                            "ADContinuousAIBoard::remaining_samples");
  }

  //- clear user buffer content
  raw_data->clear();

  //- get base addr of both buffer
  unsigned short * src_base_addr = data_src->base();
  unsigned short * dest_base_addr = raw_data->base();

  //- copy remaining samples buffer
  ::memcpy(dest_base_addr, src_base_addr + this->stop_pos_, this->stop_count_ * sizeof(unsigned short));

  //- store last sample last buffer index (pre and middle trigger)
  this->last_sample_index_in_last_daq_buffer_ = this->stop_count_ > 0 ? this->stop_count_ - 1 : 0;

  //std::cout << "ADContinuousAIBoard::remaining_samples ->" << std::endl;

  return raw_data;
}

// ============================================================================
// ADContinuousAIBoard::daq_buffers 
// ============================================================================
void ADContinuousAIBoard::daq_buffers (asl::AIRawData*& buffer_0, asl::AIRawData*& buffer_1)
    throw (asl::DAQException)
{
  if (this->daq_buffer_0_)
  {
    buffer_0 = new asl::AIRawData(*this->daq_buffer_0_);
    if (buffer_0 == 0) {
      throw asl::DAQException("data buffer allocation failed",
                              "out of memory error",
                              "ADContinuousAIBoard::daq_buffers");
    }
#if defined (_SIMULATION_)
    buffer_0->fill(999);
#endif
  }
  else
  {
    buffer_0 = 0;
  }

  if (this->daq_buffer_1_)
  {
    buffer_1 = new asl::AIRawData(*this->daq_buffer_1_);
    if (buffer_1 == 0) {
      throw asl::DAQException("data buffer allocation failed",
                              "out of memory error",
                              "ADContinuousAIBoard::daq_buffers");
    }
  }
  else
  {
    buffer_1 = 0;
  }
}  

// ============================================================================
// ADContinuousAIBoard::scale_data
// ============================================================================
void ADContinuousAIBoard::scale_data (asl::AIRawData * _raw_data, asl::AIScaledData *& scaled_data_) const 
   throw (asl::DAQException)
{
  if ( ! _raw_data ) {
    throw asl::DAQException("analog input: invalid input argument",
                            "invalid (null) pointer to raw data)",
                            "ADContinuousAIBoard::scale_data");
  }

  if ( ! scaled_data_ )
  {
    scaled_data_ = this->scale_data(_raw_data);
  }
  else if ( scaled_data_->depth() != _raw_data->depth() )
  {
    delete scaled_data_;
    scaled_data_ = this->scale_data(_raw_data);
  }
  else
  {
#if ! defined(_SIMULATION_)

  //- get range of first active channel
  adl::Range range;

  //- data scaling optimisation 
  if (this->ai_config_.active_channels_have_same_range(range)) 
  {
    ::D2K_AI_ContVScale(this->idid_, 
                        (unsigned short)range, 
                        _raw_data->base(), 
                        scaled_data_->base(),
                        _raw_data->depth());
  }
  else  
  {
    //- get active channels
    const asl::ActiveAIChannels& ac = this->ai_config_.get_active_channels();

    //- scale each data
    for (unsigned long i = 0; i < ac.size(); i++)
    {
      //- get range of the i-th active channel 
      range = ac[i].range;
      //- scale data for channel i
      for (unsigned long j = i; j < _raw_data->depth(); j += ac.size())
      {
        ::D2K_AI_VoltScale(this->idid_, 
                           range, 
                           *(_raw_data->base() + j), 
                           scaled_data_->base() + j);
      }
    }
  }

#else

  //- fill data with 5.0
  scaled_data_->fill(5.0);

#endif // _SIMULATION_
  }
}

// ============================================================================
// ADContinuousAIBoard::scale_data
// ============================================================================
asl::AIScaledData * ADContinuousAIBoard::scale_data (asl::AIRawData * _raw_data) const
    throw (asl::DAQException)
{
  if ( ! _raw_data ) {
    throw asl::DAQException("analog input: invalid input argument",
                            "invalid (null) pointer to raw data)",
                            "ADContinuousAIBoard::scale_data");
  }

  //- get active channels
  const asl::ActiveAIChannels& ac = this->ai_config_.get_active_channels();
  
  //- can't do anything if no active channel
  if (ac.size() == 0) {
    throw asl::DAQException("analog input: invalid configuration",
                            "no active AI channels",
                            "ADContinuousAIBoard::scale_data");
  }

  //- allocate buffer
  asl::AIScaledData * scaled_data = new asl::AIScaledData(_raw_data->depth());
  if (scaled_data == 0) {
    throw asl::DAQException("data buffer allocation failed",
                            "out of memory error",
                            "ADContinuousAIBoard::scale_data");
  }

#if ! defined(_SIMULATION_)

  //- get range of first active channel
  adl::Range range;

  //-----------------------------
  //- Data scaling optimisation -
  //-----------------------------

  if (this->ai_config_.active_channels_have_same_range(range)) 
  {
    ::D2K_AI_ContVScale(this->idid_, 
                        (unsigned short)range, 
                        _raw_data->base(), 
                        scaled_data->base(),
                        _raw_data->depth());
  }
  else 
  {
    //- scale each data
    for (unsigned long i = 0; i < ac.size(); i++)
    {
      //- get range of the i-th active channel 
      range = ac[i].range;
      //- scale data for channel i
      for (unsigned long j = i; j < _raw_data->depth(); j += ac.size())
      {
        ::D2K_AI_VoltScale(this->idid_, 
                           range, 
                           *(_raw_data->base() + j), 
                           scaled_data->base() + j);
      }
    }
  }

#else

  //- fill data with 66.0
  scaled_data->fill(66.0);

#endif // _SIMULATION_

  //- return scaled data
  return scaled_data;
}

} // namespace adl



