// ============================================================================
//
// = CONTEXT
//    TANGO Project - Tango Tools Library
//
// = FILENAME
//    ADContinuousAOBoard.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/adlink/ADContinuousAOBoard.h>
#include <asl/adlink/AD2502.h>

namespace adl { 

// ============================================================================
// SUPPORTED HARDWARE
// ============================================================================
#define CD_HW_SUPPORT "current impl. supports ADLink boards: DAQ-2502"

// ============================================================================
// CONSTS
// ============================================================================
#define kGROUP_A_OR_B_MAX_DEPTH   8192
#define kGROUP_A_AND_B_MAX_DEPTH 16384

// ============================================================================
// ADContinuousAOBoard::instanciate
// ============================================================================
ADContinuousAOBoard * ADContinuousAOBoard::instanciate (unsigned short _type, 
                                                        unsigned short _id,
                                                        bool _exclusive_access)
  throw (asl::DAQException)
{
	ADContinuousAOBoard * b = 0;
	
	//- type of board
	switch (_type)  
	{
		//- 2502
    case adl::DAQ2502:
		b = AD2502::instanciate (_id, _exclusive_access);
		break;
		
		//- unsupported hardware
    default:
		throw asl::DAQException("unsupported or invalid hardware type",
														CD_HW_SUPPORT,
														"ADContinuousAOBoard::instanciate");
		break;
	}
	
	return b;
}


// ============================================================================
// ADContinuousAOBoard::ADContinuousAOBoard
// ============================================================================
ADContinuousAOBoard::ADContinuousAOBoard (unsigned short _type, unsigned short _id) 
  : ADBoard(_type, _id),
    ADCalibrableAIOBoard(_type, _id),
	  ao_config_(asl::ContinuousAOConfig::get_default_config()),
    daq_buffer_idx_ (0),
    daq_buffer_0_ (0),
    daq_buffer_1_ (0),
    ao_group_setup_ (0),
    use_board_fifo_ (false)
{
  //- std::cout << "ADContinuousAOBoard::ADContinuousAOBoard <-" << std::endl;
  //- std::cout << "ADContinuousAOBoard::ADContinuousAOBoard ->" << std::endl;
}

// ============================================================================
// ADContinuousAOBoard::~ADContinuousAOBoard
// ============================================================================
ADContinuousAOBoard::~ADContinuousAOBoard()
{
	//- std::cout << "ADContinuousAOBoard::~ADContinuousAOBoard <-" << std::endl;
	 
	//- release daq buffers
	this->release_daq_buffers();
	
	//- release ao_group_setup
	if (this->ao_group_setup_) {
		delete[] this->ao_group_setup_;
		this->ao_group_setup_ = 0;
	}
	
	//- std::cout << "ADContinuousAOBoard::~ADContinuousAOBoard ->" << std::endl;
}

// ============================================================================
// ADContinuousAOBoard::configure_continuous_ao
// ============================================================================
void ADContinuousAOBoard::configure_continuous_ao (asl::ContinuousAOConfig& _ao_config)
     throw(asl::DAQException)
{
#if !defined(_SIMULATION_)
  //- check registration id (may throw an exception)
  this->check_registration();
#endif
   
  //- store config locally
  this->ao_config_ = _ao_config;  
  
	const asl::ActiveAOChannels& ac = this->ao_config_.get_active_channels();
	
  bool grp_a = false;
  bool grp_b = false;
	
  this->ao_group_setup_ = new unsigned short[ac.size()];
  if (this->ao_group_setup_ == 0) {
	  throw asl::DAQException("out of memory",
														"out of memory error",
														"ADContinuousAOBoard::configure_continuous_ao");
  }
	
	unsigned int i = 0;
	
  for (i = 0;  i < ac.size(); i++) 
  {
	  if (ac[i].id <= 3) 
	  {
		  grp_a = true;
	  }
	  else if (ac[i].id >= 4 && ac[i].id < 8)
	  {
		  grp_b = true;
	  }
	  this->ao_group_setup_[i] = ac[i].id;
  }
  
  //- setup AO group
  if (grp_a && grp_b)
  {
	  this->ao_config_.set_group(adl::group_ab);
  }
  else if (grp_a) 
  {
	  this->ao_config_.set_group(adl::group_a);
  }
  else if (grp_b)
  {
	  this->ao_config_.set_group(adl::group_b);
  }
	
#if !defined(_SIMULATION_)    

  int  err = 0;
	
  //- AO channels config -----------------------------------------------------
  //--------------------------------------------------------------------------
	
  for (i = 0;  i < ac.size();  i++)
  {
	  err = ::D2K_AO_CH_Config (this->idid_,
															ac[i].id, 
															ac[i].polarity, 
															ac[i].volt_ref_src, 
															ac[i].volt_ref);
	  if (err < 0) {
		  //- channel configuration failed
		  throw asl::DAQException("analog output channel configuration failed",
															adl::d2kdask_error_text(err),
															"AD2000::configure_continuous_ao",
															err);
	  }
  }
  
  //- AO groups setup --------------------------------------------------------
  //--------------------------------------------------------------------------
	
  err = ::D2K_AO_Group_Setup(this->idid_, 
	                           this->ao_config_.get_group(), 
	                           ac.size(), 
	                           this->ao_group_setup_);
  if (err < 0) {
	  throw asl::DAQException("group setup failed",
														adl::d2kdask_error_text(err),
														"ADContinuousAOBoard::configure_continuous_ao",
														err);
  }
	
  //- AO config --------------------------------------------------------------
  //--------------------------------------------------------------------------
  
  unsigned short config_ctrl = 
				this->ao_config_.get_conversion_source() | 
				this->ao_config_.get_group() |
				this->ao_config_.get_delay_counter_source() | 
				this->ao_config_.get_delay_break_counter_source();
  
  unsigned short trig_ctrl = 
				this->ao_config_.get_trigger_source() |
				this->ao_config_.get_trigger_mode() | 
				(this->ao_config_.retrigger_enabled() ? adl::ao_rt_enabled : adl::ao_rt_disabled) | 
				this->ao_config_.get_delay_break_mode() | 
				this->ao_config_.get_trigger_polarity(); 
  
  unsigned short delay = this->ao_config_.get_delay_counter(); 
  
  unsigned short delay_break = this->ao_config_.get_delay_break_counter();
  
  err = ::D2K_AO_Config (this->idid_, 
	                       config_ctrl, 
	                       trig_ctrl, 
	                       0,
	                       delay, 
	                       delay_break, 
	                       (BOOLEAN)0);
  if (err < 0) {
	  throw asl::DAQException("analog output failed",
														adl::d2kdask_error_text(err),
														"ADContinuousAOBoard::configure_continuous_ao",
														err);
  }
  
  //- double buffering setup -------------------------------------------------
  //--------------------------------------------------------------------------
	
  //- enable db if periodic_mode disable
  if (this->ao_config_.periodic_mode_enabled() == false) 
  {
	  //- be sure <use_board_fifo_> is false
	  this->use_board_fifo_ = false;
	  //- enable db
	  err = ::D2K_AO_AsyncDblBufferMode(this->idid_, (BOOLEAN)1);
	  if (err < 0) {
		  throw asl::DAQException("double bufferring configuration failed",
															adl::d2kdask_error_text(err),
															"ADContinuousAOBoard::configure_continuous_ao",
															err);
	  }
  }
  else
  {
	   //- be sure <use_board_fifo_> is true
	  this->use_board_fifo_ = true;
	  //- disable db
	  err = ::D2K_AO_AsyncDblBufferMode(this->idid_, (BOOLEAN)0);
	  if (err < 0) {
		  throw asl::DAQException("double bufferring configuration failed",
															adl::d2kdask_error_text(err),
															"ADContinuousAOBoard::configure_continuous_ao",
															err);
	  }
  }
	
	//- existing reset buffers
	::D2K_AO_ContBufferReset(this->idid_);
	
  //- register daq buffers
  if (this->ao_config_.periodic_mode_enabled() == false) 
  {
	  //- intialize daq buffers
	  this->init_daq_buffers();
	  
	  //- register buffer 0
	  err = ::D2K_AO_ContBufferSetup(this->idid_, 
																	 this->daq_buffer_0_->base(), 
																	 this->daq_buffer_0_->depth(), 
																	 &daq_buffer_0_id_);
	  if (err < 0) 
	  {
		  //- could not register buffer
		  throw asl::DAQException("DAQ buffer setup failed",
															adl::d2kdask_error_text(err),
															"ADContinuousAOBoard::configure_continuous_ao",
															err);
	  }
	  
	  //- register buffer 1  
	  err = ::D2K_AO_ContBufferSetup(this->idid_, 
																	 this->daq_buffer_1_->base(), 
																	 this->daq_buffer_1_->depth(), 
																	 &daq_buffer_1_id_);
	  if (err < 0) 
	  {
	    ::D2K_AO_ContBufferReset(this->idid_);
		  throw asl::DAQException("DAQ buffer setup failed",
															adl::d2kdask_error_text(err),
															"ADContinuousAOBoard::configure_continuous_ao",
															err);
	  }
  }
  //- periodic mode
  else 
  {
	  //- load periodic data into the board's fifo
	  this->load_periodic_data();
  }
	
  //- AO stop config ---------------------------------------------------------
  //--------------------------------------------------------------------------
  unsigned short grp = this->ao_config_.get_group();
  unsigned short source = ao_config_.get_stop_source();
  unsigned short mode = ao_config_.get_stop_mode();
  err = ::D2K_AO_Group_WFM_StopConfig (this->idid_, grp, source, mode);
  if (err < 0) 
  {
      throw asl::DAQException("stop mode configuration failed",
															adl::d2kdask_error_text(err),
															"ADContinuousAOBoard::configure_continuous_ao",
															err);
  }
  
  //- AO analog trigger config -----------------------------------------------
  //--------------------------------------------------------------------------
  if (this->ao_config_.get_trigger_source() == adl::external_analog) 
  {
	  //- analog trigger: control
	  unsigned short atrig_ctrl = this->ao_config_.get_analog_trigger_source() | 
		  this->ao_config_.get_analog_trigger_condition();
	  
	  //- analog trigger: high level
	  unsigned short atrig_hi_level = this->ao_config_.get_analog_high_level_condition();
	  
	  //- analog trigger: low level
	  unsigned short atrig_lo_level = this->ao_config_.get_analog_low_level_condition();
	  
	  err = ::D2K_AIO_Config(this->idid_, 
													adl::int_time_base, 
													atrig_ctrl, 
													atrig_hi_level, 
													atrig_lo_level);
	  if (err < 0) {
		  //- analog trigger configuration failed
		  throw asl::DAQException("analog input trigger configuration failed",
															adl::d2kdask_error_text(err),
															"ADContinuousAIBoard::configure_continuous_ai",
															err);
	  }
  }
  
#else // _SIMULATION_  
 
  //- register daq buffers ---------------------------------------------------
  //--------------------------------------------------------------------------

  this->init_daq_buffers();
  this->ao_group_setup_ = new unsigned short[ac.size()];
  if (this->ao_group_setup_ == 0) {
	  throw asl::DAQException("out of memory",
														"out of memory error",
														"ADContinuousAOBoard::configure_continuous_ao");
  }  
#endif // _SIMULATION_
}

// ============================================================================
// ADContinuousAOBoard::load_periodic_data
// ============================================================================
void ADContinuousAOBoard::load_periodic_data ()
  throw(asl::DAQException)
{
	std::cout << "ADContinuousAOBoard::load_periodic_data <- "<< std::endl;
	
	//- release existing buffers
	this->release_daq_buffers();
	
#if !defined(_SIMULATION_)  
    
  const asl::ActiveAOChannels& ac = this->ao_config_.get_active_channels();
  
  //----------- get user periodic data depth (for all configured channels)---------
  unsigned long total_depth = 0;
  unsigned long old_depth = 0;
  unsigned long current_depth = 0;
  int grpA = 0;
  int grpB = 0;

  for (size_t i = 0; i < ac.size(); i++)
  {
    
    //- get nb of channels per group
    if(ac[i].id < 4) //group A channel
      grpA++;
    else //group B channel
      grpB++;
    
    old_depth = current_depth;

    //- verify that a buffer has been given for every activated channel
    const asl::BaseBuffer* buf = this->ao_config_.get_channel_periodic_data(ac[i].id);
     
    if(this->ao_config_.get_channel_periodic_data(ac[i].id) == 0)
    {
      throw asl::DAQException(
        "Output buffer setup failed",
        "A buffer for a channel is missing",
        "ADContinuousAOBoard::load_periodic_data");
    }

    //- verify that all buffers have the same size
    current_depth = buf->depth();

    if( (i>0) && (current_depth != old_depth))
    {
      throw asl::DAQException(
        "Output buffer setup failed",
        "The different output buffers do not have same size",
        "ADContinuousAOBoard::load_periodic_data");
    }
    
    total_depth = current_depth + total_depth;
  }
  
  //----------can we load periodic data into board's fifo?--------------------------
  unsigned short grp = ao_config_.get_group();
  this->use_board_fifo_ = false;
	
  //- group A and B
  if (grp == adl::group_ab) 
  {
    if(total_depth <= kGROUP_A_AND_B_MAX_DEPTH)  //- FIFO mode
    {
      std::cout << "ADContinuousAOBoard::group(A and B) data fits into board's FIFO" << std::endl;
      this->use_board_fifo_ = true;
    }
    else //- DMA mode
    {
      std::cout << "ADContinuousAOBoard::group(A and B) data transfered througth DMA" << std::endl;
      //- in DMA mode, the number of channels in group A and B must be the same!
      if(grpA != grpB)
      {
        throw asl::DAQException(
          "Output buffer setup failed",
          "There must the same number of channels on each group (A and B)",
          "ADContinuousAOBoard::load_periodic_data");
      }
    }
  }
  //- group A or B
  else if(grp == adl::group_a || grp == adl::group_b)
  {
    if(total_depth <= kGROUP_A_OR_B_MAX_DEPTH) //- FIFO mode
    {
      std::cout << "ADContinuousAOBoard::group(A or B) data fits into board's FIFO" << std::endl;
      this->use_board_fifo_ = true;
    }
    else //- DMA mode
    {
      std::cout << "ADContinuousAOBoard::group(A or B) data transfered througth DMA" << std::endl;
      //- in DMA mode, double the size of the daq buffer when a single group is used.
      total_depth = total_depth * 2;
    }
  }
  
  //--------------allocate daq buffer---------------------------------------------------
  //- allocate AO buffer
  this->daq_buffer_0_ = new asl::AORawData(total_depth);
  if (this->daq_buffer_0_ == 0) 
  {
    throw asl::DAQException("AO buffers allocation failed",
      "out of memory error",
      "ADContinuousAOBoard::load_periodic_data");
  }

  //- clear AO buffer
  this->daq_buffer_0_->clear();
  
  //------------------------fill daq buffer---------------------------------------------
  for (size_t k = 0; k < ac.size(); k++)
  {
    std::cout << "ADContinuousAOBoard::load data for channel "<<ac[k].id<< std::endl;
	  //- get pointer of data from config
    const asl::AORawData* raw_buf = 
      reinterpret_cast<const asl::AORawData*>(this->ao_config_.get_channel_periodic_data(ac[k].id));
    

    if (this->ao_config_.get_data_format() == adl::scaled_data)
    {
      //- get a scaled pointer
      const asl::AOScaledData* scaled_buf = reinterpret_cast<const asl::AOScaledData*>(raw_buf);
     
      //- convert scaled data to bin data (a new buffer is allocated in this method)
      raw_buf = this->unscale_data_channel(ac[k].id, scaled_buf);
      if(raw_buf == 0)
      {
        throw asl::DAQException(
          "output buffer setup failed",
        "could not convert scaled data to raw data",
        "ADContinuousAOBoard::load_periodic_data");
      }

    }
    
    for(int j=0; j<2; j++)
      std::cout<<"ADContinuousAOBoard::load_periodic_data: ch "<<*(raw_buf->base()+j)<<std::endl;
    
    //- organize data for channel i in daq_buffer_0_ 
    int err = ::D2K_AO_ContBufferCompose(this->idid_, 
      grp,
      ac[k].id, 
      raw_buf->depth(),
      this->daq_buffer_0_->base(),
      (asl::AORawData*)raw_buf->base(),
      (BOOLEAN)this->use_board_fifo_
      );

    if (err < 0) 
    {
      throw asl::DAQException("output buffer setup failed",
        adl::d2kdask_error_text(err),
        "ADContinuousAOBoard::load_periodic_data",
        err);
    }

    if (this->ao_config_.get_data_format() == adl::scaled_data)
    {
      //- since a buffer has been allocated in unscaled_data, delete it
      if(raw_buf)
        delete raw_buf;
    }
  }

  for(int j=0; j<10; j++)
    std::cout<<"ADContinuousAOBoard::load_periodic_data: daq buffer "<<*(this->daq_buffer_0_->base()+j)<<std::endl;

  //------------------register output buffer--------------------------------------
  //- register <daq_buffer_0_>
  int err = ::D2K_AO_ContBufferSetup(this->idid_, 
    this->daq_buffer_0_->base(), 
    this->daq_buffer_0_->depth(), 
    &daq_buffer_0_id_);
  if (err < 0) 
  {
    //- could not register buffer
    throw asl::DAQException("output buffer setup failed",
      adl::d2kdask_error_text(err),
      "ADContinuousAOBoard::load_periodic_data",
      err);
  }

  //------------------load data on board's FIFO if possible--------------------------------
  if (this->use_board_fifo_) 
  {
    std::cout << "ADContinuousAOBoard::loading periodic data in board's FIFO" << std::endl;
    //- load periodic data
    err = ::D2K_AO_Group_FIFOLoad (this->idid_, 
      grp,
																	 daq_buffer_0_id_,
																	 daq_buffer_0_->depth());
    if (err < 0) 
    {
			::D2K_AO_ContBufferReset(this->idid_);
      throw asl::DAQException("could not load periodic output data into hardware FIFO",
															adl::d2kdask_error_text(err),
															"ADContinuousAOBoard::load_periodic_data",
															err);
    }
  } 
	
 std::cout << "ADContinuousAOBoard::load_periodic_data ->" << std::endl;
	
#endif // _SIMULATION_
}

// ============================================================================
// ADContinuousAOBoard::init_daq_buffers
// ============================================================================
void ADContinuousAOBoard::init_daq_buffers ()
     throw(asl::DAQException)
{
	//- nothing to do if <periodic_mode_enabled>
	if (this->ao_config_.periodic_mode_enabled() == true) {
		return;
	}
	
	//- release buffers
	this->release_daq_buffers();

  unsigned long depth;

  depth = this->ao_config_.get_buffer_depth();

	//- be sure buffer size is even
	if (depth % 2) {
		//- throw exception because user must be informed
		throw asl::DAQException("invalid DAQ buffer depth",
														"analog output buffer depth must be even",
														"ADContinuousAOBoard::init_daq_buffers");
	}
	
	//- if group a or group b -> double the size of the buffer
  if(this->ao_config_.get_group() == adl::group_a || this->ao_config_.get_group() == adl::group_b)
  {
    depth *= 2;
  }

	//- allocate buffer 0
	this->daq_buffer_0_ = new asl::AORawData(depth);
	if (this->daq_buffer_0_ == 0) 
	{
		throw asl::DAQException("AO buffers allocation failed",
														"out of memory error",
														"ADContinuousAOBoard::init_daq_buffers");
	}
	
	//- allocate buffer 1
	this->daq_buffer_1_ = new asl::AORawData(depth);
	if (this->daq_buffer_1_ == 0) 
	{
		delete this->daq_buffer_0_;
		this->daq_buffer_0_ = 0;
		throw asl::DAQException("AO buffers allocation failed",
														"out of memory error",
														"ADContinuousAOBoard::init_daq_buffers");
	}
	
	//- clear buffers content
	this->daq_buffer_0_->clear();
	this->daq_buffer_1_->clear();
}

// ============================================================================
// ADContinuousAOBoard::release_daq_buffers
// ============================================================================
void ADContinuousAOBoard::release_daq_buffers ()
{
  //- std::cout<<" ADContinuousAOBoard::release_daq_buffers<-"<<std::endl;
	if (this->daq_buffer_0_) {
		  //- std::cout<<"delete daq_buffer_0_"<<std::endl;
		delete daq_buffer_0_;
		daq_buffer_0_ = 0;
	}
	
	if (this->daq_buffer_1_) {
    //- std::cout<<"delete daq_buffer_1_"<<std::endl;
		delete daq_buffer_1_;
		daq_buffer_1_ = 0;
	}
  //- std::cout<<" ADContinuousAOBoard::release_daq_buffers->"<<std::endl;
}

// ============================================================================
// ADContinuousAOBoard::start_continuous_ao
// ============================================================================
void ADContinuousAOBoard::start_continuous_ao ()
     throw(asl::DAQException)
{
	//- reset buffer index
	this->daq_buffer_idx_ = 0;
	
#if !defined(_SIMULATION_)
  //- check registration id (may throw an exception)
	this->check_registration();


  //- std::cout << "ADContinuousAOBoard::start_continuous_ao <- " << std::endl;
	
	int err = 0;
	
	//- install db event callback for double buffering
	if (this->ao_config_.periodic_mode_enabled() == false)
	{
		//- be sure we have a valid callback
		if (this->ao_config_.get_db_event_callback() == 0) {
			throw asl::DAQException("invalid configuration",
															"invalid callback address specified",
															"ADContinuousAOBoard::start_continuous_ao");
		}
		
#if defined (WIN32)
		//- install db event handler
		err = ::D2K_AO_EventCallBack (this->idid_, 
			                            1, 
			                            adl::db_evt, 
			                            (U32)this->ao_config_.get_db_event_callback());
#else
		//- install db event handler
		err = ::D2K_AO_EventCallBack (this->idid_, 
			                            1, 
			                            adl::db_evt, 
			                            this->ao_config_.get_db_event_callback());
#endif
		
		if (err < 0) {
			throw asl::DAQException("DAQ event handler registration failed",
															adl::d2kdask_error_text(err),
															"ADContinuousAOBoard::start_continuous_ao",
															err);
		}
	}
	
	//- get user specified daq rate
	double ao_rate = this->ao_config_.get_output_rate();
	
	//- convert from sample rate (in samples/s) to sample interval (clock-ticks)
  unsigned long samp_intrv = (unsigned long)(this->clock_frequency() / ao_rate);
  
  //- compute num. of output scans per channel 
  unsigned long update_count = this->daq_buffer_0_->depth() 
      / this->ao_config_.get_active_channels().size();
  
  unsigned short grp = ao_config_.get_group();
  //- the size is divided by 2 for gpr A or B since the daq buffer size has been double in this case
  if ((this->use_board_fifo_ == false) && (grp == adl::group_a || grp == adl::group_b)) {
    update_count /= 2;
  }
														 
  std::cout << "ADContinuousAOBoard::start_continuous_ao::AO buffer depth is " << this->daq_buffer_0_->depth() << " samples" << std::endl;												 
  std::cout << "ADContinuousAOBoard::start_continuous_ao::AO buffer contains " << update_count << " samples/channel" << std::endl;
													 
  const unsigned short buffer_not_used = static_cast<unsigned short>(-1);
 
	//- periodic output using board's FIFO 
	if (this->use_board_fifo_)
	{	
		if (ao_config_.retrigger_enabled())
		{              
			err = ::D2K_AO_Group_WFM_Start (this->idid_, 
																		  this->ao_config_.get_group(), 
																		  buffer_not_used, 
																		  buffer_not_used, 
																		  update_count, 
																		  this->ao_config_.get_nb_waveforms(), 
																		  samp_intrv, 
																		  1);
		}
		else
		{
			err = ::D2K_AO_Group_WFM_Start (this->idid_, 
																			this->ao_config_.get_group(),
																			buffer_not_used,
																			buffer_not_used, 
																			update_count, 
																			1, 
																			samp_intrv, 
																			0);
		}
	}
	//- double buffering or periodic output using DMA
	else 
	{
		//- double buffering using DMA
		if (this->ao_config_.periodic_mode_enabled() == false)
		{
			err = ::D2K_AO_Group_WFM_Start (this->idid_, 
																			this->ao_config_.get_group(), 
																			this->daq_buffer_0_id_, 
																			this->daq_buffer_1_id_, 
																			update_count, 
																			1, 
																			samp_intrv, 
																			0);
		}
		//- periodic output using DMA
		else
		{
			if (ao_config_.retrigger_enabled())
			{
				err = ::D2K_AO_Group_WFM_Start (this->idid_,
																				this->ao_config_.get_group(),
																				this->daq_buffer_0_id_,
																				buffer_not_used, 
																				update_count,
																				this->ao_config_.get_nb_waveforms(),
																				samp_intrv,
																				1);
			}
			else
			{
				err = ::D2K_AO_Group_WFM_Start (this->idid_,
																				this->ao_config_.get_group(),
																				this->daq_buffer_0_id_,
																				buffer_not_used, 
																				update_count, 
																				1, 
																				samp_intrv, 
																				0);
			}
		}
	} 
	
	if (err < 0) {
		::D2K_AO_ContBufferReset(this->idid_);
		throw asl::DAQException("DAQ start failed",
														adl::d2kdask_error_text(err),
														"ADContinuousAOBoard::start_continuous_ao",
														err);
	}  
	
	//- std::cout << "ADContinuousAOBoard::start_continuous_ao ->" << std::endl;
		
#endif // _SIMULATION_
}

// ============================================================================
// ADContinuousAOBoard::ao_db_event_callback
// ============================================================================
void ADContinuousAOBoard::ao_db_event_callback (asl::BaseBuffer* ao_data, bool delete_data, void* arg)
     throw (asl::DAQException)
{
	ACE_UNUSED_ARG(arg);
	//- be sure <ao_data> is valid
	if (ao_data == 0) {
		return;
	}
	
	asl::AORawData* raw_data = 0;
	
	// convert data
	if (this->ao_config_.get_data_format() == adl::scaled_data)
	{
		raw_data = this->unscale_data(reinterpret_cast<asl::AOScaledData*>(ao_data));
	}
	else
	{
		raw_data = reinterpret_cast<asl::AORawData*>(ao_data); 
	}
	
	//- select output buffer
	unsigned short * base = ((this->daq_buffer_idx_ % 2) == 0)
							              ?
							             this->daq_buffer_0_->base()
							              :
	                         this->daq_buffer_1_->base();
	
  //- copy AO data from user to DAQ buffer (taking into account the 32bits wide DMA mechanism)
	switch (this->ao_config_.get_group())
	{ 
	  case adl::group_ab:
      {
        ACE_OS::memcpy(base, raw_data->base(), this->daq_buffer_0_->size());
      }
			break;
	  case adl::group_a:
      {
		    for (unsigned long i = 0, j = 0; j < this->daq_buffer_0_->depth(); i++, j += 2)
			    *(base + j) = *(raw_data->base() + i);
      }
			break;
	  case adl::group_b:
      {
		    for (unsigned long i = 0, j = 1; j < this->daq_buffer_0_->depth(); i++, j += 2)
			    *(base + j) = *(raw_data->base() + i);
      }
			break;
  }

	//- update daq_buffer_idx
	this->daq_buffer_idx_ = (++this->daq_buffer_idx_) % 2;
	
	//- are we allowed to delete the data ?
	if (delete_data) 
	{
		//- yes we are...
		delete ao_data; 
	}
	
	if (this->ao_config_.get_data_format() == adl::scaled_data)
	{
		delete raw_data;
	}
}

// ============================================================================
// ADContinuousAOBoard::stop_continuous_ao
// ============================================================================
void ADContinuousAOBoard::stop_continuous_ao ()
     throw(asl::DAQException)
{
  
	
#if !defined(_SIMULATION_)
  //- check registration id (may throw an exception)
	this->check_registration();

  //- here we try to do all required actions before handling errors.

	//- stop daq 
	U32 count;
	int stop_err = ::D2K_AO_Group_WFM_AsyncClear(this->idid_, 
		                                           this->ao_config_.get_group(), 
		                                           &count, 
                                               this->ao_config_.get_stop_mode());

	//- reset <daq_buffer_0_>
	int buf_err = ::D2K_AO_ContBufferReset(this->idid_);
	
  int cb_err = 0;
	//- remove event handler 
	if (this->ao_config_.periodic_mode_enabled() == false)
	{
    //- cb_err = ::D2K_AO_EventCallBack(this->idid_, 0, adl::db_evt, 0);
  }
	
  //- handle stop error first ...
  if (stop_err < 0) 
  {
    throw asl::DAQException("DAQ stop failed",
														adl::d2kdask_error_text(stop_err),
														"ADContinuousAOBoard::stop_continuous_ao",
														stop_err);
  }

  //- ...then handle buffer reset error 
	if (buf_err < 0) 
	{
		//- could not register buffer
		throw asl::DAQException("reset buffer failed",
			                      adl::d2kdask_error_text(buf_err),
			                      "ADContinuousAOBoard::stop_continuous_ao",
			                      buf_err);
	}     

  //- ... finally, handle "event removing" error
	if (cb_err < 0) 
  {
		throw asl::DAQException("could not remove event handler",
			                      adl::d2kdask_error_text(cb_err),
			                      "ADContinuousAOBoard::stop_continuous_ao",
			                      cb_err);
	}

#endif // _SIMULATION_
}
// ============================================================================
// ADContinuousAOBoard::unscale_data
// ============================================================================
asl::AORawData * ADContinuousAOBoard::unscale_data (const asl::AOScaledData * _scaled_data) const
    throw (asl::DAQException)
{
  //- std::cout<<"ADContinuousAOBoard::unscale_data<-"<<std::endl;
	//- get active channels
	const asl::ActiveAOChannels& ac = this->ao_config_.get_active_channels();
	
	//- can't do anything if no active channel
	if (ac.size() == 0) {
    throw asl::DAQException("analog input: invalid configuration",
								            "no active AO channels",
								            "ADContinuousAOBoard::unscale_data");
	}
	
	//- allocate buffer
	asl::AORawData * raw_data = new asl::AORawData(_scaled_data->depth());
	if (raw_data == 0) {
		throw asl::DAQException("data buffer allocation failed",
			                      "out of memory error",
			                      "ADContinuousAOBoard::unscale_data");
	}
	
#if !defined(_SIMULATION_)

  //- scale each data
  double v_ref ;
  //nb of activated channels of group A
  unsigned int grpA_size = 0;
  ////nb of activated channels of group B
  unsigned int grpB_size = 0;
  
  unsigned int i = 0;
  unsigned int j = 0; 

  for (i = 0; i < ac.size(); i++)
  {
    if (ac[i].id < 4)
      grpA_size++;
    else 
      grpB_size++;
  }
  
  if (this->ao_config_.get_group() == adl::group_ab)
  {
    
    //- convert groupA
    for (i = 0; i < grpA_size; i++)
    {
      v_ref = ac[i].volt_ref;
      //- std::cout<<"convert grp a channel "<<ac[i].id<<std::endl;
      //unscale data for channel i of groupA
      if(ac[i].polarity == adl::bipolar)
      {
        // groupA channels are in _scaled_data[even] 
        for (j = i * 2; j < _scaled_data->depth(); j = j + grpA_size*2) 
        {
          *(raw_data->base() + j) = 
					    static_cast<unsigned short>((*(_scaled_data->base() + j) * 2048) / v_ref + 2048);
        }
      }
      else //unipolar
      {
        for (j= i * 2; j < _scaled_data->depth(); j = j + grpA_size * 2) 
        {
          *(raw_data->base() + j) = 
					    static_cast<unsigned short>((*(_scaled_data->base() + j) * 4096) / v_ref);
        }
      }
    }
    
    //convert groupB
    for (i = grpA_size; i < (grpB_size + grpA_size); i++)
    {
      v_ref = ac[i].volt_ref;
      //- std::cout<<"convert grp b channel "<<ac[i].id<<std::endl;
      //- unscale data for channel i of groupB
      if(ac[i].polarity == adl::bipolar)
      {
        // groupB channels are in _scaled_data[odd] 
        for (j = i * 2 + 1; j < _scaled_data->depth(); j += 2 * grpB_size) 
        {
          *(raw_data->base() + j) = 
					    static_cast<unsigned short>((*(_scaled_data->base() +j) * 2048) / v_ref + 2048);
        }
      }
      else //- unipolar
      {
        for (j = i * 2 + 1; j < _scaled_data->depth(); j += 2 * grpB_size) 
        {
          *(raw_data->base() + j) = 
					    static_cast<unsigned short>((*(_scaled_data->base() + j) * 4096) / v_ref);
        }
      }
    }
  } 
  else //group a or b
  {
    for (i = 0; i < ac.size(); i++)
    {
      v_ref = ac[i].volt_ref;
      //- std::cout<<"convert channel "<<ac[i].id<<std::endl;
      if(ac[i].polarity == adl::bipolar)
      {
        
        //- scale data for channel i
        for (j = i; j < _scaled_data->depth(); j += ac.size())
        {
          *(raw_data->base() + j) = 
					    static_cast<unsigned short>((*(_scaled_data->base() +j) * 2048) / v_ref + 2048);
        }
      }
      else
      {
        
        //- scale data for channel i
        for (j = i; j < _scaled_data->depth(); j += ac.size())
        {
          *(raw_data->base() + j) = 
					    static_cast<unsigned short>((*(_scaled_data->base() + j) * 4096) / v_ref);
        }
      }
    }  
  }
 
#else
	
	//- clear buffer content
	raw_data->clear();
	
#endif // _SIMULATION_
	//- std::cout<<"ADContinuousAOBoard::unscale_data->"<<std::endl;
	//- return scaled data
	return raw_data;
}
// ============================================================================
// ADContinuousAOBoard::unscale_data_channel
// ============================================================================
asl::AORawData * ADContinuousAOBoard::unscale_data_channel (unsigned long _chan, const asl::AOScaledData * _scaled_data) const
    throw (asl::DAQException)
{
  //- std::cout<<"ADContinuousAOBoard::unscale_data_channel<-"<<std::endl;
	//- get active channels
	const asl::ActiveAOChannels& ac = this->ao_config_.get_active_channels();
	
	//- can't do anything if no active channel
	if (ac.size() == 0) {
    throw asl::DAQException("analog input: invalid configuration",
								            "no active AO channels",
								            "ADContinuousAOBoard::unscale_data");
	}
	
	//- allocate buffer
	asl::AORawData * raw_data = new asl::AORawData(_scaled_data->depth());
	if (raw_data == 0) {
		throw asl::DAQException("data buffer allocation failed",
			                      "out of memory error",
			                      "ADContinuousAOBoard::unscale_data");
	}
  
#if !defined(_SIMULATION_)
  
  //- scale each data
  int i = 0;
  while(ac[i].id != _chan)
  {
    i++;
  }
  double v_ref = ac[i].volt_ref;
  
  if(ac[i].polarity == adl::bipolar)
  {
    for (size_t j = 0; j < _scaled_data->depth(); j++) 
    {
      *(raw_data->base() + j) = 
        static_cast<unsigned short>((*(_scaled_data->base() + j) * 2048) / v_ref + 2048);
    }
  }
  else //unipolar
  {
    for (size_t j = 0; j < _scaled_data->depth(); j++) 
    {
      *(raw_data->base() + j) = 
        static_cast<unsigned short>((*(_scaled_data->base() + j) * 4096) / v_ref);
    }
  }
 
#else
	
	//- clear buffer content
	raw_data->clear();
	
#endif // _SIMULATION_
	//- std::cout<<"ADContinuousAOBoard::unscale_data_channel->"<<std::endl;
	//- return scaled data
	return raw_data;
}
} // namespace adl



