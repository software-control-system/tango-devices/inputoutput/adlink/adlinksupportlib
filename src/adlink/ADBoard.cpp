// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ADBoard.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <iostream>
#include <asl/adlink/ADLink.h>
#include <asl/adlink/ADBoard.h>

#if !defined (__ASL_INLINE__)
# include <asl/adlink/ADBoard.i>
#endif // __ASL_INLINE__

namespace adl { 

// ============================================================================
// ADBoard::ADBoard
// ============================================================================
ADBoard::ADBoard (unsigned short _type, unsigned short _id) 
	: asl::SharedObject(), 
		type_ (_type), 
		id_ (_id), 
		idid_ (kIMPOSSIBLE_IID),
		exclusive_access_ (false)
{
  //- std::cout << "ADBoard::ADBoard <-" << std::endl;
  //- std::cout << "ADBoard::ADBoard ->" << std::endl;
}

// ============================================================================
// ADBoard::~ADBoard
// ============================================================================
ADBoard::~ADBoard ()
{
  //- std::cout << "ADBoard::~ADBoard <-" << std::endl;
  //- std::cout << "ADBoard::~ADBoard ->" << std::endl;
}

// ============================================================================
// ADBoard::dump
// ============================================================================
void ADBoard::dump () const 
{
  //- noop
}

} // namespace adl







