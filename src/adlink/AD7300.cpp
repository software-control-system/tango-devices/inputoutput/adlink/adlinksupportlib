// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    AD7300.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/adlink/AD7300.h>

namespace adl {  

// ============================================================================
// CONSTS
// ============================================================================

// ============================================================================
// STATICS
// ============================================================================
AD7300::Repository AD7300::repository;
ACE_Recursive_Thread_Mutex AD7300::repository_lock;

// ============================================================================
// AD7300::instanciate
// ============================================================================
AD7300* AD7300::instanciate (unsigned short _id, bool _exclusive_access)
    throw (asl::DAQException)
{
  //- lock
  ACE_GUARD_REACTION(ACE_Recursive_Thread_Mutex, 
                     ace_mon, 
                     AD7300::repository_lock, 
                     throw asl::DAQException());

  //- search requested board in the repository
  RepositoryIterator it = AD7300::repository.find(_id);

  //- already instnciated?
  if (it != AD7300::repository.end()) {
    //- can't get ownership if _exclusive_access requested
    if (_exclusive_access || it->second->exclusive_access_) {
      //- can't get exclusive access to hardware
      throw asl::DAQException("hardware already in use",
                              "could not get exclusive access to the hardware",
                              "AD7300::instanciate");
    }
    //- share the board
    else {
      return it->second->duplicate();
    }
  }

  //- instanciate new board
  AD7300* b = new AD7300(_id);
  if (b == 0) {
    //- out of memory
    throw asl::DAQException("out of memory",
                            "out of memory error",
                            "AD7300::instanciate");
  }

  //- update board's <exclusive_access> flag
  b->exclusive_access_ = _exclusive_access;

  int err = b->register_hardware();
  if (err < 0) {
    //- release board
    b->release();
		//- reset returned pointer
	  b = 0;
    //- hardware registration failed
    throw asl::DAQException("hardware registration failed",
                            adl::pcisdask_error_text(err),
                            "AD7300::instanciate",
                            err);
  }

  return b;
}

// ============================================================================
// AD7300::duplicate
// ============================================================================
AD7300 * AD7300::duplicate ()
{
  return dynamic_cast<AD7300*>(this->SharedObject::duplicate()); 
}
 
// ============================================================================
// AD7300::AD7300
// ============================================================================
AD7300::AD7300(unsigned short _id) 
  : ADBoard(adl::PCI7300, _id),
    ADSingleShotDIBoard(adl::PCI7300, _id),
    ADSingleShotDOBoard(adl::PCI7300, _id),
    ADContinuousDIBoard(adl::PCI7300, _id)
{
  //- store board in the repository
  RepositoryValue pair(_id, this);
  AD7300::repository.insert(pair);
}

// ============================================================================
// AD7300::~AD7300
// ============================================================================
AD7300::~AD7300 ()
{
  //- lock
  ACE_GUARD_REACTION(ACE_Recursive_Thread_Mutex, 
                     ace_mon, 
                     AD7300::repository_lock, 
                     throw asl::DAQException());
										 
  //- release hardware
  this->release_hardware();

  //- remove board from repository
  RepositoryIterator it = AD7300::repository.find(this->id_);
  if (it != AD7300::repository.end()) {
    AD7300::repository.erase(it);    
  }
}

// ============================================================================
// AD7300::register_hardware
// ============================================================================
int AD7300::register_hardware ()
{
#if !defined(_SIMULATION_) 

  int bid = ::Register_Card(PCI_7300A_RevB, this->id_);
  if (bid < 0) {
	  this->idid_ = kIMPOSSIBLE_IID;
		return bid;
	}
  this->idid_ = bid;
  return 0;

#else // _SIMULATION_
 
  return this->id_;

#endif // _SIMULATION_
}

// ============================================================================
// AD7300::release_hardware
// ============================================================================
int AD7300::release_hardware ()
{
  //- be sure the board is registered
  if (this->idid_ == kIMPOSSIBLE_IID) {
    return 0;
  }

#if !defined(_SIMULATION_) 

  return ::Release_Card(this->idid_);

#else // _SIMULATION_

  return 0;
  
#endif // _SIMULATION_
}

// ============================================================================
//AD7300::configure_continuous_di
// ============================================================================
void AD7300::configure_continuous_di (const asl::ContinuousDIConfig& _di_config)
    throw (asl::DAQException)
{
  //- config card -------------------------------------------------------
  //--------------------------------------------------------------------------
   di_config_ = _di_config;
#if !defined(_SIMULATION_)

  unsigned short ctrl;
  ctrl  = _di_config.get_request_polarity();
  ctrl |= _di_config.get_ack_polarity();
  ctrl |= _di_config.get_trigger_polarity();

  int err = ::DI_7300B_Config (this->idid_, 
                               _di_config.get_port_width(), 
                               _di_config.get_trigger_source(), 
                               _di_config.get_start_mode(), 
                               _di_config.get_terminator(), 
                               ctrl, 
                               (BOOLEAN)0,
                               (BOOLEAN)0);
  if (err < 0) {
      throw asl::DAQException("configuration failed",
                              adl::pcisdask_error_text(err),
                              "AD7300::configure_continuous_di",
                              err);
  }

#endif // _SIMULATION_  

  //- init daq buffers -------------------------------------------------------
  //--------------------------------------------------------------------------
  
  if (this->init_daq_buffers() == -1) {
    //- could not allocate buffers
    throw asl::DAQException("DAQ buffers allocation failed",
                            "out of memory error",
                            "AD7300::configure_continuous_di");
  }
 
#if !defined(_SIMULATION_) 

  unsigned short buffer_id = 0; 
  
  //- setup buffers
  switch(_di_config.get_port_width())
  {
    //- use 8 bits DI port
    case adl::width_8:
    {
      err = ::DI_ContMultiBufferSetup(this->idid_, 
                      reinterpret_cast<asl::DIByteData*>(this->daq_buffer_0_)->base(),
                      reinterpret_cast<asl::DIByteData*>(this->daq_buffer_0_)->depth(), 
                      &buffer_id); 
      if (err < 0) {
        throw asl::DAQException("configuration failed",
                                adl::pcisdask_error_text(err),
                                "AD7300::configure_continuous_di",
                                err);
      }
      err = ::DI_ContMultiBufferSetup(this->idid_, 
                      reinterpret_cast<asl::DIByteData*>(this->daq_buffer_1_)->base(),
                      reinterpret_cast<asl::DIByteData*>(this->daq_buffer_1_)->depth(), 
                      &buffer_id);
      if (err < 0) {
        throw asl::DAQException("configuration failed",
                                adl::pcisdask_error_text(err),
                                "AD7300::configure_continuous_di",
                                err);
      }
    }
    break;
    //- use 16 bits DI port
    case adl::width_16:
    {
      err = ::DI_ContMultiBufferSetup(this->idid_, 
                      reinterpret_cast<asl::DIShortData*>(this->daq_buffer_0_)->base(),
                      reinterpret_cast<asl::DIShortData*>(this->daq_buffer_0_)->depth(), 
                      &buffer_id);
      if (err < 0) {
        throw asl::DAQException("configuration failed",
                                adl::pcisdask_error_text(err),
                                "AD7300::configure_continuous_di",
                                err);
      }
      err = ::DI_ContMultiBufferSetup(this->idid_, 
                      reinterpret_cast<asl::DIShortData*>(this->daq_buffer_1_)->base(),
                      reinterpret_cast<asl::DIShortData*>(this->daq_buffer_1_)->depth(), 
                      &buffer_id);
      if (err < 0) {
        throw asl::DAQException("configuration failed",
                                adl::pcisdask_error_text(err),
                                "AD7300::configure_continuous_di",
                                err);
      }
    }
    break;
    //- use 32 bits DI port
    case adl::width_32:
    {
      err = ::DI_ContMultiBufferSetup(this->idid_, 
                      reinterpret_cast<asl::DILongData*>(this->daq_buffer_0_)->base(),
                      reinterpret_cast<asl::DILongData*>(this->daq_buffer_0_)->depth(), 
                      &buffer_id);
      if (err < 0) {
        throw asl::DAQException("configuration failed",
                                adl::pcisdask_error_text(err),
                                "AD7300::configure_continuous_di",
                                err);
      }
      err = ::DI_ContMultiBufferSetup(this->idid_, 
                      reinterpret_cast<asl::DILongData*>(this->daq_buffer_1_)->base(),
                      reinterpret_cast<asl::DILongData*>(this->daq_buffer_1_)->depth(), 
                      &buffer_id);
      if (err < 0) {
        throw asl::DAQException("configuration failed",
                                adl::pcisdask_error_text(err),
                                "AD7300::configure_continuous_di",
                                err);
      }
    }
    break;
    //- undefined port width
    default:
    {
      throw asl::DAQException("invalid configuration",
                              "invalid DI port width specified",
                              "AD7300::configure_continuous_di");
    }
    break;
  }
 
#endif // _SIMULATION_
}

// ============================================================================
// AD7300::check_di_port
// ============================================================================
void AD7300::check_di_port (adl::DIOPort _port)
    throw (asl::DAQException)
{
  if (_port != adl::aux_di)
  {
	  throw asl::DAQException("invalid DI port specified",
                            "specified DI port is out of range for board PCI-7300",
                            "AD7300::check_di_port");
  }
}

// ============================================================================
// AD7300::check_di_line
// ============================================================================
void AD7300::check_di_line (int _line)
    throw (asl::DAQException)
{
  if (_line < AD7300_MIN_LINE || _line > AD7300_MAX_LINE)
  {
	  throw asl::DAQException("invalid DI line specified",
                            "specified DI port is out of range for board PCI-7300",
                            "AD7300::check_di_line");
  }
}

// ============================================================================
// AD7300::check_do_port
// ============================================================================
void AD7300::check_do_port (adl::DIOPort _port)
    throw (asl::DAQException)
{
  if (_port != adl::aux_do)
  {
	  throw asl::DAQException("invalid DO port specified",
                            "specified DO port is out of range for board PCI-7300" ,
                            "AD7300::check_do_port");
  }
}

// ============================================================================
// AD7300::check_do_line
// ============================================================================
void AD7300::check_do_line (int _line)
    throw (asl::DAQException)
{
  if (_line <AD7300_MIN_LINE || _line > AD7300_MAX_LINE)
  {
	  throw asl::DAQException("invalid DO line",
                            "specified DO line is out of range for board PCI-7300",
                            "AD7300::check_do_line");
  }
}

// ============================================================================
// AD7300::configure_di_port
// ============================================================================
void AD7300::configure_di_port (adl::DIOPort _port)
    throw (asl::DAQException)
{
 ACE_UNUSED_ARG(_port);
 throw asl::DAQException("unsupported feature",
                         "DIO ports are not user configurable",
                         "AD7300::configure_port");
}

// ============================================================================
// AD7300::configure_do_port
// ============================================================================
void AD7300::configure_do_port (adl::DIOPort _port)
    throw (asl::DAQException)
{
 ACE_UNUSED_ARG(_port);
 throw asl::DAQException("unsupported feature",
                         "DIO ports are not user configurable",
                         "AD7300::configure_port");
}

// ============================================================================
// AD7300::read_line
// ============================================================================
bool AD7300::read_line (adl::DIOPort _port, int _line)
    throw (asl::DAQException)
{
  this->check_di_port(_port);

  this->check_di_line(_line);

/*	ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());*/

  unsigned short state = 0;

#if !defined(_SIMULATION_) 

	int err = ::DI_ReadLine(this->idid_, AD7300_READ_PORT, _line, &state);

	if (err < 0) 
	{
    throw asl::DAQException("could not read DI line",
                            adl::pcisdask_error_text(err),
                            "AD7300::read_line",
                            err);
	}

#endif // _SIMULATION_

 return state ? true : false;
}

// ============================================================================
// AD7300::read_port
// ============================================================================
unsigned long AD7300::read_port (adl::DIOPort _port)
    throw (asl::DAQException)
{
  this->check_di_port(_port);

/*	ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());*/

  unsigned long state = 0;

#if !defined(_SIMULATION_) 	

  int err = ::DI_ReadPort(this->idid_, AD7300_READ_PORT, &state);

	if (err < 0) 
	{
	  throw asl::DAQException("could not read DI port",
                            adl::pcisdask_error_text(err),
                            "AD7300::read_port",
                            err);
	}

#endif // _SIMULATION_

  return state;
}

// ============================================================================
// AD7300::write_line
// ============================================================================
void AD7300::write_line (adl::DIOPort _port, int _line, bool _state)
    throw (asl::DAQException)
{
  this->check_do_port(_port);

  this->check_do_line(_line);

/*	ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());*/

#if !defined(_SIMULATION_) 

	unsigned short state = _state ? true : false;

	int err = ::DO_WriteLine(this->idid_, AD7300_WRITE_PORT, _line, state);

	if (err < 0) 
	{
		throw asl::DAQException("could not write DO line",
                            adl::pcisdask_error_text(err),
                            "AD7300::write_line",
                            err);
	}

#endif // _SIMULATION_
}

// ============================================================================
// AD7300::write_port
// ============================================================================
void AD7300::write_port (adl::DIOPort _port, unsigned long _state)
    throw (asl::DAQException)
{
  this->check_do_port(_port);

/*	ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());*/

	//- mask unused bits (4 bits port)
  _state &= 0x000F;

#if !defined(_SIMULATION_) 	

	int err = ::DO_WritePort(this->idid_, AD7300_WRITE_PORT,  _state);

	if (err < 0) 
	{
    throw asl::DAQException("write port failed",
                            adl::pcisdask_error_text(err),
                            "AD7300::write_port",
                            err);
	}

#endif // _SIMULATION_
}

// ============================================================================
// AD7300::dump
// ============================================================================
void AD7300::dump () const
{
  ACE_DEBUG ((LM_DEBUG, ". manufacturer.......ADLink\n"));
  ACE_DEBUG ((LM_DEBUG, ". type...............PCI-7300\n"));
  ACE_DEBUG ((LM_DEBUG, ". board id...........%d\n", this->id_));
  ACE_DEBUG ((LM_DEBUG, ". registration id....%d\n", this->idid_));
  ACE_DEBUG ((LM_DEBUG, ". exclusive access...%s\n", this->exclusive_access_ ? "yes" : "no"));
}

} // namespace adl


