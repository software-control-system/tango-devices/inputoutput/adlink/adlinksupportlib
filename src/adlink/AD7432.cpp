// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    AD7432.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/adlink/AD7432.h>

namespace adl { 

// ============================================================================
// CONSTS
// ============================================================================
#define AD7432_READ_PORT  0
#define AD7432_WRITE_PORT 0 

// ============================================================================
// STATICS
// ============================================================================
AD7432::Repository AD7432::repository;
ACE_Recursive_Thread_Mutex AD7432::repository_lock;

// ============================================================================
// AD7432::instanciate
// ============================================================================
AD7432* AD7432::instanciate (unsigned short _id, bool _exclusive_access)
    throw (asl::DAQException)
{
  //- lock
  ACE_GUARD_REACTION(ACE_Recursive_Thread_Mutex, 
                     ace_mon, 
                     AD7432::repository_lock, 
                     throw asl::DAQException());

  //- search requested board in the repository
  RepositoryIterator it = AD7432::repository.find(_id);

  //- already instnciated?
  if (it != AD7432::repository.end()) 
  {
    //- can't get ownership if _exclusive_access requested
    if (_exclusive_access || it->second->exclusive_access_) 
	  {
      //- can't get exclusive access to hardware
      throw asl::DAQException("hardware already in use",
                              "could not get exclusive access to the hardware",
                              "AD7432::instanciate");
    }
    //- share the board
    else
	  {
      return it->second->duplicate();
    }
  }

  //- instanciate new board
  
  AD7432* b = new AD7432(_id);
  if (b == 0) 
  {
    //- out of memory
    throw asl::DAQException("out of memory",
                            "out of memory error",
                            "AD7432::instanciate");
  }

  //- update board's <exclusive_access> flag
  b->exclusive_access_ = _exclusive_access;

  int err = b->register_hardware();
  if (err < 0) 
  {
    //- release board
    b->release();
	  //- reset returned pointer
	  b = 0;
    //- hardware registration failed
    throw asl::DAQException("hardware registration failed",
                            adl::pcisdask_error_text(err),
                            "AD7432::instanciate",
                            err);
  }

  return b;
}

// ============================================================================
// AD7432::duplicate
// ============================================================================
AD7432 * AD7432::duplicate ()
{
  return dynamic_cast<AD7432*>(this->SharedObject::duplicate()); 
}
 
// ============================================================================
// AD7432::AD7432
// ============================================================================
AD7432::AD7432 (unsigned short _id) 
  : ADBoard(adl::PCI7432, _id),
    ADSingleShotDIBoard(adl::PCI7432, _id),
    ADSingleShotDOBoard(adl::PCI7432, _id)
{
  //- store board in the repository
  RepositoryValue pair(_id, this);
  AD7432::repository.insert(pair);
}

// ============================================================================
// AD7432::~AD7432
// ============================================================================
AD7432::~AD7432()
{
  //- lock
  ACE_GUARD_REACTION(ACE_Recursive_Thread_Mutex, 
                     ace_mon, 
                     AD7432::repository_lock, 
                     throw asl::DAQException());
										 
  //- release hardware
  this->release_hardware();

  //- remove board from repository
  RepositoryIterator it = AD7432::repository.find(this->id_);
  if (it != AD7432::repository.end()) 
  {
    AD7432::repository.erase(it);    
  }
}

// ============================================================================
// AD7432::register_hardware
// ============================================================================
int AD7432::register_hardware ()
{
#if !defined(_SIMULATION_) 

  int bid = ::Register_Card(PCI_7432, this->id_);
  if (bid < 0){
	  this->idid_ = kIMPOSSIBLE_IID;
		return bid;
	}

  this->idid_ = bid;

  return 0;

#else // _SIMULATION_
  
  return this->id_;

#endif // _SIMULATION_
}

// ============================================================================
// AD7432::release_hardware
// ============================================================================
int AD7432::release_hardware ()
{
  //- be sure the board is registered
  if (this->idid_ == kIMPOSSIBLE_IID) {
    return 0;
  }

#if !defined(_SIMULATION_) 

  //- unregister card from driver
  return ::Release_Card(this->idid_);

#else // _SIMULATION_

  return 0;
  
#endif // _SIMULATION_
}

// ============================================================================
// AD7432::check_di_port
// ============================================================================
void AD7432::check_di_port (adl::DIOPort _port)
    throw (asl::DAQException)
{
  if (_port != adl::port_a) 
  {
	  throw asl::DAQException("invalid DI port specified",
                            "specified DI port is out of range for board PCI-7432",
                            "AD7432::check_di_port");
  }
}

// ============================================================================
// AD7432::check_di_line
// ============================================================================
void AD7432::check_di_line (int _line)
    throw (asl::DAQException)
{
  if (_line < AD7432_MIN_LINE  || _line > AD7432_MAX_LINE)
  {
    throw asl::DAQException("invalid DI line specified",
                            "specified DI line is out of range for board PCI-7432",
                            "AD7432::check_di_line");
  }
}

// ============================================================================
// AD7432::check_do_port
// ============================================================================
void AD7432::check_do_port (adl::DIOPort _port)
    throw (asl::DAQException)
{
  if (_port != adl::port_b) 
  {
	  throw asl::DAQException("invalid DO port specified",
                            "specified DO port is out of range for board PCI-7432" ,
                            "AD7432::check_do_port");
  }
}

// ============================================================================
// AD7432::check_do_line
// ============================================================================
void AD7432::check_do_line (int _line)
    throw (asl::DAQException)
{
  if (_line < AD7432_MIN_LINE  || _line > AD7432_MAX_LINE)
  {
	  throw asl::DAQException("invalid DO line specified",
                            "specified DO line is out of range for board PCI-7432",
                            "AD7432::check_do_line");
  }
}

// ============================================================================
// AD7432::configure_di_port
// ============================================================================
void AD7432::configure_di_port (adl::DIOPort _port)
    throw (asl::DAQException)
{
 ACE_UNUSED_ARG(_port);
 throw asl::DAQException("unsupported feature",
                         "DIO ports are not user configurable",
                         "AD7432::configure_port");
}

// ============================================================================
// AD7432::configure_do_port
// ============================================================================
void AD7432::configure_do_port (adl::DIOPort _port)
    throw (asl::DAQException)
{
 ACE_UNUSED_ARG(_port);
 throw asl::DAQException("unsupported feature",
                         "DIO ports are not user configurable",
                         "AD7432::configure_port");
}

// ============================================================================
// AD7432::read_line
// ============================================================================
bool AD7432::read_line (adl::DIOPort _port, int _line)
    throw (asl::DAQException)
{
  this->check_di_port(_port);

  this->check_di_line(_line);

	ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());

  unsigned short state = 0;

#if !defined(_SIMULATION_) 

	int err = ::DI_ReadLine(this->idid_, AD7432_READ_PORT, _line, &state);

	if (err < 0) 
	{
    throw asl::DAQException("could not read DI line",
                            adl::pcisdask_error_text(err),
                            "AD7432::read_line",
                            err);
	}

#endif // _SIMULATION_

 return state ? true : false;
}

// ============================================================================
// AD7432::read_port
// ============================================================================
unsigned long AD7432::read_port (adl::DIOPort _port)
    throw (asl::DAQException)
{
  this->check_di_port(_port);

/*	ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());*/

  unsigned long state = 0;

#if !defined(_SIMULATION_) 	

  int err = ::DI_ReadPort(this->idid_, AD7432_READ_PORT, &state);

	if (err < 0) 
	{
	  throw asl::DAQException("could not read DI port",
                            adl::pcisdask_error_text(err),
                            "AD7432::read_port",
                            err);
	}

#endif // _SIMULATION_

  return state;
}

// ============================================================================
// AD7432::write_line
// ============================================================================
void AD7432::write_line (adl::DIOPort _port, int _line, bool _state)
    throw (asl::DAQException)
{
  this->check_do_port(_port);

  this->check_do_line(_line);

/*	ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());*/

#if !defined(_SIMULATION_) 

	unsigned short state = _state ? true : false;

	int err = ::DO_WriteLine(this->idid_, AD7432_WRITE_PORT, _line, state);

	if (err < 0) 
	{
		throw asl::DAQException("could not write DO line",
                            adl::pcisdask_error_text(err),
                            "AD7432::write_line",
                            err);
	}

#endif // _SIMULATION_
}

// ============================================================================
// AD7432::write_port
// ============================================================================
void AD7432::write_port (adl::DIOPort _port, unsigned long _state)
    throw (asl::DAQException)
{
  this->check_do_port(_port);

/*	ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());*/

#if !defined(_SIMULATION_) 	

	int err = ::DO_WritePort(this->idid_, AD7432_WRITE_PORT,  _state);

	if (err < 0) 
	{
    throw asl::DAQException("write port failed",
                            adl::pcisdask_error_text(err),
                            "AD7432::write_port",
                            err);
	}

#endif // _SIMULATION_
}

// ============================================================================
// AD7432::dump
// ============================================================================
void AD7432::dump () const
{
  ACE_DEBUG ((LM_DEBUG, ". manufacturer.......ADLink\n"));
  ACE_DEBUG ((LM_DEBUG, ". type...............DAQ-7432\n"));
  ACE_DEBUG ((LM_DEBUG, ". board id...........%d\n", this->id_));
  ACE_DEBUG ((LM_DEBUG, ". registration id....%d\n", this->idid_));
  ACE_DEBUG ((LM_DEBUG, ". exclusive access...%s\n", this->exclusive_access_ ? "yes" : "no"));
}

} // namespace adl

