// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ADSingleShotAIBoard.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/adlink/AD2005.h>
#include <asl/adlink/AD2010.h>
#include <asl/adlink/AD2205.h>
#include <asl/adlink/AD2204.h>

namespace adl { 

// ============================================================================
// SUPPORTED HARDWARE
// ============================================================================
#define SS_HW_SUPPORT "current impl. supports ADLink boards: \
                       DAQ-2005, \
                       DAQ-2010, \
                       DAQ-2204, \
                       DAQ-2205"

// ============================================================================
// ADSingleShotAIBoard::instanciate
// ============================================================================
ADSingleShotAIBoard * ADSingleShotAIBoard::instanciate (unsigned short _type, 
                                                        unsigned short _id,
                                                        bool _exclusive_access)
  throw (asl::DAQException)
{
  ADSingleShotAIBoard * b = 0;

  //- type of board
  switch (_type)  
  {
    //- 2005
    case adl::DAQ2005:
      b = AD2005::instanciate (_id, _exclusive_access);
      break;

    //- 2010
    case adl::DAQ2010:
      b = AD2010::instanciate (_id, _exclusive_access);
      break;

    //- 2204
    case adl::DAQ2204:
      b = AD2204::instanciate (_id, _exclusive_access);
      break;

    //- 2205
    case adl::DAQ2205:
      b = AD2205::instanciate (_id, _exclusive_access);
      break;
	
    //- unsupported hardware
    default:
      throw asl::DAQException("unsupported or invalid hardware type",
                              SS_HW_SUPPORT,
                              "ADSingleShotAIBoard::instanciate");
      break;
  }

  return b;
}

// ============================================================================
// ADSingleShotAIBoard::ADSingleShotAIBoard
// ============================================================================
ADSingleShotAIBoard::ADSingleShotAIBoard (unsigned short _type, 
                                          unsigned short _id,
                                          unsigned short _nchans) 
  : ADBoard(_type, _id),
    ADCalibrableAIOBoard(_type, _id),
    ai_channels(_nchans)
{
  //- std::cout << "ADSingleShotAIBoard::ADSingleShotAIBoard <-" << std::endl;
  //- std::cout << "ADSingleShotAIBoard::ADSingleShotAIBoard ->" << std::endl;
}

// ============================================================================
// ADSingleShotAIBoard::~ADSingleShotAIBoard
// ============================================================================
ADSingleShotAIBoard::~ADSingleShotAIBoard()
{
  //- std::cout << "ADSingleShotAIBoard::~ADSingleShotAIBoard <-" << std::endl;
  //- std::cout << "ADSingleShotAIBoard::~ADSingleShotAIBoard ->" << std::endl;
}

// ============================================================================
// ADSingleShotAIBoard::configure_ai_channel
// ============================================================================
void ADSingleShotAIBoard::configure_ai_channel (adl::ChanId _chan_id, 
                                                adl::Range _range, 
                                                adl::GroundRef _ref)
  throw (asl::DAQException)
{
#if !defined(_SIMULATION_) 

 /* ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());*/

  if (_chan_id >= ai_channels.size())
  {
    throw asl::DAQException("invalid channel",
                            "invalid AI channel specified",
                            "ADSingleShotAIBoard::configure_ai_channel");
  }

  int err = ::D2K_AI_CH_Config(this->idid_, _chan_id, _range | _ref);
  if (err < 0)
  {
    throw asl::DAQException("AI channel configuration failed",
                            adl::d2kdask_error_text(err),
                            "ADSingleShotAIBoard::configure_ai_channel",
                            err);
  }

  ai_channels[_chan_id] = asl::ActiveAIChannel(_chan_id, _range, _ref);

#endif 
}


// ============================================================================
// ADSingleShotAIBoard::read_scaled_channel
// ============================================================================
double ADSingleShotAIBoard::read_scaled_channel (adl::ChanId _chan_id)
  throw (asl::DAQException)
{
  double value = 0;

#if !defined(_SIMULATION_) 

 /* ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());*/

  int err = ::D2K_AI_VReadChannel(this->idid_, _chan_id, &value);

  if (err < 0)
  {
    throw asl::DAQException("could not read AI channel",
                            adl::d2kdask_error_text(err),
                            "ADSingleShotAIBoard::read_scaled_channel",
                            err);
  }

#endif

  return value;
}

} // namespace adl








