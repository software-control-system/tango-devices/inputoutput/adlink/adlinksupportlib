// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ADSingleShotDOBoard.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <iostream>
#include <asl/adlink/AD6208.h>
#include <asl/adlink/AD6216.h>
#include <asl/adlink/AD7248.h>
#include <asl/adlink/AD7300.h>
#include <asl/adlink/AD7432.h>
#include <asl/adlink/AD2005.h>
#include <asl/adlink/AD2010.h>
#include <asl/adlink/AD2204.h>
#include <asl/adlink/AD2205.h>
#include <asl/adlink/AD2502.h>

namespace adl { 

// ============================================================================
// SUPPORTED HARDWARE
// ============================================================================
#define SS_HW_SUPPORT "current impl. supports ADLink boards: \
                       PCI-6208V, \
                       PCI-6216V, \
                       PCI-7248, \
                       PCI-7300, \
                       PCI-7432, \
                       DAQ-2005, \
                       DAQ-2010, \
                       DAQ-2204, \
                       DAQ-2205, \
                       DAQ-2502"

// ============================================================================
// ADSingleShotDOBoard::instanciate
// ============================================================================
ADSingleShotDOBoard * ADSingleShotDOBoard::instanciate (unsigned short _type, 
                                                        unsigned short _id,
                                                        bool _exclusive_access)
  throw (asl::DAQException)
{
  ADSingleShotDOBoard * b = 0;

  //- type of board
  switch (_type)  
  {
    //- 6208
    case adl::PCI6208:
      b = AD6208::instanciate (_id, _exclusive_access);
      break;

    //- 6216
    case adl::PCI6216:
      b = AD6216::instanciate (_id, _exclusive_access);
      break;

    //- 7248
    case adl::PCI7248:
      b = AD7248::instanciate (_id, _exclusive_access);
      break;

    //- 7300
    case adl::PCI7300:
      b = AD7300::instanciate (_id, _exclusive_access);
      break;

    //- 7432
    case adl::PCI7432:
      b = AD7432::instanciate (_id, _exclusive_access);
      break;

    //- 2005
    case adl::DAQ2005:
      b = AD2005::instanciate (_id, _exclusive_access);
      break;

    //- 2010
    case adl::DAQ2010:
      b = AD2010::instanciate (_id, _exclusive_access);
      break;

    //- 2204
    case adl::DAQ2204:
      b = AD2204::instanciate (_id, _exclusive_access);
      break;

    //- 2205
    case adl::DAQ2205:
      b = AD2205::instanciate (_id, _exclusive_access);
      break;

    //- 2502
    case adl::DAQ2502:
      b = AD2502::instanciate (_id, _exclusive_access);
      break;

    //- unsupported hardware
    default:
      throw asl::DAQException("unsupported or invalid hardware type",
                              SS_HW_SUPPORT,
                              "ADSingleShotDOBoard::instanciate");
      break;
  }

  return b;
}

// ============================================================================
// ADSingleShotDOBoard::ADSingleShotDOBoard
// ============================================================================
ADSingleShotDOBoard::ADSingleShotDOBoard (unsigned short _type, 
                                          unsigned short _id) 
  : ADBoard(_type, _id)
{
  //- std::cout << "ADSingleShotDOBoard::ADSingleShotDOBoard  <-" << std::endl;
  //- std::cout << "ADSingleShotDOBoard::ADSingleShotDOBoard  ->" << std::endl;
}

// ============================================================================
// ADSingleShotDOBoard::~ADSingleShotDOBoard
// ============================================================================
ADSingleShotDOBoard::~ADSingleShotDOBoard()
{
  //- std::cout << "ADSingleShotDOBoard::~ADSingleShotDOBoard <-" << std::endl;
  //- std::cout << "ADSingleShotDOBoard::~ADSingleShotDOBoard ->" << std::endl;
}

} // namespace adl








