// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ADSingleShotAOBoard.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/adlink/ADSingleShotAOBoard.h>
#include <asl/adlink/AD6208.h>
#include <asl/adlink/AD6216.h>
#include <asl/adlink/AD2005.h>
#include <asl/adlink/AD2010.h>
#include <asl/adlink/AD2205.h>
#include <asl/adlink/AD2204.h>

namespace adl { 

// ============================================================================
// SUPPORTED HARDWARE
// ============================================================================
#define SS_HW_SUPPORT "current impl. supports ADLink boards: \
                       DAQ-2005,\
                       DAQ-2010,\
                       DAQ-2204,\
                       DAQ-2205,\
                       PCI-6208V,\
                       PCI-6216V"

// ============================================================================
// ADSingleShotAOBoard::instanciate
// ============================================================================
ADSingleShotAOBoard * ADSingleShotAOBoard::instanciate (unsigned short _type, 
                                                        unsigned short _id,
                                                        bool _exclusive_access)
  throw (asl::DAQException)
{
  ADSingleShotAOBoard * b = 0;

  //- type of board
  switch (_type)  
  {
    //- 2005
    case adl::DAQ2005:
      b = AD2005::instanciate (_id, _exclusive_access);
      break;

    //- 2010
    case adl::DAQ2010:
      b = AD2010::instanciate (_id, _exclusive_access);
      break;

    //- 2204
    case adl::DAQ2204:
      b = AD2204::instanciate (_id, _exclusive_access);
      break;

    //- 2205
    case adl::DAQ2205:
      b = AD2205::instanciate (_id, _exclusive_access);
      break;
	
    //- 6208
    case adl::PCI6208:
      b = AD6208::instanciate (_id, _exclusive_access);
      break;

    //- 6216
    case adl::PCI6216:
      b = AD6216::instanciate (_id, _exclusive_access);
      break;

    //- unsupported hardware
    default:
      throw asl::DAQException("unsupported or invalid hardware type",
                              SS_HW_SUPPORT,
                              "ADSingleShotAOBoard::instanciate");
      break;
  }

  return b;
}

// ============================================================================
// ADSingleShotAOBoard::ADSingleShotAOBoard
// ============================================================================
ADSingleShotAOBoard::ADSingleShotAOBoard (unsigned short _type, 
                                          unsigned short _id, 
                                          unsigned short _nchans) 
  : ADBoard(_type, _id),
    ADCalibrableAIOBoard(_type, _id)
{
  ACE_UNUSED_ARG(_nchans);
  //- std::cout << "ADSingleShotAOBoard::ADSingleShotAOBoard <-" << std::endl;
  //- std::cout << "ADSingleShotAOBoard::ADSingleShotAOBoard ->" << std::endl;
}

// ============================================================================
// ADSingleShotAOBoard::~ADSingleShotAOBoard
// ============================================================================
ADSingleShotAOBoard::~ADSingleShotAOBoard()
{
  //- std::cout << "ADSingleShotAOBoard::~ADSingleShotAOBoard <-" << std::endl;
  //- std::cout << "ADSingleShotAOBoard::~ADSingleShotAOBoard ->" << std::endl;
}

// ============================================================================
// ADSingleShotAOBoard::configure_ao_channel
// ============================================================================
void ADSingleShotAOBoard::configure_ao_channel (adl::ChanId _chan_id, 
                                                adl::OutputPolarity _polarity, 
                                                adl::VoltageReferenceSource _ref_source, 
                                                double _ref_volt)
  throw (asl::DAQException)
{
#if !defined(_SIMULATION_) 

  ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());

  int err = ::D2K_AO_CH_Config(this->idid_, 
                               _chan_id, 
                               _polarity, 
                               _ref_source, 
                               _ref_volt);
  if (err < 0) 
  {
    throw asl::DAQException("AO channel configuration failed",
                            adl::d2kdask_error_text(err),
                            "ADSingleShotAOBoard::configure_ao_channel",
                            err);
  }

#endif
}

// ============================================================================
// ADSingleShotAOBoard::write_channel
// ============================================================================
void ADSingleShotAOBoard::write_channel (adl::ChanId _chan_id, unsigned short _value)
  throw (asl::DAQException)
{
#if !defined(_SIMULATION_) 

/* ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());*/

  int err = ::D2K_AO_WriteChannel(this->idid_, _chan_id, _value);

  if (err < 0)
  {
    throw asl::DAQException("could not write AO channel",
                            adl::d2kdask_error_text(err),
                            "ADSingleShotAOBoard::write_channel",
                            err);
  }
#endif
}

// ============================================================================
// ADSingleShotAOBoard::write_scaled_channel
// ============================================================================
void ADSingleShotAOBoard::write_scaled_channel (adl::ChanId _chan_id, 
                                                double _value)
  throw (asl::DAQException)
{
#if !defined(_SIMULATION_) 

 /* ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());*/

  int err = ::D2K_AO_VWriteChannel(this->idid_, _chan_id, _value);
  if (err < 0)
  {
    throw asl::DAQException("could not write AO channel",
                            adl::d2kdask_error_text(err),
                            "ADSingleShotAOBoard::write_scaled_channel",
                            err);
  }
#endif
}

} // namespace adl






