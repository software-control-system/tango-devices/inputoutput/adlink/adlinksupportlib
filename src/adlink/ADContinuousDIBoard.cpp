// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ADContinuousDIBoard.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/adlink/ADContinuousDIBoard.h>
#include <asl/adlink/AD7300.h>

namespace adl { 

  // ============================================================================
// SUPPORTED HARDWARE
// ============================================================================
#define CD_HW_SUPPORT "current impl. supports ADLink boards: PCI-7300"

// ============================================================================
// ADContinuousDIBoard::instanciate
// ============================================================================
ADContinuousDIBoard * ADContinuousDIBoard::instanciate (unsigned short _type, 
                                                        unsigned short _id,
                                                        bool _exclusive_access)
  throw (asl::DAQException)
{
	ADContinuousDIBoard * b = 0;
		
	//- type of board
	switch (_type)  
	{
		//- 7300
		case adl::PCI7300:
			b = AD7300::instanciate (_id, _exclusive_access);
			break;
			
			//- unsupported hardware
		default:
			throw asl::DAQException("unsupported or invalid hardware type",
				                      CD_HW_SUPPORT,
				                      "ADContinuousDIBoard::instanciate");
			break;
	}
		
	return b;
}
 
// ============================================================================
// ADContinuousDIBoard::ADContinuousDIBoard
// ============================================================================
ADContinuousDIBoard::ADContinuousDIBoard (unsigned short _type, 
                                          unsigned short _id) 
  : ADBoard(_type, _id),
    daq_buffer_idx_ (0),
    daq_buffer_0_ (0),
    daq_buffer_1_ (0)
{

}

// ============================================================================
// ADContinuousDIBoard::~ADContinuousDIBoard
// ============================================================================
ADContinuousDIBoard::~ADContinuousDIBoard ()
{
  //- release daq buffers
  this->release_daq_buffers();
}

// ============================================================================
//ADContinuousDIBoard::configure_continuous_di
// ============================================================================
void ADContinuousDIBoard::configure_continuous_di (const asl::ContinuousDIConfig& _di_config)
throw (asl::DAQException)
{
	//- config card ------------------------------------------------------------
	//--------------------------------------------------------------------------
	di_config_ = _di_config;
	
#if !defined(_SIMULATION_)
	
  //- check registration id (may throw an exception)
  this->check_registration();

	unsigned short ctrl;
	ctrl  = _di_config.get_request_polarity();
	ctrl |= _di_config.get_ack_polarity();
	ctrl |= _di_config.get_trigger_polarity();
	
	int err = ::DI_7300B_Config (this->idid_, 
		                           _di_config.get_port_width(), 
		                           _di_config.get_trigger_source(), 
		                           _di_config.get_start_mode(), 
		                           _di_config.get_terminator(), 
		                           ctrl, 
		                           (BOOLEAN)0,
		                           (BOOLEAN)0);
	if (err < 0) {
		throw asl::DAQException("configuration failed",
			adl::pcisdask_error_text(err),
			"AD7300::configure_continuous_di",
			err);
	}
	
#endif // _SIMULATION_  
	
	//- init daq buffers -------------------------------------------------------
	//--------------------------------------------------------------------------
	
	if (this->init_daq_buffers() == -1) {
		//- could not allocate buffers
		throw asl::DAQException("DAQ buffers allocation failed",
			                      "out of memory error",
			                      "AD7300::configure_continuous_di");
	}
	
#if !defined(_SIMULATION_) 
	
	unsigned short buffer_id = 0; 
	
	//- setup buffers
	switch(_di_config.get_port_width())
	{
		//- use 8 bits DI port
    case adl::width_8:
		{
			err = ::DI_ContMultiBufferSetup(this->idid_, 
				                              reinterpret_cast<asl::DIByteData*>(this->daq_buffer_0_)->base(),
				                              reinterpret_cast<asl::DIByteData*>(this->daq_buffer_0_)->depth(), 
				                              &buffer_id); 
			if (err < 0) {
				throw asl::DAQException("configuration failed",
					                      adl::pcisdask_error_text(err),
					                      "AD7300::configure_continuous_di",
					                      err);
			}
			err = ::DI_ContMultiBufferSetup(this->idid_, 
				                              reinterpret_cast<asl::DIByteData*>(this->daq_buffer_1_)->base(),
				                              reinterpret_cast<asl::DIByteData*>(this->daq_buffer_1_)->depth(), 
				                              &buffer_id);
			if (err < 0) {
				throw asl::DAQException("configuration failed",
					                      adl::pcisdask_error_text(err),
					                      "AD7300::configure_continuous_di",
					                      err);
			}
		}
		break;
		//- use 16 bits DI port
    case adl::width_16:
		{
			err = ::DI_ContMultiBufferSetup(this->idid_, 
				                              reinterpret_cast<asl::DIShortData*>(this->daq_buffer_0_)->base(),
				                              reinterpret_cast<asl::DIShortData*>(this->daq_buffer_0_)->depth(), 
				                              &buffer_id);
			if (err < 0) {
				throw asl::DAQException("configuration failed",
					                      adl::pcisdask_error_text(err),
					                      "AD7300::configure_continuous_di",
					                      err);
			}
			err = ::DI_ContMultiBufferSetup(this->idid_, 
				                              reinterpret_cast<asl::DIShortData*>(this->daq_buffer_1_)->base(),
				                              reinterpret_cast<asl::DIShortData*>(this->daq_buffer_1_)->depth(), 
				                              &buffer_id);
			if (err < 0) {
				throw asl::DAQException("configuration failed",
					                      adl::pcisdask_error_text(err),
					                      "AD7300::configure_continuous_di",
					                      err);
			}
		}
		break;
		//- use 32 bits DI port
    case adl::width_32:
		{
			err = ::DI_ContMultiBufferSetup(this->idid_, 
				                              reinterpret_cast<asl::DILongData*>(this->daq_buffer_0_)->base(),
				                              reinterpret_cast<asl::DILongData*>(this->daq_buffer_0_)->depth(), 
				                              &buffer_id);
			if (err < 0) {
				throw asl::DAQException("configuration failed",
					                      adl::pcisdask_error_text(err),
					                      "AD7300::configure_continuous_di",
					                      err);
			}
			err = ::DI_ContMultiBufferSetup(this->idid_, 
				                              reinterpret_cast<asl::DILongData*>(this->daq_buffer_1_)->base(),
				                              reinterpret_cast<asl::DILongData*>(this->daq_buffer_1_)->depth(), 
				                              &buffer_id);
			if (err < 0) {
				throw asl::DAQException("configuration failed",
					                      adl::pcisdask_error_text(err),
					                      "AD7300::configure_continuous_di",
					                      err);
			}
		}
		break;
		//- undefined port width
    default:
		{
			throw asl::DAQException("invalid configuration",
			                        "invalid DI port width specified",
			                        "AD7300::configure_continuous_di");
		}
		break;
	}
	
#endif // _SIMULATION_
}


// ============================================================================
// ADContinuousDIBoard::init_daq_buffers
// ============================================================================false
int ADContinuousDIBoard::init_daq_buffers ()
{
	//- release buffers
	this->release_daq_buffers();
	
	//- get <num of samples per buffer>
	unsigned long depth = this->di_config_.get_buffer_depth();
	
	//- be sure buffer size is even
	if (depth % 2) {
		depth++;
	}
	
	switch(this->di_config_.get_port_width())
	{
    //- use 8 bits DI port
    case adl::width_8:
		{
			//- allocate buffer 0
			this->daq_buffer_0_ = new asl::DIByteData(depth);
			if (this->daq_buffer_0_ == 0) 
      {
				return -1;
			}
			//- allocate buffer 1
			this->daq_buffer_1_ = new asl::DIByteData(depth);
			if (this->daq_buffer_1_ == 0) 
      {
				delete this->daq_buffer_0_;
				this->daq_buffer_0_ = 0;
				return -1;
			}
			//- clear buffers content
			reinterpret_cast<asl::DIByteData*>(this->daq_buffer_0_)->clear();
			reinterpret_cast<asl::DIByteData*>(this->daq_buffer_1_)->clear();
		}
		break;
    //- use 16 bits DI port
    case adl::width_16:
		{
			//- allocate buffer 0
			this->daq_buffer_0_ = new asl::DIShortData(depth);
			if (this->daq_buffer_0_ == 0) 
      {
				return -1;
			}
			//- allocate buffer 1
			this->daq_buffer_1_ = new asl::DIShortData(depth);
			if (this->daq_buffer_1_ == 0) 
      {
				delete this->daq_buffer_0_;
				this->daq_buffer_0_ = 0;
				return -1;
			}
			//- clear buffers content
			reinterpret_cast<asl::DIShortData*>(this->daq_buffer_0_)->clear();
			reinterpret_cast<asl::DIShortData*>(this->daq_buffer_1_)->clear();
		}
		break;
    //- use 32 bits DI port
    case adl::width_32:
		{
			//- allocate buffer 0
			this->daq_buffer_0_ = new asl::DILongData(depth);
			if (this->daq_buffer_0_ == 0) 
      {
				return -1;
			}
			//- allocate buffer 1
			this->daq_buffer_1_ = new asl::DILongData(depth);
			if (this->daq_buffer_1_ == 0) 
      {
				delete this->daq_buffer_0_;
				this->daq_buffer_0_ = 0;
				return -1;
			}
			//- clear buffers content
			reinterpret_cast<asl::DILongData*>(this->daq_buffer_0_)->clear();
			reinterpret_cast<asl::DILongData*>(this->daq_buffer_1_)->clear();
		}
		break;
    default:
		{
			throw asl::DAQException("invalid configuration",
		                          "invalid DI port width specified",
		                          "ADContinuousDIBoard::configure_continuous_di");
		}
		break;
	}
	
	return 0;
}

// ============================================================================
// ADContinuousDIBoard::release_daq_buffers
// ============================================================================
void ADContinuousDIBoard::release_daq_buffers ()
{
	if (this->daq_buffer_0_) 
  {
		delete daq_buffer_0_;
		daq_buffer_0_ = 0;
	}
	
	if (this->daq_buffer_1_) 
  {
		delete daq_buffer_1_;
		daq_buffer_1_ = 0;
	} 
}

// ============================================================================
// ADContinuousDIBoard::start_continuous_di
// ============================================================================
void ADContinuousDIBoard::start_continuous_di ()
  throw (asl::DAQException)
{
#if !defined(_SIMULATION_) 
	
  //- check registration id (may throw an exception)
  this->check_registration();

	//- install db-event-handler
	asl::DAQCallback cb = this->di_config_.get_db_event_callback();
	
	//- be sure callback address is valid
	if (cb == 0) {
		throw asl::DAQException("invalid configuration",
			                      "invalid callback address specified",
			                      "ADContinuousDIBoard::start_continuous_di");
	}
	
	//- install the event and event-handler
#if defined (WIN32)
  int err = ::DI_EventCallBack (this->idid_, 1, adl::db_evt, (U32)cb);
#else
  int err = ::DI_EventCallBack (this->idid_, 1, adl::db_evt, cb);
#endif
	if (err < 0) 
	{
		throw asl::DAQException("DAQ start failed",
			                      adl::pcisdask_error_text(err),
			                      "ADContinuousDIBoard::start_continuous_di",
			                      err);
	}

  //- install daq-end-event-handler
  cb = this->di_config_.get_di_end_event_callback();
	
	//- be sure callback address is valid
	if (cb == 0) {
		throw asl::DAQException("invalid configuration",
			                      "invalid callback address specified",
			                      "ADContinuousDIBoard::start_continuous_di");
	}
	
	//- install the event and event-handler
#if defined (WIN32)
  err = ::DI_EventCallBack (this->idid_, 1, adl::daq_end, (U32)cb);
#else
  err = ::DI_EventCallBack (this->idid_, 1, adl::daq_end, cb);
#endif
	if (err < 0) 
	{
		throw asl::DAQException("DAQ start failed",
			                      adl::pcisdask_error_text(err),
			                      "ADContinuousDIBoard::start_continuous_di",
			                      err);
	}

	//- start continuous DAQ
	err = ::DI_ContMultiBufferStart(this->idid_, 0, di_config_.get_sampling_rate());
	if (err < 0) 
	{
		throw asl::DAQException("DAQ start failed",
			                      adl::pcisdask_error_text(err),
			                      "ADContinuousDIBoard::start_continuous_di",
			                      err);
	}
	
#endif // _SIMULATION_
}

// ============================================================================
// ADContinuousDIBoard::ai_db_event_callback
// ============================================================================
asl::BaseBuffer* ADContinuousDIBoard::di_db_event_callback (void* arg)
  throw (asl::DataLostException, asl::DAQException)
{
	ACE_UNUSED_ARG(arg);
	
	asl::BaseBuffer* raw_data = 0;
	
#if !defined(_SIMULATION_)
	
	//- get next buffer
	BOOLEAN half_ready;
	int err = ::DI_AsyncMultiBufferNextReady(this->idid_, &half_ready, &daq_buffer_idx_);
	if (err < 0) {
		throw asl::DAQException("internal DAQ error",
                            adl::pcisdask_error_text(err),
                            "ADContinuousDIBoard::di_db_event_callback",
                            err);
	}
	
	//- select current buffer
	asl::BaseBuffer* data_src = (this->daq_buffer_idx_ % 2) 
                                ?
                               this->daq_buffer_1_ 
                                : 
                               this->daq_buffer_0_;
	
	//- copy DAQ data to user buffer
	switch (this->di_config_.get_port_width())
	{
    case adl::width_8:
		  raw_data = new asl::DIByteData(* reinterpret_cast<asl::DIByteData*>(data_src));
		  break;
    case adl::width_16:
		  raw_data = new asl::DIShortData(* reinterpret_cast<asl::DIShortData*>(data_src));
		  break;
    case adl::width_32:
		  raw_data = new asl::DILongData(* reinterpret_cast<asl::DILongData*>(data_src));
		  break;    
    default:
		  throw asl::DAQException("invalid configuration",
		                          "invalid DI port width",
		                          "ADContinuousDIBoard::di_db_event_callback");
		  break;
	}
	
	//- check memory allocation 
	if (raw_data == 0)
	{
		throw asl::DAQException("out of memory error",
			                      "data buffer allocation failed",
			                      "ADContinuousDIBoard::di_db_event_callback");
	}

  //- notify driver the data have been handled
  err = ::DI_AsyncDblBufferTransfer(this->idid_, 0);

     //- check overrun status
  unsigned short overrun_status;
  ::DI_AsyncDblBufferOverrun(this->idid_, 0, &overrun_status);

  //- in case of buffer overrun ...
  if (overrun_status) 
  {
    //- clear overrun flag
    ::DI_AsyncDblBufferOverrun(this->idid_, 1, 0);
    //- apply overrun strategy
    switch (this->di_config_.get_data_lost_strategy()) 
    {
      case adl::trash:
      case adl::abort:
      case adl::restart:
      case adl::notify:
        //- release allocated buffer
        delete raw_data;
        //-throw data_lost exception
        throw asl::DataLostException(asl::DataLostException::DAQ_BUFFER_OVERRUN);
        break;
      case adl::ignore:
      default:
        break;
    }
  }

#else // _SIMULATION_ 
	
	switch(this->di_config_.get_port_width())
	{
    case adl::width_8:
	  	raw_data = new asl::DIByteData(reinterpret_cast<asl::DIByteData*>(this->daq_buffer_0_)->depth());
		  (reinterpret_cast<asl::DIByteData*>(raw_data))->fill(204);
		  break;
    case adl::width_16:
		  raw_data = new asl::DIShortData(reinterpret_cast<asl::DIShortData*>(this->daq_buffer_0_)->depth());
		  (reinterpret_cast<asl::DIShortData*>(raw_data))->fill(204);
		  break;
    case adl::width_32:
		  raw_data = new asl::DILongData(reinterpret_cast<asl::DILongData*>(this->daq_buffer_0_)->depth());
		  (reinterpret_cast<asl::DILongData*>(raw_data))->fill(1204);
		  break;
    default:
		  throw asl::DAQException("invalid configuration",
			                        "invalid DI port width specified",
			                        "ADContinuousDIBoard::di_db_event_callback");
		  break;
	}
	if (raw_data == 0) {
		throw asl::DAQException("out of memory error",
			                      "data buffer allocation failed",
			                      "ADContinuousDIBoard::di_db_event_callback");
	}
	
#endif // _SIMULATION_ 
	
	return raw_data;
}

// ============================================================================
// ADContinuousDIBoard::stop_continuous_di
// ============================================================================
void ADContinuousDIBoard::stop_continuous_di ()
throw (asl::DAQException)
{
	
#if !defined(_SIMULATION_)
	
  //- check registration id (may throw an exception)
	this->check_registration();

	//- IMPORTANT NOTE:
	//- THE DRIVER WAIT FOR THE CALLBACK TO COMPLETE BEFORE STOPPING THE
	//- ASYNC OPERATION. If THE CALLBACK NEVER RETURNS, THE PROCESS HANGS.
	
	unsigned long access_count = 0;
	int err = ::DI_AsyncClear(this->idid_, &access_count);
	if (err < 0)
	{
		throw asl::DAQException("stop acquisition failed",
			                      adl::pcisdask_error_text(err),
			                      "ADContinuousDIBoard::stop_continuous_di",
			                      err);
	}
	
  //- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  //- ON WINDOWS: UNINSTALLING THE EVTS HANDLERS AFTER <D2K_AI_AsyncClear> 
  //- CAUSE THE <DAQEND> EVENT TO REMAIN PENDING AND FIRED ON NEXT START
  //- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  //- uninstall adl::db_evt
  //- err = ::DI_EventCallBack(this->idid_, 0, adl::db_evt, 0);
  //- uninstall adl::daq_end
  //- err = ::DI_EventCallBack(this->idid_, 0, adl::daq_end, 0);
	
#endif // _SIMULATION_ 
}

} // namespace adl
