// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    AD6216.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/adlink/AD6216.h>

namespace adl { 

// ============================================================================
// CONSTS
// ============================================================================
#define AD6216_READ_PORT  0
#define AD6216_WRITE_PORT 0

// ============================================================================
// STATICS
// ============================================================================
AD6216::Repository AD6216::repository;
ACE_Recursive_Thread_Mutex AD6216::repository_lock;

// ============================================================================
// AD6216::instanciate
// ============================================================================
AD6216 * AD6216::instanciate (unsigned short _id, bool _exclusive_access)
  throw (asl::DAQException)
{
  //- lock
  ACE_GUARD_REACTION(ACE_Recursive_Thread_Mutex, 
		     ace_mon, 
		     AD6216::repository_lock,
		     throw asl::DAQException());

  //- search requested board in the repository
  RepositoryIterator it = AD6216::repository.find(_id);

  //- already instnciated?
  if (it != AD6216::repository.end()) {
    //- can't get ownership if _exclusive_access requested
    if (_exclusive_access || it->second->exclusive_access_) {
      //- can't get exclusive access to hardware
      throw asl::DAQException("hardware already in use",
			      "could not get exclusive access to the hardware",
			      "AD6216::instanciate");
    }
    //- share the board
    else {
      return it->second->duplicate();
    }
  }

  //- instanciate new board
  AD6216* b = new AD6216(_id);
  if (b == 0) {
    //- out of memory
    throw asl::DAQException("out of memory",
			    "out of memory error",
			    "AD6216::instanciate");
  }

  //- update board's <exclusive_access> flag
  b->exclusive_access_ = _exclusive_access;

  int err = b->register_hardware();
  if (err < 0) {
    //- release board
    b->release();
    //- reset returned pointer
    b = 0;
    //- hardware registration failed
    throw asl::DAQException("hardware registration failed",
			    adl::pcisdask_error_text(err),
			    "AD6216::instanciate",
			    err);
  }

  return b;
}

// ============================================================================
// AD6216::duplicate
// ============================================================================
AD6216 * AD6216::duplicate ()
{
  return dynamic_cast<AD6216*>(this->SharedObject::duplicate());
}
 
// ============================================================================
// AD6216::AD6216
// ============================================================================
AD6216::AD6216(unsigned short _id)
  : ADBoard(adl::PCI6216, _id),
    ADCalibrableAIOBoard(adl::PCI6216, _id),
    ADSingleShotAOBoard(adl::PCI6216, _id, AD6216_NUM_CHANNELS),
    ADSingleShotDIBoard(adl::PCI6216, _id),
    ADSingleShotDOBoard(adl::PCI6216, _id)
{
  //- std::cout << "AD6216::AD6216 <-" << std::endl;

  //- store board in the repository
  RepositoryValue pair(_id, this);
  AD6216::repository.insert(pair);

  //- std::cout << "AD6216::AD6216 ->" << std::endl;
}

// ============================================================================
// AD6216::~AD6216
// ============================================================================
AD6216::~AD6216()
{
  //- lock
  ACE_GUARD_REACTION(ACE_Recursive_Thread_Mutex, 
		     ace_mon, 
		     AD6216::repository_lock,
		     throw asl::DAQException());
										 
  //- std::cout << "AD6216::~AD6216 <-" << std::endl;

  //- release hardware
  this->release_hardware();

  //- remove board from repository
  RepositoryIterator it = AD6216::repository.find(this->id_);
  if (it != AD6216::repository.end()) {
    AD6216::repository.erase(it);
  }

  //- std::cout << "AD6216::~AD6216 <-" << std::endl;
}

// ============================================================================
// AD6216::register_hardware
// ============================================================================
int AD6216::register_hardware ()
{
#if !defined(_SIMULATION_) 
	
	int bid = ::Register_Card(PCI_6208V, this->id_);
	if (bid < 0) {
	  this->idid_ = kIMPOSSIBLE_IID;
		return bid;
	}
	this->idid_ = bid;
	return 0;
	
#else // _SIMULATION_
	
	return this->id_;
	
#endif // _SIMULATION_	
}

// ============================================================================
// AD6216::release_hardware
// ============================================================================
int AD6216::release_hardware ()
{
	//- be sure the board is registered
	if (this->idid_ == kIMPOSSIBLE_IID) {
		return 0;
	}
	
#if !defined(_SIMULATION_) 
	
	return ::Release_Card(this->idid_);
	
#else // _SIMULATION_
	
	return 0;
	
#endif // _SIMULATION_ 
} 

// ============================================================================
// AD6216::configure_ao_channel
// ============================================================================
void AD6216::configure_ao_channel (adl::ChanId _chan_id,
				   adl::OutputPolarity _polarity, 
				   adl::VoltageReferenceSource _ref_source, 
				   double _ref_volt)
  throw (asl::DAQException)
{
 ACE_UNUSED_ARG(_chan_id);
 ACE_UNUSED_ARG(_polarity);
 ACE_UNUSED_ARG(_ref_source);
 ACE_UNUSED_ARG(_ref_volt);
 throw asl::DAQException("unsupported feature",
			 "AO channels are not user configurable for board PCI-6216V",
			 "AD6216::configure_ao_channel");
}

// ============================================================================
// AD6216::write_channel
// ============================================================================
void AD6216::write_channel (adl::ChanId _chan_id, unsigned short _value)
  throw (asl::DAQException)
{
#if !defined(_SIMULATION_) 

/* ACE_GUARD_REACTION(ACE_Thread_Mutex, 
		     ace_mon, 
		     this->lock_, 
		     throw asl::DAQException());*/

  if (_chan_id > AD6216_NUM_CHANNELS)
  {
    throw asl::DAQException("invalid channel specified",
			    "specified channel is out of range for board PCI-6216V",
			    "AD6216::write_channel");
  }

  int err = ::AO_WriteChannel(this->idid_, _chan_id, _value);

  if (err < 0)
  {
    throw asl::DAQException("could not write AO channel",
			    adl::pcisdask_error_text(err),
			    "AD6216::write_channel",
			    err);
  }
#endif
}

// ============================================================================
// AD6216::write_scaled_channel
// ============================================================================
void AD6216::write_scaled_channel (adl::ChanId _chan_id, double _value)
  throw (asl::DAQException)
{
#if !defined(_SIMULATION_) 

 /* ACE_GUARD_REACTION(ACE_Thread_Mutex, 
		     ace_mon, 
		     this->lock_, 
		     throw asl::DAQException());*/

  if (_chan_id > AD6216_NUM_CHANNELS)
  {
    throw asl::DAQException("invalid channel specified",
			    "specified channel is out of range for board PCI-6216V",
			    "AD6216::write_scaled_channel");
  }

  if (_value < AD6216_MIN_VOLTAGE || _value > AD6216_MAX_VOLTAGE)
  {
    throw asl::DAQException("invalid voltage value specified",
			    "specified value is out of range for board PCI-6216V",
			    "AD6216::write_scaled_channel");
  }

  int err = ::AO_VWriteChannel(this->idid_, _chan_id, _value);

  if (err < 0)
  {
    throw asl::DAQException("could not write AO channel",
			    adl::pcisdask_error_text(err),
			    "AD6216::write_scaled_channel",
			    err);
  }
#endif
}

// ============================================================================
// AD6216::check_di_port
// ============================================================================
void AD6216::check_di_port (adl::DIOPort _port)
    throw (asl::DAQException)
{
  if (_port != adl::port_a) 
  {
	  throw asl::DAQException("invalid DI port specified",
			    "specified DI port is out of range for board PCI-6216V",
			    "AD6216::check_di_port");
  }
}

// ============================================================================
// AD6216::check_di_line
// ============================================================================
void AD6216::check_di_line (int _line)
    throw (asl::DAQException)
{
  if (_line < AD6216_MIN_LINE  || _line > AD6216_MAX_LINE)
  {
    throw asl::DAQException("invalid DI line specified",
			    "specified DI line is out of range for board PCI-6216V",
			    "AD6216::check_di_line");
  }
}

// ============================================================================
// AD6216::check_do_port
// ============================================================================
void AD6216::check_do_port (adl::DIOPort _port)
    throw (asl::DAQException)
{
  if (_port != adl::port_b) 
  {
	  throw asl::DAQException("invalid DO port specified",
			    "specified DO port is out of range for board PCI-6216V" ,
			    "AD6216::check_do_port");
  }
}

// ============================================================================
// AD6216::check_do_line
// ============================================================================
void AD6216::check_do_line (int _line)
    throw (asl::DAQException)
{
  if (_line < AD6216_MIN_LINE  || _line > AD6216_MAX_LINE)
  {
	  throw asl::DAQException("invalid DO line specified",
			    "specified DO line is out of range for board PCI-6216V",
			    "AD6216::check_do_line");
  }
}

// ============================================================================
// AD6216::configure_di_port
// ============================================================================
void AD6216::configure_di_port (adl::DIOPort _port)
    throw (asl::DAQException)
{
 ACE_UNUSED_ARG(_port);
 throw asl::DAQException("unsupported feature",
			 "DIO ports are not user configurable",
			 "AD6216::configure_port");
}

// ============================================================================
// AD6216::configure_do_port
// ============================================================================
void AD6216::configure_do_port (adl::DIOPort _port)
    throw (asl::DAQException)
{
 ACE_UNUSED_ARG(_port);
 throw asl::DAQException("unsupported feature",
			 "DIO ports are not user configurable",
			 "AD6216::configure_port");
}

// ============================================================================
// AD6216::read_line
// ============================================================================
bool AD6216::read_line (adl::DIOPort _port, int _line)
    throw (asl::DAQException)
{
  this->check_di_port(_port);

  this->check_di_line(_line);

	ACE_GUARD_REACTION(ACE_Thread_Mutex, 
		     ace_mon, 
		     this->lock_, 
		     throw asl::DAQException());

  unsigned short state = 0;

#if !defined(_SIMULATION_) 

	int err = ::DI_ReadLine(this->idid_, AD6216_READ_PORT, _line, &state);

	if (err < 0) 
	{
    throw asl::DAQException("could not read DI line",
			    adl::pcisdask_error_text(err),
			    "AD6216::read_line",
			    err);
	}

#endif // _SIMULATION_

 return state ? true : false;
}

// ============================================================================
// AD6216::read_port
// ============================================================================
unsigned long AD6216::read_port (adl::DIOPort _port)
    throw (asl::DAQException)
{
  this->check_di_port(_port);

/*	ACE_GUARD_REACTION(ACE_Thread_Mutex, 
		     ace_mon, 
		     this->lock_, 
		     throw asl::DAQException());*/

  unsigned long state = 0;

#if !defined(_SIMULATION_)	

  int err = ::DI_ReadPort(this->idid_, AD6216_READ_PORT, &state);

	if (err < 0) 
	{
	  throw asl::DAQException("could not read DI port",
			    adl::pcisdask_error_text(err),
			    "AD6216::read_port",
			    err);
	}

#endif // _SIMULATION_

  return state;
}

// ============================================================================
// AD6216::write_line
// ============================================================================
void AD6216::write_line (adl::DIOPort _port, int _line, bool _state)
    throw (asl::DAQException)
{
  this->check_do_port(_port);

  this->check_do_line(_line);

/*	ACE_GUARD_REACTION(ACE_Thread_Mutex, 
		     ace_mon, 
		     this->lock_, 
		     throw asl::DAQException());*/

#if !defined(_SIMULATION_) 

	unsigned short state = _state ? true : false;

	int err = ::DO_WriteLine(this->idid_, AD6216_WRITE_PORT, _line, state);

	if (err < 0) 
	{
		throw asl::DAQException("could not write DO line",
			    adl::pcisdask_error_text(err),
			    "AD6216::write_line",
			    err);
	}

#endif // _SIMULATION_
}

// ============================================================================
// AD6216::write_port
// ============================================================================
void AD6216::write_port (adl::DIOPort _port, unsigned long _state)
    throw (asl::DAQException)
{
  this->check_do_port(_port);

/*	ACE_GUARD_REACTION(ACE_Thread_Mutex, 
		     ace_mon, 
		     this->lock_, 
		     throw asl::DAQException());*/

#if !defined(_SIMULATION_)	

	int err = ::DO_WritePort(this->idid_, AD6216_WRITE_PORT,  _state);

	if (err < 0) 
	{
    throw asl::DAQException("write port failed",
			    adl::pcisdask_error_text(err),
			    "AD6216::write_port",
			    err);
	}

#endif // _SIMULATION_
}

// ============================================================================
// AD6216::auto_calibrate
// ============================================================================
void AD6216::auto_calibrate (bool _save_data)
  throw (asl::DAQException)
{
  ACE_UNUSED_ARG(_save_data);
  //- noop
}

// ============================================================================
// AD6216::save_calibration
// ============================================================================
void AD6216::save_calibration (adl::AIOCalibrationBank _bank)
  throw (asl::DAQException)
{
  ACE_UNUSED_ARG(_bank);
  //- noop
}


// ============================================================================
// AD6216::load_calibration
// ============================================================================
void AD6216::load_calibration (adl::AIOCalibrationBank _bank)
  throw (asl::DAQException)
{
  ACE_UNUSED_ARG(_bank);
  //- noop
}

// ============================================================================
// AD6216::dump
// ============================================================================
void AD6216::dump () const
{
	ACE_DEBUG ((LM_DEBUG, ". manufacturer.......ADLink\n"));
	ACE_DEBUG ((LM_DEBUG, ". type...............PCI-6216V\n"));
	ACE_DEBUG ((LM_DEBUG, ". board id...........%d\n", this->id_));
	ACE_DEBUG ((LM_DEBUG, ". registration id....%d\n", this->idid_));
	ACE_DEBUG ((LM_DEBUG, ". exclusive access...%s\n", this->exclusive_access_ ? "yes" : "no"));
}

} // namespace adl


