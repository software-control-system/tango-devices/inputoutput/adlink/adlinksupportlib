// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ADCalibrableAIOBoard.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <iostream>
#include <asl/adlink/ADLink.h>
#include <asl/adlink/ADCalibrableAIOBoard.h>

namespace adl { 

// ============================================================================
// ADCalibrableAIOBoard::ADCalibrableAIOBoard
// ============================================================================
ADCalibrableAIOBoard::ADCalibrableAIOBoard (unsigned short _type, 
                                            unsigned short _id) 
  : ADBoard(_type, _id) 
{
  //- std::cout << "ADCalibrableAIOBoard::ADCalibrableAIOBoard <-" << std::endl;
  //- std::cout << "ADCalibrableAIOBoard::ADCalibrableAIOBoard ->" << std::endl;
}

// ============================================================================
// ADCalibrableAIOBoard::~ADCalibrableAIOBoard
// ============================================================================
ADCalibrableAIOBoard::~ADCalibrableAIOBoard ()
{
  //- std::cout << "ADCalibrableAIOBoard::~ADCalibrableAIOBoard <-" << std::endl;
  //- std::cout << "ADCalibrableAIOBoard::~ADCalibrableAIOBoard ->" << std::endl;
}

// ============================================================================
// ADCalibrableAIOBoard::auto_calibrate
// ============================================================================
void ADCalibrableAIOBoard::auto_calibrate (bool _save_data)
  throw (asl::DAQException)
{
#if !defined(_SIMULATION_)

  int err = this->release_hardware();
  if (err < 0) {
    //- hardware registration failed
    throw asl::DAQException("hardware release failed",
                            adl::d2kdask_error_text(err),
                            "ADCalibrableAIOBoard::auto_calibrate",
                            err);
  }

  ACE_OS::sleep(ACE_Time_Value(0,100000));

  err = this->register_hardware();
  if (err < 0) {
    //- hardware registration failed
    throw asl::DAQException("hardware registration failed",
                            adl::d2kdask_error_text(err),
                            "ADCalibrableAIOBoard::auto_calibrate",
                            err);
  }

  err = ::D2K_DB_Auto_Calibration_ALL(this->idid_);
  if (err != NoError) {
    throw asl::DAQException("hardware calibration failed",
                            adl::d2kdask_error_text(err),
                            "ADCalibrableAIOBoard::auto_calibrate",
                            err);
  }
	
  if (_save_data)
  {
    this->save_calibration(kDEFAULT_CALIBRATION_BANK); 
  }
	

  err = this->release_hardware();
  if (err < 0) {
    //- hardware registration failed
    throw asl::DAQException("hardware release failed",
                            adl::d2kdask_error_text(err),
                            "ADCalibrableAIOBoard::auto_calibrate",
                            err);
  }

  ACE_OS::sleep(ACE_Time_Value(0,100000));

  err = this->register_hardware();
  if (err < 0) {
    //- hardware registration failed
    throw asl::DAQException("hardware registration failed",
                            adl::d2kdask_error_text(err),
                            "ADCalibrableAIOBoard::auto_calibrate",
                            err);
  }

#else //- _SIMULATION_

  ACE_OS::sleep(10);

#endif //- _SIMULATION_
}

// ============================================================================
// ADCalibrableAIOBoard::save_calibration
// ============================================================================
void ADCalibrableAIOBoard::save_calibration (adl::AIOCalibrationBank _bank) 
  throw (asl::DAQException)
{
#if !defined(_SIMULATION_)

  int err = ::D2K_EEPROM_CAL_Constant_Update(this->idid_, _bank);
  if (err < 0) {
    throw asl::DAQException("could not save calibration data",
                            adl::d2kdask_error_text(err),
                            "ADCalibrableAIOBoard::save_calibration",
                            err);
  }

#endif
}

// ============================================================================
// ADCalibrableAIOBoard::load_calibration
// ============================================================================
void ADCalibrableAIOBoard::load_calibration (adl::AIOCalibrationBank _bank) 
  throw (asl::DAQException)
{
#if !defined(_SIMULATION_)

  int err = ::D2K_Load_CAL_Data(this->idid_, _bank);
  if (err < 0) {
    throw asl::DAQException("could not load calibration data",
                            adl::d2kdask_error_text(err),
                            "ADCalibrableAIOBoard::load_calibration",
                            err);
  }

#endif
}

} // namespace adl







