// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    SingleShotAO.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/SingleShotAO.h>

#if !defined (__ASL_INLINE__)
# include <asl/SingleShotAO.i>
#endif // __ASL_INLINE__

namespace asl {

// ============================================================================
// SingleShotAO:: SingleShotAO
// ============================================================================
SingleShotAO:: SingleShotAO () 
	: SingleShotDAQ(), daq_hw_ (0)
{
	//- std::cout << "SingleShotAO::SingleShotAO <-" << std::endl;	
	//- std::cout << "SingleShotAO::SingleShotAO ->" << std::endl;
}

// ============================================================================
// SingleShotAO:: ~SingleShotAO
// ============================================================================
SingleShotAO::~SingleShotAO () 
{
	//- std::cout << "SingleShotAO::~SingleShotAO <-" << std::endl;
	
	//- release DAQ hardware
	if (this->daq_hw_) {
		this->daq_hw_->release();
		this->daq_hw_ = 0;
	}
	
	//- std::cout << "SingleShotAO::~SingleShotAO ->" << std::endl;
}

// ============================================================================
// SingleShotAO::init
// ============================================================================
void SingleShotAO::init (unsigned short _type, unsigned short _id)
throw (asl::DAQException)
{
	try 
	{
		//- instanciate the board - don't get exclusive access
		this->daq_hw_ = adl::ADSingleShotAOBoard::instanciate(_type, _id, false);
	}
	catch (const asl::DAQException&) 
	{
		//- rethrow exception
		throw;
	}
	catch (...) 
	{
		//- throw unknown error
		throw DAQException();
	}
}

// ============================================================================
// SingleShotAO::calibrate_hardware
// ============================================================================
void SingleShotAO::calibrate_hardware ()
throw (asl::DAQException)
{
	try 
	{
		//- calibrate hardware
		this->daq_hw_->auto_calibrate(false);
		//- save calibration dat into default bank
		this->daq_hw_->save_calibration(kDEFAULT_CALIBRATION_BANK);
	}
	catch (const DAQException&)
	{
		throw;
	}
	catch (...) 
	{
		throw DAQException();
	}
}


// ============================================================================
// SingleShotAO::configure_channel
// ============================================================================
void SingleShotAO::configure_channel (adl::ChanId _chan_id, 
									  adl::OutputPolarity _polarity, 
									  adl::VoltageReferenceSource _ref_source, 
									  double _ref_volt) throw (asl::DAQException)
{
	if (this->initialized() == false) 
	{
		throw asl::DAQException("unitialized DAQ",
			"SingleShotAO::init must be called before any other functions",
			"SingleShotAO::configure_channel");
	}
	
	try
	{
		this->daq_hw_->configure_ao_channel(_chan_id, _polarity, _ref_source, _ref_volt);
	} 
	catch (const DAQException&)
	{
		throw;
	}
	catch (...) {
		throw DAQException();
	}
	
}
// ============================================================================
// SingleShotAO::write_channel
// ============================================================================
void SingleShotAO::write_channel (adl::ChanId _chan_id, unsigned short _value) 
  throw (asl::DAQException)
{	
	if (this->initialized() == false) 
	{
		throw asl::DAQException("unitialized DAQ",
			"SingleShotAO::init must be called before any other functions",
			"SingleShotAO::write_channel");
	}
	
	try
	{
		this->daq_hw_->write_channel(_chan_id, _value );
	} 
	catch (const DAQException&) 
	{
		throw;
	}
	catch (...) 
	{
		throw DAQException();
	}
}
// ============================================================================
// SingleShotAO::write_scaled_channel
// ============================================================================
void SingleShotAO::write_scaled_channel (adl::ChanId _chan_id, double _value) 
  throw (asl::DAQException) 
{
	if (this->initialized() == false)
	{
		throw asl::DAQException("unitialized DAQ",
			"SingleShotAO::init must be called before any other functions",
			"SingleShotAO::write_scaled_channel");
	}
	
	try
	{
		this->daq_hw_->write_scaled_channel(_chan_id, _value);
	} 
	catch (const DAQException&) 
	{
		throw;
	}
	catch (...) 
	{
		throw DAQException();
	}
}

} //namespace


