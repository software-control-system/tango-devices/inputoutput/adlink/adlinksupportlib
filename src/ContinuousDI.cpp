// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ContinuousDI.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#if defined(_SIMULATION_)
# include <asl/PulsedTask.h>
#endif
#include <asl/Message.h>
#include <asl/Extractor_T.h>
#include <asl/DIData.h>
#include <asl/ContinuousDI.h>
#include <asl/ContinuousDAQHandler.h>
#include <asl/DAQException.h>

#if !defined (__ASL_INLINE__)
# include <asl/ContinuousDI.i>
#endif // __ASL_INLINE__

namespace asl {

// ============================================================================
// STATICS
// ============================================================================
ContinuousDI* ContinuousDI::instance = 0;
ACE_Thread_Mutex ContinuousDI::delete_lock;

#if defined(_SIMULATION_)
// ============================================================================
// PERIOD BETWEEN TWO CALLS TO THE CALLBACK IN MILLISECONDS
// ============================================================================
#define kCB_PERIOD 5

// ============================================================================
// CallbackCaller : an asl::PulsedTask to simulate DAQ activity
// ============================================================================
class CallbackCaller : public asl::PulsedTask
{
public:

  CallbackCaller () {/*noop*/};

  virtual ~CallbackCaller () {/*noop*/};

  virtual int pulsed (void* arg) 
  {
    DAQCallback cb = ACE_reinterpret_cast(DAQCallback, arg);
    if (cb) cb();
    return 0;
  }

};
#endif // _SIMULATION_

// ============================================================================
// The ContinuousDI underlying Worker's Work
// ============================================================================
class ContinuousDIWork : public asl::Work
{
public:

  //- ctor
  ContinuousDIWork (ContinuousDI * _di) 
		: daq_end_received_(false), 
      di_(_di), 
      di_port_width_(adl::width_8),
      thread_executing_hook_(0) 
  {
    /*noop*/
  };

  //- dtor
  virtual ~ContinuousDIWork () 
  {
    /*noop*/
  };

  //- <thread executing hook>'s id
  ACE_thread_t thread_executing_hook () const 
  {
    return this->thread_executing_hook_;
  };

  virtual int on_start (WorkerError *&err) 
	{
    this->daq_end_received_ = false;
    this->di_port_width_ = this->di_->configuration().get_port_width();
		err = 0;
		return 0;
  };

  //- process hook overload
  virtual int process (Message *in, WorkerError *&err)
  {
    //- init return value
    int result = 0;
    //- init <out> error message  
    err = 0;
  
    //- save id of thread executing user hook
    thread_executing_hook_ = ACE_Thread::self();

    // Filter message... 
    //-------------------------
    switch (in->msg_type())
    {
      // <in> is a <DATA> message.
      //-------------------------
      case MessageType::MSG_DATA:
      {
		    //- avoid sending data to user after daq-end evt
		    if (this->daq_end_received_ == false) 
			  {
          BaseBuffer * data;
          //- get message content
          if (message_data(in, data) == -1) 
          {
            ASL_NEW(err, WorkerError("unexpected data", 1));
            in->release();
            result = -1;
            break;
          }
          //- detach data (i.e. get ownership)
          in->detach_data();
          //- release input message to avoid memory leaks
          in->release();
          //- call user defined handle_xxx_input
          try 
          {
            switch (this->di_port_width_)
            {
              case adl::width_8:
                this->di_->handle_byte_input(reinterpret_cast<asl::DIByteData*>(data));
                break;
              case adl::width_16:
                this->di_->handle_short_input(reinterpret_cast<asl::DIShortData*>(data));
                break;
              case adl::width_32:
                this->di_->handle_long_input(reinterpret_cast<asl::DILongData*>(data));
                break;
              default:
                throw asl::DAQException("invalid configuration",
                                        "invalid port width speccified",
                                        "ContinuousDIWork::process");
                break;
              }
          }
          catch (...) 
          {
				    //- ignore exception 
			    }
        }
			  else 
			  {
          in->release();
			  }
      }
      break;

      // <in> is a <OVERRUN> message.
      //-----------------------------
      case MessageType::MSG_OVERRUN:
      case MessageType::MSG_OVERLOAD:
      {
        //- release input message to avoid memory leaks
        in->release();
        //- call user defined data_lost
        try 
        {
          this->di_->handle_data_lost();
        }
        catch (...) 
        {
          //- ignore exception 
        }
      }
      break;

		  // <in> is a <MSG_DAQEND> message.
		  //-----------------------------
	    case MessageType::MSG_DAQEND:
		  {
		    //- mark evt 
		    this->daq_end_received_ = true;
		    //- expected data
			  Container<ContinuousDAQ::DaqEndReason> * why;
			  //- get message content
			  if (message_data(in, why) == -1) 
        {
				  ASL_NEW(err, WorkerError("unexpected data", 1));
				  in->release();
				  result = -1;
				  break;
			  }
			  //- call user defined overrun
			  try 
        {
				  this->di_->handle_daq_end(why->content());
			  }
			  catch (...) 
        {
				  //- ignore exception 
			  }
		    //- release input message to avoid memory leaks
			  in->release();
		  }
		  break;


      // <in> is a <TIMEOUT> message.
      //-----------------------------
      case MessageType::MSG_TIMEOUT:
      {
        //- release input message to avoid memory leaks
        in->release();
        //- call user defined overrun
        try 
        {
          this->di_->handle_timeout();
        }
        catch (...) 
        {
          //- ignore exception 
        }
      }
      break;

      // <in> is a <MSG_FATAL_ERROR> message.
      //-----------------------------
      case MessageType::MSG_FATAL_ERROR:
      {
        //- expected data
        Container<const DAQException>* c = 0;
        //- get message content
        if (message_data(in, c) == -1) 
        {
          ASL_NEW(err, WorkerError("unexpected data", 1));
          in->release();
          result = -1;
          break;
        }
        //- call user defined handle_error
        try 
        {
          this->di_->handle_error(c->content());
        }
        catch (...) 
        {
          //- ignore exception 
        }
        //- release input message to avoid memory leaks
        in->release();
      }
      break;

      // <in> has a type we do not handle
      //---------------------------------
      default:
      {
        //- release input message to avoid memory leaks
        in->release();
      }  
      break;
    }

    return result;
  }

private:

  //- dad-end evt flag
	bool daq_end_received_; 
	
  //- the associated ContinuousDI
  ContinuousDI* di_;

  //- store di port width locally (little optimisation)
  adl::DIOPortWidth di_port_width_;

  //- id of the thread executing a user hook
  ACE_thread_t thread_executing_hook_; 
};

// ============================================================================
// ContinuousDI::ContinuousDI
// ============================================================================
ContinuousDI::ContinuousDI ()
  : worker_(0), 
    work_(0), 
    daq_hw_ (0)
#if defined(_SIMULATION_)
    ,cb_caller_(0)
#endif
{
  ContinuousDI::instance = this;
}

// ============================================================================
// ContinuousDI::~ContinuousDI
// ============================================================================
ContinuousDI::~ContinuousDI ()
{
  //- std::cout << "ContinuousDI::~ContinuousDI <-" << std::endl;

  //- need to lock 
  //- let current action complete (e.g. calibration)
  ACE_GUARD_REACTION(ACE_SYNCH_MUTEX, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());

  //- also need to fight against race condition between the 
  //- ContinuousDI::di_end_event_callback and this dtor. 
  //- ContinuousDI::di_end_event_callback may try to use a dead
  //- ContinuousDI::instance in case the dtor is deleting all.
  ACE_GUARD_REACTION(ACE_SYNCH_MUTEX, 
                     ace_delete_mon, 
                     ContinuousDI::delete_lock, 
                     throw asl::DAQException());

  //- try to stop the DAQ
  try 
  {
		//- std::cout << "ContinuousDI::~ContinuousDI::stop DAQ <-" << std::endl;
    if (this->daq_hw_)
		  this->stop_i();
		//- std::cout << "ContinuousDI::~ContinuousDI::stop DAQ <-" << std::endl;
  }
  catch (...) 
  {
    //- ignore any exception
  }

  //- quit then release worker
  if (this->worker_) 
  {
    //- std::cout << "ContinuousDI::~ContinuousDI::abort worker <-" << std::endl;
		this->worker_->abort();
		//- std::cout << "ContinuousDI::~ContinuousDI::abort worker ->" << std::endl;

		//- std::cout << "ContinuousDI::~ContinuousDI::quit worker <-" << std::endl;
		this->worker_->quit();
		//- std::cout << "ContinuousDI::~ContinuousDI::quit worker ->" << std::endl;

		//- std::cout << "ContinuousDI::~ContinuousDI::delete worker ->" << std::endl;
		delete this->worker_;
		//- std::cout << "ContinuousDI::~ContinuousDI::delete worker <-" << std::endl;

		this->worker_ = 0;
  }

  //- quit then release worker
  if (this->work_) 
  {
		//- std::cout << "ContinuousDI::~ContinuousDI::delete work <-" << std::endl;
		delete this->work_;
		//- std::cout << "ContinuousDI::~ContinuousDI::delete work ->" << std::endl;
		this->work_ = 0;
  }

  //- release DAQ hardware
  if (this->daq_hw_) 
  {
		//- std::cout << "ContinuousDI::~ContinuousDI::release hardware <-" << std::endl;
		this->daq_hw_->release();
		//- std::cout << "ContinuousDI::~ContinuousDI::release hardware ->" << std::endl;
		this->daq_hw_ = 0;
  }

  ContinuousDI::instance = 0;

  //- std::cout << "ContinuousDI::~ContinuousDI ->" << std::endl;
}

// ============================================================================
// ContinuousDI::init
// ============================================================================
void ContinuousDI::init (unsigned short _type, unsigned short _id)
     throw (asl::DAQException)
{
  //- std::cout << "ContinuousDI::init<-" << std::endl;

  ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());

  //- return if already initialized
  if (this->initialized()) 
  {
    return; 
  }

  //- instanciate the work
  ASL_NEW(this->work_, ContinuousDIWork(this));
  if (this->work_ == 0) 
  {
    this->state_ = FAULT;
    //- out of memory
    throw asl::DAQException("out of memory",
                            "out of memory error",
                            "ContinuousDI::init");
  }

  //- instanciate the worker
  ASL_NEW(this->worker_, Worker("unnamed", this->work_, false));
  if (this->worker_ == 0) 
  {
    this->state_ = FAULT;
    //- out of memory
    throw asl::DAQException("out of memory",
                            "out of memory error",
                            "ContinuousDI::init");
  }

  //- init worker
  if (this->worker_->init() == -1) 
  {
    this->state_ = FAULT;
    throw asl::DAQException("internal error",
                            "could start deferred procesing task",
                            "ContinuousDI::init");
  }

  try 
  {
    //- instanciate the board - get exclusive access
    this->daq_hw_ = adl::ADContinuousDIBoard::instanciate(_type, _id, false);
  }
	catch (const asl::DAQException&) 
  {
    this->state_ = FAULT;
		//- rethrow exception
		throw;
	}
	catch (...) 
  {
    this->state_ = FAULT;
		//- throw unknown error
		throw DAQException();
	}

  //- change state to STANDBY (ready to run with default DAQ config)
  this->state_ = STANDBY;

  //- std::cout << "ContinuousDI::init ->" << std::endl;
}

// ============================================================================
// ContinuousDI::configure
// ============================================================================
void ContinuousDI::configure (const ContinuousDIConfig& _config)
  throw (asl::DAQException, asl::DeviceBusyException)
{
	//- std::cout << "ContinuousDI::configure <-" << std::endl;

  TIMEOUT_GUARD(this->lock_, "ContinuousDI::configure"); 

  this->action_allowed("ContinuousDI::configure");

  if (this->state() == RUNNING)
  {
    throw asl::DeviceBusyException("device busy",
                                   "can't modify configuration while DAQ is running",
                                   "ContinuousDI::configure");
  }

  CHECK_GUARD_LOCKED("ContinuousDI::configure");

  //- configure without blocking nor changing internal state
  this->configure_i(_config);

  //- change state to STANDBY (ready to run with user config)
  this->state_ = STANDBY;

	//- std::cout << "ContinuousDI::configure ->" << std::endl;
}

// ============================================================================
// ContinuousDI::configure_i
// ============================================================================
void ContinuousDI::configure_i (const ContinuousDIConfig& _config)
     throw (asl::DAQException)
{
	//- std::cout << "ContinuousDI::configure_i <-" << std::endl;

  //- throw if not initialized
  if (this->initialized() == false) 
  {
    throw asl::DAQException("unitialized DAQ",
                            "ContinuousDI::init must be called before any other functions",
                            "ContinuousDI::configure");
  }

  //- copy config locally
  this->config_ = _config;

  //- verbose
#if defined(ASL_DEBUG)
  this->config_.dump();
#endif

  //- store db event callback in configuration
  this->config_.set_db_event_callback(ContinuousDI::db_event_callback);

	//- store di-end event callback in configuration
	this->config_.set_di_end_event_callback(ContinuousDI::di_end_event_callback);

  //- adapt worker's message queue lo/hi water mark to DAQ buffer depth
  size_t wm = this->config_.get_buffer_depth()
            * (this->config_.get_port_width() / 8)
            * kMAX_MESSAGES;

  //- apply limit 
  if (wm > kMAX_MSGQ_BYTES) 
  {
    wm = kMAX_MSGQ_BYTES;  
  }

  //- set worker's lo water mark
  this->worker_->low_water_mark(wm);

  //- set worker's hi water mark
  this->worker_->high_water_mark(wm);

  //- set worker's timeout
  this->worker_->timeout(this->config_.get_timeout());

  //- hardware configuration is done when daq is started

	//- std::cout << "ContinuousDI::configure_i ->" << std::endl;
}

// ============================================================================
// ContinuousDI::start
// ============================================================================
void ContinuousDI::start ()
  throw (asl::DAQException, asl::DeviceBusyException)
{
  //- std::cout << "ContinuousDI::start <-" << std::endl;

  TIMEOUT_GUARD(this->lock_, "ContinuousDI::start");

  this->action_allowed("ContinuousDI::start");

  if (this->state() == RUNNING)
    return;

  CHECK_GUARD_LOCKED("ContinuousDI::start");

  //- start without blocking nor changing internal state 
  this->start_i();
  
  //- change state to RUNNING
  this->state_ = RUNNING;

  //- std::cout << "ContinuousDI::start ->" << std::endl;
}

// ============================================================================
// ContinuousDI::start_i
// ============================================================================
void ContinuousDI::start_i ()
     throw (asl::DAQException)
{
	//- std::cout << "ContinuousDI::start_i <-" << std::endl;

  //- throw if not initialized
  if (this->initialized() == false) 
  {
    throw asl::DAQException("unitialized DAQ",
                            "ContinuousDI::init must be called before any other functions",
                            "ContinuousDI::start");
  }

	bool wait = true;
	if (this->work_->thread_executing_hook() == ACE_Thread::self()) 
  {
		//- abort called from user hook: don't wait to avoid deadlock!
		wait = false;
	}

	//- start worker
	bool tmo_expired;
	Timeout tmo = ACE_OS::gettimeofday() + Timeout(0, kPOST_TIMEOUT);
	int err = this->worker_->start(wait, &tmo, &tmo_expired);
  if (err == -1)
  {
    if (tmo_expired) 
    {
      //- ignore timeout (worker just busy)
      err = 0;
    }
    else
    {
      this->state_ = FAULT;
      throw asl::DAQException("asl internal error",
                              "could not start deferred processing task",
                              "ContinuousDI::start");
    }
  }

  try 
  {
    //- configure hardware
    this->daq_hw_->configure_continuous_di(this->config_);
    //- start daq
    this->daq_hw_->start_continuous_di();
  } 
  catch (DAQException) 
  {
    //- abort worker
    this->worker_->abort(wait);
    throw;
  }
  catch (...) 
  {
    //- abort worker
    this->worker_->abort(wait);
    throw DAQException();
  }

#if defined(_SIMULATION_)

  //- start CallbackCaller
  ASL_NEW(this->cb_caller_, CallbackCaller);
  if (this->cb_caller_ == 0) 
  {
    //- abort worker
    this->worker_->abort(wait);
    throw asl::DAQException();
  }
  this->cb_caller_->start(ContinuousDI::db_event_callback, kCB_PERIOD);

#endif // _SIMULATION_

	//- std::cout << "ContinuousDI::start_i ->" << std::endl;
}

// ============================================================================
// ContinuousDI::stop
// ============================================================================
void ContinuousDI::stop ()
  throw (asl::DAQException, asl::DeviceBusyException)
{
  //- std::cout << "ContinuousDI::stop <-" << std::endl;

  TIMEOUT_GUARD(this->lock_, "ContinuousDI::stop");

  this->action_allowed("ContinuousDI::stop");

  if (this->state() == STANDBY)
    return;

  CHECK_GUARD_LOCKED("ContinuousDI::stop");

  try
  {
    //- change state to ABORTING
    this->state_ = ABORTING;
    //- stop without blocking nor changing internal state
    this->stop_i();
    //- change state to STANDBY
    this->state_ = STANDBY;
  }
  catch (...)
  {
    //- change state to FAULT
    this->state_ = FAULT;
    //- forward exception to caller
    throw;
  }

  //- std::cout << "ContinuousDI::stop ->" << std::endl;
}

// ============================================================================
// ContinuousDI::stop_i
// ============================================================================
void ContinuousDI::stop_i ()
     throw (asl::DAQException)
{
	//- std::cout << "ContinuousDI::stop_i <-" << std::endl;

  //- throw if not initialized
  if (this->initialized() == false) 
  {
    throw asl::DAQException("unitialized DAQ",
                            "ContinuousDI::init must be called before any other functions",
                            "ContinuousDI::stop");
  }

	bool wait = true;
	if (this->work_->thread_executing_hook() == ACE_Thread::self()) 
  {
		//- abort called from user hook: don't wait to avoid deadlock!
		wait = false;
	}

	//- stop worker
	bool tmo_expired;
	Timeout tmo = ACE_OS::gettimeofday() + Timeout(0, kPOST_TIMEOUT);
	int err = this->worker_->stop(wait, &tmo, &tmo_expired);
  //- worker error will be handled after call to <daq_hw_->stop_continuous_ai> 
  //- because we must try to stop DAQ before leaving the place - here we just 
  //- ignore tmo errors
  if (err == -1 && tmo_expired)
  {
    //- ignore timeout (worker just busy)
    err = 0;
  }

  //- stop daq
  try 
  {
    this->daq_hw_->stop_continuous_di();
  } 
  catch (DAQException) 
  {
    throw;
  }
  catch (...) 
  {
    throw DAQException();
  }

#if defined(_SIMULATION_)

  //- stop CallbackCaller
  if (this->cb_caller_) 
  {
    this->cb_caller_->stop();
    delete this->cb_caller_;
    this->cb_caller_ =0;
  }

#endif // _SIMULATION_

  //- handle worker error
  if (err == -1) 
  {
    //- throw daq error
    throw asl::DAQException("internal error",
                            "could not stop deferred processing task",
                            "ContinuousDI::stop");
  }

	//- std::cout << "ContinuousDI::stop_i ->" << std::endl;
}

// ============================================================================
// ContinuousDI::abort
// ============================================================================
void ContinuousDI::abort ()
  throw (asl::DAQException, asl::DeviceBusyException)
{
  //- std::cout << "ContinuousDI::abort <-" << std::endl;

  TIMEOUT_GUARD(this->lock_, "ContinuousDI::abort");

  this->action_allowed("ContinuousDI::abort");

  if (this->state() == STANDBY)
    return;

  CHECK_GUARD_LOCKED("ContinuousDI::abort");

  try
  {
    //- change state to ABORTING
    this->state_ = ABORTING;
    //- abort without blocking nor changing internal state
    this->abort_i();
    //- change state to STANDBY
    this->state_ = STANDBY;
  }
  catch (...)
  {
    //- change state to FAULT
    this->state_ = FAULT;
    //- forward exception to caller
    throw;
  }

  //- std::cout << "ContinuousDI::abort ->" << std::endl;
}

// ============================================================================
// ContinuousDI::abort_i
// ============================================================================
void ContinuousDI::abort_i ()
     throw (asl::DAQException)
{
	//- std::cout << "ContinuousDI::abort_i <-" << std::endl;

	//- throw if not initialized
	if (this->initialized() == false) 
  {
		throw asl::DAQException("unitialized DAQ",
														"ContinuousDI::init must be called before any other functions",
														"ContinuousDI::abort");
	}

	bool wait = true;
	if (this->work_->thread_executing_hook() == ACE_Thread::self()) 
  {
		//- abort called from user hook: don't wait to avoid deadlock!
		wait = false;
	}

	//- abort worker
	bool tmo_expired;
	Timeout tmo = ACE_OS::gettimeofday() + Timeout(0, kPOST_TIMEOUT);
	int err = this->worker_->abort(wait, &tmo, &tmo_expired);
  //- worker error will be handled after call to <daq_hw_->stop_continuous_ai> 
  //- because we must try to stop DAQ before leaving the place - here we just 
  //- ignore tmo errors
  if (err == -1 && tmo_expired)
  {
    //- ignore timeout (worker just busy)
    err = 0;
  }

	//- stop daq
	try 
  {
		this->daq_hw_->stop_continuous_di();
	} 
	catch (const DAQException&) 
  {
		throw;
	}
	catch (...) 
  {
		throw DAQException();
	}

#if defined(_SIMULATION_)

	//- stop CallbackCaller
	if (this->cb_caller_) 
  {
		this->cb_caller_->stop();
		delete this->cb_caller_;
		this->cb_caller_ = 0;
	}

#endif // _SIMULATION_

	//- handle worker error
	if (err == -1) 
  {
		//- throw daq error
		throw asl::DAQException("asl internal error",
														"could not abort deferred processing task",
														"ContinuousDI::abort");
	}

	//- std::cout << "ContinuousDI::abort_i ->" << std::endl;
}

// ============================================================================
// ContinuousDI::handle_data_lost_exception
// ============================================================================
void ContinuousDI::handle_data_lost_exception ()
     throw (asl::DAQException)
{
  //- std::cout << "ContinuousDI::handle_data_lost_exception <-" << std::endl;

  //- lock 
  ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());

  //- check exception status
  if (this->exception_status_ != HANDLING_OVERRUN) 
  {
    //- nothing to do here
    return;
  }

  //- apply overrun strategy
  try 
  {
    adl::DataLostStrategy dls = this->config_.get_data_lost_strategy();
    switch (dls)
    {
      case adl::abort:
        //- abort without locking nor changing internal state
        this->abort_i();
        //- change state
        this->state_ = STANDBY;
        break;
      case adl::restart:
        //- stop without locking nor changing internal state
        //- why stop instead of abort?
        //- if abort, any valid data store in the msg-q will be ignored
        this->stop_i();
        //- change state
        this->state_ = STANDBY;
        //- (re)start without locking nor changing internal state
        this->start_i();
        //- change state
        this->state_ = RUNNING;
        break;
      default:
        break;
    } 
  }
  catch (...) 
  {
    //- change state
    this->state_ = FAULT;
    //- we are running in the continuous DAQ handler's thread so...
    //- ignore exception
  }

  //- exception/evt handled
  this->exception_status_ = HANDLING_NONE;
  
  //- std::cout << "ContinuousDI::handle_data_lost_exception ->" << std::endl;
}

// ============================================================================
// ContinuousDI::handle_daq_exception
// ============================================================================
void ContinuousDI::handle_daq_exception ()
     throw (asl::DAQException)
{
  //- std::cout << "ContinuousDI::handle_daq_exception <-" << std::endl;

  ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());

  //- check exception status
  if (this->exception_status_ != HANDLING_ERROR) 
  {
    //- nothing to do here
    return;
  }

  try 
  {
    //- abort without locking nor changing internal state
    this->abort_i();
    //- change state
    this->state_ = STANDBY;
  }
  catch (...) 
  {
    //- change state
    this->state_ = FAULT;
    //- we are running in the continuous DAQ handler's thread so...
    //- ignore exception
  }

  //- exception/evt handled
  this->exception_status_ = HANDLING_NONE;

  //- std::cout << "ContinuousDI::handle_daq_exception ->" << std::endl;
}

// ============================================================================
// ContinuousDI::db_event_callback
// ============================================================================
#if defined (WIN32)
  void ContinuousDI::db_event_callback ()
#else
  void ContinuousDI::db_event_callback (int sig_num)
#endif
{
 //- std::cout << "ContinuousDI::db_event_callback <-" << std::endl;

 ContinuousDI* instance = ContinuousDI::instance;

#if defined (__linux)
  ACE_UNUSED_ARG(sig_num);
#endif

  //- IMPORTANT NOTE:
  //- DO NOT BLOCK TO ACQUIRE THE CONTINUOUS-DAQ LOCKING OBJECT IN THIS METHOD.
  //- POTENTIAL DEALOCK! THE STOP (OR ABORT) MEMBER ACQUIRE THE LOCK AND CALL 
  //- D2K_AI_ASYNCCLEAR. THE DRIVER THEN WAIT FOR THE CALLBACK TO COMPLETE
  //- BEFORE STOPPING THE ASYNC OPERATION. IF THIS CALLBACK IS WAITING FOR THE 
  //- LOCKING OBJECT TO BE FREE, THE PROCESS HANGS (DEADLOCK).

	//- try to acquire the DAQ <lock_>
	if (instance->lock_.tryacquire() == -1)
	{
    //- someone else hold the <lock_> or could not lock. in the first case: 
    //- means that the DAQ is either handling exception, calibrating the 
    //- hardware or handling an external request (such as stop or abort).
    //- in the second case (i.e. could not lock) this is an error we can't
    //- handle here because we are called by ADLink driver - in both cases 
    //- just we leave the place ...
    return;
	}

	//- check internal state: DAQ must be running whitout handling exception
	if (instance->state_ != RUNNING || instance->exception_status_ != HANDLING_NONE) {
		//- release the DAQ <lock_>
		instance->lock_.release();
		return;
	}

  try 
  {
    //- get data from hardware
    BaseBuffer * data = instance->daq_hw_->di_db_event_callback(); 
		//- check result
		if (data == 0) 
    {
      //- the following exception will be catched locally (see below)
			throw asl::DAQException("out of memory",
															"out of memory error",
															"ContinuousDI::db_event_callback");
		}
    //- instanciate message
    Message * msg;
    ASL_NEW(msg, Message(data));
    if (msg == 0) {
      delete data;
      //- the following exception will be catched locally (see below)
      throw asl::DAQException("out of memory",
                              "out of memory error",
                              "ContinuousDI::db_event_callback");
    }
    //- send it to the consumer (i.e. Worker)
    Timeout tmo = ACE_OS::gettimeofday() + Timeout(0, kPOST_TIMEOUT);
    int err = instance->worker_->post(msg, &tmo);
    if (err == -1) 
    {
      msg->release();
      if (errno == EWOULDBLOCK) 
      {
        //- the following exception will be catched locally (see below)
        throw DataLostException(DataLostException::PROC_TASK_OVERLOAD);
      } 
      else 
      {
        //- the following exception will be catched locally (see below)
        throw DAQException("asl internal error",
                           "could not post data to deferred processing task",
                           "ContinuousDI::db_event_callback");
      }
    }
  }
  catch (const DataLostException& dle) 
  {
		//- apply overrun strategy
		adl::DataLostStrategy dls = instance->config_.get_data_lost_strategy();
		switch (dls)
		{
      case adl::notify:
      case adl::abort:
      case adl::restart:
				{
				  if (dls != adl::abort) 
					{
						//- post <MSG_OVERRUN> : no timeout
						int msg_type = (dle.why() == DataLostException::PROC_TASK_OVERLOAD) 
															?
														MessageType::MSG_OVERLOAD
															:
														MessageType::MSG_OVERRUN;
						Message * msg = new Message(msg_type, true);
						if (msg == 0) 
            {
							instance->handle_daq_exception_i(DAQException("out of memory",
																														"out of memory error",
																														"ContinuousDI::db_event_callback"));
						}
						int err = instance->worker_->post(msg);
						if (err == -1) 
            {
							msg->release();
							instance->handle_daq_exception_i(DAQException("asl internal error",
																														"could not post OVERRUN message to deferred processing task",
																														"ContinuousDI::db_event_callback"));
						}
					}
					else if (dls != adl::notify) 
					{
						instance->handle_data_lost_exception_i();
					}
				}
				break;
      case adl::trash:
			default:
        break;
    }
  }
  catch (const DAQException& ex) 
  {
    //- daq error
    instance->handle_daq_exception_i(ex);
  }
  catch (...) 
  {
    //- unknown error
    instance->handle_daq_exception_i(DAQException());
  }

  //- release the DAQ <lock_>
  instance->lock_.release();

  //- std::cout << "ContinuousDI::db_event_callback ->" << std::endl;
}

// ============================================================================
// ContinuousDI::di_end_event_callback
// ============================================================================
#if defined (WIN32)
	void ContinuousDI::di_end_event_callback ()
#else
	void ContinuousDI::di_end_event_callback (int sig_num)
#endif
{
#if defined(__linux)
  ACE_UNUSED_ARG(sig_num);
#endif

  //- std::cout << "ContinuousDI::di_end_event_callback <-" << std::endl;

  //- need to fight against race condition between
  //- ContinuousDI::di_end_event_callback and ContinuousDI dtor. 
  //- ContinuousDI::di_end_event_callback may try to use a dead
  //- ContinuousDI::instance in case the dtor is deleting all.
  ACE_GUARD_REACTION(ACE_SYNCH_MUTEX, 
                     ace_delete_mon, 
                     ContinuousDI::delete_lock, 
                     return);

  ContinuousDI* instance = ContinuousDI::instance;

  //- ContinuousDI::instance has been killed - leave the place
  if (instance == 0)
  {
    return;
  }

	//- find out why the acquisition stopped
	DaqEndReason why = instance->daq_end_reason();
	
  //- to not notify user in the following cases:
  //- daq end due to buffer overrun and overrun strategy is set to restart 
	if ( 
       why == DAQEND_ON_OVERRUN 
        && 
       instance->config_.get_data_lost_strategy() == adl::restart
     )
	   return;
	
  //- in case daq end is due to ext.trigger...
  if (why == DAQEND_ON_EXTERNAL_TRIGGER)
  {
    //- change state to STANDBY
    instance->state_ = STANDBY;
  }

	//- encapsulate <why> into a asl::Container
  Container<ContinuousDAQ::DaqEndReason>* msg_data;
  ASL_NEW(msg_data, Container<ContinuousDAQ::DaqEndReason>(why));
  if (msg_data == 0) 
  {
    //- can't do anything here: we are called by ADLink driver
    return;
  }

  //- post <MessageType::MSG_DAQEND> : no timeout 
  Message * msg;
  ASL_NEW(msg, Message(msg_data, MessageType::MSG_DAQEND, true));
  if (msg == 0) 
  {
    //- can't do anything here: we are called by ADLink driver
    delete msg_data;
    return;
  }
	
  //- we can safely use the worker: thanks to the ContinuousDI::delete_lock,
  //- if the ContinuousDI::instance is alive then its worker is also alive
  int err = instance->worker_->post(msg);
  if (err == -1) 
  {
    //- can't do anything here: we are called by ADLink driver
    msg->release();
    return;
  }

	//- std::cout << "ContinuousDI::di_end_event_callback ->" << std::endl;
}
  
// ============================================================================
// ContinuousDI::daq_end_reason
// ============================================================================
ContinuousDAQ::DaqEndReason ContinuousDI::daq_end_reason ()
{
	//- find out why the acquisition stopped
	ContinuousDAQ::DaqEndReason why = DAQEND_ON_UNKNOWN_EVT;
	
  //- std::cout << "ContinuousDI::di_end_event_callback::state::" << this->state_ << std::endl;
  //- std::cout << "ContinuousDI::di_end_event_callback::exception_status::" << this->exception_status_ << std::endl;

	if (this->exception_status_ == HANDLING_NONE) 
	{
	  if (this->state_ == ABORTING || this->state_ == STANDBY) 
		{
		  why =  DAQEND_ON_USER_REQUEST;
	    //- std::cout << "ContinuousDI::ai_end_event_callback::DAQ stopped on user request" << std::endl;
		}
	}
	else if (this->exception_status_ == HANDLING_ERROR) 
	{
    why = DAQEND_ON_ERROR;
	  //- std::cout << "ContinuousDI::ai_end_event_callback::DAQ stopped on error" << std::endl;
	}
	else if (this->exception_status_ == HANDLING_OVERRUN) 
	{
    why = DAQEND_ON_OVERRUN;
	  //- std::cout << "ContinuousDI::ai_end_event_callback::DAQ stopped on overrun (ignored if strategy==restart)" << std::endl;
	}
	else 
	{
	  why = DAQEND_ON_UNKNOWN_EVT;
	  //- std::cout << "ContinuousDI::ai_end_event_callback::DAQ stopped on unknwon event" << std::endl;
	}
	
	return why;
}

// ============================================================================
// ContinuousDI::handle_daq_exception_i
// ============================================================================
void ContinuousDI::handle_daq_exception_i (const DAQException& _ex)
{
  //- std::cout << "ContinuousDI::handle_daq_exception_i <-" << std::endl;

  //- tell the world we are handling an error
  //- note: we have to change state before posting the error message
  this->state_ = ABORTING;
  this->exception_status_ = HANDLING_ERROR;

  do 
  {
    //- encapsulate the exception into a asl::Container
    Container<const DAQException>* msg_data;
    ASL_NEW(msg_data, Container<const DAQException>(_ex));  
    if (msg_data == 0) 
    {
      //- too many errors 
      break;
    }
    //- post <MSG_FATAL_ERROR> : no timeout 
    Message * msg;
    ASL_NEW(msg, Message(msg_data, MessageType::MSG_FATAL_ERROR, true));
    if (msg == 0) 
    {
      //- too many errors 
      delete msg_data;
      break;
    }
    //- post the error message
    if (this->worker_->post(msg) == -1) 
    {
      //- too many errors
      msg->release();
      break;
    }
  } 
  while (0);

  //- ask a ContinuousDAQHandler to  call <abort>.
  ContinuousDAQHandler * r;
  ASL_NEW(r, ContinuousDAQHandler); 
  if (r) 
  {
    //- spawn external handler
    r->call_handle_daq_exception(this);
  }
  else 
  {
    //- can't handle such error, panic...
    this->state_ = FAULT;
    this->exception_status_ = HANDLING_NONE;
  }

  //- std::cout << "ContinuousDI::handle_daq_exception_i::spawn DAQ handler ->" << std::endl;

  //- std::cout << "ContinuousDI::handle_daq_exception_i ->" << std::endl;
}

// ============================================================================
// ContinuousDI::handle_data_lost_exception_i
// ============================================================================
void ContinuousDI::handle_data_lost_exception_i ()
{
	//- std::cout << "ContinuousDI::handle_data_lost_exception_i <-" << std::endl;

  //- std::cout << "ContinuousDI::handle_data_lost_exception_i <-" << std::endl;

  //- tell the world we are handling an error  
  //- note: change state and/or exception_status before posting the error message
  this->exception_status_ = HANDLING_OVERRUN;
  if (this->config_.get_data_lost_strategy() == adl::abort)
  {
    this->state_ = ABORTING;
  }

  //- ask a ContinuousDAQHandler to  call <this::handle_data_lost>.
  ContinuousDAQHandler * r;
  ASL_NEW(r, ContinuousDAQHandler); 
  //- std::cout << "ContinuousDI::handle_data_lost_exception_i::spawn DAQ handler <-" << std::endl;
  if (r) 
  {
    //- spawn external handler
    r->call_handle_data_lost_exception(this);
  }
  else 
  {
    //- can't handle such error, panic...
    this->state_ = FAULT;
    this->exception_status_ = HANDLING_NONE;
  }

  //- std::cout << "ContinuousDI::handle_data_lost_exception_i::spawn DAQ handler ->" << std::endl;

  //- std::cout << "ContinuousDI::handle_data_lost_exception_i ->" << std::endl;
}

// ============================================================================
// ContinuousDI::dump
// ============================================================================
void ContinuousDI::dump () const
{
  ACE_GUARD(ACE_Thread_Mutex, ace_mon, 
            ACE_const_cast(ACE_Thread_Mutex&, this->lock_));

  ACE_DEBUG ((LM_DEBUG, "----------------------------\n"));
  ACE_DEBUG ((LM_DEBUG, "-- ContinuousDI             \n"));
  ACE_DEBUG ((LM_DEBUG, "----------------------------\n"));
  //- return if not initialized
  if (! this->initialized()) {
    ACE_DEBUG ((LM_DEBUG, ". not initialized\n"));
    return; 
  }

  switch (this->state_) 
  {
    case STANDBY:
      ACE_DEBUG ((LM_DEBUG, ". state: STANDBY\n"));
      break;
    case RUNNING:
      ACE_DEBUG ((LM_DEBUG, ". state: RUNNING\n"));
      break;
    case ABORTING:
      ACE_DEBUG ((LM_DEBUG, ". state: ABORTING\n"));
      break;
    case FAULT:
      ACE_DEBUG ((LM_DEBUG, ". state: FAULT\n"));
      break;
    default:
      ACE_DEBUG ((LM_DEBUG, ". state: UNKNOWN\n"));
      break;
  }

  ACE_DEBUG ((LM_DEBUG, "-- DAQ Hardware: \n"));

  this->daq_hw_->dump();

  ACE_DEBUG ((LM_DEBUG, "-- Async.Task:\n"));

  this->worker_->dump();

  ACE_DEBUG ((LM_DEBUG, "----------------------------\n"));
}

// ============================================================================
// ContinuousDI::handle_byte_input (asl::DIByteData* data)
// ============================================================================
void ContinuousDI::handle_byte_input (asl::DIByteData* data)
{
  if (data) delete data;
  throw asl::DAQException("programming error",
                          "call to ContinuousDI::handle_byte_input default implementation - should be user defined",
                          "ContinuousDI::handle_byte_input");
}

// ============================================================================
// ContinuousDI::handle_short_input (asl::DIShortData* data)
// ============================================================================
void ContinuousDI::handle_short_input (asl::DIShortData* data)
{
  if (data) delete data;
  throw asl::DAQException("programming error",
                          "call to ContinuousDI::handle_short_input default implementation - should be user defined",
                          "ContinuousDI::handle_short_input");
}

// ============================================================================
// ContinuousDI::handle_input_long (asl::DILongData* data)
// ============================================================================
void ContinuousDI::handle_long_input (asl::DILongData* data)
{
  if (data) delete data;
  throw asl::DAQException("programming error",
                          "call to ContinuousDI::handle_long_input default implementation - should be user defined",
                          "ContinuousDI::handle_long_input");
}

} // namespace asl




