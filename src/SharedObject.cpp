// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    SharedObject.cpp
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
//- #include <iostream>
#include <asl/SharedObject.h>

#if !defined (__ASL_INLINE__)
# include <asl/SharedObject.i>
#endif // __ASL_INLINE__

namespace asl {

// ============================================================================
// STATICs
// ============================================================================
#if defined(ASL_DEBUG)
ASL_EXPORT AtomicOp SharedObject::instance_counter_ = 0;
#endif

// ============================================================================
// SharedObject::SharedObject
// ============================================================================
SharedObject::SharedObject()
  : reference_count_ (1)
{
#if defined(ASL_DEBUG)
  //- increment instance counter
  ++SharedObject::instance_counter_;
#endif  
  //- std::cout << "SharedObject::SharedObject <-" << std::endl;
  //- std::cout << "SharedObject::SharedObject ->" << std::endl;
}

// ============================================================================
// SharedObject::~SharedObject
// ============================================================================
SharedObject::~SharedObject ()
{
#if defined(ASL_DEBUG)
  //- decrement instance counter
  --SharedObject::instance_counter_;
#endif  
  
  //- check reference_count_ value
  ACE_ASSERT(this->reference_count_ <= 1);

  //- std::cout << "SharedObject::~SharedObject <-" << std::endl;
  //- std::cout << "SharedObject::~SharedObject ->" << std::endl;
}

// ============================================================================
// SharedObject::duplicate
// ============================================================================
SharedObject * SharedObject::duplicate ()
{
  ACE_GUARD_RETURN(ACE_SYNCH_MUTEX, ace_mon, this->lock_, 0);
  
  this->reference_count_++;

  return this;
}

// ============================================================================
// SharedObject::release
// ============================================================================
void SharedObject::release ()
{
  if (this->release_i() == 0)
    delete this;
}

// ============================================================================
// SharedObject::release_i
// ============================================================================
SharedObject * SharedObject::release_i ()
{
  ACE_GUARD_RETURN(ACE_SYNCH_MUTEX, ace_mon, this->lock_, this);

  ACE_ASSERT(this->reference_count_ > 0);

  this->reference_count_--;

  return (this->reference_count_ == 0) ? 0 : this;
}

#if defined(ASL_DEBUG)
// ============================================================================
// SharedObject::instance_counter
// ============================================================================
u_long SharedObject::instance_counter () 
{
  return instance_counter_.value();
}
#endif  

} // namespace asl


