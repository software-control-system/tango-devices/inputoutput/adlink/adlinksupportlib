// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    SingleShotDAQ.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/SingleShotDAQ.h>

#if !defined (__ASL_INLINE__)
# include <asl/SingleShotDAQ.i>
#endif // __ASL_INLINE__

namespace asl {

// ============================================================================
// STATICs
// ============================================================================
#if defined(ASL_DEBUG)
ACE_Atomic_Op<ACE_Thread_Mutex, u_long> SingleShotDAQ::instance_counter_ = 0;
#endif

// ============================================================================
// SingleShotDAQ::SingleShotDAQ
// ============================================================================
SingleShotDAQ::SingleShotDAQ (void)
 : state_ (STANDBY)
{
#if defined(ASL_DEBUG)
  //- increment instance counter
  ++SingleShotDAQ::instance_counter_;
#endif  
}

// ============================================================================
// SingleShotDAQ::~SingleShotDAQ
// ============================================================================
SingleShotDAQ::~SingleShotDAQ (void)
{
#if defined(ASL_DEBUG)
  //- decrement instance counter
  --SingleShotDAQ::instance_counter_;
#endif  
}

#if defined(ASL_DEBUG)
// ============================================================================
// SingleShotDAQ::instance_counter
// ============================================================================
u_long SingleShotDAQ::instance_counter (void) 
{
  return instance_counter_.value();
}
#endif  

} // namespace asl


