// ============================================================================
//
// = CONTEXT
//   asl::SingleShotDIO class test
//
// = FILENAME
//   main.cpp
//
// = AUTHORS
//   GA & NL �- Synchrotron SOLEIL
//
// ============================================================================

#include <iostream>
using namespace std;

#include <ace/ACE.h>
#include <asl/SingleShotDI.h>
#include <asl/SingleShotDO.h>
#include <asl/Thread.h>
using namespace asl;
using namespace adl;

// ============================================================================
// Thread di 7300
// ============================================================================
class ThreadDI7300 : public asl::Thread
{
public:
	
	ThreadDI7300 ();
	
	virtual ~ThreadDI7300 ();
	
protected:
	
	virtual ACE_THR_FUNC_RETURN svc (void* arg);
};
//-----------------------------------------------------------------------------  
ThreadDI7300::ThreadDI7300 () 
: asl::Thread()
{
	//-noop ctor
}
//-----------------------------------------------------------------------------  
ThreadDI7300::~ThreadDI7300 ()
{
	//-noop ctor
}
//----------------------------------------------------------------------------- 
ACE_THR_FUNC_RETURN ThreadDI7300::svc (void* arg)
{
	//- verbose
	ACE_DEBUG((LM_DEBUG, "Entering ThreadDI7300::svc (arg:%x)\n", arg));
	
	SingleShotDI di_7300;
	di_7300.init(adl::PCI7300, 0);
	bool state = false;
	
	//- enter almost infinite loop
	do 
	{	
		state = di_7300.read_line(aux_di, 0);
		ACE_DEBUG((LM_DEBUG, "state %d\n", state));	
	} 
	while (quit_requested() == false);
	
	//- verbose
	ACE_DEBUG((LM_DEBUG, "Leaving ThreadDI7300::svc\n"));
	
	//- return result (here just return <arg> back)
	return ACE_static_cast(ACE_THR_FUNC_RETURN, arg);
}
// ============================================================================
// test thread 7300
// ============================================================================
void test_thread_7300()
{
	try
	{	
		// create a di on a 7300 card
		ThreadDI7300 * t = new ThreadDI7300();
		if (t == 0)
		{
			//- out of memory error
			return ;
		}
		
		//create a do on 7300 board
		SingleShotDO do_7300;
		
		do_7300.init(adl::PCI7300, 0);
		
		bool state1 =false;
		bool state2 =true;
		
		//- run the thread di
		if (t->run() == -1) {
			ACE_DEBUG((LM_DEBUG, "Thread::run failed\n"));  
			return ;
		}
		//run do
		for(int i = 0 ; i<500000; i++)
		{
			if(i%2==0)
				do_7300.write_line(aux_do, 1 , state1);
			else
				do_7300.write_line(aux_do, 1 , state2);
			//ACE_DEBUG((LM_DEBUG, "write\n"));  
		}
		
		//- ask the thread to quit
		if (t->quit() == -1) {
			ACE_DEBUG((LM_DEBUG, "Thread::quit failed\n"));  
			return ;
		}
		delete t;
	}
	catch (const DAQException& de)
	{
		
		ACE_DEBUG((LM_DEBUG, "asl::DAQException caught\n"));
		for (unsigned int i = 0; i < de.errors.size(); i++) 
		{
			ACE_DEBUG((LM_DEBUG, "- reason.....%s\n", de.errors[i].reason.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- desc.......%s\n", de.errors[i].desc.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- origin.....%s\n", de.errors[i].origin.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- code.......%d\n", de.errors[i].code));
			ACE_DEBUG((LM_DEBUG, "- severity...%d\n", de.errors[i].severity)); 
		}
	}
	
}
// ============================================================================
// Thread di 2005
// ============================================================================
class ThreadDI2005 : public asl::Thread
{
public:
	
	ThreadDI2005 ();
	
	virtual ~ThreadDI2005 ();
	
protected:
	
	virtual ACE_THR_FUNC_RETURN svc (void* arg);
};
//-----------------------------------------------------------------------------  
ThreadDI2005::ThreadDI2005 () 
: asl::Thread()
{
	//-noop ctor
}
//-----------------------------------------------------------------------------  
ThreadDI2005::~ThreadDI2005 ()
{
	//-noop ctor
}
//----------------------------------------------------------------------------- 
ACE_THR_FUNC_RETURN ThreadDI2005::svc (void* arg)
{
	//- verbose
	ACE_DEBUG((LM_DEBUG, "Entering ThreadDI2005::svc (arg:%x)\n", arg));
	
	SingleShotDI di_2005;
	di_2005.init(adl::DAQ2005, 0);
  	di_2005.configure_port(adl::port_1a);
	bool state = false;
	
	//- enter almost infinite loop
	do 
	{	
		state = di_2005.read_line(adl::port_1a, 0);
		ACE_DEBUG((LM_DEBUG, "state %d\n", state));	
	} 
	while (quit_requested() == false);
	
	//- verbose
	ACE_DEBUG((LM_DEBUG, "Leaving ThreadDI2005::svc\n"));
	
	//- return result (here just return <arg> back)
	return ACE_static_cast(ACE_THR_FUNC_RETURN, arg);
}
// ============================================================================
// test thread 2005
// ============================================================================
void test_thread_2005()
{
	try
	{	
		// create a di on a 2005 card
		ThreadDI2005 * t = new ThreadDI2005();
		if (t == 0)
		{
			//- out of memory error
			return ;
		}
		
		//create a do on 2005 board
		SingleShotDO do_2005;
		
		do_2005.init(adl::DAQ2005, 0);
		do_2005.configure_port(adl::port_1b);
		bool state1 =false;
		bool state2 =true;
		
		//- run the thread di
		if (t->run() == -1) {
			ACE_DEBUG((LM_DEBUG, "Thread::run failed\n"));  
			return ;
		}
		//run do
		for(int i = 0 ; i<500000; i++)
		{
			if(i%2==0)
				do_2005.write_line(adl::port_1b, 1 , state1);
			else
				do_2005.write_line(adl::port_1b, 1 , state2);
			//ACE_DEBUG((LM_DEBUG, "write\n"));  
		}
		
		//- ask the thread to quit
		if (t->quit() == -1) {
			ACE_DEBUG((LM_DEBUG, "Thread::quit failed\n"));  
			return ;
		}
		delete t;
	}
	catch (const DAQException& de)
	{
		
		ACE_DEBUG((LM_DEBUG, "asl::DAQException caught\n"));
		for (unsigned int i = 0; i < de.errors.size(); i++) 
		{
			ACE_DEBUG((LM_DEBUG, "- reason.....%s\n", de.errors[i].reason.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- desc.......%s\n", de.errors[i].desc.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- origin.....%s\n", de.errors[i].origin.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- code.......%d\n", de.errors[i].code));
			ACE_DEBUG((LM_DEBUG, "- severity...%d\n", de.errors[i].severity)); 
		}
	}
	
}
// ============================================================================
// test 7300
// ============================================================================
void test_7300()
{
	//-----------tests for 7300 in DI and DO
	//---------------------------------------
	
	try
	{	
		//create a single shot digital input
		SingleShotDI di_7300;
		//create a single shot digital output
		SingleShotDO do_7300;
		
		di_7300.init(adl::PCI7300, 0);
		do_7300.init(adl::PCI7300, 0);
		
		bool state1 = true;
		bool state2 = false;
		
		/*
		//generate a square signal in line 1 of auxiliary port
		for(int i = 0 ; i<50000; i++)
		{
		if(i%2==0)
		do_7300.write_line(aux_do, 1 , state1);
		else
        do_7300.write_line(aux_do, 1 , state2);
		}
		*/

		unsigned long value = 0x111F;
		
		// read the value of input port
		value = di_7300.read_port(aux_di);
		std::cout<<"read port of 7300:"<<std::hex<<value<<std::endl;
		do_7300.write_port(aux_do , value);
		
		
		// read the input line 0 and write it to the output line 0
		for(int i = 0 ; i<500000; i++)
		{
			state1 = di_7300.read_line(aux_di, 0);
			std::cout<<"read line of 7300:"<<state1<<std::endl;
			do_7300.write_line(aux_do, 0, 1);
		}
		
		
		
	}
	catch (const DAQException& de)
	{
		ACE_DEBUG((LM_DEBUG, "asl::DAQException caught\n"));
		for (unsigned int i = 0; i < de.errors.size(); i++) 
		{
			ACE_DEBUG((LM_DEBUG, "- reason.....%s\n", de.errors[i].reason.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- desc.......%s\n", de.errors[i].desc.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- origin.....%s\n", de.errors[i].origin.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- code.......%d\n", de.errors[i].code));
			ACE_DEBUG((LM_DEBUG, "- severity...%d\n", de.errors[i].severity)); 
		}
	}
	
}
// ============================================================================
// test 7432
// ============================================================================
void test_7432()
{
	
	//-----------tests for 7432 in DI and DO
	//---------------------------------------
	
	try
	{	
		//create a single shot digital input
		SingleShotDI di_7432;
		//create a single shot digital output
		SingleShotDO do_7432;
		
		di_7432.init(adl::PCI7432, 0);
		do_7432.init(adl::PCI7432, 0);
		
		bool state1 = true;
		bool state2 = false;
		
		unsigned long value = 0xFFFF;
		unsigned long value2 = 0x0000;
		/*
		//write square signals to port B 
		for(int i = 0 ; i<500000; i++)
		{
		if(i%2==0)
		do_7432.write_port(port_b, value);
		else
        do_7432.write_port(port_b , value2);
		}
		
		  //write a square signal to line 0 of port B  
		  for(int i = 0 ; i<500000; i++)
		  {
		  if(i%4 == 0)
		  do_7432.write_line(port_b, 0, state1);
		  if(i%4 == 2)
		  do_7432.write_line(port_b ,0, state2);
		  }
		*/
		//read value from portA line0 and output it to portB line0
		for(int i = 0 ; i<500000; i++)
		{
			//di_7432.read_line(port_a, 0, state1);
			ACE_OS::sleep(1);
			//std::cout<<"read line:"<<state1<<std::endl;
			do_7432.write_line(port_b, 0, state1);
			ACE_OS::sleep(1);
			do_7432.write_line(port_b, 0, state2);
		}
		
    }
	catch (const DAQException& de)
	{
		
		ACE_DEBUG((LM_DEBUG, "asl::DAQException caught\n"));
		for (unsigned int i = 0; i < de.errors.size(); i++) 
		{
			ACE_DEBUG((LM_DEBUG, "- reason.....%s\n", de.errors[i].reason.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- desc.......%s\n", de.errors[i].desc.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- origin.....%s\n", de.errors[i].origin.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- code.......%d\n", de.errors[i].code));
			ACE_DEBUG((LM_DEBUG, "- severity...%d\n", de.errors[i].severity)); 
		}
	}
	
}
// ============================================================================
// test 2005
// ============================================================================
void test_2005()
{
	
	//-----------tests for 2005 in DI and DO
	//---------------------------------------
	
	try
	{	
		//create a single shot digital input
		SingleShotDI di_2005;
		//create a single shot digital output
		SingleShotDO do_2005;
		
		di_2005.init(adl::DAQ2005, 0);
		do_2005.init(adl::DAQ2005, 0);
		
		bool state1 = true;
		bool state2 = false;
		
		unsigned short value = 0x00;
		unsigned short value2 = 0xFF;
		di_2005.configure_port(adl::port_1a);
		do_2005.configure_port(adl::port_1b);
		
		state1= di_2005.read_port(port_1a);
    std::cout<<"read_port "<<state1<<std::endl;
		// do_2005.write_line(port_1a, 0, state1);
		do_2005.write_port(port_1b, state1);
		
    /*
		//write square signals to port B 
		for(int i = 0 ; i<500000; i++)
		{ 
		if(i%2==0)
		do_2005.write_port(port_1b, value);
		else
        do_2005.write_port(port_1b , value2);
    }*/
		
	/*
    //write a square signal to line 0 of port B  
    for(int i = 0 ; i<500000; i++)
    {
		  if(i%2 == 0)
		  do_2005.write_line(port_1b, 0, state1);
		  else
		  do_2005.write_line(port_1b ,0, state2);
    }*/
		
		//read value from portA line0 and output it to portB line0
		for(int i = 0 ; i<500000; i++)
		{
			value = di_2005.read_port(port_1a);
			//std::cout<<"read port:"<<std::hex<<value<<std::endl;
			do_2005.write_port(port_1b, value);
		}
		
    }
	catch (const DAQException& de)
	{
		
		ACE_DEBUG((LM_DEBUG, "asl::DAQException caught\n"));
		for (unsigned int i = 0; i < de.errors.size(); i++) 
		{
			ACE_DEBUG((LM_DEBUG, "- reason.....%s\n", de.errors[i].reason.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- desc.......%s\n", de.errors[i].desc.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- origin.....%s\n", de.errors[i].origin.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- code.......%d\n", de.errors[i].code));
			ACE_DEBUG((LM_DEBUG, "- severity...%d\n", de.errors[i].severity)); 
		}
	}
	
}
// ============================================================================
// test 2204
// ============================================================================
void test_2204()
{
	
	//-----------tests for 2204 in DI and DO
	//---------------------------------------
	
	try
	{	
		//create a single shot digital input
		SingleShotDI di_2204;
		//create a single shot digital output
		SingleShotDO do_2204;
		
		di_2204.init(adl::DAQ2204, 0);
		do_2204.init(adl::DAQ2204, 0);
		
		bool state1 = true;
		bool state2 = false;
		
		unsigned short value = 0x00;
		unsigned short value2 = 0xFF;
		
		di_2204.configure_port(adl::port_1a);
		do_2204.configure_port(adl::port_1b);
		
		// state1= di_2204.read_line(port_1b, 0);
		/*
		//write square signals to port B 
		for(int i = 0 ; i<500000; i++)
		{ 
		if(i%2==0)
		do_2204.write_port(port_1b, value);
		else
        do_2204.write_port(port_1b , value2);
    }*/
		
	/*
    //write a square signal to line 0 of port B
    for(int i = 0 ; i<500000; i++)
    {
		  if(i%2 == 0)
		  do_2204.write_line(port_1b, 0, state1);
		  else
		  do_2204.write_line(port_1b ,0, state2);
    }*/
		
		//read value from portA line0 and output it to portB line0
		for(int i = 0 ; i<500000; i++)
		{
			state1 = di_2204.read_line(port_1a, 0);
			std::cout<<"read line:"<<state1<<std::endl;
			do_2204.write_line(port_1b, 0, state1 );
		}
		
    }
	catch (const DAQException& de)
	{
		
		ACE_DEBUG((LM_DEBUG, "asl::DAQException caught\n"));
		for (unsigned int i = 0; i < de.errors.size(); i++) 
		{
			ACE_DEBUG((LM_DEBUG, "- reason.....%s\n", de.errors[i].reason.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- desc.......%s\n", de.errors[i].desc.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- origin.....%s\n", de.errors[i].origin.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- code.......%d\n", de.errors[i].code));
			ACE_DEBUG((LM_DEBUG, "- severity...%d\n", de.errors[i].severity)); 
		}
	}
	
}
// ============================================================================
// test 2502
// ============================================================================
void test_2502()
{
	
	//-----------tests for 2502 in DI and DO
	//---------------------------------------
	
	try
	{	
		//create a single shot digital input
		SingleShotDI di_2502;
		//create a single shot digital output
		SingleShotDO do_2502;
		
		di_2502.init(adl::DAQ2502, 0);
		do_2502.init(adl::DAQ2502, 0);
		
		bool state1 = true;
		bool state2 = false;
		
		unsigned short value = 0x00;
		unsigned short value2 = 0xFF;
		
		di_2502.configure_port(adl::port_1a);
		do_2502.configure_port(adl::port_1b);
		
		// state1= di_2502.read_line(port_1b, 0);
		/*
		//write square signals to port B 
		for(int i = 0 ; i<500000; i++)
		{ 
		if(i%2==0)
		do_2502.write_port(port_1b, value);
		else
        do_2502.write_port(port_1b , value2);
    }*/
		
	/*
    //write a square signal to line 0 of port B
    for(int i = 0 ; i<500000; i++)
    {
		  if(i%2 == 0)
		  do_2502.write_line(port_1b, 0, state1);
		  else
		  do_2502.write_line(port_1b ,0, state2);
    }*/
		
		//read value from portA line0 and output it to portB line0
		for(int i = 0 ; i<500000; i++)
		{
			value = di_2502.read_line(port_1a, 0);
			//std::cout<<"read port:"<<std::hex<<value<<std::endl;
			do_2502.write_line(port_1b, 0, value );
		}
		
    }
	catch (const DAQException& de)
	{
		
		ACE_DEBUG((LM_DEBUG, "asl::DAQException caught\n"));
		for (unsigned int i = 0; i < de.errors.size(); i++) 
		{
			ACE_DEBUG((LM_DEBUG, "- reason.....%s\n", de.errors[i].reason.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- desc.......%s\n", de.errors[i].desc.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- origin.....%s\n", de.errors[i].origin.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- code.......%d\n", de.errors[i].code));
			ACE_DEBUG((LM_DEBUG, "- severity...%d\n", de.errors[i].severity)); 
		}
	}
	
}

// ============================================================================
// Main
// ============================================================================
int main (int, char**) 
{
	 //test_2005();
   test_thread_2005();
	//test_7432();
	//test_2204();
	//test_2502();
	//test_7300();
	//test_thread_7300();
	return 0;
}
