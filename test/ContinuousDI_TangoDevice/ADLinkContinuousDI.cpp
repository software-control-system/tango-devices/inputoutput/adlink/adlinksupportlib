//===================================================================
// DEPENDENCIES
//===================================================================
#include <ADLinkContinuousDI.h>

//===================================================================
// ADLinkContinuousDI::ADLinkContinuousDI
//===================================================================
ADLinkContinuousDI::ADLinkContinuousDI (Tango::DeviceImpl *dev)
  : Tango::LogAdapter(dev),
    data_counter (0),
    tmo_counter (0),
    error_counter (0),
    overrun_counter (0),
		daq_end_evt_received(false)
{

}

//===================================================================
// ADLinkContinuousDI::~ADLinkContinuousDI
//===================================================================
ADLinkContinuousDI::~ADLinkContinuousDI (void) 
{ 
  //- noop
}

//===================================================================
// ADLinkContinuousDI::start
//===================================================================
void ADLinkContinuousDI::start (void) throw (asl::DAQException)
{
	//- reset local data
	data_counter = 0;
	tmo_counter = 0;
	overrun_counter = 0;
	daq_end_evt_received = false;
	//- call mother class start method (may throw asl::DAQException)
	this->ContinuousDI::start();
}
	
//===================================================================
// ADLinkContinuousDI::handle_byte_input
//===================================================================
void ADLinkContinuousDI::handle_byte_input (asl::DIByteData* data)
{
  data_counter++;

	DEBUG_STREAM << "::: ADLinkContinuousDI::handle_byte_input :::" << std::endl; 

  //- release data to avoid memory leak
  delete data;
}

//===================================================================
// ADLinkContinuousDI::handle_short_input
//===================================================================
void ADLinkContinuousDI::handle_short_input (asl::DIShortData* data)
{
  data_counter++;

	DEBUG_STREAM << "::: ADLinkContinuousDI::handle_short_input :::" << std::endl; 

  //- release data to avoid memory leak
  delete data;
}

//===================================================================
// ADLinkContinuousDI::handle_long_input
//===================================================================
void ADLinkContinuousDI::handle_long_input (asl::DILongData* data)
{
  data_counter++;

	DEBUG_STREAM << "::: ADLinkContinuousDI::handle_long_input :::" << std::endl; 

  //- release data to avoid memory leak
  delete data;
}

//===================================================================
// ADLinkContinuousDI::handle_daq_end
//===================================================================
void ADLinkContinuousDI::handle_daq_end (asl::ContinuousDAQ::DaqEndReason why)
{
	DEBUG_STREAM << "::: ADLinkContinuousDI::handle_daq_end :::" << std::endl; 

	DEBUG_STREAM << std::endl; 

	daq_end_evt_received = true;
	
	switch (why)
	{
		case ContinuousDAQ::DAQEND_ON_USER_REQUEST:
			DEBUG_STREAM << "\treason: user request" << std::endl; 
			break;
		case ContinuousDAQ::DAQEND_ON_EXTERNAL_TRIGGER:
			DEBUG_STREAM << "\treason: digital external trigger raised" << std::endl; 
			break;
		case ContinuousDAQ::DAQEND_ON_ERROR:
			DEBUG_STREAM << "\treason: daq error" << std::endl; 
			break;		
		case ContinuousDAQ::DAQEND_ON_OVERRUN:
			DEBUG_STREAM << "\treason: daq buffer overrun" << std::endl; 
			DEBUG_STREAM << "\treason: daq has been <aborted> as required by the specified overrun strategy" << std::endl; 
			break;
		case ContinuousDAQ::DAQEND_ON_UNKNOWN_EVT:
		default:
		  DEBUG_STREAM << "\treason: unknown" << std::endl; 
			break;
	}

	DEBUG_STREAM << std::endl;
}
	
//===================================================================
// ADLinkContinuousDI::handle_error
//===================================================================
void ADLinkContinuousDI::handle_error (const asl::DAQException& de)
{
  error_counter++;

  DEBUG_STREAM << "::: ADLinkContinuousDI::handle_error :::" << std::endl;

  ERROR_STREAM << this->daq_to_tango_exception(de) << endl;
}

//===================================================================
// ADLinkContinuousDI::handle_data_lost
//===================================================================
void ADLinkContinuousDI::handle_data_lost (void)
{
  overrun_counter++;

  DEBUG_STREAM << "::: ADLinkContinuousDI::handle_data_lost :::" << std::endl;

  DEBUG_STREAM << std::endl; 

  DEBUG_STREAM << "\toverrun notification #" <<  overrun_counter << std::endl;

  DEBUG_STREAM << std::endl; 
}

//===================================================================
// ADLinkContinuousDI::handle_timeout
//===================================================================
void ADLinkContinuousDI::handle_timeout (void)
{
  tmo_counter++;

  DEBUG_STREAM << "::: ADLinkContinuousDI::handle_timeout :::" << std::endl;
  
  DEBUG_STREAM << std::endl; 

  DEBUG_STREAM << "\ttimeout notification #" <<  tmo_counter << std::endl;

  DEBUG_STREAM << std::endl; 

	DEBUG_STREAM << "\treason: the DAQ internal processing task did not receive data since " 
               << this->configuration().get_timeout()
               << " msec" 
               << std::endl; 

	DEBUG_STREAM << "\treason: this is the timeout specified int the DAQ configuration" << std::endl; 

	DEBUG_STREAM << "\treason: the DAQ buffer depth may be too large for the selected sampling rate" << std::endl; 

	DEBUG_STREAM << "\treason: the timeout delay may also be too short compared to the time required to fill a daq buffer" << std::endl; 
	DEBUG_STREAM << "\treason: try to increase either the timeout delay or sampling rate" << std::endl; 

  DEBUG_STREAM << std::endl; 
}

//-------------------------------------------------------------------
// ADLinkContinuousDI::daq_to_tango_exception
//-------------------------------------------------------------------
Tango::DevFailed ADLinkContinuousDI::daq_to_tango_exception (const asl::DAQException& de)
{
  Tango::DevErrorList error_list(de.errors.size());

  error_list.length(de.errors.size());

  for (int i = 0; i < de.errors.size(); i++) 
  {
    error_list[i].reason = CORBA::string_dup(de.errors[i].reason.c_str());
    error_list[i].desc   = CORBA::string_dup(de.errors[i].desc.c_str());
    error_list[i].origin = CORBA::string_dup(de.errors[i].origin.c_str());
    switch (de.errors[i].severity) 
    {
      case asl::WARN:
        error_list[i].severity = Tango::WARN;
        break;
      case asl::PANIC:
        error_list[i].severity = Tango::PANIC;
        break;
      case asl::ERR:
      default:
        error_list[i].severity = Tango::ERR;
        break;
    }
  }

  return Tango::DevFailed(error_list);
}
