//=============================================================================
//
// file :	  ContinuousDIDevClass.h
//
// description :  Include for the ContinuousDIDevClass root class.
//		  This class is represents the singleton class for
//		  the ContinuousDIDev device class.
//		  It contains all properties and methods which the 
//		  ContinuousDIDev requires only once e.g. the commands.
//			
// project :	  TANGO Device Server
//
// $Author: syldup $
//
// $Revision: 1.1.1.1 $
//
// $Log: not supported by cvs2svn $
//
// copyleft :	  European Synchrotron Radiation Facility
//		  BP 220, Grenoble 38043
//		  FRANCE
//
//=============================================================================
//
//		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//	   (c) - Software Engineering Group - ESRF
//=============================================================================

#ifndef _ContinuousDIDevCLASS_H
#define _ContinuousDIDevCLASS_H

#include <tango.h>


namespace ContinuousDIDev
{
//
// Define classes for commands
//
class StopCmd : public Tango::Command
{
public:
	StopCmd(const char *,Tango::CmdArgType, Tango::CmdArgType,const char *,const char *, Tango::DispLevel);
	StopCmd(const char *,Tango::CmdArgType, Tango::CmdArgType);
	~StopCmd() {};
	
	virtual bool is_allowed (Tango::DeviceImpl *, const CORBA::Any &);
	virtual CORBA::Any *execute (Tango::DeviceImpl *, const CORBA::Any &);
};


class StartCmd : public Tango::Command
{
public:
	StartCmd(const char *,Tango::CmdArgType, Tango::CmdArgType,const char *,const char *, Tango::DispLevel);
	StartCmd(const char *,Tango::CmdArgType, Tango::CmdArgType);
	~StartCmd() {};
	
	virtual bool is_allowed (Tango::DeviceImpl *, const CORBA::Any &);
	virtual CORBA::Any *execute (Tango::DeviceImpl *, const CORBA::Any &);
};

//
// The ContinuousDIDevClass singleton definition
//

class ContinuousDIDevClass : public Tango::DeviceClass
{
public:

//	add your own data members here
//------------------------------------

public:

//	Method prototypes
	static ContinuousDIDevClass *init(const char *);
	static ContinuousDIDevClass *instance();
	~ContinuousDIDevClass();
	
protected:
	ContinuousDIDevClass(string &);
	static ContinuousDIDevClass *_instance;
	void command_factory();
	void attribute_factory(vector<Tango::Attr *> &);
	void write_class_property();

private:
	void device_factory(const Tango::DevVarStringArray *);
};


}	//	namespace ContinuousDIDev

#endif // _ContinuousDIDevCLASS_H
