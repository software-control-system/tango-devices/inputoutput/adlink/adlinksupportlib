static const char *RcsId = "$Header: /segfs/tango/tmp/tango-ds.new/cvsroot/InputOutput/ADLINK/AdlinkSupportLib/test/ContinuousDI_TangoDevice/ContinuousDIDev.cpp,v 1.1.1.1 2004-11-08 16:34:14 syldup Exp $";
//+=============================================================================
//
// file :	  ContinuousDIDev.cpp
//
// description :  C++ source for the ContinuousDIDev and its commands. 
//		  The class is derived from Device. It represents the
//		  CORBA servant object which will be accessed from the
//		  network. All commands which can be executed on the
//		  ContinuousDIDev are implemented in this file.
//
// project :	  TANGO Device Server
//
// $Author: syldup $
//
// $Revision: 1.1.1.1 $
//
// $Log: not supported by cvs2svn $
//
// copyleft :	  European Synchrotron Radiation Facility
//		  BP 220, Grenoble 38043
//		  FRANCE
//
//-=============================================================================
//
//		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//	   (c) - Software Engineering Group - ESRF
//=============================================================================


//===================================================================
//
//	The folowing table gives the correspondance
//	between commands and method's name.
//
//	Command's name	|	Method's name
//	----------------------------------------
//	State	|	dev_state()
//	Status	|	dev_status()
//	Start	|	start()
//	Stop	|	stop()
//
//===================================================================
#include <ADLinkContinuousDI.h>
#include <ContinuousDIDev.h>

//===================================================================
// DEVICE PROPERTIES DEFAULT VALUES
//===================================================================
#define kDEFAULT_BOARD_TYPE_STR (const char*)"DIO_7300"
#define kDEFAULT_BOARD_TYPE_ID  adl::PCI7300
#define kDEFAULT_BOARD_ID       0
#define kMAX_CPCI_BOARD         7
#define kDEFAULT_SAMPLING_RATE  500000.
#define kDEFAULT_BUFFER_DEPTH   10000
#define kDEFAULT_PORT_WIDTH     32
#define kMIN_DAQ_TIMEOUT    	  10
#define kDEFAULT_DAQ_TIMEOUT    3000

#if defined (__linux)
# define USE_LINUX_WORKAROUND_FOR_ASL_DEVICES
#endif

namespace ContinuousDIDev
{
//+----------------------------------------------------------------------------
//
// method :		ContinuousDIDev::ContinuousDIDev(string &s)
//
// description :	constructor for simulated ContinuousDIDev
//

// in : - cl : Pointer to the DeviceClass object
//	- s : Device name
//
//-----------------------------------------------------------------------------
ContinuousDIDev::ContinuousDIDev(Tango::DeviceClass *cl,string &s)
  : Tango::Device_2Impl(cl,s.c_str())
{
	init_device();
}

ContinuousDIDev::ContinuousDIDev(Tango::DeviceClass *cl,const char *s)
  : Tango::Device_2Impl(cl,s)
{
	init_device();
}

ContinuousDIDev::ContinuousDIDev(Tango::DeviceClass *cl,const char *s,const char *d)
  : Tango::Device_2Impl(cl,s,d)
{
	init_device();
}

ContinuousDIDev::~ContinuousDIDev(void)
{
  DEBUG_STREAM << "ContinuousDIDev::~ContinuousDIDev <-" << std::endl;
  delete_device();
  DEBUG_STREAM << "ContinuousDIDev::~ContinuousDIDev ->" << std::endl;
}

//+----------------------------------------------------------------------------
//
// method :		ContinuousDIDev::init_device()
//
// description :	will be called at device initialization.
//
//-----------------------------------------------------------------------------
void ContinuousDIDev::init_device()
{
  static unsigned long init_counter = 0;
		
  //- verbose
  INFO_STREAM << "ContinuousDIDev::init device "
              << device_name
              << " ["
              << init_counter
              << "]"
              << endl;
												
  //- update internal state
  this->set_state(Tango::UNKNOWN);

  //- first device initialization:
  //- properly initialize members to their default value
  if (init_counter == 0)  {
    this->daq = 0;
    this->board_type = kDEFAULT_BOARD_TYPE_ID;
    this->board_id = kDEFAULT_BOARD_ID;
    this->sampling_rate = kDEFAULT_SAMPLING_RATE;
    this->buffer_depth = kDEFAULT_BUFFER_DEPTH;
    this->port_width = kDEFAULT_PORT_WIDTH;
    this->daq_timeout = kDEFAULT_DAQ_TIMEOUT;
  }

#if defined(USE_LINUX_WORKAROUND_FOR_ASL_DEVICES)
if (init_counter == 0) 
{
#endif

  //- instanciate the daq
  daq = new ADLinkContinuousDI(this);
  if (daq == 0) {
    Tango::Except::throw_exception((const char *)"out of memory",
                                   (const char *)"out of memory error",
                                   (const char *)"ContinuousDIDev::init_device");
  }
	
#if defined(USE_LINUX_WORKAROUND_FOR_ASL_DEVICES)
}
#endif

  //- read device properties from tango database
  this->get_device_property();

  //- the DAQ configuration
  asl::ContinuousDIConfig daq_config;

  //--------------------------------------------------------
  //---- CONFIG EXAMPLE ------------------------------------
  //--------------------------------------------------------
  //- be sure port_width is valid (8, 16 or 32 bits)
  adl::DIOPortWidth pw = adl::width_8;
  if (this->port_width > 16) 
    pw = adl::width_32;
  else if (this->port_width > 8)
    pw = adl::width_16;
  //- set DI port width (8, 16 or 32 bits)
  daq_config.set_port_width(pw); 
  //- trigger source internal
  daq_config.set_trigger_source (adl::internal);
  //- the trigger signal is rising edge active
  //- daq_config.set_trigger_polarity (adl::trigger_di_rising_edge); 
  //- the acquisition starts immediatly when user starts it.
  daq_config.set_start_mode(adl::start_immediatly);
  //- the terminator is on
  //- daq_config.set_terminator (adl::terminator_on);
  //- the request signal is rising edge active
  //- daq_config.set_request_polarity (adl::request_di_rising_edge);
  //- the request signal is rising edge active
  //- daq_config.set_ack_polarity (adl::ack_di_rising_edge);
  //- set sampling rate in Hz.
  daq_config.set_sampling_rate (this->sampling_rate);
  //- set DAQ buffer depth in samples/channel (sampling_rate dependent)
  daq_config.set_buffer_depth (this->buffer_depth); 
  //- set overrun strategy (notify)
  daq_config.set_data_lost_strategy (adl::notify);
  //- set timeout (in ms)
  daq_config.set_timeout(this->daq_timeout);

  try
  {
    DEBUG_STREAM << "ContinuousDIDev::init_device::init DAQ <-" << std::endl;
    //- initialize daq
    daq->init(this->board_type, this->board_id);
    DEBUG_STREAM << "ContinuousDIDev::init_device::init DAQ ->" << std::endl;
    
    DEBUG_STREAM << "ContinuousDIDev::init_device::conf DAQ <-" << std::endl;
    //- configure daq
    daq->configure(daq_config);
    DEBUG_STREAM << "ContinuousDIDev::init_device::conf DAQ ->" << std::endl;
    
    DEBUG_STREAM << "ContinuousDIDev::init_device::start DAQ <-" << std::endl;
    //- start daq
    daq->start();
    DEBUG_STREAM << "ContinuousDIDev::init_device::start DAQ ->" << std::endl;
    
    //- update internal state
    this->set_internal_state();
  }
  catch (const asl::DAQException& de)
  {
	  //- verbose
    for (unsigned int i = 0; i < de.errors.size(); i++) 
			{
				DEBUG_STREAM << "- reason....." << de.errors[i].reason.c_str() << std::endl; 
				DEBUG_STREAM << "- desc......." << de.errors[i].desc.c_str() << std::endl;
				DEBUG_STREAM << "- origin....." << de.errors[i].origin.c_str() << std::endl;
				DEBUG_STREAM << "- code......." << de.errors[i].code << std::endl;
				DEBUG_STREAM << "- severity..." << de.errors[i].severity << std::endl; 
			}
    //- update internal state
    this->set_internal_state();
    //- convert daq to tango error
    Tango::DevFailed df = daq->daq_to_tango_exception(de);
    //- verbose
    ERROR_STREAM << "ContinuousDIDev::init_device::DAQException caught" << std::endl;
    //-TODO: fix TANGO bug ERROR_STREAM << df << std::endl;
    //- delete all
    this->delete_device();
    //-TODO: can we throw an exception from init_device?
#if defined(CAN_THROW_EXCEPTION_FROM_INIT_DEVICE)
    //- rethrow exception
    Tango::Except::re_throw_exception(df,
            static_cast<const char*>("device initialization failed"),
				    static_cast<const char*>("caugth asl::DAQException"),
				    static_cast<const char*>("ContinuousDIDev::init_device"));
#endif
  }
  catch (...)
  {
    //- update internal state
    this->set_internal_state();
    ERROR_STREAM << "ContinuousDIDev::init_device::unknown exception caught" << std::endl;
    //- delete all
    this->delete_device();
    //-TODO: can we throw an exception from init_device?
#if defined(CAN_THROW_EXCEPTION_FROM_INIT_DEVICE)
    //- throw exception
    Tango::Except::throw_exception((const char *)"device initialization failed",
                                   (const char *)"DAQ initialization failed",
                                   (const char *)"ContinuousDIDev::init_device");
#endif
  }
	//- init counter
	init_counter++;
}

//+----------------------------------------------------------------------------
//
// method :		ContinuousDIDev::delete_device()
//
// description :	will be called at device destruction or at init command.
//
//-----------------------------------------------------------------------------
void ContinuousDIDev::delete_device()
{
  static unsigned long delete_counter = 0;

  //- verbose
  INFO_STREAM << "ContinuousDIDev::delete device "
	      << device_name
	      << " ["
	      << ++delete_counter
	      << "]"
	      << endl;

  if (daq != 0)
  {
    try
    {
      DEBUG_STREAM << "ContinuousDIDev::delete_device::abort DAQ <-" << std::endl;
       daq->abort();
      DEBUG_STREAM << "ContinuousDIDev::delete_device::abort DAQ ->" << std::endl;
    } catch (...) {
      //- ignore any error
    }
#if !defined(USE_LINUX_WORKAROUND_FOR_ASL_DEVICES)
  //- release daq
    DEBUG_STREAM << "ContinuousDIDev::delete_device::delete DAQ <-" << std::endl;
    delete daq;
    daq = 0;
    DEBUG_STREAM << "ContinuousDIDev::delete_device::delete DAQ ->" << std::endl;
#endif
  }

}

//+----------------------------------------------------------------------------
//
// method :		ContinuousDIDev::get_device_property()
//
// description :	reads device properties from database.
//
//-----------------------------------------------------------------------------
void ContinuousDIDev::get_device_property (void)
{
  //	Initialize your default values here.
	//------------------------------------------

	//	Read class properties from database.(Automatic code generation)
	//-------------------------------------------------------------
	Tango::DbData	data;
	data.push_back(Tango::DbDatum("daq_board_type"));
	data.push_back(Tango::DbDatum("daq_board_id"));
  data.push_back(Tango::DbDatum("daq_sampling_rate"));
	data.push_back(Tango::DbDatum("daq_buffer_depth"));
	data.push_back(Tango::DbDatum("daq_port_width"));
	data.push_back(Tango::DbDatum("daq_timeout"));

  try {
	  //	Call database and extract values
	  //--------------------------------------------

    //- get properties from tango database
	  get_db_device()->get_property(data);

    //- extract <daq_board_type> ----------------------------------------------
    //-------------------------------------------------------------------------
	  if (data[0].is_empty() == false)
    {
      //- extract as a string
      std::string btype;
      data[0] >> btype;
      //- verbose
      DEBUG_STREAM << "dev-property::daq_board_type::" <<  btype << endl;
      //- convert from string to board id
      if (btype == "DIO_7300") {
        this->board_type = adl::PCI7300;
      }
      else {
        WARN_STREAM << "dev-property <daq_board_type> is invalid. "
                    << "using default value: " << kDEFAULT_BOARD_TYPE_STR << endl;
      }
    }
    else
    {
      this->board_type = kDEFAULT_BOARD_TYPE_ID;
      WARN_STREAM << "dev-property <daq_board_type> is not set in database. "
                  << "using default value: " << kDEFAULT_BOARD_TYPE_STR << endl;
    }
    //- extract <daq_board_id> ------------------------------------------------
    //-------------------------------------------------------------------------
	  if (data[1].is_empty() == false)
    {
      //- extract as an unsigned short
      short bid;
      data[1] >> bid;
      //- verbose
      DEBUG_STREAM << "dev-property::daq_board_id::" <<  bid << endl;
      //- check value
      if (bid < 0 || bid >= kMAX_CPCI_BOARD) {
        this->board_id = kDEFAULT_BOARD_ID;
        WARN_STREAM << "dev-property <daq_board_id> is invalid. "
                    << "using default value: " 
                    << this->board_id 
                    << endl;
      }
      else {
        this->board_id = (unsigned short)bid;
      }
    }
    else
    {
      WARN_STREAM << "dev-property <daq_board_id> is not set in database. "
                  << "using default value: " 
                  << this->board_id 
                  << endl;
    }
    //- extract <daq_sampling_rate> -------------------------------------------
    //-------------------------------------------------------------------------
	  if (data[2].is_empty() == false)
    {
      //- extract as a double
      double sr;
      data[2] >> sr;
      //- verbose
      DEBUG_STREAM << "dev-property::daq_sampling_rate::" <<  sr << endl;
      //- TODO: check value
      this->sampling_rate = sr;
    }
    else
    {
      this->sampling_rate = kDEFAULT_SAMPLING_RATE;
      WARN_STREAM << "dev-property <daq_sampling_rate> is not set in database. "
                  << "using default value: " 
                  << this->sampling_rate 
                  << endl;
    }
    //- extract <daq_buffer_depth> --------------------------------------------
    //-------------------------------------------------------------------------
	  if (data[3].is_empty() == false)
    {
      //- extract as a long
      long bd;
      data[3] >> bd;
      //- verbose
      DEBUG_STREAM << "dev-property::daq_buffer_depth::" <<  bd << endl;
      //- check value
      if (bd <= 0) {
        this->buffer_depth = kDEFAULT_BUFFER_DEPTH;
        WARN_STREAM << "dev-property <daq_buffer_depth> is invalid. "
                    << "using default value: " 
                    << this->buffer_depth 
                    << endl;
      }
      else {
        this->buffer_depth = bd;
      }
    }
    else
    {
      this->buffer_depth = kDEFAULT_BUFFER_DEPTH;
      WARN_STREAM << "dev-property <daq_buffer_depth> is not set in database. "
                  << "using default value: " 
                  << this->buffer_depth 
                  << endl;
    }
    //- extract <daq_port_width> ----------------------------------------------
    //-------------------------------------------------------------------------
    if (data[4].is_empty() == false)
    {
      //- extract as a double
      unsigned short pw;
      data[4] >> pw;
      //- TODO: check value
      this->port_width = pw;
      //- verbose
      DEBUG_STREAM << "dev-property::daq_port_width::" 
                   << this->port_width 
                   << "-bits" 
                   << endl;
    }
    else
    {
      this->port_width = kDEFAULT_PORT_WIDTH;
      WARN_STREAM << "dev-property <daq_port_width> is not set in database. "
                  << "using default value: " 
                  << this->port_width 
                  << "-bits" << endl;
    }
    //- extract <daq_timeout> -------------------------------------------------
    //-------------------------------------------------------------------------
    if (data[5].is_empty() == false)
    {
      //- extract as a double
      long tmo;
      data[5] >> tmo;
      //- verbose
      DEBUG_STREAM << "dev-property::daq_timeout::" <<  tmo << endl;
      //- check value
      if (tmo < kMIN_DAQ_TIMEOUT) {
        daq_timeout = kMIN_DAQ_TIMEOUT;
        WARN_STREAM << "dev-property <daq_timeout> is invalid. "
                    << "using default value: " 
                    << daq_timeout 
                    << " msec" 
                    << endl;
      }
      else {
        daq_timeout = tmo;
      }
    }
    else
    {
      daq_timeout = kDEFAULT_DAQ_TIMEOUT;
      WARN_STREAM << "dev-property <daq_timeout> is not set in database. "
                  << "using default value: " 
                  << daq_timeout 
                  << " msec" 
                  << endl;
    }
	  //	End of Automatic code generation
	  //-------------------------------------------------------------
  }
  catch (const Tango::DevFailed& ex)
  {
    ERROR_STREAM << ex << endl;
  }
}

//+----------------------------------------------------------------------------
//
// method :		ContinuousDIDev::always_executed_hook()
//
// description :	method always executed before any command is executed
//
//-----------------------------------------------------------------------------
void ContinuousDIDev::always_executed_hook()
{

}

//+----------------------------------------------------------------------------
//
// method :		ContinuousDIDev::write_attr_hardware()
//
// description :
//
//-----------------------------------------------------------------------------
void ContinuousDIDev::write_attr_hardware(vector<long> &attr_list)
{
/*
	for (long i=0 ; i < attr_list.size() ; i++)
  {
    Tango::WAttribute &att = dev_attr->get_w_attr_by_ind(attr_list[i]);
    string attr_name = att.get_name();
    if (attr_name == ....)
    {
      Tango::DevDouble dd;
      att.get_write_value(dd);

    }
    else if (attr_name == ...)
    {
      Tango::DevShort ds;
      att.get_write_value(ds);
      cobdp = ds ? 1 : 0;
    }
  }
*/
}

//+----------------------------------------------------------------------------
//
// method :		ContinuousDIDev::read_attr_hardware()
//
// description :	Hardware acquisition for attributes.
//
//-----------------------------------------------------------------------------
void ContinuousDIDev::read_attr_hardware(vector<long> &attr_list)
{
  //- not initilized: throw exception in <read_attr>
  if (daq == 0) {
   return;
  }

  //	  Add your own code here
  //---------------------------------
  daq->lock_data();
    //...
  daq->unlock_data();
}

//+----------------------------------------------------------------------------
//
// method :		ContinuousDIDev::read_attr()
//
// description :	Extract real attribute values from
//					hardware acquisition result.
//
//-----------------------------------------------------------------------------
void ContinuousDIDev::read_attr (Tango::Attribute &attr)
{
  //- not initilized: throw exception
  if (daq == 0) {
    Tango::Except::throw_exception(
            static_cast<const char*>("read device attribute(s)"),
				    static_cast<const char*>("device is not properly initialized"),
				    static_cast<const char*>("ContinuousDIDev::read_attr"));
  }

  string &attr_name = attr.get_name();

  //	  Switch on attribute name
  //---------------------------------
  if (attr_name == "data_counter") {
    attr.set_value(&daq->data_counter);
  }
  else if (attr_name == "error_counter") {
    attr.set_value(&daq->error_counter);
  }
  else if (attr_name == "overrun_counter") {
    attr.set_value(&daq->overrun_counter);
  }
  else if (attr_name == "timeout_counter") {
    attr.set_value(&daq->tmo_counter);
  }
}

//+------------------------------------------------------------------
/**
 *	method: ContinuousDIDev::start
 *
 *	description:	method to execute "Start"
 *	Start data acquisition
 */
//+------------------------------------------------------------------
void ContinuousDIDev::start()
{
  //- not initilized: throw exception
  if (daq == 0) {
    this->set_internal_state();
    Tango::Except::throw_exception(
            static_cast<const char*>("could not start DAQ"),
				    static_cast<const char*>("device is not properly initialized"),
				    static_cast<const char*>("ContinuousDIDev::start"));
  }

  //- try to start daq
  try {
    daq->start();
    this->set_internal_state();
    DEBUG_STREAM << "ContinuousDIDev::start::daq successfully started" << endl;
  }
  //- device busy
  catch (const asl::DeviceBusyException& db)
  {
    //- convert daq to tango error
    Tango::DevFailed df = daq->daq_to_tango_exception(db);
    //- log
    ERROR_STREAM << df << std::endl;
    //- update internal state
    this->set_internal_state();
    //- rethrow exception
    Tango::Except::re_throw_exception(df,
            static_cast<const char*>("caught asl::DeviceBusyException"),
				    static_cast<const char*>("could not start DAQ"),
				    static_cast<const char*>("ContinuousDIDev::start"));
  }
	//- DAQ exception
  catch (const asl::DAQException& de)
  {
    //- convert daq to tango error
    Tango::DevFailed df = daq->daq_to_tango_exception(de);
    //- log
    ERROR_STREAM << df << std::endl;
    //- update internal state
    this->set_internal_state();
    //- rethrow exception
    Tango::Except::re_throw_exception(df,
            static_cast<const char*>("caught asl::DAQException"),
				    static_cast<const char*>("could not start DAQ"),
				    static_cast<const char*>("ContinuousDIDev::start"));
  }
  //- unknown exception
  catch (...)
  {
    this->set_internal_state();
		//- build Tango::DevFailed exception
		Tango::DevErrorList errors(1);
		errors.length(1);
		errors[0].severity = Tango::ERR;
		errors[0].reason = CORBA::string_dup("unknown exception caught");
		errors[0].desc = CORBA::string_dup("could not start DAQ");
		errors[0].origin = CORBA::string_dup("ContinuousDIDev::start");
		Tango::DevFailed df(errors);
	  //- log it
    ERROR_STREAM << df << std::endl;
		//- throw it
		throw df;
  }
}

//+------------------------------------------------------------------
/**
 *	method: ContinuousDIDev::stop
 *
 *	description:	method to execute "Stop"
 *	Stop data acquisition
 *
 *
 */
//+------------------------------------------------------------------
void ContinuousDIDev::stop()
{
  //- not initilized: throw exception
  if (daq == 0) {
    this->set_internal_state();
    Tango::Except::throw_exception(
            static_cast<const char*>("could not stop DAQ"),
				    static_cast<const char*>("device is not properly initialized"),
				    static_cast<const char*>("ContinuousDIDev::stop"));
  }

  try {
    //- try to stop daq
    daq->stop();
    //- update internal state
    this->set_internal_state();
    //- verbose
    DEBUG_STREAM << "ContinuousDIDev::stop::daq successfully stopped" << endl;
  }
  //- device busy
  catch (const asl::DeviceBusyException& db)
  {
    //- convert daq to tango error
    Tango::DevFailed df = daq->daq_to_tango_exception(db);
    //- log
    ERROR_STREAM << df << std::endl;
    //- update internal state
    this->set_internal_state();
    //- rethrow exception
    Tango::Except::re_throw_exception(df,
            static_cast<const char*>("caught asl::DeviceBusyException"),
				    static_cast<const char*>("could not stop DAQ"),
				    static_cast<const char*>("ContinuousDIDev::stop"));
  }
  //- daq error
  catch (const asl::DAQException& de)
  {
    //- convert daq to tango error
    Tango::DevFailed df = daq->daq_to_tango_exception(de);
    //- log
    ERROR_STREAM << df << std::endl;
    //- update internal state
    this->set_internal_state();
    //- rethrow exception
    Tango::Except::re_throw_exception(df,
            static_cast<const char*>("caught asl::DAQException"),
				    static_cast<const char*>("could not stop DAQ"),
				    static_cast<const char*>("ContinuousDIDev::stop"));
  }
  //- unknown exception
  catch (...)
  {
    this->set_internal_state();
		//- build Tango::DevFailed exception
		Tango::DevErrorList errors(1);
		errors.length(1);
		errors[0].severity = Tango::ERR;
		errors[0].reason = CORBA::string_dup("unknown exception caught");
		errors[0].desc = CORBA::string_dup("could not stop DAQ");
		errors[0].origin = CORBA::string_dup("ContinuousDIDev::stop");
		Tango::DevFailed df(errors);
	  //- log it
    ERROR_STREAM << df << std::endl;
		//- throw it
		throw df;
  }
}

//-------------------------------------------------------------------
// ContinuousDIDev::set_internal_state
//-------------------------------------------------------------------
void ContinuousDIDev::set_internal_state (void)
{
  if (daq == 0)
  {
    //- uninitialized
    this->set_state(Tango::UNKNOWN);
  }
  else
  {
    //- device state is daq state
    switch (daq->state()) 
    {
      case asl::ContinuousDI::STANDBY:
        this->set_state(Tango::STANDBY);
        break;
      case asl::ContinuousDI::RUNNING:
      case asl::ContinuousDI::ABORTING:
      case asl::ContinuousDI::CALIBRATING_HW:
        this->set_state(Tango::RUNNING);
        break;
      case asl::ContinuousDI::UNKNOWN:
      default:
        this->set_state(Tango::UNKNOWN);
        break;
    }

  }
}

} // namespace


