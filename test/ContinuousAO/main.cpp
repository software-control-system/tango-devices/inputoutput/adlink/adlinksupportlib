// ============================================================================
//
// = CONTEXT
//   tll::ContinuousAO class test
//
// = FILENAME
//   main.cpp
//
// = AUTHORS
//   GA & NL �- Synchrotron SOLEIL
//
// ============================================================================
#include <math.h>
#include <ace/ACE.h>
#include <asl/ContinuousAO.h>

#define PRINT_OUT_INSTANCE_COUNTER(X) \
ACE_DEBUG((LM_DEBUG, #X "::ic::%u\n", X::instance_counter()))  

#define kDATA_SIZE_FITS_INTO_FIFO      2048
#define kDATA_SIZE_DONT_FITS_INTO_FIFO 16384
#define kSAMPLE_PER_CHANNEL            kDATA_SIZE_FITS_INTO_FIFO

//------------------------------------------------------------------------------
//---- USER DEFINE CONTINUOUS AO DAQ: SUPPORT THE ADLINK 2502 A0 BOARD
//------------------------------------------------------------------------------
class ADLinkContinuousAO : public asl::ContinuousAO
{
public: 
  
	//----------------------------------------------
	//---- CTOR
	//----------------------------------------------
	ADLinkContinuousAO () 
	: asl::ContinuousAO ()
	{

	};

	//----------------------------------------------
	//---- DTOR
	//----------------------------------------------
	virtual ~ADLinkContinuousAO () 
	{ 

	};

	//----------------------------------------------
	//---- OVERLOAD START BEHAVIOR 
	//----------------------------------------------
	virtual void start (void) 
		throw (asl::DAQException, asl::DeviceBusyException)
	{ 
		this->buffer_counter_ = 0;
		this->ContinuousAO::start();
	};


	//----------------------------------------------
	//---- OUTPUT DATA HANDLER
	//----------------------------------------------
	virtual asl::AORawData* handle_output (unsigned int _num_active_chans, 
	                                       unsigned long _samples_per_active_chan, 
																				 bool& transfer_ownership) 
	{
	  ++this->buffer_counter_;
		
	  /*
		std::cout << "::: ADLinkContinuousAO::handle_output #"
							<< this->buffer_counter_   
							<< " :::" 
							<< std::endl;   
    */
		
		//- caller is allowed to delete the data when done with it
		transfer_ownership = true;
		
		//- generate the data
		return this->generate_output_data(_num_active_chans, _samples_per_active_chan);
	};

	//----------------------------------------------
	//---- OUTPUT DATA HANDLER
	//----------------------------------------------
	virtual asl::AOScaledData* handle_scaled_output (unsigned int _num_active_chans, 
	                                                 unsigned long _samples_per_active_chan, 
																				           bool& transfer_ownership) 
	{
    ++this->buffer_counter_;
		
	  /*
		std::cout << "::: ADLinkContinuousAO::handle_scaled_output #" 
							<< this->buffer_counter_ 
							<< " :::" 
							<< std::endl;   
    */
		
		//- caller is allowed to delete the data when done with it
		transfer_ownership = true;

		//- generate the data
		return this->generate_output_scaled_data(_num_active_chans, _samples_per_active_chan);
	};

	//----------------------------------------------
	//---- DAQ ERROR HANDLER
	//----------------------------------------------
	virtual void handle_error (const asl::DAQException& de)
	{
		std::cout << "::: ADLinkContinuousAO::handle_error :::" << std::endl;  

		for (unsigned int i = 0; i < de.errors.size(); i++)
		{
			ACE_DEBUG((LM_DEBUG, "- reason.....%s\n", de.errors[i].reason.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- desc.......%s\n", de.errors[i].desc.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- origin.....%s\n", de.errors[i].origin.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- code.......%d\n", de.errors[i].code));
			ACE_DEBUG((LM_DEBUG, "- severity...%d\n", de.errors[i].severity)); 
		}
	}

	//----------------------------------------------
	//---- OUTPUT DATA GENERATOR
	//---------------------------------num_chans-------------
	asl::AORawData* generate_output_data (unsigned int _num_chans, unsigned long _samples_per_chan)
	{
		//- instanciate the buffer
		asl::AORawData* ao_data = new asl::AORawData(_num_chans * _samples_per_chan);
		if (ao_data == 0) {
				return 0;
		}
		/*
		//test for 2 channels of group a and 2 channels of group b
		//-------fill group A
		for (unsigned long i = 0; i < _depth; i=i+4)
		{
				*(ao_data->base() + i) = sin( (i /2048.0)* 3.14159)  * 2047 + 2048;
		}
		for (i = 2; i < _depth; i=i+4)
		{
				*(ao_data->base() + i) = i;
		}
		//--------fill group B
		for (i = 1; i < _depth; i=i+4)
		{
				*(ao_data->base() + i) = sin( (i /2048.0)* 3.14159)  * 2047 + 2048;
		}
		for (i = 3; i < _depth; i=i+4)
		{
				*(ao_data->base() + i) = i;
		}
		*/

		//- test 2 channels of same group -----------------------------------
		unsigned long i;

		unsigned long  depth = _num_chans * _samples_per_chan;
		
		//-  fill buffer for 1st channel
		for (i = 0; i < depth; i = i + 2)
		{
			*(ao_data->base() + i) = static_cast<unsigned short>((::sin( (double)i / 64. * 3.14159) * 0x7ff) + 0x800);
			//- *(ao_data->base() + i + 1) = static_cast<unsigned short>(i);
		}

		return ao_data;
	}

	//----------------------------------------------
	//---- OUTPUT DATA GENERATOR
	//----------------------------------------------
	asl::AOScaledData* generate_output_scaled_data (unsigned int _num_chans, unsigned long _samples_per_chan)
	{
	  unsigned long depth = _num_chans * _samples_per_chan;
	
		//- instanciate the buffer
		asl::AOScaledData* ao_data = new asl::AOScaledData(depth);
		if (ao_data == 0) {
			return 0;
		}
		/*
		//test for 2 channels of group a and 2 channels of group b
		//-------fill group A
		//- channel 0
		for (unsigned long i = 0; i < _depth; i=i+4)
		{
				*(ao_data->base() + i) = sin( (i /2048.0)* 3.14159)  * 10;
		}
		//-channel 1
		for (i = 2; i < _depth; i=i+4)
		{
				*(ao_data->base() + i) = i*0.00488;
		}
		//--------fill group B
		//-channel 4
		for (i = 1; i < _depth; i=i+4)
		{
				*(ao_data->base() + i) = i*0.00488;

		}
		//-channel 5
		for (i = 3; i < _depth; i=i+4)
		{

				*(ao_data->base() + i) = sin( (i /2048.0)* 3.14159)  * 10;
		}
		*/

		//- test 2 channels of same group -----------------------------------
		unsigned long i;
		
		for (i = 0; i < depth; i += _num_chans)
		{
		  //- fill buffer for 1st channel
			*(ao_data->base() + i) = ::sin((double)i * 2. * 3.14159 / (double)_samples_per_chan) * 10.;
			//- fill buffer for 2nd channel			
			if (_num_chans >= 2) 
			{
        *(ao_data->base() + i + 1) = (double)i * 10. / (double)_samples_per_chan;
			}
		}

		return ao_data;
	}

  unsigned long buffer_counter_;
};

//------------------------------------------------------------------------------
//---- MAIN
//------------------------------------------------------------------------------
int main (int, char**) 
{
  //- to test peridic or aperiodic mode
  bool periodic = false;
  //- to test ouputing with scaled or non-scaled data
  bool scaled = true;
  //- to test retrigger or not
  bool retrig = false;
  
	//- the continuous AO object 
  ADLinkContinuousAO * ao = 0;
  
  try 
  {
    //- create a continuous analog output daq
    ao = new ADLinkContinuousAO();
    
		//- check memory allocation
    if (ao == 0) {
      ACE_DEBUG((LM_DEBUG, "out of memory\n"));
      return 0;
    }
		
    //--------------------------------------------------------
    //---- CONFIG EXAMPLE ------------------------------------
    //--------------------------------------------------------
    //- config analog output daq
    asl::ContinuousAOConfig config;
		
    //- activate some channels
    asl::ActiveAOChannel ac;

    //- common config. parameters for all active channels
    ac.polarity = adl::bipolar;
    ac.volt_ref_src = adl::internal_reference;
    ac.volt_ref = 10.0;
		
    //- active/config. channel 0
    ac.id = 0;
    config.add_active_channel(ac);
    
    //- active/config. channel 1
    ac.id = 1;
    config.add_active_channel(ac);
    
		
    //- The following values are the default asl::ContinuousAOConfig values 
    //_ They are here only to illustrate the asl::ContinuousAO the configuration
		
    //- the conversion clock is internal
    config.set_conversion_source (adl::ao_internal_timer);
    //- the delay counter source : internal (only usefull in delay mode)
    config.set_delay_counter_source (adl::ao_delay_internal_timer);
    //- the delay break counter source : internal
    config.set_delay_break_counter_source(adl::ao_delay_brk_internal_timer);
    //-  the delay counter :  0
    config.set_delay_counter(0);
    //-  the delay break (delay between to waveforms generations) : 0
    config.set_delay_break_counter(0);
		//- set stop source : software
    config.set_stop_source(adl::software_termination);
		//- set stop mode : stop immediatly
    config.set_stop_mode(adl::stop_immediatly);
    //- set output rate (in Hz)
    config.set_output_rate(500000);
    
    int num_chans = config.get_active_channels().size();
    
    //--------------------------------------------------------
    //---- PERIODIC MODE CONFIG EXAMPLE ----------------------
    //--------------------------------------------------------
    if (periodic)
    {
      //---- DATA : RAW or SCALED VALUES ---------------------
      if (scaled)
      {
        //- use scaled data
        config.set_data_format(adl::scaled_data);
        //- set periodic data (also enable periodic ouput mode)
        //- buffer depth = num_chans * number of samples per channel
        //- be aware that changing the size can change the way the data is generated (DMA or on board FIFO)
				//- instanciate periodic data buffer buffer
        unsigned long depth = num_chans * kSAMPLE_PER_CHANNEL;
				asl::AOScaledData* ao_data = new asl::AOScaledData(depth);
				if (ao_data == 0) {
					return 0;
				}
		    //- fill buffer
				for (unsigned long i = 0; i < depth; i += num_chans)
				{
					//- fill buffer for 1st channel
					*(ao_data->base() + i) = ::sin((double)i * 2. * 3.14159 / (double) kSAMPLE_PER_CHANNEL) * 10.;
					//- fill buffer for 2nd channel			
					if (num_chans >= 2) 
					{
						*(ao_data->base() + i + 1) = (double)i * 10. / (double) kSAMPLE_PER_CHANNEL;
					}
				}
				//- store periodic data into the AO configuration
        config.set_periodic_data(ao_data);
      }
      else
      {
        //- use non-scaled (i.e. raw) data 
        config.set_data_format(adl::raw_data);
        //- set periodic data (also enable periodic ouput mode)
        //- buffer depth = num_chans * number of samples per channel
        //- be aware that changing the size can change the way the data is generated (DMA or on board FIFO)
				//- instanciate periodic data buffer buffer
        unsigned long depth = num_chans * kSAMPLE_PER_CHANNEL;
				asl::AORawData* ao_data = new asl::AORawData(depth);
				if (ao_data == 0) {
					return 0;
				}
		    //- fill buffer
				//- TODO : same waveform in raw mode 
				//- store periodic data into the AO configuration
        config.set_periodic_data(ao_data);
      }
      //---- CONFIG EXAMPLE FOR INFINITE RETRIGGER ----------- 
      if (retrig)
      {
        //- enable retrigger (only in periodic mode)
        config.enable_retrigger();
        //- set external digital trigger
        config.set_trigger_source(adl::external_digital);
        //- post trigger
        config.set_trigger_mode(adl::ao_post);
        //- set the number of waveforms to output after each trigger
        config.set_nb_waveforms(1);
      }
    }
    //--------------------------------------------------------
    //---- APERIODIC MODE CONFIG EXAMPLE ----------------------
    //--------------------------------------------------------
    else
    {
      if (scaled)
      {
        config.set_data_format(adl::scaled_data);
      }
      else
      {
        config.set_data_format(adl::raw_data);
      }
      //- set buffer depth for non-periodic (in samples/channel)
      config.set_buffer_depth(kSAMPLE_PER_CHANNEL);
    }
		
    //--------------------------------------------------------
    //---- INIT/CONFIGURE/START/STOP DAQ ---------------------
    //--------------------------------------------------------
    //- initialize (may throw an exception)
    ao->init(adl::DAQ2502, 0);
    
    //- configure (may throw an exception) 
    ao->configure(config); 
    
    //- start async. continuous AO operation (may throw an exception) 
    
    for (int i = 0; i < 1; i++)
    {
      //- start async. continuous AO operation (may throw an exception) 
      std::cout << "::: Start continuous async. AO operation :::" << std::endl;  
			
      ao->start(); 
      
      std::cout << "::: Let the waveform generation run for a while..." << std::endl;  
      
			while (ao->buffer_counter_ < 100000)
        ACE_OS::sleep(ACE_Time_Value(0, 2500000)); 
      
      //- stop async. continuous AO operation (may throw an exception) 
      std::cout << "::: Stop continuous async. AO operation :::" << std::endl;  
			
      ao->stop();
    }
  }
  catch (const asl::DeviceBusyException& db) 
  {
	  ACE_DEBUG((LM_DEBUG, "\t asl::DeviceBusyException caught\n")); 
	  for (unsigned int i = 0; i < db.errors.size(); i++) 
	  {
		  ACE_DEBUG((LM_DEBUG, "\t reason.....%s\n", db.errors[i].reason.c_str()));  
		  ACE_DEBUG((LM_DEBUG, "\t desc.......%s\n", db.errors[i].desc.c_str())); 
		  ACE_DEBUG((LM_DEBUG, "\t origin.....%s\n", db.errors[i].origin.c_str())); 
		  ACE_DEBUG((LM_DEBUG, "\t code.......%d\n", db.errors[i].code));
		  ACE_DEBUG((LM_DEBUG, "\t severity...%d\n", db.errors[i].severity)); 
	  }
  }
  catch (const asl::DAQException& de)
  {
    ACE_DEBUG((LM_DEBUG, "asl::DAQException caught\n"));
    for (unsigned int i = 0; i < de.errors.size(); i++) 
    {
      ACE_DEBUG((LM_DEBUG, "\t reason.....%s\n", de.errors[i].reason.c_str())); 
      ACE_DEBUG((LM_DEBUG, "\t desc.......%s\n", de.errors[i].desc.c_str())); 
      ACE_DEBUG((LM_DEBUG, "\t origin.....%s\n", de.errors[i].origin.c_str())); 
      ACE_DEBUG((LM_DEBUG, "\t code.......%d\n", de.errors[i].code));
      ACE_DEBUG((LM_DEBUG, "\t severity...%d\n", de.errors[i].severity)); 
    }
  }
  catch (...) 
  {
    ACE_DEBUG((LM_DEBUG, "\t unknown exception caught\n"));  
  }
  
  //- release memory
  if (ao) delete ao;
    
    //- any memory leaks?
#if defined(ASL_DEBUG)
#if defined(_SIMULATION_)
    ACE_DEBUG((LM_DEBUG, "Memory leak counter: %d \n", MLD_COUNTER));
#else
    ACE_DEBUG((LM_DEBUG, "Memory leak counter: %d \n", MLD_COUNTER));
#endif
    if (MLD_COUNTER) {
      PRINT_OUT_INSTANCE_COUNTER(asl::Data);
    }
#endif
    
    return 0;
} 
