// ============================================================================
//
// = CONTEXT
//   asl::ContinuousAI class test
//
// = FILENAME
//   main.cpp
//
// = AUTHORS
//   GA & NL - Synchrotron SOLEIL
//
// ============================================================================
#include <ace/ACE.h>
#include <asl/ContinuousAI.h>

//------------------------------------------------------------------------------
//---- A MACRO TO PRINTOUT INSTANCE COUNTERS (DEBUG ONLY)
//------------------------------------------------------------------------------
#if defined(ASL_DEBUG)
#define PRINT_OUT_INSTANCE_COUNTER(X) \
ACE_DEBUG((LM_DEBUG, #X "::ic::%u\n", X::instance_counter()))  
#endif

//------------------------------------------------------------------------------
//---- USER DEFINED CONTINUOUS AI DAQ: SUPPORT THE ADLINK 2000 SERIE AI BOARDS
//------------------------------------------------------------------------------
class ADLinkContinuousAI : public asl::ContinuousAI
{
public: 
	//----------------------------------------------
	//---- CTOR
	//----------------------------------------------
	ADLinkContinuousAI (void) 
		: asl::ContinuousAI(),
      data_counter(0),
      tmo_counter(0),
      overrun_counter(0),
      daq_end_evt_received(false)
	{
		
	};
	
	//----------------------------------------------
	//---- DTOR
	//----------------------------------------------
	virtual ~ADLinkContinuousAI (void) 
	{ 
		//- noop
	};
	
	//----------------------------------------------
	//---- CUSTOMIZE START BEHAVIOR
	//----------------------------------------------
  virtual void start (void) throw (asl::DAQException)
	{
	  //- reset local data
	  data_counter = 0;
    tmo_counter = 0;
    overrun_counter = 0;
		daq_end_evt_received = false;
		//- call mother class' start method (may throw asl::DAQException)
		this->ContinuousAI::start();
	}
			
	//----------------------------------------------
	//---- DATA PROCESSING
	//----------------------------------------------
	virtual void handle_input (asl::AIRawData* raw_data)
	{
		data_counter++;
		
		/*
		ACE_DEBUG((LM_DEBUG, "\n- ADLinkContinuousAI::handle_input ---------------\n"));

			if (raw_data) {
				ACE_DEBUG((LM_DEBUG, "\t-raw data.............%x\n", raw_data));
				ACE_DEBUG((LM_DEBUG, "\t-raw data base @......%x\n", raw_data->base()));
				ACE_DEBUG((LM_DEBUG, "\t-raw data depth.......%d\n", raw_data->depth())); 
			}
			else {
				ACE_DEBUG((LM_DEBUG, "\t-raw data..........none\n"));
				return;
			}
		*/
		
		asl::AIScaledData * scaled_data = 0;

		try {
			scaled_data = this->scale_data(raw_data);
		}
		catch (asl::DAQException&) {
			ACE_DEBUG((LM_DEBUG, "exception thrown from ContinousAI::scale_data\n"));
			delete raw_data;
			return;
		}
		
		/*
		ACE_DEBUG((LM_DEBUG, "\t-scaled data..........%x\n", scaled_data));
		ACE_DEBUG((LM_DEBUG, "\t-scaled data base @...%x\n", scaled_data->base()));
		ACE_DEBUG((LM_DEBUG, "\t-scaled data depth....%d\n", scaled_data->depth()));
		*/

		double average = 0;
		for (unsigned int i = 0; i < scaled_data->depth(); i++) 
		{
			average += *(scaled_data->base() + i);
		}
		average /= scaled_data->depth();
		
		ACE_DEBUG((LM_DEBUG, "\t-scaled data av...%.6fV\n", average));
		
		//- release data to avoid memory leak
		delete scaled_data;
		
		//- release data to avoid memory leak
		delete raw_data;
	}
	
	//----------------------------------------------
	//---- DAQ-END HANDLER
	//----------------------------------------------
	virtual void handle_daq_end (asl::ContinuousDAQ::DaqEndReason why)
	{
		ACE_DEBUG((LM_DEBUG, "\n- ADLinkContinuousAI::handle_daq_end -------------\n"));

		ACE_DEBUG((LM_DEBUG, "\n"));
				
		daq_end_evt_received = true;

		switch (why)
    {
      case DAQEND_ON_USER_REQUEST:
		    ACE_DEBUG((LM_DEBUG, "\treason: user request\n"));
				break;
      case DAQEND_ON_EXTERNAL_TRIGGER:
		    ACE_DEBUG((LM_DEBUG, "\treason: digital external trigger raised (pre-trigger mode)\n"));
		    ACE_DEBUG((LM_DEBUG, "\treason: history buffer frozen\n"));
				break;
      case DAQEND_ON_ERROR:
        ACE_DEBUG((LM_DEBUG, "\treason: daq error\n"));
				break;		
      case DAQEND_ON_OVERRUN:
        ACE_DEBUG((LM_DEBUG, "\treason: daq buffer overrun\n"));
        ACE_DEBUG((LM_DEBUG, "\treason: daq has been <aborted> as required by the specified overrun strategy\n"));
				break;
      case DAQEND_ON_UNKNOWN_EVT:
			default:
				  ACE_DEBUG((LM_DEBUG, "\treason: unknown\n"));
          break;
    }

		ACE_DEBUG((LM_DEBUG, "\n"));
		
    //- get history history (i.e. post-mortem) buffer
		if (why == DAQEND_ON_EXTERNAL_TRIGGER && this->configuration().history_enabled())
		{
	    asl::AIScaledData * scaled_history = 0;
			try 
			{
			  //- get history as scaled data
			  scaled_history = this->scaled_history();
			  ACE_DEBUG((LM_DEBUG, "\thistory buffer depth....%d samples\n", scaled_history->depth()));
 			  ACE_DEBUG((LM_DEBUG, "\thistory buffer depth....%d daq-buffers\n", scaled_history->depth() / this->configuration().get_buffer_depth()));
			  ACE_DEBUG((LM_DEBUG, "\thistory buffer length...%d msec\n", this->configuration().get_history_length()));
 			}
			catch (const asl::DAQException& de) 
			{
				ACE_DEBUG((LM_DEBUG, "asl::DAQException caught while trying to get history buffer\n")); 
				for (unsigned int i = 0; i < de.errors.size(); i++) 
				{
					ACE_DEBUG((LM_DEBUG, "- reason.....%s\n", de.errors[i].reason.c_str())); 
					ACE_DEBUG((LM_DEBUG, "- desc.......%s\n", de.errors[i].desc.c_str())); 
					ACE_DEBUG((LM_DEBUG, "- origin.....%s\n", de.errors[i].origin.c_str())); 
					ACE_DEBUG((LM_DEBUG, "- code.......%d\n", de.errors[i].code));
					ACE_DEBUG((LM_DEBUG, "- severity...%d\n", de.errors[i].severity)); 
				}
      }
			catch (...)  
			{
				ACE_DEBUG((LM_DEBUG, "Unknown exception caught while trying to get history buffer\n")); 
			}
			
			//- release history to avois memory leak
			if (scaled_history) delete scaled_history;
			
		  ACE_DEBUG((LM_DEBUG, "\n"));
		}
	}

	//----------------------------------------------
	//---- DAQ ERROR HANDLER
	//----------------------------------------------
	virtual void handle_error (const asl::DAQException& de)
	{
		ACE_DEBUG((LM_DEBUG, "\n- ADLinkContinuousAI::handle_error -------------\n"));
		
		ACE_DEBUG((LM_DEBUG, "\n"));
		
		for (unsigned int i = 0; i < de.errors.size(); i++) 
		{
			ACE_DEBUG((LM_DEBUG, "\t. reason.....%s\n", de.errors[i].reason.c_str())); 
			ACE_DEBUG((LM_DEBUG, "\t. desc.......%s\n", de.errors[i].desc.c_str())); 
			ACE_DEBUG((LM_DEBUG, "\t. origin.....%s\n", de.errors[i].origin.c_str())); 
			ACE_DEBUG((LM_DEBUG, "\t. code.......%d\n", de.errors[i].code));
			ACE_DEBUG((LM_DEBUG, "\t. severity...%d\n", de.errors[i].severity)); 
			ACE_DEBUG((LM_DEBUG, "\n")); 
		}
	}
	
	//----------------------------------------------
	//---- OVERRUN HANDLER
	//----------------------------------------------
	virtual void handle_data_lost (void)
	{
		overrun_counter++;
		
		ACE_DEBUG((LM_DEBUG, "\n- ADLinkContinuousAI::handle_overrun -------------\n"));
		
		ACE_DEBUG((LM_DEBUG, "\n"));
		ACE_DEBUG((LM_DEBUG, "\t - overrun notification #%d\n", overrun_counter));
		ACE_DEBUG((LM_DEBUG, "\n"));
	}
	
	//----------------------------------------------
	//---- TIMEOUT HANDLER
	//----------------------------------------------
	virtual void handle_timeout (void)
	{
		tmo_counter++;
		ACE_DEBUG((LM_DEBUG, "\n- ADLinkContinuousAI::handle_timeout -------------\n"));

		ACE_DEBUG((LM_DEBUG, "\n"));
		ACE_DEBUG((LM_DEBUG, "\ttimeout notification #%d\n", tmo_counter));
		ACE_DEBUG((LM_DEBUG, "\n"));
		
		//- infinite retrigger mode
		if (this->configuration().retrigger_enabled()) 
		{
		  ACE_DEBUG((LM_DEBUG, "\treason: trigger signal is either off or disconnected\n"));
		}
		//- pre-triggger mode
		else if (daq_end_evt_received == true) 
		{
		  ACE_DEBUG((LM_DEBUG, "\treason: DAQ has been stopped by interlock trigger\n"));
		  ACE_DEBUG((LM_DEBUG, "\treason: in this case the timeout event can be ignored\n"));
		}
		//- remaining cases
		else 
		{
		  ACE_DEBUG((LM_DEBUG, "\treason: may be due to a DAQ configuration problem\n"));
		  ACE_DEBUG((LM_DEBUG, "\treason: the DAQ buffer depth may be too large for the selected sampling rate\n"));
			ACE_DEBUG((LM_DEBUG, "\treason: the timeout delay may also be too short compared to the time required to fill a daq buffer\n"));
		  ACE_DEBUG((LM_DEBUG, "\treason: try to increase either the timeout delay or sampling rate\n"));
		}

		ACE_DEBUG((LM_DEBUG, "\n"));
	}
	
	//-----------------------------------------------
	// stat: number of data buffers received
	//-----------------------------------------------
	unsigned long data_counter;
	
	//-----------------------------------------------
	// stat: number of timeout notifications received
	//-----------------------------------------------
	unsigned long tmo_counter;
	
	//-----------------------------------------------
	// stat: number of overrun notifications received
	//-----------------------------------------------
	unsigned long overrun_counter;

	//-----------------------------------------------
	// flag: daq_end received
	//-----------------------------------------------
	bool daq_end_evt_received;
	
}; //- end of class ADLinkContinuousAI

//------------------------------------------------------------------------------
//---- MAIN
//------------------------------------------------------------------------------
int main (int, char**) 
{
  //- let's start with an asl::circular buffer test
  unsigned long value = 0;

  const size_t kBUFFER_SIZE = 1;

  asl::Buffer<unsigned long> b(kBUFFER_SIZE);
  asl::CircularBuffer<unsigned long> cb(kBUFFER_SIZE, 16);
  for (size_t j = 0; j < 34; j++)
  {
     for (size_t j = 0; j < kBUFFER_SIZE; j++)
       b[j] = value;
     value++;
     cb.push(&b);
  }

  asl::Buffer<unsigned long> * od = cb.ordered_data();

  size_t max = kBUFFER_SIZE * 16;
  unsigned long previous_value = *(od->base());
  unsigned long current_value;
  for (size_t k = 1; k < max; k++)
  {
    current_value = *(od->base() + k);
    ACE_DEBUG((LM_DEBUG, "previous-val::%d - current-val::%d\n", previous_value, current_value));
    if (current_value < previous_value)
    {
      ACE_DEBUG((LM_DEBUG, "OOPS...there is something wrong in the asl::CircularBuffer impl ):-o\n"));
      return 0;
    }
    previous_value = current_value; 
  }

  ACE_DEBUG((LM_DEBUG, "OK...there is nothing wrong in the asl::CircularBuffer impl (:-)\n"));

	//- select board to use (supported hw: adl::DAQ2005, adl::DAQ2010) 
	unsigned short board_type = adl::DAQ2005; 
	
	//- select board id 
	unsigned short board_id = 0; 
	
	for (int i = 0; i < 1; i++) 
	{
		//- a continuous analog input daq.
		ADLinkContinuousAI * ai = new ADLinkContinuousAI;
		if (ai == 0) {
			ACE_DEBUG((LM_DEBUG, "out of memory\n"));
			break;
		}
		
		try 
		{   
			//--------------------------------------------------------
			//---- CONFIGURATION -------------------------------------
			//--------------------------------------------------------
			//- the continuous analog input DAQ configuration
			asl::ContinuousAIConfig config;
			
			//--------------------------------------------------------
			//---- ACTIVE CHANNELS CONFIG EXAMPLE --------------------
			//--------------------------------------------------------
			//- activate AI channels (board dependent)
			asl::ActiveAIChannel ac;
			//- configure/activate channel 0
			ac.id = 0;
			ac.range = adl::bp_10;
			config.add_active_channel(ac);
			//- configure/activate channel 1
			// ac.id = 1;
			// ac.range = adl::bp_10;
			// config.add_active_channel(ac);
			//- configure/activate channel 2
			// ac.id = 2;
			// ac.range = adl::bp_10;
			// config.add_active_channel(ac);
			//- configure/activate channel 3
			// ac.id = 3;
			// ac.range = adl::bp_10;
			// config.add_active_channel(ac);
			
			//--------------------------------------------------------
			//---- INFINITE RETRIGGER CONFIG EXAMPLE -----------------
			//--------------------------------------------------------
/*
			//- enable infinite retrigger
			config.enable_retrigger();
			//- set sampling rate (valid range is board dependent)
			config.set_sampling_rate(500000);
			//- set DAQ buffer depth in samples/channel (sampling_rate dependent)
			config.set_buffer_depth(10000);
			//- set trigger mode
			config.set_trigger_mode(adl::ai_delay);
			//- set delay unit 
			config.set_delay_unit(adl::clock_ticks);
			//- set delay value in micro-secs (delay unit is clock_ticks)
			config.set_middle_or_delay_scans(25);
			//- set trigger source
			config.set_trigger_source(adl::external_digital);
			//- set overrun strategy (just notify)
			config.set_data_lost_strategy(adl::notify);
			//- set timeout (in milliseconds)
			config.set_timeout(1000);
*/
			//--------------------------------------------------------
			//---- DOUBLE BUFFERING CONFIG EXAMPLE -------------------
			//--------------------------------------------------------

			//- set sampling rate (range is board dependent - unit is Hz)
			config.set_sampling_rate(500000);
			//- set DAQ buffer depth  (sampling_rate dependent - unit is in samples/channel)
			config.set_buffer_depth(500); 
			//- set overrun strategy (can be adl::ignore, adl::notify, adl::abort or adl::restart)
			config.set_data_lost_strategy(adl::ignore);
			//- set timeout (unit is milliseconds)
			config.set_timeout(10000);
      //- enable a one second long history buffer (unit is milliseconds)
			config.enable_history(1000);	
			//- set trigger mode
			config.set_trigger_mode(adl::ai_pre);
			//- set trigger source
			config.set_trigger_source(adl::external_digital);

			//--------------------------------------------------------
			//---- INIT/CONFIGURE/START/STOP DAQ ---------------------
			//--------------------------------------------------------
			//- initialize (may throw an exception)
			ai->init(board_type, board_id);
			//- configure (may throw an exception) 
			ai->configure(config);
			//- start async. continuous AI operation (may throw an exception) 
			ai->start(); 
			//- let the DAQ run 
      //while (ai->data_counter < 1 && ai->tmo_counter == 0 && ai->daq_end_evt_received == false)
        //ACE_OS::sleep(ACE_Time_Value(0,250000)); 
			//- calibrate hardware
			//ai->calibrate_hardware();
			//- let the DAQ run 
      while (ai->data_counter < 2 && ai->tmo_counter == 0 && ai->daq_end_evt_received == false)
        ACE_OS::sleep(ACE_Time_Value(3600,0)); 
      //- stop async. continuous AI operation (may throw an exception) 
      ai->stop();
      //- wait daq_end_evt
#if !defined(_SIMULATION_)
			while (ai->daq_end_evt_received == false)
				ACE_OS::sleep(1); 
#endif
			//- printout some stats
		  ACE_DEBUG((LM_DEBUG, "\n"));
			ACE_DEBUG((LM_DEBUG, "- DAQ-Stats: data counter......%d\n", ai->data_counter)); 
			ACE_DEBUG((LM_DEBUG, "- DAQ-Stats: overrun counter...%d\n", ai->overrun_counter));
			ACE_DEBUG((LM_DEBUG, "- DAQ-Stats: timeout counter...%d\n", ai->tmo_counter)); 
		  ACE_DEBUG((LM_DEBUG, "\n"));
		}
		catch (const asl::DeviceBusyException& db) 
		{
			ACE_DEBUG((LM_DEBUG, "asl::DeviceBusyException caught\n")); 
			for (unsigned int i = 0; i < db.errors.size(); i++) 
			{
				ACE_DEBUG((LM_DEBUG, "- reason.....%s\n", db.errors[i].reason.c_str()));  
				ACE_DEBUG((LM_DEBUG, "- desc.......%s\n", db.errors[i].desc.c_str())); 
				ACE_DEBUG((LM_DEBUG, "- origin.....%s\n", db.errors[i].origin.c_str())); 
				ACE_DEBUG((LM_DEBUG, "- code.......%d\n", db.errors[i].code));
				ACE_DEBUG((LM_DEBUG, "- severity...%d\n", db.errors[i].severity)); 
			}
		}
		catch (const asl::DAQException& de) 
		{
			ACE_DEBUG((LM_DEBUG, "asl::DAQException caught\n")); 
			for (unsigned int i = 0; i < de.errors.size(); i++) 
			{
				ACE_DEBUG((LM_DEBUG, "- reason.....%s\n", de.errors[i].reason.c_str()));  
				ACE_DEBUG((LM_DEBUG, "- desc.......%s\n", de.errors[i].desc.c_str())); 
				ACE_DEBUG((LM_DEBUG, "- origin.....%s\n", de.errors[i].origin.c_str())); 
				ACE_DEBUG((LM_DEBUG, "- code.......%d\n", de.errors[i].code));
				ACE_DEBUG((LM_DEBUG, "- severity...%d\n", de.errors[i].severity)); 
			}
		}
		catch (...) 
		{
			ACE_DEBUG((LM_DEBUG, "unknown exception caught\n"));  
		}
		
		if (ai) delete ai;
		
  } // for
  
  //- any memory leaks?
#if defined(ASL_DEBUG)
#if defined(_SIMULATION_)
  ACE_DEBUG((LM_DEBUG, "Memory leak counter: %d (expected 1 - PulsedTaskManager still alive)\n", MLD_COUNTER));
#else
  ACE_DEBUG((LM_DEBUG, "Memory leak counter: %d (should be 0)\n", MLD_COUNTER));
#endif
  if (MLD_COUNTER) 
  {
	  PRINT_OUT_INSTANCE_COUNTER(asl::Data);
	  PRINT_OUT_INSTANCE_COUNTER(asl::Message);
  }
#endif
  
  return 0;
} 

