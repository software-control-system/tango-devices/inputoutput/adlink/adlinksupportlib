// ============================================================================
//
// = CONTEXT
//   asl::ContinuousDI class test
//
// = FILENAME
//   main.cpp
//
// = AUTHORS
//   GA & NL �- Synchrotron SOLEIL
//
// ============================================================================
#include <asl/Data_T.h>
#include <asl/Worker.h>
#include <asl/Thread.h>
#include <asl/PulsedTask.h>
#include <asl/Extractor_T.h>
#include <asl/adlink/AD2005.h>
#include <asl/adlink/AD2010.h>
#include <asl/ContinuousDI.h>

//------------------------------------------------------------------------------
//---- A MACRO TO PRINTOUT INSTANCE COUNTERS (DEBUG ONLY)
//------------------------------------------------------------------------------
#if defined(ASL_DEBUG)
#define PRINT_OUT_INSTANCE_COUNTER(X) \
    ACE_DEBUG((LM_DEBUG, #X "::ic::%u\n", X::instance_counter()))  
#endif

//------------------------------------------------------------------------------
//---- USER DEFINED CONTINUOUS DI DAQ: SUPPORT THE ADLINK 2000 SERIE DI BOARDS
//------------------------------------------------------------------------------
class ADLinkContinuousDI : public asl::ContinuousDI
{
public: 
  //----------------------------------------------
  //---- CTOR
  //----------------------------------------------
  ADLinkContinuousDI (void) 
    : asl::ContinuousDI(),
      data_counter(0),
      tmo_counter(0),
      overrun_counter(0),
      daq_end_evt_received(false)
  {
    //- noop
  };

  //----------------------------------------------
  //---- DTOR
  //----------------------------------------------
  virtual ~ADLinkContinuousDI (void) 
  { 
    //- noop
  };

	//----------------------------------------------
	//---- CUSTOMIZE START BEHAVIOR
	//----------------------------------------------
  virtual void start (void) throw (asl::DAQException)
	{
	  //- reset local data
	  data_counter = 0;
    tmo_counter = 0;
    overrun_counter = 0;
		daq_end_evt_received = false;
		//- call mother class' start method (may throw asl::DAQException)
		this->ContinuousDI::start();
	}

  //----------------------------------------------
  //---- DATA PROCESSING FOR 8 BITS DATA
  //----------------------------------------------
  virtual void handle_byte_input (asl::DIByteData* data)
  {
    data_counter++;

    ACE_DEBUG((LM_DEBUG, "\n- ADLinkContinuousDI::handle_byte_input ----------\n"));

    //- release data to avoid memory leak
    delete data;
  }


  //----------------------------------------------
  //---- DATA PROCESSING FOR 16 BITS DATA
  //----------------------------------------------
  virtual void handle_short_input (asl::DIShortData* data)
  {
    data_counter++;

    ACE_DEBUG((LM_DEBUG, "\n- ADLinkContinuousDI::handle_short_input ---------\n"));

    //- release data to avoid memory leak
    delete data;
  }

  //----------------------------------------------
  //---- DATA PROCESSING FOR 32 BITS DATA
  //----------------------------------------------
  virtual void handle_long_input (asl::DILongData* data)
  {
    data_counter++;

    ACE_DEBUG((LM_DEBUG, "\n- ADLinkContinuousDI::handle_long_input ----------\n"));

    //- release data to avoid memory leak
    delete data;
  }

//----------------------------------------------
	//---- DAQ-END HANDLER
	//----------------------------------------------
	virtual void handle_daq_end (asl::ContinuousDAQ::DaqEndReason why)
	{
		ACE_DEBUG((LM_DEBUG, "\n- ADLinkContinuousDI::handle_daq_end -------------\n"));

		ACE_DEBUG((LM_DEBUG, "\n"));
				
		daq_end_evt_received = true;

		switch (why)
    {
      case DAQEND_ON_USER_REQUEST:
		    ACE_DEBUG((LM_DEBUG, "\treason: user request\n"));
				break;
      case DAQEND_ON_EXTERNAL_TRIGGER:
		    ACE_DEBUG((LM_DEBUG, "\treason: digital external trigger raised\n"));
				break;
      case DAQEND_ON_ERROR:
        ACE_DEBUG((LM_DEBUG, "\treason: daq error\n"));
				break;		
      case DAQEND_ON_OVERRUN:
        ACE_DEBUG((LM_DEBUG, "\treason: daq buffer overrun\n"));
        ACE_DEBUG((LM_DEBUG, "\treason: daq has been <aborted> as required by the specified overrun strategy\n"));
				break;
      case DAQEND_ON_UNKNOWN_EVT:
			default:
				  ACE_DEBUG((LM_DEBUG, "\treason: unknown\n"));
          break;
    }

		ACE_DEBUG((LM_DEBUG, "\n"));
	}

  //----------------------------------------------
  //---- DAQ ERROR HANDLER
  //----------------------------------------------
  virtual void handle_error (const asl::DAQException& de)
  {
    ACE_DEBUG((LM_DEBUG, "\n- ADLinkContinuousDI::handle_error --------------\n"));

    for (unsigned int i = 0; i < de.errors.size(); i++) 
    {
      ACE_DEBUG((LM_DEBUG, "\t. reason.....%s\n", de.errors[i].reason.c_str())); 
      ACE_DEBUG((LM_DEBUG, "\t. desc.......%s\n", de.errors[i].desc.c_str())); 
      ACE_DEBUG((LM_DEBUG, "\t. origin.....%s\n", de.errors[i].origin.c_str())); 
      ACE_DEBUG((LM_DEBUG, "\t. code.......%d\n", de.errors[i].code));
      ACE_DEBUG((LM_DEBUG, "\t. severity...%d\n", de.errors[i].severity)); 
      ACE_DEBUG((LM_DEBUG, "\n")); 
    }
  }

	//----------------------------------------------
	//---- OVERRUN HANDLER
	//----------------------------------------------
	virtual void handle_data_lost (void)
	{
		overrun_counter++;
		
		ACE_DEBUG((LM_DEBUG, "\n- ADLinkContinuousDI::handle_overrun -------------\n"));
		
		ACE_DEBUG((LM_DEBUG, "\n"));
		ACE_DEBUG((LM_DEBUG, "\t - overrun notification #%d\n", overrun_counter));
		ACE_DEBUG((LM_DEBUG, "\n"));
	}

  //----------------------------------------------
	//---- TIMEOUT HANDLER
	//----------------------------------------------
	virtual void handle_timeout (void)
	{
		tmo_counter++;
		ACE_DEBUG((LM_DEBUG, "\n- ADLinkContinuousAI::handle_timeout -------------\n"));

		ACE_DEBUG((LM_DEBUG, "\n"));
		ACE_DEBUG((LM_DEBUG, "\ttimeout notification #%d\n", tmo_counter));
		ACE_DEBUG((LM_DEBUG, "\n"));
		
		if (daq_end_evt_received == true) 
		{
		  ACE_DEBUG((LM_DEBUG, "\treason: DAQ has been stopped by interlock trigger\n"));
		  ACE_DEBUG((LM_DEBUG, "\treason: in this case the timeout event can be ignored\n"));
		}
		//- remaining cases
		else 
		{
		  ACE_DEBUG((LM_DEBUG, "\treason: may be due to a DAQ configuration problem\n"));
		  ACE_DEBUG((LM_DEBUG, "\treason: the DAQ buffer depth may be too large for the selected sampling rate\n"));
			ACE_DEBUG((LM_DEBUG, "\treason: the timeout delay may also be too short compared to the time required to fill a daq buffer\n"));
		  ACE_DEBUG((LM_DEBUG, "\treason: try to increase either the timeout delay or sampling rate\n"));
		}

		ACE_DEBUG((LM_DEBUG, "\n"));
	}
  //-----------------------------------------------
  // stat: number of data buffers received
  //-----------------------------------------------
  unsigned long data_counter;

  //-----------------------------------------------
  // stat: number of timeout notifications received
  //-----------------------------------------------
  unsigned long tmo_counter;

  //-----------------------------------------------
  // stat: number of overrun notifications received
  //-----------------------------------------------
  unsigned long overrun_counter;

	//-----------------------------------------------
	// flag: daq_end received
	//-----------------------------------------------
	bool daq_end_evt_received;

}; //- end of class ADLinkContinuousDI

//------------------------------------------------------------------------------
//---- MAIN
//------------------------------------------------------------------------------
int main (int, char**) 
{
  //- select board to use (supported hw: adl::DAQ7300) 
  unsigned short board_type = adl::PCI7300; 

  //- select board id 
  unsigned short board_id = 0; 

  for (int i = 0; i < 1; i++) 
  {
    //- a continuous analog input daq.
    ADLinkContinuousDI * di = new ADLinkContinuousDI;
    if (di == 0) {
      ACE_DEBUG((LM_DEBUG, "out of memory\n"));
      break;
    }

    try 
    {   
      //--------------------------------------------------------
      //---- CONFIGURATION -------------------------------------
      //--------------------------------------------------------
        //- the continuous analog input DAQ configuration
        asl::ContinuousDIConfig config;

      //--------------------------------------------------------
      //---- CONFIG EXAMPLE ------------------------------------
      //--------------------------------------------------------
		    //- most of the values are the values by default. They are here for information.
		    //- set DI port width (8, 16 or 32 bits)
		    config.set_port_width (adl::width_32); 
		    //- trigger source internal
		    config.set_trigger_source (adl::trigger_clock_20Mhz);
        //- the trigger signal is rising edge active
		    //- config.set_trigger_polarity (adl::trigger_di_rising_edge); 
		    //- the acquisition starts immediatly when user starts it.
		    config.set_start_mode(adl::start_immediatly);
		    //- the terminator is on
		    //- config.set_terminator (adl::terminator_on);
		    //- the request signal is rising edge active
		    //- config.set_request_polarity (adl::request_di_rising_edge);
		    //- the request signal is rising edge active
		    //- config.set_ack_polarity (adl::ack_di_rising_edge);
        //- set sampling rate in Hz.
        config.set_sampling_rate (500000);
        //- set DAQ buffer depth in samples/channel (sampling_rate dependent)
        config.set_buffer_depth (250000); 
        //- set overrun strategy (notify)
        config.set_data_lost_strategy (adl::notify);
        //- set timeout (in ms)
        config.set_timeout(3000);

      //--------------------------------------------------------
      //---- INIT/CONFIGURE/START/STOP DAQ ---------------------
      //--------------------------------------------------------
        //- initialize (may throw an exception)
        di->init(board_type, board_id);
        //- configure (may throw an exception) 
        di->configure(config); 
        //- start async. continuous AI operation (may throw an exception) 
        di->start(); 
				//- let the DAQ run 
				while (di->data_counter < 100 && di->tmo_counter == 0 && di->daq_end_evt_received == false)
					ACE_OS::sleep(1); 
        //- stop async. continuous AI operation (may throw an exception) 
        di->stop();
        //- wait daq_end_evt
#if !defined(_SIMULATION_)
			  while (di->daq_end_evt_received == false)
				  ACE_OS::sleep(1); 
#endif
			  //- printout some stats
		    ACE_DEBUG((LM_DEBUG, "\n"));
			  ACE_DEBUG((LM_DEBUG, "- DAQ-Stats: data counter......%d\n", di->data_counter)); 
			  ACE_DEBUG((LM_DEBUG, "- DAQ-Stats: overrun counter...%d\n", di->overrun_counter));
			  ACE_DEBUG((LM_DEBUG, "- DAQ-Stats: timeout counter...%d\n", di->tmo_counter)); 
		    ACE_DEBUG((LM_DEBUG, "\n"));
    }
		catch (const asl::DeviceBusyException& db) 
		{
			ACE_DEBUG((LM_DEBUG, "asl::DeviceBusyException caught\n")); 
			for (unsigned int i = 0; i < db.errors.size(); i++) 
			{
				ACE_DEBUG((LM_DEBUG, "- reason.....%s\n", db.errors[i].reason.c_str()));  
				ACE_DEBUG((LM_DEBUG, "- desc.......%s\n", db.errors[i].desc.c_str())); 
				ACE_DEBUG((LM_DEBUG, "- origin.....%s\n", db.errors[i].origin.c_str())); 
				ACE_DEBUG((LM_DEBUG, "- code.......%d\n", db.errors[i].code));
				ACE_DEBUG((LM_DEBUG, "- severity...%d\n", db.errors[i].severity)); 
			}
		}
    catch (const asl::DAQException& de) 
    {
      ACE_DEBUG((LM_DEBUG, "asl::DAQException caught\n")); 
      for (unsigned int i = 0; i < de.errors.size(); i++) 
      {
        ACE_DEBUG((LM_DEBUG, "- reason.....%s\n", de.errors[i].reason.c_str())); 
        ACE_DEBUG((LM_DEBUG, "- desc.......%s\n", de.errors[i].desc.c_str())); 
        ACE_DEBUG((LM_DEBUG, "- origin.....%s\n", de.errors[i].origin.c_str())); 
        ACE_DEBUG((LM_DEBUG, "- code.......%d\n", de.errors[i].code));
        ACE_DEBUG((LM_DEBUG, "- severity...%d\n", de.errors[i].severity)); 
      }
      if (di) delete di;
      break;
    }
    catch (...) 
    {
      ACE_DEBUG((LM_DEBUG, "unknown exception caught\n"));  
      if (di) delete di;
      break;
    }
  
    if (di) delete di;

  } // for

  //- any memory leaks?
#if defined(ASL_DEBUG)
#if defined(_SIMULATION_)
  ACE_DEBUG((LM_DEBUG, "Memory leak counter: %d (expected 1 - PulsedTaskManager still alive)\n", MLD_COUNTER));
#else
  ACE_DEBUG((LM_DEBUG, "Memory leak counter: %d (should be 0)\n", MLD_COUNTER));
#endif
  if (MLD_COUNTER) {
    PRINT_OUT_INSTANCE_COUNTER(asl::Data);
    PRINT_OUT_INSTANCE_COUNTER(asl::Message);
    PRINT_OUT_INSTANCE_COUNTER(asl::Work);
    PRINT_OUT_INSTANCE_COUNTER(asl::Worker);
    PRINT_OUT_INSTANCE_COUNTER(asl::Thread);
    PRINT_OUT_INSTANCE_COUNTER(asl::PulsedTask);
    PRINT_OUT_INSTANCE_COUNTER(asl::SharedObject);
  }
#endif

  return 0;
} 
