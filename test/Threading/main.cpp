// ============================================================================
//
// = CONTEXT
//   tll::Worker class test
//
// = FILENAME
//   main.cpp
//
// = AUTHOR
//   Nicolas Leclercq
//
// ============================================================================
#include <asl/Data_T.h>
#include <asl/Worker.h>
#include <asl/Thread.h>
#include <asl/PulsedTask.h>
#include "MyDataProcessing.h"

#define PRINT_OUT_INSTANCE_COUNTER(X) \
  ACE_DEBUG((LM_DEBUG, "X::ic::%u\n", X::instance_counter()))  

//*****************************************************************************
//*****************************************************************************
class ThreadTest : public asl::Thread
{
public:

  ThreadTest ();

  virtual ~ThreadTest ();

protected:

  virtual ACE_THR_FUNC_RETURN svc (void* arg);
};
//-----------------------------------------------------------------------------  
ThreadTest::ThreadTest () 
 : asl::Thread()
{
  //-noop ctor
}
//-----------------------------------------------------------------------------  
ThreadTest::~ThreadTest ()
{
  //-noop ctor
}
//----------------------------------------------------------------------------- 
ACE_THR_FUNC_RETURN ThreadTest::svc (void* arg)
{
  //- verbose
  ACE_DEBUG((LM_DEBUG, "Entering ThreadTest::svc (arg:%x)\n", arg));

  //- sleep time
  ACE_Time_Value st(0, 500000);

  //- enter almost infinite loop
  do {

    ACE_OS::sleep(st);  

    ACE_DEBUG((LM_DEBUG, "ThreadTest::svc::wokenup\n"));

  } while (quit_requested() == false);

  //- verbose
  ACE_DEBUG((LM_DEBUG, "Leaving ThreadTest::svc\n"));

  //- return result (here just return <arg> back)
  return ACE_static_cast(ACE_THR_FUNC_RETURN, arg);
}
//*****************************************************************************
//*****************************************************************************
class PulsedTaskTest : public asl::PulsedTask
{
public:

  PulsedTaskTest ();

  virtual ~PulsedTaskTest ();

  virtual int pulsed (void* arg);
};
//-----------------------------------------------------------------------------  
PulsedTaskTest::PulsedTaskTest () 
 : asl::PulsedTask()
{
  //-noop ctor
}
//-----------------------------------------------------------------------------  
PulsedTaskTest::~PulsedTaskTest () 
{
  //-noop ctor
}
//----------------------------------------------------------------------------- 
int PulsedTaskTest::pulsed (void* arg)
{
  //- verbose
  ACE_DEBUG((LM_DEBUG, "Entering PulsedTaskTest::pulsed (arg:%x)\n", arg));

  return 0;
}
//*****************************************************************************
//*****************************************************************************
void print_out_worker_state (const asl::Worker& worker) 
{
  std::string txt("Worker::");
  txt += worker.name();
  txt += "'s state is ";
  switch (worker.state()) {
    case asl::Worker::UP:
      txt += "UP";
      break;
    case asl::Worker::DOWN:
      txt += "DOWN";
      break;
    case asl::Worker::STANDBY:
      txt += "STANDBY";
      break;
    case asl::Worker::WORKING:
      txt += "WORKING";
      break;
    case asl::Worker::UNDEFINED:
      txt += "UNDEFINED";
      break;
    default:
      txt += "UNKNOWN (this is a bug!)";
      break;
  }
  ACE_DEBUG((LM_DEBUG, "%s\n", txt.c_str()));
}
//*****************************************************************************
//*****************************************************************************
int main (int, char**) 
{

  //- allocate the work to do (always allocated on heap using new)
  //- the worker gets ownership of its work and relese it when itself 
  //- destroyed. consequently, never try to delete <dp> since it belongs 
  //- to the worker-> 
  MyDataProcessing *mdp = new MyDataProcessing;
  if (mdp == 0) {
    //- out of memory error
    return 0;
  }

  //- instanciate the Worker specifing its name and the work to do
  asl::Worker *worker = new asl::Worker("DataProcessor", mdp); 
  if (worker == 0) {
    //- out of memory error
    return 0;
  }

  //- print out current Worker state
  ::print_out_worker_state(*worker);

  //- low and high water marks of the Worker's message queue
  size_t lwm = 1024 * 1024; //- 1Mb 
  size_t hwm = 4096 * 1024; //- 4Mb 

  //- worker initialization
  int err = worker->init(lwm, hwm);
  if (err == -1) {
    //- initialization failed
    asl::WorkerErrorPtr werr = worker->last_error();
    ACE_DEBUG((LM_DEBUG, "err:%s - code:%d \n", werr->text().c_str(), werr->code()));
    return 0;
  }

  //- print out current Worker state
  ::print_out_worker_state(*worker);

  //- start the worker
  err = worker->start();
  if (err == -1) {
    //- initialization failed
    asl::WorkerErrorPtr werr = worker->last_error();
    ACE_DEBUG((LM_DEBUG, "err:%s - code:%d \n", werr->text().c_str(), werr->code()));
    return 0;
  }

  //- print out current Worker state
  ::print_out_worker_state(*worker);

  //- send 10 data message containing one buffer to the Worker
  asl::Buffer<int> * data;
  for (int i = 0; i < 10; i++) {
    data = new asl::Buffer<int>(1024);
    if (!data) break;
    data->fill(i);
    worker->post(new asl::Message(data));
  }

  //- send a user defined message to the worker
  //- encapsulate astring in a container
  std::string hw("hello world");
  asl::Container<std::string> *c = new asl::Container<std::string>(hw);
  if (c == 0) {
    //- out of memory error
    return 0;
  }
  worker->post(new asl::Message(c, HELLO_WORLD_MSG));
  
  //- send 1 message containg 10 buffers to the Worker
  data = 0;
  asl::Buffer<int> * head = 0;
  asl::Buffer<int> * tmp  = 0;
  for (int j = 0; j < 10; j++) {
    tmp = new asl::Buffer<int>(1024);
    if (!tmp) break;
    tmp->fill(j);
    if (data) {
      data->cont(tmp);
    }
    else {
      head = tmp;
    }
    data = tmp;
  }
  worker->post(new asl::Message(head));

  //- stop the worker
  err = worker->stop();
  if (err == -1) {
    //- initialization failed
    asl::WorkerErrorPtr werr = worker->last_error();
    ACE_DEBUG((LM_DEBUG, "err:%s - code:%d \n", werr->text().c_str(), werr->code()));
    return 0;
  }

  //- print out current Worker state
  ::print_out_worker_state(*worker);

  //- check error
  if (worker->has_error()) {
    asl::WorkerErrorPtr werr = worker->last_error();
    ACE_DEBUG((LM_DEBUG, "err:%s - code:%d \n", werr->text().c_str(), werr->code()));
  }

  //- ask the worker to quit
  err = worker->quit();
  if (err == -1) {
    //- initialization failed
    asl::WorkerErrorPtr werr = worker->last_error();
    ACE_DEBUG((LM_DEBUG, "err:%s - code:%d \n", werr->text().c_str(), werr->code()));      
    return 0;
  }

  //- give the worker a chance to process the data
  ACE_OS::sleep(1);

  //- print out current Worker state
  ::print_out_worker_state(*worker);

  //- dump worker internal state
  worker->dump();

  //- test asl::Thread
  ACE_DEBUG((LM_DEBUG, "Spawn new Thread with arg:%x\n", worker));

  ThreadTest *t = new ThreadTest();
  if (t == 0) {
    //- out of memory error
    return 0;
  }

  //- run the thread
  if (t->run(ACE_static_cast(void*, worker)) == -1) {
    ACE_DEBUG((LM_DEBUG, "Thread::run failed\n"));  
    return 0;
  }

  //- give the thread some time to run
  ACE_OS::sleep(4);

  //- print out result
  ACE_DEBUG((LM_DEBUG, "Ask the Thread to quit\n"));  

  //- ask the thread to quit and join with it
  ACE_THR_FUNC_RETURN status = 0;
  if (t->quit_and_join(&status) == -1) {
    ACE_DEBUG((LM_DEBUG, "Thread::quit_and_join failed\n"));  
    return 0;
  }

  //- print out result
  ACE_DEBUG((LM_DEBUG, "Thread::result: %x\n", status));  


  PulsedTaskTest *pt = new PulsedTaskTest();
  if (pt == 0) {
    //- out of memory error
    return 0;
  }

  //- start the pulsed task
  if (pt->start(ACE_static_cast(void*, worker), 100, 250) == -1) {
    ACE_DEBUG((LM_DEBUG, "PulsedTask::start failed\n"));  
    return 0;
  }

  //- give the pulsed task some time to run
  ACE_OS::sleep(5);

  //- stop pulsed task
  pt->stop();

  delete worker;
  delete t;
  delete pt;

  //- check memory leak 
#if defined(asl_DEBUG)
  ACE_DEBUG((LM_DEBUG, "Memory leak counter: %d (should be 0)\n", MLD_COUNTER));
  if (MLD_COUNTER) {
    PRINT_OUT_INSTANCE_COUNTER(asl::Data);
    PRINT_OUT_INSTANCE_COUNTER(asl::Message);
    PRINT_OUT_INSTANCE_COUNTER(asl::Work);
    PRINT_OUT_INSTANCE_COUNTER(asl::Worker);
    PRINT_OUT_INSTANCE_COUNTER(asl::WorkerError);
    PRINT_OUT_INSTANCE_COUNTER(asl::Thread);
    PRINT_OUT_INSTANCE_COUNTER(asl::PulsedTask);
  }
#endif

  return 0;
} 