// ============================================================================
//
// = CONTEXT
//   tll::Worker class test
//
// = FILENAME
//   MyMyDataProcessing.h
//
// = AUTHOR
//   Nicolas Leclercq
//
// ============================================================================
#ifndef _MY_DATA_PROCESSING_H_
#define _MY_DATA_PROCESSING_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/Work.h>

//- example of user defined message
#define HELLO_WORLD_MSG (asl::MessageType::MSG_FIRST_USER_MSG + 1)

// ============================================================================
// CLASS: MyDataProcessing
// ============================================================================
class MyDataProcessing : public asl::Work
{
public:
  
  // = Initializatation and termination methods.
  //--------------------------------------------
  MyDataProcessing (void);
  // Constructor.

  virtual ~MyDataProcessing (void);
  // Destructor.

protected:

  // = <Actions>
  //--------------------------------------------
  virtual int on_init (asl::WorkerError *&err);
  // Called when Worker is initialized.

  virtual int on_start (asl::WorkerError *&err);
  // Called when the Worker  receive a <START> message.

  virtual int on_reset (asl::WorkerError *&err);
  // Called fwhen the Worker  receive a <RESET> message.

  virtual int on_stop (asl::WorkerError *&err);
  // Called when the Worker  receive a <STOP> message.

  virtual int on_abort (asl::WorkerError *&err);
  // Called when the Worker  receive a <ABORT> message.

  virtual void on_quit (void);
  // Called when the Worker  receive a <QUIT> message.

  virtual int process(asl::Message *in, asl::WorkerError *&err);
  // Called the Worker receive a <DATA> or a user defined message.

private:

  // = Disallow these operations.
  //--------------------------------------------
  ACE_UNIMPLEMENTED_FUNC(MyDataProcessing& operator= (const MyDataProcessing&))
  ACE_UNIMPLEMENTED_FUNC(MyDataProcessing(const MyDataProcessing&))
};

#endif // _MY_DATA_PROCESSING_H_
