// ============================================================================
//
// = CONTEXT
//   tll::Worker class test
//
// = FILENAME
//   MyDataProcessing.cpp
//
// = AUTHOR
//   Nicolas Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIEs
// ============================================================================
#include <iostream>
#include <asl/Data_T.h>
#include <asl/Extractor_T.h>
#include "MyDataProcessing.h"

// ============================================================================
// MyDataProcessing::MyDataProcessing
// ============================================================================
MyDataProcessing::MyDataProcessing (void) 
  : Work()
{
  //- no op. constructor. 
  //- initialization code should be placed in <MyDataProcessing::on_init>.
}

// ============================================================================
// MyDataProcessing::~MyDataProcessing
// ============================================================================
MyDataProcessing::~MyDataProcessing(void)
{
  //- release resources here
}

// ============================================================================
// MyDataProcessing::on_init
// ============================================================================
int MyDataProcessing::on_init (asl::WorkerError *&err) 
{
  //- verbose
  ACE_DEBUG((LM_DEBUG, "MyDataProcessing::on_init\n"));

  //- init <output> error message to 0
  err = 0;

  // if (error an occured) {
  //  err = new asl::WorkerError("fatal error", 1);
  //  return -1;
  // }

  return 0; // or -1 on error.
}

// ============================================================================
// MyDataProcessing::on_start
// ============================================================================
int MyDataProcessing::on_start (asl::WorkerError *&err) 
{
  //- verbose
  ACE_DEBUG((LM_DEBUG, "MyDataProcessing::on_start\n"));

  //- init <output> error message to 0
  err = 0;

  // if (error an occured) {
  //  err = new asl::WorkerError("fatal error", 1);
  //  return -1;
  // }

  return 0; // or -1 on error.
}

// ============================================================================
// MyDataProcessing::on_reset
// ============================================================================
int MyDataProcessing::on_reset (asl::WorkerError *&err) 
{
  //- verbose
  ACE_DEBUG((LM_DEBUG, "MyDataProcessing::on_reset\n"));

  //- init <output> error message to 0
  err = 0;

  // if (error an occured) {
  //  err = new asl::WorkerError("fatal error", 1);
  //  return -1;
  // }

  return 0; // or -1 on error.
}

// ============================================================================
// MyDataProcessing::on_stop
// ============================================================================
int MyDataProcessing::on_stop (asl::WorkerError *&err)   
{
  //- verbose
  ACE_DEBUG((LM_DEBUG, "MyDataProcessing::on_stop\n"));

  //- init <output> error message to 0
  err = 0;

  // if (error an occured) {
  //  err = new asl::WorkerError("fatal error", 1);
  //  return -1;
  // }

  return 0; // or -1 on error.
}

// ============================================================================
// MyDataProcessing::on_abort 
// ============================================================================
int MyDataProcessing::on_abort (asl::WorkerError *&err) 
{
  //- verbose
  ACE_DEBUG((LM_DEBUG, "MyDataProcessing::on_abort\n"));

  //- init <output> error message to 0
  err = 0;

  // if (error an occured) {
  //  err = new asl::WorkerError("fatal error", 1);
  //  return -1;
  // }

  return 0; // or -1 on error.
}

// ============================================================================
// MyDataProcessing::on_quit
// ============================================================================
void MyDataProcessing::on_quit (void)
{
  //- verbose
  ACE_DEBUG((LM_DEBUG, "MyDataProcessing::on_quit\n"));

  //...
}

// ============================================================================
// MyDataProcessing::process
// ============================================================================
int MyDataProcessing::process (asl::Message *in, asl::WorkerError *&err)
{
  //- init return value
  int result = 0;
  //- init <out> error message  
  err = 0;
  
  // Filter message... 
  //-------------------------
  switch (in->msg_type())
  {
    // <in> is a <DATA> message.
    //-------------------------
    case asl::MessageType::MSG_DATA:
    {
      //- verbose
      ACE_DEBUG((LM_DEBUG, "MyDataProcessing::process - MSG_DATA received\n"));  

      asl::Buffer<int>* data;
      //- get message content
      if (asl::message_data(in, data) == -1) {
        err =  new asl::WorkerError("unexpected data", 1);
        in->release();
        result = -1;
        break;
      }

      //- process data...
      unsigned long cont = 0;
      long average ;
      do {
        //- compute buffer average 
        average = 0;
        for (int i = 0; i < data->depth(); i++) {
          average += (*data)[i];
        }
        average /= data->depth();
        //- print out result
        ACE_DEBUG((LM_DEBUG, "average: %d\n", average));  
        //- get next data in continuation field
        data = ACE_reinterpret_cast(asl::Buffer<int>*, data->cont());
        //- 
        cont++;
      } while (data);

      //- release input message to avoid memory leaks (also release data)
      in->release();
    }
    break;

    // <in> is a <HELLO_WORLD_MSG> message.
    //-------------------------------------
    case HELLO_WORLD_MSG:
    {
      asl::Container<std::string>* c;
      if (asl::message_data(in, c) == -1) {
        err = new asl::WorkerError("unexpected data", 1);
        result = -1;
      }
      else {
        ACE_DEBUG((LM_DEBUG, "HELLO_WORLD_MSG content: %s\n", c->content().c_str()));  
      }
      in->release();
    }
    break;

    // <in> has a type we do not handle
    //---------------------------------
    default:
      //- release input message to avoid memory leaks
      in->release();
      break;
  }

  return result;
}

