// ============================================================================
//
// = CONTEXT
//   asl::ContinuousAI and Single ShotDIO class test
//
// = FILENAME
//   main.cpp
//
// = AUTHORS
//   GA & NL �- Synchrotron SOLEIL
//
// ============================================================================
#include <ace/ACE.h>
#include <asl/ContinuousAI.h>
#include <asl/SingleShotDI.h>
#include <asl/SingleShotDO.h>
#include <asl/Thread.h>
//------------------------------------------------------------------------------
//---- A MACRO TO PRINTOUT INSTANCE COUNTERS (DEBUG ONLY)
//------------------------------------------------------------------------------
#if defined(ASL_DEBUG)
#define PRINT_OUT_INSTANCE_COUNTER(X) \
ACE_DEBUG((LM_DEBUG, #X "::ic::%u\n", X::instance_counter()))  
#endif

//------------------------------------------------------------------------------
//---- USER DEFINED CONTINUOUS AI DAQ: SUPPORT THE ADLINK 2000 SERIE AI BOARDS
//------------------------------------------------------------------------------
class ADLinkContinuousAI : public asl::ContinuousAI
{
public: 
	//----------------------------------------------
	//---- CTOR
	//----------------------------------------------
	ADLinkContinuousAI (void) 
		: asl::ContinuousAI(),
        data_counter(0),
        tmo_counter(0),
        overrun_counter(0)
	{
		
	};
	
	//----------------------------------------------
	//---- DTOR
	//----------------------------------------------
	virtual ~ADLinkContinuousAI (void) 
	{ 
		//- noop
	};
	
	//----------------------------------------------
	//---- DATA PROCESSING
	//----------------------------------------------
	virtual void handle_input (asl::AIRawData* raw_data)
	{
		data_counter++;
		
		
		ACE_DEBUG((LM_DEBUG, "\n- ADLinkContinuousAI::handle_input ---------------\n"));
		
		if (raw_data) {
			//ACE_DEBUG((LM_DEBUG, "\t-raw data.............%x\n", raw_data));
			//ACE_DEBUG((LM_DEBUG, "\t-raw data base @......%x\n", raw_data->base()));
			//ACE_DEBUG((LM_DEBUG, "\t-raw data depth.......%d\n", raw_data->depth())); 
			ACE_DEBUG((LM_DEBUG, "\t-raw data 0......%x\n", *(raw_data->base())));
		}
		else {
			ACE_DEBUG((LM_DEBUG, "\t-raw data..........none\n"));
			return;
		}
		
		
		asl::AIScaledData * scaled_data = 0;
		
		try 
		{
			scaled_data = this->scale_data(raw_data);
		}
		catch (asl::DAQException&) {
			ACE_DEBUG((LM_DEBUG, "exception thrown from ContinousAI::scale_data\n"));
			delete raw_data;
			return;
		}
		
		
		//ACE_DEBUG((LM_DEBUG, "\t-scaled data..........%x\n", scaled_data));
		//ACE_DEBUG((LM_DEBUG, "\t-scaled data base @...%x\n", scaled_data->base()));
		//ACE_DEBUG((LM_DEBUG, "\t-scaled data depth....%d\n", scaled_data->depth()));
		ACE_DEBUG((LM_DEBUG, "\t-scaled data 0....%f\n", *(scaled_data->base())));
		
		/*
		double average = 0;
		for (int i = 0; i < scaled_data->depth(); i++) {
		if (scaled_data->depth() < 20) {
        ACE_DEBUG((LM_DEBUG, "\t-scaled data [%d]...%.4fV\n", i, *(scaled_data->base() + i)));
		}
		average += *(scaled_data->base() + i);
		}
		average /= scaled_data->depth();
		
		  ACE_DEBUG((LM_DEBUG, "\t-scaled data av...%.4fV\n", average));
		*/
		//- release data to avoid memory leak
		delete scaled_data;
		
		//- release data to avoid memory leak
		delete raw_data;
	}
	
	//----------------------------------------------
	//---- DAQ ERROR HANDLER
	//----------------------------------------------
	virtual void handle_error (const asl::DAQException& de)
	{
		ACE_DEBUG((LM_DEBUG, "\n- ADLinkContinuousAI::handle_error -------------\n"));
		
		for (int i = 0; i < de.errors.size(); i++) 
		{
			ACE_DEBUG((LM_DEBUG, "\t. reason.....%s\n", de.errors[i].reason.c_str())); 
			ACE_DEBUG((LM_DEBUG, "\t. desc.......%s\n", de.errors[i].desc.c_str())); 
			ACE_DEBUG((LM_DEBUG, "\t. origin.....%s\n", de.errors[i].origin.c_str())); 
			ACE_DEBUG((LM_DEBUG, "\t. code.......%d\n", de.errors[i].code));
			ACE_DEBUG((LM_DEBUG, "\t. severity...%d\n", de.errors[i].severity)); 
			ACE_DEBUG((LM_DEBUG, "\n")); 
		}
	}
	
	//----------------------------------------------
	//---- OVERRUN HANDLER
	//----------------------------------------------
	virtual void handle_overrun (void)
	{
		overrun_counter++;
		ACE_DEBUG((LM_DEBUG, "\n- ADLinkContinuousAI::handle_overrun -------------\n"));
		ACE_DEBUG((LM_DEBUG, "\n"));
		ACE_DEBUG((LM_DEBUG, "\t ********** OVERRUN #%d **********\n", overrun_counter));
		ACE_DEBUG((LM_DEBUG, "\n"));
	}
	
	//----------------------------------------------
	//---- TIMEOUT HANDLER
	//----------------------------------------------
	virtual void handle_timeout (void)
	{
		tmo_counter++;
		ACE_DEBUG((LM_DEBUG, "\n- ADLinkContinuousAI::handle_timeout -------------\n"));
		ACE_DEBUG((LM_DEBUG, "\n"));
		ACE_DEBUG((LM_DEBUG, "\t ********** TIMEOUT #%d **********\n", tmo_counter));
		ACE_DEBUG((LM_DEBUG, "\n"));
	}
	
	//-----------------------------------------------
	// stat: number of data buffers received
	//-----------------------------------------------
	unsigned long data_counter;
	
	//-----------------------------------------------
	// stat: number of timeout notifications received
	//-----------------------------------------------
	unsigned long tmo_counter;
	
	//-----------------------------------------------
	// stat: number of overrun notifications received
	//-----------------------------------------------
	unsigned long overrun_counter;
	
}; //- end of class ADLinkContinuousAI
// ============================================================================
// Thread dio
// ============================================================================
class ThreadDIO : public asl::Thread
{
public:
	
	ThreadDIO ();
	
	virtual ~ThreadDIO ();
	
protected:
	
	virtual ACE_THR_FUNC_RETURN svc (void* arg);
};
//-----------------------------------------------------------------------------  
ThreadDIO::ThreadDIO () 
: asl::Thread()
{
	//-noop ctor
}
//-----------------------------------------------------------------------------  
ThreadDIO::~ThreadDIO ()
{
	//-noop ctor
}
//----------------------------------------------------------------------------- 
ACE_THR_FUNC_RETURN ThreadDIO::svc (void* arg)
{
	//- verbose
	ACE_DEBUG((LM_DEBUG, "Entering ThreadDIO::svc (arg:%x)\n", arg));
	
	asl::SingleShotDI* di_2005 = new asl::SingleShotDI();
	asl::SingleShotDO* do_2005 = new asl::SingleShotDO();
	
	bool state = false;
	unsigned long value;
	//- enter almost infinite loop
	try 
	{   
		di_2005->init(adl::DAQ2005, 0);
		do_2005->init(adl::DAQ2005, 0);
		
		di_2005->configure_port(adl::port_1a);
		do_2005->configure_port(adl::port_1b);
		
		do 
		{
			ACE_OS::sleep(ACE_Time_Value(0, 100000)); 
			value = di_2005->read_port(adl::port_1a);
			ACE_DEBUG((LM_DEBUG, "Single Shot DI state: %d\n", value));
			do_2005->write_port(adl::port_1b, value);
		}
		while (quit_requested() == false);
	}
	catch (const asl::DAQException& de) 
	{
		ACE_DEBUG((LM_DEBUG, "asl::DAQException caught\n")); 
		for (unsigned int i = 0; i < de.errors.size(); i++) 
		{
			ACE_DEBUG((LM_DEBUG, "- reason.....%s\n", de.errors[i].reason.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- desc.......%s\n", de.errors[i].desc.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- origin.....%s\n", de.errors[i].origin.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- code.......%d\n", de.errors[i].code));
			ACE_DEBUG((LM_DEBUG, "- severity...%d\n", de.errors[i].severity)); 
		}
		
	}
	catch (...) 
	{
		ACE_DEBUG((LM_DEBUG, "unknown exception caught\n"));
	}
	
	if (di_2005) delete di_2005;
	if (do_2005) delete do_2005;
	
	//- verbose
	//ACE_DEBUG((LM_DEBUG, "Leaving ThreadDI::svc\n"));
	
	//- return result (here just return <arg> back)
	return ACE_static_cast(ACE_THR_FUNC_RETURN, arg);
}
//------------------------------------------------------------------------------
//---- MAIN
//------------------------------------------------------------------------------
int main (int, char**) 
{
	//- select board to use (supported hw: adl::DAQ2005, adl::DAQ2010) 
	unsigned short board_type = adl::DAQ2005; 
	
	//- select board id 
	unsigned short board_id = 0; 
	
	for (int i = 0; i < 1; i++) 
	{
		//- a continuous analog input daq.
		ADLinkContinuousAI * ai = new ADLinkContinuousAI;
		if (ai == 0) {
			ACE_DEBUG((LM_DEBUG, "out of memory\n"));
			break;
		}
		
		// create a dio  2005 card
		ThreadDIO * t = new ThreadDIO();
		if (t == 0)
		{
			//- out of memory error
			return 0;
		}
		
		try 
		{   
			//--------------------------------------------------------
			//---- CONFIGURATION -------------------------------------
			//--------------------------------------------------------
			//- the continuous analog input DAQ configuration
			asl::ContinuousAIConfig config;
			
			//--------------------------------------------------------
			//---- ACTIVE CHANNELS CONFIG EXAMPLE --------------------
			//--------------------------------------------------------
			//- activate AI channels (board dependent)
			asl::ActiveAIChannel ac;
			//- configure/activate channel 0
			ac.id = 0;
			ac.range = adl::bp_10;
			config.add_active_channel(ac);
			//- configure/activate channel 1
			// ac.id = 1;
			// ac.range = adl::bp_10;
			// config.add_active_channel(ac);
			//- configure/activate channel 2
			// ac.id = 2;
			// ac.range = adl::bp_10;
			// config.add_active_channel(ac);
			//- configure/activate channel 3
			// ac.id = 3;
			// ac.range = adl::bp_10;
			// config.add_active_channel(ac);
			
			//--------------------------------------------------------
			//---- INFINITE RETRIGGER CONFIG EXAMPLE -----------------
			//--------------------------------------------------------
			/*       //- enable infinite retrigger
			config.enable_retrigger();
			//- set sampling rate (valid range is board dependent)
			config.set_sampling_rate(500000);
			//- set DAQ buffer depth in samples/channel (sampling_rate dependent)
			config.set_buffer_depth(10000);
			//- set trigger mode
			config.set_trigger_mode(adl::ai_delay);
			//- set delay unit 
			config.set_delay_unit(adl::clock_ticks);
			//- set delay value in micro-secs (delay unit is clock_ticks)
			config.set_middle_or_delay_scans(25);
			//- set trigger source
			config.set_trigger_source(adl::external_digital);
			//- set overrun strategy (just notify)
			config.set_data_lost_strategy(adl::notify);
			//- set timeout (in seconds)
			config.set_timeout(1000);
			*/
			//--------------------------------------------------------
			//---- DOUBLE BUFFERING CONFIG EXAMPLE -------------------
			//--------------------------------------------------------
			
			//- set sampling rate (range is board dependent)
			config.set_sampling_rate(500000);
			//- set DAQ buffer depth in samples/channel (sampling_rate dependent)
			config.set_buffer_depth(50000); 
			//- set overrun strategy (auto restart DAQ then notify)
			config.set_data_lost_strategy(adl::notify);
			//- set timeout (in seconds)
			config.set_timeout(1000);
			
			//--------------------------------------------------------
			//---- INIT/CONFIGURE/START/STOP DAQ ---------------------
			//--------------------------------------------------------
			//- initialize (may throw an exception)
			ai->init(board_type, board_id);
			//- configure (may throw an exception) 
			ai->configure(config); 
			
			//- run the thread di
			if (t->run() == -1) 
			{
				ACE_DEBUG((LM_DEBUG, "Thread::run failed\n"));  
				return 0;
			}
			//- start async. continuous AI operation (may throw an exception) 
			ai->start(); 
			//- let the DAQ run 
			ACE_OS::sleep(5); 
			//- stop async. continuous AI operation (may throw an exception) 
			ai->stop();
			//- ask the thread to quit
			if (t->quit_and_join() == -1)
			{
				ACE_DEBUG((LM_DEBUG, "Thread::quit failed\n"));  
				return 0;
			}
			//- printout some stats
			ACE_DEBUG((LM_DEBUG, "- DAQ-Stats: data counter......%d\n", ai->data_counter)); 
			ACE_DEBUG((LM_DEBUG, "- DAQ-Stats: overrun counter...%d\n", ai->overrun_counter));
			ACE_DEBUG((LM_DEBUG, "- DAQ-Stats: timeout counter...%d\n", ai->tmo_counter)); 
			//- dump DAQ info.
			ai->dump();
		}
		catch (const asl::DAQException& de) 
		{
			ACE_DEBUG((LM_DEBUG, "asl::DAQException caught\n")); 
			for (unsigned int i = 0; i < de.errors.size(); i++) 
			{
				ACE_DEBUG((LM_DEBUG, "- reason.....%s\n", de.errors[i].reason.c_str())); 
				ACE_DEBUG((LM_DEBUG, "- desc.......%s\n", de.errors[i].desc.c_str())); 
				ACE_DEBUG((LM_DEBUG, "- origin.....%s\n", de.errors[i].origin.c_str())); 
				ACE_DEBUG((LM_DEBUG, "- code.......%d\n", de.errors[i].code));
				ACE_DEBUG((LM_DEBUG, "- severity...%d\n", de.errors[i].severity)); 
			}
			
			break;
		}
		catch (...) 
		{
			ACE_DEBUG((LM_DEBUG, "unknown exception caught\n"));  
			
			break;
		}
		
		if (ai) delete ai;
		if (t) delete t;
		
  } // for
  
  //- any memory leaks?
#if defined(ASL_DEBUG)
#if defined(_SIMULATION_)
  ACE_DEBUG((LM_DEBUG, "Memory leak counter: %d (expected 1 - PulsedTaskManager still alive)\n", MLD_COUNTER));
#else
  ACE_DEBUG((LM_DEBUG, "Memory leak counter: %d (should be 0)\n", MLD_COUNTER));
#endif
  if (MLD_COUNTER) {
	  PRINT_OUT_INSTANCE_COUNTER(asl::Data);
	  PRINT_OUT_INSTANCE_COUNTER(asl::Message);
  }
#endif
  
  return 0;
} 
