//===================================================================
// DEPENDENCIES
//===================================================================
#include <ADLinkContinuousAI.h>

//===================================================================
// ADLinkContinuousAI::ADLinkContinuousAI
//===================================================================
ADLinkContinuousAI::ADLinkContinuousAI (Tango::DeviceImpl *dev)
  : Tango::LogAdapter(dev),
    data_counter (0),
    tmo_counter (0),
    error_counter (0),
    overrun_counter (0),
    ict1_current (0.),
    ict2_current (0.),
		daq_end_evt_received(false)
{

}

//===================================================================
// ADLinkContinuousAI::~ADLinkContinuousAI
//===================================================================
ADLinkContinuousAI::~ADLinkContinuousAI (void) 
{ 
  //- noop
}

//===================================================================
// ADLinkContinuousAI::start
//===================================================================
void ADLinkContinuousAI::start (void) throw (asl::DAQException)
{
	//- reset local data
	data_counter = 0;
	tmo_counter = 0;
	overrun_counter = 0;
	daq_end_evt_received = false;
	//- call mother class start method (may throw asl::DAQException)
	this->ContinuousAI::start();
}
	
//===================================================================
// ADLinkContinuousAI::handle_input
//===================================================================
void ADLinkContinuousAI::handle_input (asl::AIRawData* raw_data)
{
  data_counter++;

  DEBUG_STREAM << "::: ADLinkContinuousAI::handle_input :::" << std::endl;

  asl::AIScaledData * scaled_data = 0;

  try {
    scaled_data = this->scale_data(raw_data);
  }
  catch (asl::DAQException& de) {
    ERROR_STREAM << this->daq_to_tango_exception(de) << endl;
    delete raw_data;
    return;
  }

  double average_ict1 = 0;
  double average_ict2 = 0;

  for (int sd = 0; sd < scaled_data->depth(); sd += 2) 
  {
/*
     DEBUG_STREAM << "\t-scaled data [" 
                   << sd 
                   << "] = " 
                   << *(scaled_data->base() + sd) 
                   << "V" 
                   << std::endl;

     DEBUG_STREAM << "\t-scaled data [" 
                   << sd + 1
                   << "] = " 
                   << *(scaled_data->base() + sd + 1) 
                   << "V" 
                   << std::endl;
*/
    average_ict1 += *(scaled_data->base() + sd);
    average_ict2 += *(scaled_data->base() + sd + 1);
  }
	
  average_ict1 /= scaled_data->depth() / 2;
  average_ict2 /= scaled_data->depth() / 2;

	DEBUG_STREAM << "\tADLinkContinuousAI::average-1::" << average_ict1 << " Volts" << std::endl;
	DEBUG_STREAM << "\tADLinkContinuousAI::average-2::" << average_ict2 << " Volts" << std::endl;
	
  //- store ict currents
  this->lock_data();
    this->ict1_current = average_ict1;
    this->ict2_current = average_ict2;
  this->unlock_data();

  //- release data to avoid memory leak
  delete scaled_data;

  //- release data to avoid memory leak
  delete raw_data;
}

//===================================================================
// ADLinkContinuousAI::handle_daq_end
//===================================================================
void  ADLinkContinuousAI::handle_daq_end (asl::ContinuousDAQ::DaqEndReason why)
{
	DEBUG_STREAM << "::: ADLinkContinuousAI::handle_daq_end :::" << std::endl; 

	DEBUG_STREAM << std::endl; 

	daq_end_evt_received = true;
	
	switch (why)
	{
		case ContinuousDAQ::DAQEND_ON_USER_REQUEST:
			DEBUG_STREAM << "\treason: user request" << std::endl; 
			break;
		case ContinuousDAQ::DAQEND_ON_EXTERNAL_TRIGGER:
			DEBUG_STREAM << "\treason: digital external trigger raised (pre-trigger mode)" << std::endl; 
			DEBUG_STREAM << "\treason: history buffer frozen" << std::endl; 
			break;
		case ContinuousDAQ::DAQEND_ON_ERROR:
			DEBUG_STREAM << "\treason: daq error" << std::endl; 
			break;		
		case ContinuousDAQ::DAQEND_ON_OVERRUN:
			DEBUG_STREAM << "\treason: daq buffer overrun" << std::endl; 
			DEBUG_STREAM << "\treason: daq has been <aborted> as required by the specified overrun strategy" << std::endl; 
			break;
		case ContinuousDAQ::DAQEND_ON_UNKNOWN_EVT:
		default:
		  DEBUG_STREAM << "\treason: unknown" << std::endl; 
			break;
	}

	DEBUG_STREAM << std::endl; 

	//- get history history (i.e. post-mortem) buffer
	if (why == DAQEND_ON_EXTERNAL_TRIGGER && this->configuration().history_enabled())
	{
		asl::AIScaledData * scaled_history = 0;
		try 
		{
			//- get history as scaled data
			scaled_history = this->scaled_history();
			DEBUG_STREAM << "\thistory buffer depth...." << scaled_history->depth() << " samples" << std::endl; 
			DEBUG_STREAM << "\thistory buffer depth...." << scaled_history->depth() / this->configuration().get_buffer_depth() << " daq-buffers"  << std::endl; 
			DEBUG_STREAM << "\thistory buffer length..." << this->configuration().get_history_length() << " msec" << std::endl;
		}
		catch (const asl::DAQException& de) 
		{
			DEBUG_STREAM << "asl::DAQException caught while trying to get history buffer" << std::endl;  
			for (unsigned int i = 0; i < de.errors.size(); i++) 
			{
				DEBUG_STREAM << "- reason....." << de.errors[i].reason.c_str() << std::endl; 
				DEBUG_STREAM << "- desc......." << de.errors[i].desc.c_str() << std::endl;
				DEBUG_STREAM << "- origin....." << de.errors[i].origin.c_str() << std::endl;
				DEBUG_STREAM << "- code......." << de.errors[i].code << std::endl;
				DEBUG_STREAM << "- severity..." << de.errors[i].severity << std::endl; 
			}
		}
		catch (...)  
		{
			DEBUG_STREAM << "Unknown exception caught while trying to get history buffer" << std::endl;  
		}

		//- release history to avois memory leak
		if (scaled_history) delete scaled_history;

		DEBUG_STREAM << std::endl; 
  }
}
	
//===================================================================
// ADLinkContinuousAI::handle_error
//===================================================================
void ADLinkContinuousAI::handle_error (const asl::DAQException& de)
{
  error_counter++;

  DEBUG_STREAM << "::: ADLinkContinuousAI::handle_error :::" << std::endl;

  ERROR_STREAM << this->daq_to_tango_exception(de) << endl;
}

//===================================================================
// ADLinkContinuousAI::handle_data_lost
//===================================================================
void ADLinkContinuousAI::handle_data_lost (void)
{
  overrun_counter++;

  DEBUG_STREAM << "::: ADLinkContinuousAI::handle_error :::" << std::endl;

	DEBUG_STREAM << std::endl; 

	DEBUG_STREAM << "\toverrun notification #" <<  overrun_counter << std::endl;
	
  DEBUG_STREAM << std::endl; 
}

//===================================================================
// ADLinkContinuousAI::handle_timeout
//===================================================================
void ADLinkContinuousAI::handle_timeout (void)
{
  tmo_counter++;

  DEBUG_STREAM << "::: ADLinkContinuousAI::handle_timeout :::" << std::endl; 
  
  DEBUG_STREAM << std::endl; 
  
  DEBUG_STREAM << "\ttimeout notification #" <<  tmo_counter << std::endl;
  
  DEBUG_STREAM << std::endl; 

	DEBUG_STREAM << "\treason: the DAQ internal processing task did not receive data since " 
               << this->configuration().get_timeout()
               << " msec" 
               << std::endl; 

	DEBUG_STREAM << "\treason: this is the timeout specified int the DAQ configuration" << std::endl; 

  //- infinite retrigger mode
  if (this->configuration().retrigger_enabled()) 
  {
	  DEBUG_STREAM << "\treason: trigger signal is either off or disconnected" << std::endl; 
  }
  //- pre-triggger mode
  else if (daq_end_evt_received == true) 
  {
	  DEBUG_STREAM << "\treason: DAQ has been stopped by external trigger" << std::endl; 
	  DEBUG_STREAM << "\treason: in this case the timeout event can be ignored" << std::endl; 
  }
  //- remaining cases
  else 
  {
	  DEBUG_STREAM << "\treason: may be due to a DAQ configuration problem" << std::endl; 
	  DEBUG_STREAM << "\treason: the DAQ buffer depth may be too large for the selected sampling rate" << std::endl; 
	  DEBUG_STREAM << "\treason: the timeout delay may also be too short compared to the time required to fill a daq buffer" << std::endl; 
	  DEBUG_STREAM << "\treason: try to increase either the timeout delay or sampling rate" << std::endl; 
  }

  DEBUG_STREAM << std::endl; 
}

//-------------------------------------------------------------------
// ADLinkContinuousAI::daq_to_tango_exception
//-------------------------------------------------------------------
Tango::DevFailed ADLinkContinuousAI::daq_to_tango_exception (const asl::DAQException& de)
{
  Tango::DevErrorList error_list(de.errors.size());

  error_list.length(de.errors.size());

  for (int i = 0; i < de.errors.size(); i++) 
  {
    error_list[i].reason = CORBA::string_dup(de.errors[i].reason.c_str());
    error_list[i].desc   = CORBA::string_dup(de.errors[i].desc.c_str());
    error_list[i].origin = CORBA::string_dup(de.errors[i].origin.c_str());
    switch (de.errors[i].severity) 
    {
      case asl::WARN:
        error_list[i].severity = Tango::WARN;
        break;
      case asl::PANIC:
        error_list[i].severity = Tango::PANIC;
        break;
      case asl::ERR:
      default:
        error_list[i].severity = Tango::ERR;
        break;
    }
  }

  return Tango::DevFailed(error_list);
}
