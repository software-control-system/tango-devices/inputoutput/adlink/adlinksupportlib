static const char *RcsId = "$Header: /segfs/tango/tmp/tango-ds.new/cvsroot/InputOutput/ADLINK/AdlinkSupportLib/test/ContinuousAI_TangoDevice/ContinuousAIDev.cpp,v 1.3 2013-02-25 19:24:39 buteau Exp $";
//+=============================================================================
//
// file :	  ContinuousAIDev.cpp
//
// description :  C++ source for the ContinuousAIDev and its commands. 
//		  The class is derived from Device. It represents the
//		  CORBA servant object which will be accessed from the
//		  network. All commands which can be executed on the
//		  ContinuousAIDev are implemented in this file.
//
// project :	  TANGO Device Server
//
// $Author: buteau $
//
// $Revision: 1.3 $
//
// $Log: not supported by cvs2svn $
// Revision 1.2  2008/03/06 14:35:15  nleclercq
// Added support for the 2205 AI board
//
// Revision 1.1.1.1  2004/11/08 16:34:13  syldup
// initial import
//
//
// copyleft :	  European Synchrotron Radiation Facility
//		  BP 220, Grenoble 38043
//		  FRANCE
//
//-=============================================================================
//
//		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//	   (c) - Software Engineering Group - ESRF
//=============================================================================


//===================================================================
//
//	The folowing table gives the correspondance
//	between commands and method's name.
//
//	Command's name	|	Method's name
//	----------------------------------------
//	State	|	dev_state()
//	Status	|	dev_status()
//	Start	|	start()
//	Stop	|	stop()
//
//===================================================================
#include <ADLinkContinuousAI.h>
#include <ContinuousAIDev.h>

//===================================================================
// DEVICE PROPERTIES DEFAULT VALUES
//===================================================================
#define kDEFAULT_BOARD_TYPE_STR (const char*)"SAI_2005"
#define kDEFAULT_BOARD_TYPE_ID  adl::DAQ2005
#define kDEFAULT_BOARD_ID       0
#define kMAX_CPCI_BOARD         7
#define kDEFAULT_SAMPLING_RATE  500000.
#define kDEFAULT_BUFFER_DEPTH   10000
#define kDEFAULT_CHANNEL_1      0
#define kDEFAULT_CHANNEL_2      1
#define kDEFAULT_NSEC_DELAY     0.
#define kMIN_DAQ_TIMEOUT    	  10
#define kDEFAULT_DAQ_TIMEOUT    30000

#if defined (__linux)
# define USE_LINUX_WORKAROUND_FOR_ASL_DEVICES
#endif

namespace ContinuousAIDev
{
//+----------------------------------------------------------------------------
//
// method :		ContinuousAIDev::ContinuousAIDev(string &s)
//
// description :	constructor for simulated ContinuousAIDev
//

// in : - cl : Pointer to the DeviceClass object
//	- s : Device name
//
//-----------------------------------------------------------------------------
ContinuousAIDev::ContinuousAIDev(Tango::DeviceClass *cl,string &s)
  : Tango::Device_2Impl(cl,s.c_str())
{
	init_device();
}

ContinuousAIDev::ContinuousAIDev(Tango::DeviceClass *cl,const char *s)
  : Tango::Device_2Impl(cl,s)
{
	init_device();
}

ContinuousAIDev::ContinuousAIDev(Tango::DeviceClass *cl,const char *s,const char *d)
  : Tango::Device_2Impl(cl,s,d)
{
	init_device();
}

ContinuousAIDev::~ContinuousAIDev(void)
{
  DEBUG_STREAM << "ContinuousAIDev::~ContinuousAIDev <-" << std::endl;
  delete_device();
  DEBUG_STREAM << "ContinuousAIDev::~ContinuousAIDev ->" << std::endl;
}

//+----------------------------------------------------------------------------
//
// method :		ContinuousAIDev::init_device()
//
// description :	will be called at device initialization.
//
//-----------------------------------------------------------------------------
void ContinuousAIDev::init_device()
{
  static unsigned long init_counter = 0;
		
  //- verbose
  INFO_STREAM << "ContinuousAIDev::init device "
              << device_name
              << " ["
              << init_counter
              << "]"
              << endl;
												
  //- update internal state
  this->set_state(Tango::UNKNOWN);

  //- first device initialization:
  //- properly initialize members to their default value
  if (init_counter == 0)  {
    daq = 0;
    this->board_type = kDEFAULT_BOARD_TYPE_ID;
    this->board_id = kDEFAULT_BOARD_ID;
    this->sampling_rate = kDEFAULT_SAMPLING_RATE;
    this->buffer_depth = kDEFAULT_BUFFER_DEPTH;
    this->nsec_delay = kDEFAULT_NSEC_DELAY;
    daq_timeout = kDEFAULT_DAQ_TIMEOUT;
  }

#if defined(USE_LINUX_WORKAROUND_FOR_ASL_DEVICES)
if (init_counter == 0) 
{
#endif

  //- instanciate the daq
  daq = new ADLinkContinuousAI(this);
  if (daq == 0) {
    Tango::Except::throw_exception((const char *)"out of memory",
                                   (const char *)"out of memory error",
                                   (const char *)"ContinuousAIDev::init_device");
  }
	
#if defined(USE_LINUX_WORKAROUND_FOR_ASL_DEVICES)
}
#endif

  //- read device properties from tango database
  this->get_device_property();

  //- the DAQ configuration
  asl::ContinuousAIConfig daq_config;

  //--------------------------------------------------------
  //---- ACTIVE CHANNELS CONFIG EXAMPLE --------------------
  //--------------------------------------------------------
    //- activate AI channels (board dependent)
    asl::ActiveAIChannel ac;
    //- configure/activate channel 0
    ac.id = 0;
    ac.range = adl::bp_10;
    daq_config.add_active_channel(ac);
		
    //- configure/activate channel 1
    ac.id = 1;
    ac.range = adl::bp_10;
    daq_config.add_active_channel(ac);

  //--------------------------------------------------------
  //---- INFINITE RETRIGGER CONFIG EXAMPLE -----------------
  //--------------------------------------------------------
/*
    //- enable infinite retrigger
    daq_config.enable_retrigger();
    //- set sampling rate (valid range is board dependent)
    daq_config.set_sampling_rate(this->sampling_rate);
    //- set DAQ buffer depth in samples/channel (sampling_rate dependent)
    daq_config.set_buffer_depth(this->buffer_depth);
    //- set trigger mode
    daq_config.set_trigger_mode(adl::ai_delay);
    //- set delay unit
    daq_config.set_delay_unit(adl::clock_ticks);
    //- set delay value in nano-secs (delay unit is clock_ticks)
    daq_config.set_middle_or_delay_scans(1000);
    //- set trigger source
    daq_config.set_trigger_source(adl::external_digital);
    //- set overrun strategy (just notify)
    daq_config.set_data_lost_strategy(adl::ignore);
    //- set timeout (in msec)
    daq_config.set_timeout(this->daq_timeout);
*/

  //--------------------------------------------------------
  //---- DOUBLE BUFFERING CONFIG EXAMPLE -------------------
  //--------------------------------------------------------
    //- set sampling rate (range is board dependent)
    daq_config.set_sampling_rate(this->sampling_rate);
    //- set DAQ buffer depth in samples/channel (sampling_rate dependent)
    daq_config.set_buffer_depth(this->buffer_depth);
    //- set overrun strategy 
    daq_config.set_data_lost_strategy(adl::ignore);
    //- set timeout (in seconds)
    daq_config.set_timeout(this->daq_timeout);

  try
  {
    DEBUG_STREAM << "ContinuousAIDev::init_device::init DAQ <-" << std::endl;
    //- initialize daq
    daq->init(this->board_type, this->board_id);
    DEBUG_STREAM << "ContinuousAIDev::init_device::init DAQ ->" << std::endl;
    
    DEBUG_STREAM << "ContinuousAIDev::init_device::conf DAQ <-" << std::endl;
    //- configure daq
    daq->configure(daq_config);
    DEBUG_STREAM << "ContinuousAIDev::init_device::conf DAQ ->" << std::endl;
    
    DEBUG_STREAM << "ContinuousAIDev::init_device::start DAQ <-" << std::endl;
    //- start daq
    daq->start();
    DEBUG_STREAM << "ContinuousAIDev::init_device::start DAQ ->" << std::endl;
    
    //- update internal state
    this->set_internal_state();
  }
  catch (const asl::DAQException& de)
  {
	  //- verbose
    for (unsigned int i = 0; i < de.errors.size(); i++) 
			{
				DEBUG_STREAM << "- reason....." << de.errors[i].reason.c_str() << std::endl; 
				DEBUG_STREAM << "- desc......." << de.errors[i].desc.c_str() << std::endl;
				DEBUG_STREAM << "- origin....." << de.errors[i].origin.c_str() << std::endl;
				DEBUG_STREAM << "- code......." << de.errors[i].code << std::endl;
				DEBUG_STREAM << "- severity..." << de.errors[i].severity << std::endl; 
			}
    //- update internal state
    this->set_internal_state();
    //- convert daq to tango error
    Tango::DevFailed df = daq->daq_to_tango_exception(de);
    //- verbose
    ERROR_STREAM << "ContinuousAIDev::init_device::DAQException caught" << std::endl;
    //-TODO: fix TANGO bug ERROR_STREAM << df << std::endl;
    //- delete all
    this->delete_device();
    //-TODO: can we throw an exception from init_device?
#if defined(CAN_THROW_EXCEPTION_FROM_INIT_DEVICE)
    //- rethrow exception
    Tango::Except::re_throw_exception(df,
            static_cast<const char*>("device initialization failed"),
				    static_cast<const char*>("caugth asl::DAQException"),
				    static_cast<const char*>("ContinuousAIDev::init_device"));
#endif
  }
  catch (...)
  {
    //- update internal state
    this->set_internal_state();
    ERROR_STREAM << "ContinuousAIDev::init_device::unknown exception caught" << std::endl;
    //- delete all
    this->delete_device();
    //-TODO: can we throw an exception from init_device?
#if defined(CAN_THROW_EXCEPTION_FROM_INIT_DEVICE)
    //- throw exception
    Tango::Except::throw_exception((const char *)"device initialization failed",
                                   (const char *)"DAQ initialization failed",
                                   (const char *)"ContinuousAIDev::init_device");
#endif
  }
	//- init counter
	init_counter++;
}

//+----------------------------------------------------------------------------
//
// method :		ContinuousAIDev::delete_device()
//
// description :	will be called at device destruction or at init command.
//
//-----------------------------------------------------------------------------
void ContinuousAIDev::delete_device()
{
  static unsigned long delete_counter = 0;

  //- verbose
  INFO_STREAM << "ContinuousAIDev::delete device "
	      << device_name
	      << " ["
	      << ++delete_counter
	      << "]"
	      << endl;

  if (daq != 0)
  {
    try
    {
      DEBUG_STREAM << "ContinuousAIDev::delete_device::abort DAQ <-" << std::endl;
       daq->abort();
      DEBUG_STREAM << "ContinuousAIDev::delete_device::abort DAQ ->" << std::endl;
    } catch (...) {
      //- ignore any error
    }
#if !defined(USE_LINUX_WORKAROUND_FOR_ASL_DEVICES)
  //- release daq
    DEBUG_STREAM << "ContinuousAIDev::delete_device::delete DAQ <-" << std::endl;
    delete daq;
    daq = 0;
    DEBUG_STREAM << "ContinuousAIDev::delete_device::delete DAQ ->" << std::endl;
#endif
  }

}

//+----------------------------------------------------------------------------
//
// method :		ContinuousAIDev::get_device_property()
//
// description :	reads device properties from database.
//
//-----------------------------------------------------------------------------
void ContinuousAIDev::get_device_property (void)
{
  //	Initialize your default values here.
	//------------------------------------------

	//	Read class properties from database.(Automatic code generation)
	//-------------------------------------------------------------
	Tango::DbData	data;
	data.push_back(Tango::DbDatum("daq_board_type"));
	data.push_back(Tango::DbDatum("daq_board_id"));
  data.push_back(Tango::DbDatum("daq_sampling_rate"));
	data.push_back(Tango::DbDatum("daq_buffer_depth"));
	data.push_back(Tango::DbDatum("trigger_nsec_delay"));
	data.push_back(Tango::DbDatum("daq_timeout"));

  try {
	  //	Call database and extract values
	  //--------------------------------------------

    //- get properties from tango database
	  get_db_device()->get_property(data);

    //- extract <daq_board_type> ----------------------------------------------
    //-------------------------------------------------------------------------
	  if (data[0].is_empty() == false)
    {
      //- extract as a string
      std::string btype;
      data[0] >> btype;
      //- verbose
      DEBUG_STREAM << "dev-property::daq_board_type::" <<  btype << endl;
      //- convert from string to board id
      if (btype == "SAI_2005") {
        this->board_type = adl::DAQ2005;
      }
      else if (btype == "SAI_2010") {
        this->board_type = adl::DAQ2010;
      }
      else if (btype == "SAI_2205") {
        this->board_type = adl::DAQ2205;
      }
      else {
        WARN_STREAM << "dev-property <daq_board_type> is invalid. "
                    << "using default value: " << kDEFAULT_BOARD_TYPE_STR << endl;
      }
    }
    else
    {
      this->board_type = kDEFAULT_BOARD_TYPE_ID;
      WARN_STREAM << "dev-property <daq_board_type> is not set in database. "
                  << "using default value: " << kDEFAULT_BOARD_TYPE_STR << endl;
    }
    //- extract <daq_board_id> ------------------------------------------------
    //-------------------------------------------------------------------------
	  if (data[1].is_empty() == false)
    {
      //- extract as an unsigned short
      short bid;
      data[1] >> bid;
      //- verbose
      DEBUG_STREAM << "dev-property::daq_board_id::" <<  bid << endl;
      //- check value
      if (bid < 0 || bid >= kMAX_CPCI_BOARD) {
        this->board_id = kDEFAULT_BOARD_ID;
        WARN_STREAM << "dev-property <daq_board_id> is invalid. "
                    << "using default value: " << this->board_id << endl;
      }
      else {
        this->board_id = (unsigned short)bid;
      }
    }
    else
    {
      WARN_STREAM << "dev-property <daq_board_id> is not set in database. "
                  << "using default value: " << this->board_id << endl;
    }
    //- extract <daq_sampling_rate> -------------------------------------------
    //-------------------------------------------------------------------------
	  if (data[2].is_empty() == false)
    {
      //- extract as a double
      double sr;
      data[2] >> sr;
      //- verbose
      DEBUG_STREAM << "dev-property::daq_sampling_rate::" <<  sr << endl;
      //- check value
      switch (this->board_type)
      {
        //- sampling rate for board DAQ2205
        case adl::DAQ2205:
          {
            if (sr < AD2205_MIN_SRATE) {
              this->sampling_rate = AD2205_MIN_SRATE;
              WARN_STREAM << "dev-property <daq_sampling_rate> is invalid. "
                          << "using min.value: " << this->sampling_rate << " Hz" << endl;
            }
            else if (sr > AD2205_MAX_SRATE) {
              this->sampling_rate = AD2205_MAX_SRATE;
              WARN_STREAM << "dev-property <daq_sampling_rate> is invalid. "
                          << "using max.value: " << this->sampling_rate << " Hz" << endl;
            }
            else {
              this->sampling_rate = sr;
            }
          }
          break;
        //- sampling rate for board DAQ2005
        case adl::DAQ2010:
          {
            if (sr < AD2010_MIN_SRATE) {
              this->sampling_rate = AD2010_MIN_SRATE;
              WARN_STREAM << "dev-property <daq_sampling_rate> is invalid. "
                          << "using min.value: " << this->sampling_rate << " Hz" << endl;
            }
            else if (sr > AD2010_MAX_SRATE) {
              this->sampling_rate = AD2010_MAX_SRATE;
              WARN_STREAM << "dev-property <daq_sampling_rate> is invalid. "
                          << "using max.value: " << this->sampling_rate << " Hz" << endl;
            }
            else {
              this->sampling_rate = sr;
            }
          }
          break;
        //- sampling rate for board DAQ2005
        case adl::DAQ2005:
        default:
          {
            if (sr < AD2005_MIN_SRATE) {
              this->sampling_rate = AD2005_MIN_SRATE;
              WARN_STREAM << "dev-property <daq_sampling_rate> is invalid. "
                          << "using min.value: " << this->sampling_rate << " Hz" << endl;
            }
            else if (sr > AD2005_MAX_SRATE) {
              this->sampling_rate = AD2005_MAX_SRATE;
              WARN_STREAM << "dev-property <daq_sampling_rate> is invalid. "
                          << "using max.value: " << this->sampling_rate << " Hz" << endl;
            }
            else {
              this->sampling_rate = sr;
            }
          }
          break;
      }
    }
    else
    {
      this->sampling_rate = kDEFAULT_SAMPLING_RATE;
      WARN_STREAM << "dev-property <daq_sampling_rate> is not set in database. "
                  << "using default value: " << this->sampling_rate << endl;
    }
    //- extract <daq_buffer_depth> --------------------------------------------
    //-------------------------------------------------------------------------
	  if (data[3].is_empty() == false)
    {
      //- extract as a long
      long bd;
      data[3] >> bd;
      //- verbose
      DEBUG_STREAM << "dev-property::daq_buffer_depth::" <<  bd << endl;
      //- check value
      if (bd <= 0) {
        this->buffer_depth = kDEFAULT_BUFFER_DEPTH;
        WARN_STREAM << "dev-property <daq_buffer_depth> is invalid. "
                    << "using default value: " << this->buffer_depth << " scans/channel" << endl;
      }
      else {
        this->buffer_depth = bd;
      }
    }
    else
    {
      this->buffer_depth = kDEFAULT_BUFFER_DEPTH;
      WARN_STREAM << "dev-property <daq_buffer_depth> is not set in database. "
                  << "using default value: " << this->buffer_depth << endl;
    }
    //- extract <trigger_nsec_delay> ------------------------------------------
    //-------------------------------------------------------------------------
    if (data[4].is_empty() == false)
    {
      //- extract as a double
      double nsec_delay;
      data[4] >> nsec_delay;
      //- verbose
      DEBUG_STREAM << "dev-property::trigger_nsec_delay::" <<  nsec_delay << endl;
      //- check value
      if (nsec_delay <= 0) {
        this->nsec_delay = kDEFAULT_NSEC_DELAY;
        WARN_STREAM << "dev-property <trigger_nsec_delay> is invalid. "
                    << "using default value: " << this->nsec_delay << " ns" << endl;
      }
      else {
        this->nsec_delay = nsec_delay;
      }
    }
    else
    {
      this->nsec_delay = kDEFAULT_NSEC_DELAY;
      WARN_STREAM << "dev-property <trigger_nsec_delay> is not set in database. "
                  << "using default value: " << this->nsec_delay << " ns" << endl;
    }
    //- extract <daq_timeout> -------------------------------------------------
    //-------------------------------------------------------------------------
    if (data[5].is_empty() == false)
    {
      //- extract as a double
      long tmo;
      data[5] >> tmo;
      //- verbose
      DEBUG_STREAM << "dev-property::daq_timeout::" <<  tmo << endl;
      //- check value
      if (tmo < kMIN_DAQ_TIMEOUT) {
        daq_timeout = kMIN_DAQ_TIMEOUT;
        WARN_STREAM << "dev-property <daq_timeout> is invalid. "
                    << "using default value: " << daq_timeout << " s" << endl;
      }
      else {
        daq_timeout = tmo;
      }
    }
    else
    {
      daq_timeout = kDEFAULT_DAQ_TIMEOUT;
      WARN_STREAM << "dev-property <daq_timeout> is not set in database. "
                  << "using default value: " << daq_timeout << " s" << endl;
    }
	  //	End of Automatic code generation
	  //-------------------------------------------------------------
  }
  catch (const Tango::DevFailed& ex)
  {
    ERROR_STREAM << ex << endl;
  }
}

//+----------------------------------------------------------------------------
//
// method :		ContinuousAIDev::always_executed_hook()
//
// description :	method always executed before any command is executed
//
//-----------------------------------------------------------------------------
void ContinuousAIDev::always_executed_hook()
{

}

//+----------------------------------------------------------------------------
//
// method :		ContinuousAIDev::write_attr_hardware()
//
// description :
//
//-----------------------------------------------------------------------------
void ContinuousAIDev::write_attr_hardware(vector<long> &attr_list)
{
/*
	for (long i=0 ; i < attr_list.size() ; i++)
  {
    Tango::WAttribute &att = dev_attr->get_w_attr_by_ind(attr_list[i]);
    string attr_name = att.get_name();
    if (attr_name == ....)
    {
      Tango::DevDouble dd;
      att.get_write_value(dd);

    }
    else if (attr_name == ...)
    {
      Tango::DevShort ds;
      att.get_write_value(ds);
      cobdp = ds ? 1 : 0;
    }
  }
*/
}

//+----------------------------------------------------------------------------
//
// method :		ContinuousAIDev::read_attr_hardware()
//
// description :	Hardware acquisition for attributes.
//
//-----------------------------------------------------------------------------
void ContinuousAIDev::read_attr_hardware(vector<long> &attr_list)
{
  //- not initilized: throw exception in <read_attr>
  if (daq == 0) {
   return;
  }

  //	  Add your own code here
  //---------------------------------
  daq->lock_data();
    this->ict1_current =  daq->ict1_current;
    this->ict2_current =  daq->ict2_current;
  daq->unlock_data();
}

//+----------------------------------------------------------------------------
//
// method :		ContinuousAIDev::read_attr()
//
// description :	Extract real attribute values from
//					hardware acquisition result.
//
//-----------------------------------------------------------------------------
void ContinuousAIDev::read_attr (Tango::Attribute &attr)
{
  //- not initilized: throw exception
  if (daq == 0) {
    Tango::Except::throw_exception(
            static_cast<const char*>("read device attribute(s)"),
				    static_cast<const char*>("device is not properly initialized"),
				    static_cast<const char*>("ContinuousAIDev::read_attr"));
  }

  string &attr_name = attr.get_name();

  //	  Switch on attribute name
  //---------------------------------
  if (attr_name == "ict1_current") {
    attr.set_value(&ict1_current);
  }
  if (attr_name == "ict2_current") {
    attr.set_value(&ict2_current);
  }
  else if (attr_name == "data_counter") {
    attr.set_value(&daq->data_counter);
  }
  else if (attr_name == "error_counter") {
    attr.set_value(&daq->error_counter);
  }
  else if (attr_name == "overrun_counter") {
    attr.set_value(&daq->overrun_counter);
  }
  else if (attr_name == "timeout_counter") {
    attr.set_value(&daq->tmo_counter);
  }
}

//+------------------------------------------------------------------
/**
 *	method: ContinuousAIDev::start
 *
 *	description:	method to execute "Start"
 *	Start data acquisition
 */
//+------------------------------------------------------------------
void ContinuousAIDev::start()
{
  //- not initilized: throw exception
  if (daq == 0) {
    this->set_internal_state();
    Tango::Except::throw_exception(
            static_cast<const char*>("could not start DAQ"),
				    static_cast<const char*>("device is not properly initialized"),
				    static_cast<const char*>("ContinuousAIDev::start"));
  }

  //- try to start daq
  try {
    daq->start();
    this->set_internal_state();
    DEBUG_STREAM << "ContinuousAIDev::start::daq successfully started" << endl;
  }
  //- device busy
  catch (const asl::DeviceBusyException& db)
  {
    //- convert daq to tango error
    Tango::DevFailed df = daq->daq_to_tango_exception(db);
    //- log
    ERROR_STREAM << df << std::endl;
    //- update internal state
    this->set_internal_state();
    //- rethrow exception
    Tango::Except::re_throw_exception(df,
            static_cast<const char*>("caught asl::DeviceBusyException"),
				    static_cast<const char*>("could not start DAQ"),
				    static_cast<const char*>("ContinuousAIDev::start"));
  }
	//- DAQ exception
  catch (const asl::DAQException& de)
  {
    //- convert daq to tango error
    Tango::DevFailed df = daq->daq_to_tango_exception(de);
    //- log
    ERROR_STREAM << df << std::endl;
    //- update internal state
    this->set_internal_state();
    //- rethrow exception
    Tango::Except::re_throw_exception(df,
            static_cast<const char*>("caught asl::DAQException"),
				    static_cast<const char*>("could not start DAQ"),
				    static_cast<const char*>("ContinuousAIDev::start"));
  }
  //- unknown exception
  catch (...)
  {
    this->set_internal_state();
		//- build Tango::DevFailed exception
		Tango::DevErrorList errors(1);
		errors.length(1);
		errors[0].severity = Tango::ERR;
		errors[0].reason = CORBA::string_dup("unknown exception caught");
		errors[0].desc = CORBA::string_dup("could not start DAQ");
		errors[0].origin = CORBA::string_dup("ContinuousAIDev::start");
		Tango::DevFailed df(errors);
	  //- log it
    ERROR_STREAM << df << std::endl;
		//- throw it
		throw df;
  }
}

//+------------------------------------------------------------------
/**
 *	method: ContinuousAIDev::stop
 *
 *	description:	method to execute "Stop"
 *	Stop data acquisition
 *
 *
 */
//+------------------------------------------------------------------
void ContinuousAIDev::stop()
{
  //- not initilized: throw exception
  if (daq == 0) {
    this->set_internal_state();
    Tango::Except::throw_exception(
            static_cast<const char*>("could not stop DAQ"),
				    static_cast<const char*>("device is not properly initialized"),
				    static_cast<const char*>("ContinuousAIDev::stop"));
  }

  try {
    //- try to stop daq
    daq->stop();
    //- update internal state
    this->set_internal_state();
    //- verbose
    DEBUG_STREAM << "ContinuousAIDev::stop::daq successfully stopped" << endl;
  }
  //- device busy
  catch (const asl::DeviceBusyException& db)
  {
    //- convert daq to tango error
    Tango::DevFailed df = daq->daq_to_tango_exception(db);
    //- log
    ERROR_STREAM << df << std::endl;
    //- update internal state
    this->set_internal_state();
    //- rethrow exception
    Tango::Except::re_throw_exception(df,
            static_cast<const char*>("caught asl::DeviceBusyException"),
				    static_cast<const char*>("could not stop DAQ"),
				    static_cast<const char*>("ContinuousAIDev::stop"));
  }
  //- daq error
  catch (const asl::DAQException& de)
  {
    //- convert daq to tango error
    Tango::DevFailed df = daq->daq_to_tango_exception(de);
    //- log
    ERROR_STREAM << df << std::endl;
    //- update internal state
    this->set_internal_state();
    //- rethrow exception
    Tango::Except::re_throw_exception(df,
            static_cast<const char*>("caught asl::DAQException"),
				    static_cast<const char*>("could not stop DAQ"),
				    static_cast<const char*>("ContinuousAIDev::stop"));
  }
  //- unknown exception
  catch (...)
  {
    this->set_internal_state();
		//- build Tango::DevFailed exception
		Tango::DevErrorList errors(1);
		errors.length(1);
		errors[0].severity = Tango::ERR;
		errors[0].reason = CORBA::string_dup("unknown exception caught");
		errors[0].desc = CORBA::string_dup("could not stop DAQ");
		errors[0].origin = CORBA::string_dup("ContinuousAIDev::stop");
		Tango::DevFailed df(errors);
	  //- log it
    ERROR_STREAM << df << std::endl;
		//- throw it
		throw df;
  }
}

//+------------------------------------------------------------------
/**
 *	method: ContinuousAIDev::calibrate_hardware
 *
 *	description:	method to execute "CalibrateHardware"
 *	Performs a hardware calibration asynchronously
 *
 *
 */
//+------------------------------------------------------------------
void ContinuousAIDev::calibrate_hardware(void)
{
  //- not initilized: throw exception
  if (daq == 0) {
    this->set_internal_state();
    Tango::Except::throw_exception(
            static_cast<const char*>("could not stop DAQ"),
				    static_cast<const char*>("device is not properly initialized"),
				    static_cast<const char*>("ContinuousAIDev::calibrate_hardware"));
  }

  try {
    //- launch calibration process 
    daq->calibrate_hardware();
    //- update internal state
    this->set_internal_state();
  }
  //- device busy
  catch (const asl::DeviceBusyException& db)
  {
    //- convert daq to tango error
    Tango::DevFailed df = daq->daq_to_tango_exception(db);
    //- log
    ERROR_STREAM << df << std::endl;
    //- update internal state
    this->set_internal_state();
    //- rethrow exception
    Tango::Except::re_throw_exception(df,
            static_cast<const char*>("caught asl::DeviceBusyException"),
				    static_cast<const char*>("could not start calibration process DAQ"),
				    static_cast<const char*>("ContinuousAIDev::calibrate_hardware"));
  }
  //- daq error
  catch (const asl::DAQException& de)
  {
    //- convert daq to tango error
    Tango::DevFailed df = daq->daq_to_tango_exception(de);
    //- log
    ERROR_STREAM << df << std::endl;
    //- update internal state
    this->set_internal_state();
    //- rethrow exception	
    Tango::Except::re_throw_exception(df,
            static_cast<const char*>("caught asl::DAQException"),
				    static_cast<const char*>("could not start calibration process DAQ"),
				    static_cast<const char*>("ContinuousAIDev::calibrate_hardware"));
  }
  //- unknown exception
  catch (...)
  {
    this->set_internal_state();
		//- build Tango::DevFailed exception
		Tango::DevErrorList errors(1);
		errors.length(1);
		errors[0].severity = Tango::ERR;
		errors[0].reason = CORBA::string_dup("unknown exception caught");
		errors[0].desc = CORBA::string_dup("could not start calibration process DAQ");
		errors[0].origin = CORBA::string_dup("ContinuousAIDev::calibrate_hardware");
		Tango::DevFailed df(errors);
	  //- log it
    ERROR_STREAM << df << std::endl;
		//- throw it
		throw df;
  }
}

//+-----------------------------------------------------------------	-
/**
 *	method: ContinuousAIDev::calibration_status
 *
 *	description:	method to execute "CalibrationStatus"
 *	Returns the calibration status
 *
 *
 */
//+------------------------------------------------------------------
void ContinuousAIDev::calibration_status(void)
{
  //- not initilized: throw exception
  if (daq == 0) {
    this->set_internal_state();
    Tango::Except::throw_exception(
            static_cast<const char*>("could not stop DAQ"),
				    static_cast<const char*>("device is not properly initialized"),
				    static_cast<const char*>("ContinuousAIDev::calibration_status"));
  }
  try {
#if defined (USE_ASYNC_CALIBRATION)
    daq->check_hardware_calibration();
#endif
    //- update internal state
    this->set_internal_state();
  }
  //- device busy
  catch (const asl::DeviceBusyException& db)
  {
    //- convert daq to tango error
    Tango::DevFailed df = daq->daq_to_tango_exception(db);
    //- verbose
    ERROR_STREAM << df << std::endl;
    //- update internal state
    this->set_internal_state();
    //- rethrow exception
    Tango::Except::re_throw_exception(df,
            static_cast<const char*>("caught asl::DeviceBusyException"),
				    static_cast<const char*>("could not obtain calibration status"),
				    static_cast<const char*>("ContinuousAIDev::calibration_status"));
  }
  //- daq error
  catch (const asl::DAQException& de)
  {
    //- convert daq to tango error
    Tango::DevFailed df = daq->daq_to_tango_exception(de);
    //- verbose
    ERROR_STREAM << df << std::endl;
    //- update internal state
    this->set_internal_state();
    //- rethrow exception
    throw df;
  }
  //- unknown exception
  catch (...)
  {
    this->set_internal_state();
		//- build Tango::DevFailed exception
		Tango::DevErrorList errors(1);
		errors.length(1);
		errors[0].severity = Tango::ERR;
		errors[0].reason = CORBA::string_dup("unknown exception caught");
		errors[0].desc = CORBA::string_dup("could not obtain calibration status");
		errors[0].origin = CORBA::string_dup("ContinuousAIDev::calibration_status");
		Tango::DevFailed df(errors);
	  //- log it
    ERROR_STREAM << df << std::endl;
		//- throw it
		throw df;
  }
}

//-------------------------------------------------------------------
// ContinuousAIDev::set_internal_state
//-------------------------------------------------------------------
void ContinuousAIDev::set_internal_state (void)
{
  if (daq == 0)
  {
    //- uninitialized
    this->set_state(Tango::UNKNOWN);
  }
  else
  {
    //- device state is daq state
    switch (daq->state()) 
    {
      case asl::ContinuousAI::STANDBY:
        this->set_state(Tango::STANDBY);
        break;
      case asl::ContinuousAI::RUNNING:
      case asl::ContinuousAI::ABORTING:
      case asl::ContinuousAI::CALIBRATING_HW:
        this->set_state(Tango::RUNNING);
        break;
      case asl::ContinuousAI::UNKNOWN:
      default:
        this->set_state(Tango::UNKNOWN);
        break;
    }

  }
}

} // namespace


