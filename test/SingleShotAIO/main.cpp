// ============================================================================
//
// = CONTEXT
//   asl::SingleShotAIO class test
//
// = FILENAME
//   main.cpp
//
// = AUTHORS
//   GA & NL �- Synchrotron SOLEIL
//
// ============================================================================

#include <iostream>
using namespace std;

#include <ace/ACE.h>
#include <asl/SingleShotAI.h>
#include <asl/SingleShotAO.h>
#include <asl/Thread.h>
using namespace asl;
using namespace adl;

// ============================================================================
// ThreadSSAO2005
// ============================================================================
class ThreadSSAO2005 : public asl::Thread
{
public:
	
	ThreadSSAO2005 () : asl::Thread()
	{
		ACE_DEBUG((LM_DEBUG, "\t ThreadSSAO2005 ctor <-\n"));
    ACE_DEBUG((LM_DEBUG, "\t ThreadSSAO2005 ctor ->\n"));
	}

	virtual ~ThreadSSAO2005 ()
	{
		ACE_DEBUG((LM_DEBUG, "\t ThreadSSAO2005 dtor <-\n"));
    ACE_DEBUG((LM_DEBUG, "\t ThreadSSAO2005 dtor ->\n"));
	}
	
protected:

	virtual ACE_THR_FUNC_RETURN svc (void* arg)
	{
		//- verbose
		ACE_DEBUG((LM_DEBUG, "\t ThreadSSAO2005::svc (arg:%x) <-\n", arg));

		//- create a single shot analog output object
		asl::SingleShotAO ao_2005;
		//- init with adl::DAQ2005 #0
		ao_2005.init(adl::DAQ2005, 0);

		//- write_scaled_channel
		try 
		{
			do {
				ao_2005.write_scaled_channel(0, 0.);
			}
			while (quit_requested() == false);
    }
		catch (const asl::DAQException& de) 
		{
      ACE_DEBUG((LM_DEBUG, "\t ThreadSSAO2005::svc::asl::DaqException caught\n"));
			for (unsigned int i = 0; i < de.errors.size(); i++) 
			{
				ACE_DEBUG((LM_DEBUG, "\t reason.....%s\n", de.errors[i].reason.c_str())); 
				ACE_DEBUG((LM_DEBUG, "\t desc.......%s\n", de.errors[i].desc.c_str())); 
				ACE_DEBUG((LM_DEBUG, "\t origin.....%s\n", de.errors[i].origin.c_str())); 
				ACE_DEBUG((LM_DEBUG, "\t code.......%d\n", de.errors[i].code));
				ACE_DEBUG((LM_DEBUG, "\t severity...%d\n", de.errors[i].severity)); 
				ACE_DEBUG((LM_DEBUG, "\n")); 
			}
		}
		catch (...) 
		{
      ACE_DEBUG((LM_DEBUG, "\t ThreadSSAO2005::svc::unknown exception caught\n"));
		}
		
		//- verbose
		ACE_DEBUG((LM_DEBUG, "\t ThreadSSAO2005::svc ->\n"));

		//- return result (here just return <arg> back)
		return ACE_static_cast(ACE_THR_FUNC_RETURN, arg);
	}
};

// ============================================================================
// ThreadSSAI2005
// ============================================================================
class ThreadSSAI2005 : public asl::Thread
{
public:
	
	ThreadSSAI2005 () : asl::Thread()
	{
		ACE_DEBUG((LM_DEBUG, "\t ThreadSSAI2005 ctor <-\n"));
    ACE_DEBUG((LM_DEBUG, "\t ThreadSSAI2005 ctor ->\n"));
	}

	virtual ~ThreadSSAI2005 ()
	{
		ACE_DEBUG((LM_DEBUG, "\t ThreadSSAI2005 dtor <-\n"));
    ACE_DEBUG((LM_DEBUG, "\t ThreadSSAI2005 dtor ->\n"));
	}
	
protected:

	virtual ACE_THR_FUNC_RETURN svc (void* arg)
	{
		//- verbose
		ACE_DEBUG((LM_DEBUG, "\t ThreadSSAI2005::svc (arg:%x) <-\n", arg));

		//- create a single shot analog output object
		asl::SingleShotAI ai_2005;
		//- init with adl::DAQ2005 #0
		ai_2005.init(adl::DAQ2005, 0);
		
		//- read_scaled_channel
    try 
		{
			do {
				ai_2005.read_scaled_channel(1);
			}
			while (quit_requested() == false);
    }
		catch (const asl::DAQException& de) 
		{
      ACE_DEBUG((LM_DEBUG, "\t ThreadSSAI2005::svc::asl::DaqException caught\n"));
			for (unsigned int i = 0; i < de.errors.size(); i++) 
			{
				ACE_DEBUG((LM_DEBUG, "\t reason.....%s\n", de.errors[i].reason.c_str())); 
				ACE_DEBUG((LM_DEBUG, "\t desc.......%s\n", de.errors[i].desc.c_str())); 
				ACE_DEBUG((LM_DEBUG, "\t origin.....%s\n", de.errors[i].origin.c_str())); 
				ACE_DEBUG((LM_DEBUG, "\t code.......%d\n", de.errors[i].code));
				ACE_DEBUG((LM_DEBUG, "\t severity...%d\n", de.errors[i].severity)); 
			}
		}
		catch (...) 
		{
      ACE_DEBUG((LM_DEBUG, "\t ThreadSSAI2005::svc::unknown exception caught\n"));
		}
		
		//- verbose
		ACE_DEBUG((LM_DEBUG, "\t ThreadSSAI2005::svc ->\n"));

		//- return result (here just return <arg> back)
		return ACE_static_cast(ACE_THR_FUNC_RETURN, arg);
	}
};

// ============================================================================
// Concurrency test
// ============================================================================
void test_threads_2005 (void)
{
  ACE_DEBUG((LM_DEBUG, "::: ASL single shot AI/AO test for board DAQ2005 (threads) :::\n"));
 
	//- instanciate single shot AO thread
	ThreadSSAO2005 * ao = new ThreadSSAO2005();
	if (ao == 0) {
		//- out of memory error
		return ;
	}

	//- instanciate single shot AI thread
	ThreadSSAI2005 * ai = new ThreadSSAI2005();
	if (ai == 0) {
		//- out of memory error
		return ;
	}

	//- run ao thread
	if (ao->run() == -1) {
		ACE_DEBUG((LM_DEBUG,"\t ThreadSSA02005::run failed\n"));  
		ACE_DEBUG((LM_DEBUG, "\t ASL single shot AI/AO test for board DAQ2005 (threads) FAILED\n"));
		return ;
	}

	//- run ai thread
	if (ai->run() == -1) {
		ao->quit();
		ACE_DEBUG((LM_DEBUG, "\t ThreadSSAI2005::run failed\n"));  
		ACE_DEBUG((LM_DEBUG, "\t ASL single shot AI/AO test for board DAQ2005 (threads) FAILED\n"));
		return ;
	}

	ACE_DEBUG((LM_DEBUG, "\t Let threads run for a while...\n"));
	
	ACE_OS::sleep(5);

	//- ask threads to quit
	ao->quit();
	ai->quit();
	
	delete ao;
	delete ai;
	
	ACE_DEBUG((LM_DEBUG, "\t ASL single shot AI/AO test for board DAQ2005 (threads) PASSED\n"));
}

// ============================================================================
// test 2005
// ============================================================================
void test_2005()
{
  ACE_DEBUG((LM_DEBUG, "::: ASL single shot AI/AO test for board DAQ2005 :::\n"));

	try
	{	
    //- create a single shot analog input object 
    asl::SingleShotAI ai_2005;
		//- initialize the hw 
		//- The type of the card is DAQ2005
		//- The number of the card in the cPCI crate is 0.
		ai_2005.init(adl::DAQ2005, 0);
    ai_2005.configure_channel(0, adl::bp_1_25);
		
		//- create a single shot analog output DAQ
		asl::SingleShotAO ao_2005;
		//- initialize the same hw as for ai_2005
		ao_2005.init(adl::DAQ2005, 0);
		
		//- calibrate hardware using ao_2005 (could use also use ai_2005)
		try {
		  ACE_DEBUG((LM_DEBUG, "\t calibrating hardware...\n"));
      ao_2005.calibrate_hardware();
		  ACE_DEBUG((LM_DEBUG, "\t hardware successfully calibrated\n"));
		}
		catch (const asl::DAQException& de)
		{
			ACE_DEBUG((LM_DEBUG, "\t calibration failed - asl::DAQException caught\n"));
			for (unsigned int i = 0; i < de.errors.size(); i++) 
			{
				ACE_DEBUG((LM_DEBUG, "\t reason.....%s\n", de.errors[i].reason.c_str())); 
				ACE_DEBUG((LM_DEBUG, "\t desc.......%s\n", de.errors[i].desc.c_str())); 
				ACE_DEBUG((LM_DEBUG, "\t origin.....%s\n", de.errors[i].origin.c_str())); 
				ACE_DEBUG((LM_DEBUG, "\t code.......%d\n", de.errors[i].code));
				ACE_DEBUG((LM_DEBUG, "\t severity...%d\n", de.errors[i].severity)); 
			}
			ACE_DEBUG((LM_DEBUG, "\t ASL single shot AI/AO test for board DAQ2005 FAILED!\n\n"));
			return;
		}
		catch (...)
		{
			ACE_DEBUG((LM_DEBUG, "\t calibration failed - unknown exception caught\n"));
			ACE_DEBUG((LM_DEBUG, "\t ASL single shot AI/AO test for board DAQ2005 FAILED!\n\n"));
      return;
		}
		
		double vvalue = 0;
		unsigned short nvalue = 0;
		 
		
		//- read channel 0 : numeric version
		nvalue = ai_2005.read_channel(0);
		std::cout << "\t AD2005::read_channel 0  : " << std::hex <<nvalue << std::endl;
		
		//- read channel 0 : voltage version
		vvalue = ai_2005.read_scaled_channel(0);
		std::cout << "\t AD2005::read_scaled_channel 0  : " << vvalue << " Volts" << std::endl;
		
		//- read channel 1 : numeric version
		nvalue = ai_2005.read_channel(1);
		std::cout << "\t AD2005::read_channel 1  : " << std::hex << nvalue << std::endl;
		
		//- read channel 1 : voltage version
		vvalue = ai_2005.read_scaled_channel(1);
		std::cout << "\t AD2005::read_scaled_channel 1  : " << vvalue << " Volts" << std::endl;
		
		asl::AIRawData* rd = 0;
		
		//- read several channels : numeric version
		rd = ai_2005.read_channels(1);
		
		if (rd)
		{
			std::cout << "\t AD2005::read_channels 0  : " << std::hex << (*rd)[0] << std::endl;
			std::cout << "\t AD2005::read_channels 1  : " << std::hex << (*rd)[1] << std::endl;
			delete rd;
			rd = 0;
		}
		
		asl::AIScaledData* sd = 0;
		
		//- read several channels : voltage version
		sd = ai_2005.read_scaled_channels(1);
		if (sd)
		{
			std::cout << "\t AD2005::read_scaled_channels 0  : " << (*sd)[0]<< " Volts" << std::endl;
			std::cout << "\t AD2005::read_scaled_channels 1  : " << (*sd)[1]<< " Volts" << std::endl;
			delete sd;
			sd = 0;
		}
		
		//- write channel
		for(int i= 0; i < 5; i++)
		{
			ao_2005.write_scaled_channel(0, 1.23456);
			ACE_OS::sleep(ACE_Time_Value(0, 125000)); 
			ao_2005.write_channel(0, 0);
			ACE_OS::sleep(ACE_Time_Value(0, 125000)); 
		}
	}
	catch (const asl::DAQException& de)
	{
		ACE_DEBUG((LM_DEBUG, "asl::DAQException caught\n"));
		for (unsigned int i = 0; i < de.errors.size(); i++) 
		{
			ACE_DEBUG((LM_DEBUG, "\t reason.....%s\n", de.errors[i].reason.c_str())); 
			ACE_DEBUG((LM_DEBUG, "\t desc.......%s\n", de.errors[i].desc.c_str())); 
			ACE_DEBUG((LM_DEBUG, "\t origin.....%s\n", de.errors[i].origin.c_str())); 
			ACE_DEBUG((LM_DEBUG, "\t code.......%d\n", de.errors[i].code));
			ACE_DEBUG((LM_DEBUG, "\t severity...%d\n", de.errors[i].severity)); 
		}
		ACE_DEBUG((LM_DEBUG, "\t ASL single shot AI/AO test for board DAQ2005 FAILED!:::\n"));
		return;
	}
	catch (...)
	{
		ACE_DEBUG((LM_DEBUG, "\t unknown exception caught\n"));
		ACE_DEBUG((LM_DEBUG, "\t ASL single shot AI/AO test for board DAQ2005 FAILED!:::\n"));
		return;
	}
	
	ACE_DEBUG((LM_DEBUG, "\t ASL single shot AI/AO test for board DAQ2005 PASSED\n"));
}
// ============================================================================
// test 2204
// ============================================================================
void test_2204()
{
  ACE_DEBUG((LM_DEBUG, "::: ASL single shot AI/AO test for board DAQ2204 :::\n"));
	
	{
	  
	
	}
	
	try
	{	
		//- create a single shot analog input object
		asl::SingleShotAI ai_2204;
		//- use board adl::DAQ2204 #0
		ai_2204.init(adl::DAQ2204, 0);
		//- config. channel 0
		ai_2204.configure_channel(0, adl::bp_10);
		
		//- create a single shot analog output object
		asl::SingleShotAO ao_2204;
		//- use board adl::DAQ2204 #0
		ao_2204.init(adl::DAQ2204, 0);
		
		//- calibrate hardware using ao_2204 (could use also use ai_2204)
		try {
		  ACE_DEBUG((LM_DEBUG, "\t calibrating hardware...\n"));
      ao_2204.calibrate_hardware();
		  ACE_DEBUG((LM_DEBUG, "\t hardware successfully calibrated\n"));
		}
		catch (const asl::DAQException& de)
		{
			ACE_DEBUG((LM_DEBUG, "\t calibration failed - asl::DAQException caught\n"));
			for (unsigned int i = 0; i < de.errors.size(); i++) 
			{
				ACE_DEBUG((LM_DEBUG, "\t reason.....%s\n", de.errors[i].reason.c_str())); 
				ACE_DEBUG((LM_DEBUG, "\t desc.......%s\n", de.errors[i].desc.c_str())); 
				ACE_DEBUG((LM_DEBUG, "\t origin.....%s\n", de.errors[i].origin.c_str())); 
				ACE_DEBUG((LM_DEBUG, "\t code.......%d\n", de.errors[i].code));
				ACE_DEBUG((LM_DEBUG, "\t severity...%d\n", de.errors[i].severity)); 
			}
			ACE_DEBUG((LM_DEBUG, "\t ASL single shot AI/AO test for board DAQ2204 FAILED!\n\n"));
			return;
		}
		catch (...)
		{
			ACE_DEBUG((LM_DEBUG, "\t calibration failed - unknown exception caught\n"));
			ACE_DEBUG((LM_DEBUG, "\t ASL single shot AI/AO test for board DAQ2204 FAILED!\n\n"));
      return;
		}
		
		double vvalue = 0;
		
		unsigned short nvalue = 0;
		
		//- read channel 0 : numeric version
		nvalue = ai_2204.read_channel(0);
		std::cout << "\t AD2204::read_channel 0  : " << std::hex << nvalue << std::endl;
		
		//- read channel 0 : voltage version
		vvalue = ai_2204.read_scaled_channel(0);
		std::cout << "\t AD2204::read_scaled_channel 0  : " << vvalue << " Volts" << std::endl;
		
		//- read channel 1 : numeric version
		nvalue = ai_2204.read_channel(1); 
		std::cout << "\t AD2204::read_channel 1  : " << std::hex << nvalue << std::endl;
		
		//- read channel 1 : voltage version
		vvalue = ai_2204.read_scaled_channel(1);
		std::cout << "\t AD2204:: read_scaled_channel 1  : " << vvalue << " Volts" << std::endl;
		
		asl::AIRawData* rd = 0;
		
		//- read several channels : numeric version
		rd = ai_2204.read_channels(1);
		
		if (rd)
		{
			std::cout << "\t AD2204::read_channels 0  : " << std::hex << (*rd)[0] << std::endl;
			std::cout << "\t AD2204::read_channels 1  : " << std::hex << (*rd)[1] << std::endl;
			delete rd;
			rd = 0;
		}
		
		asl::AIScaledData* sd = 0;
		
		//- read several channels : voltage version
		sd = ai_2204.read_scaled_channels(1);
		if (sd) 
    {
			std::cout << "\t AD2204::read_scaled_channels 0  : " << (*sd)[0]<< " Volts" << std::endl;
			std::cout << "\t AD2204::read_scaled_channels 1  : " << (*sd)[1]<< " Volts" << std::endl;
			delete sd;
			sd = 0;
		}
		
		//- write channel
		for(int i= 0; i < 5; i++)
		{
			ao_2204.write_scaled_channel(0, 1.23456);
			ACE_OS::sleep(ACE_Time_Value(0, 125000)); 
			ao_2204.write_channel(0, 0);
			ACE_OS::sleep(ACE_Time_Value(0, 125000)); 
		}
	}
	catch (const asl::DAQException& de)
	{
		ACE_DEBUG((LM_DEBUG, "\t asl::DAQException caught\n"));
		for (unsigned int i = 0; i < de.errors.size(); i++) 
		{
			ACE_DEBUG((LM_DEBUG, "\t reason.....%s\n", de.errors[i].reason.c_str())); 
			ACE_DEBUG((LM_DEBUG, "\t desc.......%s\n", de.errors[i].desc.c_str())); 
			ACE_DEBUG((LM_DEBUG, "\t origin.....%s\n", de.errors[i].origin.c_str())); 
			ACE_DEBUG((LM_DEBUG, "\t code.......%d\n", de.errors[i].code));
			ACE_DEBUG((LM_DEBUG, "\t severity...%d\n", de.errors[i].severity)); 
		}
		ACE_DEBUG((LM_DEBUG, "\t ASL single shot AI/AO test for board DAQ2204 FAILED!\n\n"));
		return;
	}
	catch (...)
	{
		ACE_DEBUG((LM_DEBUG, "\t unknown exception caught\n"));
		ACE_DEBUG((LM_DEBUG, "\t ASL single shot AI/AO test for board DAQ2204 FAILED!\n\n"));
		return;
	}
	
	ACE_DEBUG((LM_DEBUG, "\t ASL single shot AI/AO test for board DAQ2204 PASSED\n\n"));
}


// ============================================================================
// Main
// ============================================================================
int main (int, char**) 
{
  ACE_DEBUG((LM_DEBUG,"\n"));
 
  try {
	  test_2204();
		ACE_DEBUG((LM_DEBUG,"\n"));
    test_2005();
		ACE_DEBUG((LM_DEBUG,"\n"));
    test_threads_2005();
	}
	catch (...) {
	  
	}
	
	ACE_DEBUG((LM_DEBUG,"\n"));
	
	return 0;
}
