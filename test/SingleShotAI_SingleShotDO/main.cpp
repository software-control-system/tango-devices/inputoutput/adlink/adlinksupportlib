// ============================================================================
//
// = CONTEXT
//   asl::SingleShotDO asl::SingleShotDO class test
//
// = FILENAME
//   main.cpp
//
// = AUTHOR
//   Gwena�lle Abeill� - Synchrotron SOLEIL
//
// ============================================================================

#include <iostream>
using namespace std;

#include <ace/ace.h>
#include <asl/SingleShotAI.h>
#include <asl/SingleShotDO.h>
#include <asl/Thread.h>

// ============================================================================
// Thread AI
// ============================================================================
class ThreadAI : public asl::Thread
{	
public:			
	ThreadAI ();			
	virtual ~ThreadAI ();			
protected:			
	virtual ACE_THR_FUNC_RETURN svc (void* arg);	
};
//-----------------------------------------------------------------------------  
ThreadAI::ThreadAI () 
: asl::Thread()
{	
	//-noop ctor	
}
//-----------------------------------------------------------------------------  

ThreadAI::~ThreadAI ()
{	
	//-noop ctor	
}
//----------------------------------------------------------------------------- 
ACE_THR_FUNC_RETURN ThreadAI::svc (void* arg)
{	
	//- verbose	
	ACE_DEBUG((LM_DEBUG, "Entering ThreadAI::svc (arg:%x)\n", arg));	
	asl::SingleShotAI ai_2005;	
	ai_2005.init(adl::DAQ2005, 0);	
	ai_2005.configure_channel(0, adl::bp_10);	
	double volt;	
	//- enter almost infinite loop	
	do 	
	{	
		ACE_OS::sleep(1);	
		volt = ai_2005.read_scaled_channel(0);
		ACE_DEBUG((LM_DEBUG, "Analog input : %f\n", volt));
	}while (quit_requested() == false);
	//- verbose
	ACE_DEBUG((LM_DEBUG, "Leaving ThreadAI::svc\n"));	
	//- return result (here just return <arg> back)	
	return ACE_reinterpret_cast(ACE_THR_FUNC_RETURN, arg);	
}
// ============================================================================
// Main
// ============================================================================
int main (int, char**) 
{
	asl::SingleShotDO do_2005;

	try	
	{		
		do_2005.init(adl::DAQ2005, 0);	
		do_2005.configure_port(adl::port_1b);			
		
		// create a dio  2005 card	
		ThreadAI * t = new ThreadAI();	
		
		if (t == 0)	
		{	
			//- out of memory error	
			return 0;	
		}	
		//- run the thread di	
		if (t->run() == -1) {
			
			ACE_DEBUG((LM_DEBUG, "Thread::run failed\n"));		
			return 0;	
		}
		for (int i = 0; i < 50; i++)	
		{	
			ACE_OS::sleep(1); 
			do_2005.write_line(adl::port_1b, 0, 1);	
			ACE_OS::sleep(1); 	
			do_2005.write_line(adl::port_1b, 0, 0);	
		}
		//- ask the thread to quit
		if (t->quit() == -1) {
			ACE_DEBUG((LM_DEBUG, "Thread::quit failed\n"));  
			return 0;
		}
		delete t;
	}
	catch (const asl::DAQException& de) 	
	{	
		ACE_DEBUG((LM_DEBUG, "asl::DAQException caught\n")); 	
		for (int i = 0; i < de.errors.size(); i++) 		
		{
			ACE_DEBUG((LM_DEBUG, "- reason.....%s\n", de.errors[i].reason.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- desc.......%s\n", de.errors[i].desc.c_str())); 
			ACE_DEBUG((LM_DEBUG, "- origin.....%s\n", de.errors[i].origin.c_str())); 	
			ACE_DEBUG((LM_DEBUG, "- code.......%d\n", de.errors[i].code));
			ACE_DEBUG((LM_DEBUG, "- severity...%d\n", de.errors[i].severity)); 
		}
	}
	catch (...) 	
	{	
		ACE_DEBUG((LM_DEBUG, "unknown exception caught\n"));	
	}
	return 0;
	
}