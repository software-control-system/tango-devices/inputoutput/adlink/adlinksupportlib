//===================================================================
// DEPENDENCIES
//===================================================================
#include <ADLinkContinuousAI.h>

//===================================================================
// ADLinkContinuousAI::ADLinkContinuousAI
//===================================================================
ADLinkContinuousAI::ADLinkContinuousAI (Tango::DeviceImpl *dev)
  : Tango::LogAdapter(dev),
    data_counter (0),
    tmo_counter (0),
    error_counter (0),
    overrun_counter (0),
	daq_end_evt_received (false),
    post_trig_evt_buffer_0 (0),
    post_trig_evt_buffer_1 (0),
    scaled_history_buffer (0)
{

}

//===================================================================
// ADLinkContinuousAI::~ADLinkContinuousAI
//===================================================================
ADLinkContinuousAI::~ADLinkContinuousAI (void) 
{ 
  //- noop
}

//===================================================================
// ADLinkContinuousAI::start
//===================================================================
void ADLinkContinuousAI::start (void) throw (asl::DAQException)
{
  this->lock_data();

	//- reset local data
	this->data_counter = 0;
	this->tmo_counter = 0;
	this->overrun_counter = 0;
	this->daq_end_evt_received = false;
  
  delete this->post_trig_evt_buffer_0;
  this->post_trig_evt_buffer_0 = 0;
  
  delete this->post_trig_evt_buffer_1;
  this->post_trig_evt_buffer_1 = 0;
  
  delete this->scaled_history_buffer;
  this->scaled_history_buffer = 0;
  
  delete this->last_buffer;
  this->last_buffer = 0;
  
  this->unlock_data();

	//- call mother class start method (may throw asl::DAQException)
	this->ContinuousAI::start();
}
	
//===================================================================
// ADLinkContinuousAI::handle_input
//===================================================================
void ADLinkContinuousAI::handle_input (asl::AIRawData* raw_data)
{
  //- inc our data counter
  this->data_counter++;

  //- DEBUG_STREAM << "::: ADLinkContinuousAI::handle_input :::" << std::endl;

  this->lock_data();
    delete this->last_buffer;
    this->last_buffer = this->scale_data(raw_data);
  this->unlock_data();
  
  //- release data to avoid memory leak
  delete raw_data;
}

//===================================================================
// ADLinkContinuousAI::handle_daq_end
//===================================================================
void  ADLinkContinuousAI::handle_daq_end (asl::ContinuousDAQ::DaqEndReason why)
{
	INFO_STREAM << "::: ADLinkContinuousAI::handle_daq_end :::" << std::endl; 

	this->daq_end_evt_received = true;
  
	switch (why)
	{
		case ContinuousDAQ::DAQEND_ON_USER_REQUEST:
			INFO_STREAM << "\treason: user request" << std::endl; 
			break;
		case ContinuousDAQ::DAQEND_ON_EXTERNAL_TRIGGER:
			INFO_STREAM << "\treason: digital external trigger raised (pre or middle trigger mode)" << std::endl; 
			INFO_STREAM << "\treason: history buffer frozen" << std::endl; 
			break;
		case ContinuousDAQ::DAQEND_ON_ERROR:
			INFO_STREAM << "\treason: daq error" << std::endl; 
			break;		
		case ContinuousDAQ::DAQEND_ON_OVERRUN:
			INFO_STREAM << "\treason: daq buffer overrun" << std::endl; 
			INFO_STREAM << "\treason: daq has been <aborted> as required by the specified overrun strategy" << std::endl; 
			break;
		case ContinuousDAQ::DAQEND_ON_UNKNOWN_EVT:
		default:
		  INFO_STREAM << "\treason: unknown" << std::endl; 
			break;
	}

	DEBUG_STREAM << std::endl; 

  this->lock_data();
  
    asl::AIRawData * buffer_0; 
    asl::AIRawData * buffer_1; 
    this->daq_buffers(buffer_0, buffer_1);

    delete this->post_trig_evt_buffer_0;
	  this->post_trig_evt_buffer_0 = this->scale_data(buffer_0);
  	delete buffer_0;

    delete this->post_trig_evt_buffer_1;
	  this->post_trig_evt_buffer_1 = this->scale_data(buffer_1);
	  delete buffer_1;
    
    delete this->scaled_history_buffer;
    this->scaled_history_buffer = this->scaled_history();
  
	  //- get history history (i.e. post-mortem) buffer
	  if (why == DAQEND_ON_EXTERNAL_TRIGGER && this->configuration().history_enabled())
	  {
      delete this->scaled_history_buffer;
  
		  try 
		  {
			  //- get history as scaled data
			  this->scaled_history_buffer = this->scaled_history();
			  INFO_STREAM << "\thistory buffer depth...." << this->scaled_history_buffer->depth() << " samples" << std::endl; 
			  INFO_STREAM << "\thistory buffer depth...." << this->scaled_history_buffer->depth() / this->configuration().get_buffer_depth() << " daq-buffers"  << std::endl; 
			  INFO_STREAM << "\thistory buffer length..." << this->configuration().get_history_length() << " msec" << std::endl;
		  }
		  catch (const asl::DAQException& de) 
		  {
			  INFO_STREAM << "asl::DAQException caught while trying to get history buffer" << std::endl;  
			  for (unsigned int i = 0; i < de.errors.size(); i++) 
			  {
				  INFO_STREAM << "- reason....." << de.errors[i].reason.c_str() << std::endl; 
				  INFO_STREAM << "- desc......." << de.errors[i].desc.c_str() << std::endl;
				  INFO_STREAM << "- origin....." << de.errors[i].origin.c_str() << std::endl;
				  INFO_STREAM << "- code......." << de.errors[i].code << std::endl;
				  INFO_STREAM << "- severity..." << de.errors[i].severity << std::endl; 
			  }
		  }
		  catch (...)  
		  {
			  INFO_STREAM << "Unknown exception caught while trying to get history buffer" << std::endl;  
		  }

		  INFO_STREAM << std::endl; 
    }

  this->unlock_data();
}
	
//===================================================================
// ADLinkContinuousAI::handle_error
//===================================================================
void ADLinkContinuousAI::handle_error (const asl::DAQException& de)
{
  error_counter++;

  DEBUG_STREAM << "::: ADLinkContinuousAI::handle_error :::" << std::endl;

  ERROR_STREAM << this->daq_to_tango_exception(de) << endl;
}

//===================================================================
// ADLinkContinuousAI::handle_data_lost
//===================================================================
void ADLinkContinuousAI::handle_data_lost (void)
{
  overrun_counter++;

  WARN_STREAM << "::: ADLinkContinuousAI::handle_data_lost :::" << std::endl;

	DEBUG_STREAM << std::endl; 

	DEBUG_STREAM << "\toverrun notification #" <<  overrun_counter << std::endl;
	
  DEBUG_STREAM << std::endl; 
}

//===================================================================
// ADLinkContinuousAI::handle_timeout
//===================================================================
void ADLinkContinuousAI::handle_timeout (void)
{
  tmo_counter++;

  DEBUG_STREAM << "::: ADLinkContinuousAI::handle_timeout :::" << std::endl; 
  
  DEBUG_STREAM << std::endl; 
  
  DEBUG_STREAM << "\ttimeout notification #" <<  tmo_counter << std::endl;
  
  DEBUG_STREAM << std::endl; 

	DEBUG_STREAM << "\treason: the DAQ internal processing task did not receive data since " 
               << this->configuration().get_timeout()
               << " msec" 
               << std::endl; 

	DEBUG_STREAM << "\treason: this is the timeout specified int the DAQ configuration" << std::endl; 

  //- infinite retrigger mode
  if (this->configuration().retrigger_enabled()) 
  {
	  DEBUG_STREAM << "\treason: trigger signal is either off or disconnected" << std::endl; 
  }
  //- pre-triggger mode
  else if (daq_end_evt_received == true) 
  {
	  DEBUG_STREAM << "\treason: DAQ has been stopped by external trigger" << std::endl; 
	  DEBUG_STREAM << "\treason: in this case the timeout event can be ignored" << std::endl; 
  }
  //- remaining cases
  else 
  {
	  DEBUG_STREAM << "\treason: may be due to a DAQ configuration problem" << std::endl; 
	  DEBUG_STREAM << "\treason: the DAQ buffer depth may be too large for the selected sampling rate" << std::endl; 
	  DEBUG_STREAM << "\treason: the timeout delay may also be too short compared to the time required to fill a daq buffer" << std::endl; 
	  DEBUG_STREAM << "\treason: try to increase either the timeout delay or sampling rate" << std::endl; 
  }

  DEBUG_STREAM << std::endl; 
}

//-------------------------------------------------------------------
// ADLinkContinuousAI::daq_to_tango_exception
//-------------------------------------------------------------------
Tango::DevFailed ADLinkContinuousAI::daq_to_tango_exception (const asl::DAQException& de)
{
  Tango::DevErrorList error_list(de.errors.size());

  error_list.length(de.errors.size());

  for (int i = 0; i < de.errors.size(); i++) 
  {
    error_list[i].reason = CORBA::string_dup(de.errors[i].reason.c_str());
    error_list[i].desc   = CORBA::string_dup(de.errors[i].desc.c_str());
    error_list[i].origin = CORBA::string_dup(de.errors[i].origin.c_str());
    switch (de.errors[i].severity) 
    {
      case asl::WARN:
        error_list[i].severity = Tango::WARN;
        break;
      case asl::PANIC:
        error_list[i].severity = Tango::PANIC;
        break;
      case asl::ERR:
      default:
        error_list[i].severity = Tango::ERR;
        break;
    }
  }

  return Tango::DevFailed(error_list);
}
