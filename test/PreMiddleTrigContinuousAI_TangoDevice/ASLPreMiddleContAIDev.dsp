# Microsoft Developer Studio Project File - Name="ASLPreMiddleContAIDev" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=ASLPreMiddleContAIDev - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "ASLPreMiddleContAIDev.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "ASLPreMiddleContAIDev.mak" CFG="ASLPreMiddleContAIDev - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ASLPreMiddleContAIDev - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "ASLPreMiddleContAIDev - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "ASLPreMiddleContAIDev - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I "." /I "$(OMNIORB_INC)" /I "$(TANGO_INC)" /I "../../include" /I "../../src" /I "$(D2KDASK_INC)" /I "$(PCISDASK_INC)" /I "$(ACE_INC)" /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /FR /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 asl.lib ace.lib omniORB407_rt.lib omniDynamic407_rt.lib omnithread32_rt.lib COS407_rt.lib tango.lib log4tango.lib kernel32.lib gdi32.lib comctl32.lib user32.lib advapi32.lib ws2_32.lib /nologo /subsystem:console /machine:I386 /out:"Release/ds_ASLPreMiddleContAIDev.exe" /libpath:"$(TANGO_LIB)\mix" /libpath:"$(OMNIORB_LIB)" /libpath:"$(D2KDASK_LIB)" /libpath:"$(PCISDASK_LIB)" /libpath:"$(ACE_LIB)" /libpath:"..\..\win32\msvc6\build"

!ELSEIF  "$(CFG)" == "ASLPreMiddleContAIDev - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "debug"
# PROP Intermediate_Dir "debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GR /GX /ZI /Od /I "." /I "$(OMNIORB_INC)" /I "$(TANGO_INC)" /I "../../include" /I "../../src" /I "$(D2KDASK_INC)" /I "$(PCISDASK_INC)" /I "$(ACE_INC)" /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /D "_WINSTATIC" /FR /YX /FD /I /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 asld.lib aced.lib omniORB407_rtd.lib omniDynamic407_rtd.lib omnithread32_rtd.lib COS407_rtd.lib tangod.lib log4tangod.lib kernel32.lib gdi32.lib comctl32.lib user32.lib advapi32.lib ws2_32.lib /nologo /subsystem:console /debug /machine:I386 /out:"debug/ds_ASLPreMiddleContAIDev.exe" /pdbtype:sept /libpath:"$(TANGO_LIB)\mix" /libpath:"$(OMNIORB_LIB)" /libpath:"$(D2KDASK_LIB)" /libpath:"$(PCISDASK_LIB)" /libpath:"$(ACE_LIB)" /libpath:"..\..\win32\msvc6\build"

!ENDIF 

# Begin Target

# Name "ASLPreMiddleContAIDev - Win32 Release"
# Name "ASLPreMiddleContAIDev - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\ADLinkContinuousAI.cpp
# End Source File
# Begin Source File

SOURCE=ASLPreMiddleContAIDev.cpp
# End Source File
# Begin Source File

SOURCE=ASLPreMiddleContAIDevClass.cpp
# End Source File
# Begin Source File

SOURCE=ASLPreMiddleContAIDevStateMachine.cpp
# End Source File
# Begin Source File

SOURCE=ClassFactory.cpp
# End Source File
# Begin Source File

SOURCE=main.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\ADLinkContinuousAI.h
# End Source File
# Begin Source File

SOURCE=ASLPreMiddleContAIDev.h
# End Source File
# Begin Source File

SOURCE=ASLPreMiddleContAIDevClass.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
