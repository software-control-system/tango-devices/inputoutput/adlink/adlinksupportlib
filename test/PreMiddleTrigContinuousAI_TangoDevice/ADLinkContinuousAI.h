//===================================================================
// DEPENDENCIES
//===================================================================
#include <asl/AIData.h>
#include <asl/adlink/AD2005.h>
#include <asl/adlink/AD2010.h>
#include <asl/ContinuousAI.h>
#include <tango.h>

//------------------------------------------------------------------------------
//---- USER DEFINED CONTINUOUS AI DAQ: SUPPORT THE ADLINK 2000 SERIE AI BOARDS
//------------------------------------------------------------------------------
class ADLinkContinuousAI : public asl::ContinuousAI , public Tango::LogAdapter
{
public: 
  //----------------------------------------------
  //---- CTOR
  //----------------------------------------------
  ADLinkContinuousAI (Tango::DeviceImpl *dev);

  //----------------------------------------------
  //---- DTOR
  //----------------------------------------------
  virtual ~ADLinkContinuousAI (void);

  //----------------------------------------------
  //---- CUSTOMIZE START BEHAVIOR
  //----------------------------------------------
  void start (void) throw (asl::DAQException);

  //----------------------------------------------
  //---- DATA PROCESSING
  //----------------------------------------------
  virtual void handle_input (asl::AIRawData* raw_data);

  //----------------------------------------------
  //---- DAQ ERROR HANDLER
  //----------------------------------------------
  virtual void handle_error (const asl::DAQException& de);

  //----------------------------------------------
  //---- OVERRUN HANDLER
  //----------------------------------------------
  virtual void handle_data_lost (void);

  //----------------------------------------------
  //---- TIMEOUT HANDLER
  //----------------------------------------------
  virtual void handle_timeout (void);

  //----------------------------------------------
  //---- DAQ-END HANDLER
  //----------------------------------------------
  virtual void handle_daq_end (ContinuousDAQ::DaqEndReason why);  

  //----------------------------------------------
  //---- DATA PROTECTION AGAINST RACE CONDITIONS
  //----------------------------------------------
  inline void lock_data (void) {
    this->data_lock_.acquire(); 
  };

  inline void unlock_data (void) {
    this->data_lock_.release(); 
  }

  //----------------------------------------------
  //---- EXCEPTION ADAPTER
  //----------------------------------------------
  Tango::DevFailed daq_to_tango_exception (const asl::DAQException& de);

  //-----------------------------------------------
  // stats: number of message data processed
  //-----------------------------------------------
  long data_counter;

  //-----------------------------------------------
  // stat: number of timeout notifications received
  //-----------------------------------------------
  long tmo_counter;

  //-----------------------------------------------
  // stat: number of error notifications received
  //-----------------------------------------------
  long error_counter;

  //-----------------------------------------------
  // stat: number of overrun notifications received
  //-----------------------------------------------
  long overrun_counter;

  //-----------------------------------------------
  // flag: daq_end_evt_received
  //-----------------------------------------------
  bool daq_end_evt_received;

  //-----------------------------------------------
  // the last DAQ buffer 
  //-----------------------------------------------
  asl::AIScaledData * last_buffer;
  
  //-----------------------------------------------
  // the DAQ buffers as they are retreived after the pre or middle trig event
  //-----------------------------------------------
  asl::AIScaledData * post_trig_evt_buffer_0;
  asl::AIScaledData * post_trig_evt_buffer_1;
  
  //-----------------------------------------------
  // the scaled history buffer
  //-----------------------------------------------
  asl::AIScaledData * scaled_history_buffer;

  
private:
  //-----------------------------------------------
  // lock
  //-----------------------------------------------
  ACE_Thread_Mutex data_lock_;
}; 


