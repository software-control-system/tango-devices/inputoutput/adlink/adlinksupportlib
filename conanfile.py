from conan import ConanFile

class aslRecipe(ConanFile):
    name = "asl"
    version = "1.6.0"
    package_type = "library"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Library"
    
    license = "GPL-2"
    author = "Nicolas Leclercq"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/inputoutput/adlink/asl"
    description = "Adlink Support library"
    topics = ("utility", "control-system")

    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}

    exports_sources = "CMakeLists.txt", "src/**", "include/**"

    def requirements(self):
        self.requires("ace/5.7.0@soleil/stable", transitive_headers=True, transitive_libs=True)
        self.requires("d2k-dask/18.10@soleil/stable", transitive_headers=True, transitive_libs=True)
        self.requires("pcis-dask/5.10@soleil/stable", transitive_headers=True, transitive_libs=True)
