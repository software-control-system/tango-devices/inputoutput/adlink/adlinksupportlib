// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    DataExtractor_T.h
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

#ifndef _DATA_EXTRACTOR_T_H_
#define _DATA_EXTRACTOR_T_H_

// ============================================================================
// DEPENDENCIEs
// ============================================================================
#include <asl/ASLConfig.h>

namespace asl {

  // ============================================================================
// FORWARD DECLARATION
// ============================================================================
class Message;

// ============================================================================
// message_data
// ----------------------------------------------------------------------------
// Template standalone function : simplifies the usage of asl::DataExtractor.
// It allows the user to extract the message content (i.e. the Data it 
// contains). If the latter could not be converted to T*, the function 
// returns -1, 0 otherwise. 
// ============================================================================
template <class T> int message_data (Message *_msg, T *&data_);

// ============================================================================
//! DataExtractor
// ============================================================================
//!  
//! detailed description to be written
//! 
// ============================================================================
template <class T>
class DataExtractor
{
public:

  DataExtractor ();
  // Ctor.

  virtual ~DataExtractor ();
  // Dtor.

  T * operator() (Message *_msg) const;
  // Returns message's content.

private:

  // = Disallow these operations.
  //--------------------------------------------
  ACE_UNIMPLEMENTED_FUNC(DataExtractor& operator= (const DataExtractor&))
  ACE_UNIMPLEMENTED_FUNC(DataExtractor(const DataExtractor&))
};

} // namespace asl

#if defined (__ASL_INLINE__)
# include "asl/Extractor_T.i"
#endif // __ASL_INLINE__

#if defined (ACE_TEMPLATES_REQUIRE_SOURCE)
#if defined (ASL_BUILD) || defined (ASL_BUILD_TEST)
# include <Extractor_T.cpp>
#else
# include <asl/Extractor_T.cpp>
#endif
#endif // ACE_TEMPLATES_REQUIRE_SOURCE

#if defined (ACE_TEMPLATES_REQUIRE_PRAGMA)
#if defined (ASL_BUILD) || defined (ASL_BUILD_TEST)
# pragma implementation (<Extractor_T.cpp>)
#else
# pragma implementation (<asl/Extractor_T.cpp>)
#endif
#endif // ACE_TEMPLATES_REQUIRE_PRAGMA

#endif // _DATA_EXTRACTOR_T_H_



