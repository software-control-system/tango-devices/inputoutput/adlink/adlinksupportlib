// ============================================================================
//
// = CONTEXT
//    TANGO Project - Tango Tools Library
//
// = FILENAME
//    ContinuousAO.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _CONTINUOUS_AO_H_
#define _CONTINUOUS_AO_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/AOData.h>
#include <asl/adlink/ADContinuousAOBoard.h>
#include <asl/ContinuousDAQ.h>
#include <asl/ContinuousAOConfig.h>

namespace asl {

// ============================================================================
// FORWARD DECLARATION
// ============================================================================
#if defined(_SIMULATION_)
  class CallbackCallerAO;
#endif

// ============================================================================
//! The ASL continuous analog output DAQ abstraction class.
// ============================================================================
//! Provides functions to perform continuous analog output on ADLink boards that
//! support this functions. \n
//! The main functionnalities are:
//! - Perform analog output with a periodic signal (the ouput buffer is always the same
//! and provided only one time by user before beginning outputing).
//! - Perform analog output with an aperiodic signal (a new output buffer is gave by user each time 
//! the previous buffer have been outputed).
//! - Ouput n times the output buffer after external trigger occur.
// ============================================================================
class ASL_EXPORT ContinuousAO : public ContinuousDAQ
{
public:

  /**
   * Initialization. 
   */
  ContinuousAO ();
  
  /**
   * Release resources.
   */
  virtual ~ContinuousAO ();

  //
  // Continuous DAQ control methods
  //-----------------------------------------------------------

  /**
   * Configure the continuous analog output DAQ.
   * @param _config The configuration for analog output.
   */
  virtual void configure (const ContinuousAOConfig& _config)
          throw (asl::DAQException, asl::DeviceBusyException);

   /**
   * Configure the continuous analog output DAQ.
   * @param _config The configuration for analog output.
   */
  virtual void configure (ContinuousAOConfig* _config)
          throw (asl::DAQException, asl::DeviceBusyException);

  /**
   * Returns the current configuration.
   */
  const ContinuousAOConfig& configuration () const;

  /**
   * Initialize the continuous analog output DAQ.
   * @param hw_type The type of the card.
   * @param hw_id The number of the card in the cPCI crate.
   */
  virtual void init (unsigned short hw_type, unsigned short hw_id)
          throw (asl::DAQException);

  /**
   * Performs hardware auto-calibration asynchronously (i.e launch
   * calibration process then return immediately). See also ContinuousDAQ
   * calibration_completed and check_hardware_calibration.
   */
  virtual void calibrate_hardware ()
          throw (asl::DAQException, asl::DeviceBusyException);

  /**
   * Start the continuous analog output DAQ.
   */
  virtual void start ()
          throw (asl::DAQException, asl::DeviceBusyException);

  /**
   * Stop the continuous analog output DAQ.
   */
  virtual void stop ()
          throw (asl::DAQException, asl::DeviceBusyException);

  /**
   * Abort the continuous analog output DAQ.
   */
  virtual void abort ()
          throw (asl::DAQException, asl::DeviceBusyException);

  //
  // User hooks
  //-----------------------------------------------------------

  /**
   * AO data generation hook.
   * The user have to fill the output buffer in non-periodic mode with his values in this function. 
   * @param num_active_chan  The number of active channels.
	 * @param samples_per_chan  The expected number of samples per active channel.
   * @param transfer_ownership Tranfer the ownership of the data to the caller if true (i.e. allow caller to delete the data).
   * @return A buffer of output data.
   */
  virtual asl::AORawData* handle_output (unsigned int _num_active_chan, 
	                                       unsigned long _samples_per_chan, 
																				 bool& transferownership);
  
  /**
   * AO data generation hook.
   * The user have to fill the output buffer in non-periodic mode with his values in this function. 
   * @param num_active_chan  The number of active channels.
	 * @param samples_per_chan  The expected number of samples per active channel.
   * @param transfer_ownership Tranfer the ownership of the data to the caller if true (i.e. allow caller to delete the data).
   * @return A buffer of output data.
   */
  virtual asl::AOScaledData* handle_scaled_output  (unsigned int _num_active_chan, 
	                                                  unsigned long _samples_per_chan, 
																				            bool& transferownership);

  /**
   * DAQ exception hook.
   * The user have to define a behaviour when an exception occurs.
   * @param ex A DAQException.
   */
  virtual void handle_error (const asl::DAQException& ex) = 0;

  //
  // Misc
  //----------------------------------------------------------- 
  
  /**
   * Dump DAQ info. 
   */
  void dump () const;

  /**
   * @return true if data have been loaded in FIFO. 
   * The information is available after calling start()
   */
  bool use_board_fifo () const;
 
protected:

  /**
   * Handle DAQ exception (called by external handler). 
   */
  virtual void handle_daq_exception ()
     throw (asl::DAQException);

#if defined (USE_ASYNC_CALIBRATION)
  /**
   * Handle calibration (called by external handler). 
   */
  virtual void handle_hardware_calibration () 
    throw (DAQException);
#endif

  /**
   * Class wide half-buffer-ready event callback.
   */
#if defined (WIN32)
   static void event_callback ();
#else
   static void event_callback (int sig_num);
#endif

private:
  /**
   * Return true if initialized, false otherwise.
   */
   bool initialized () const;

  /**
   * Internal implementation of the configure member.
   */
  void configure_i (const ContinuousAOConfig& config) throw (asl::DAQException);

  /**
   * Internal implementation of the start member.
   */
  void start_i () throw (asl::DAQException);

  /**
   * Internal implementation of the stop member.
   */
  void stop_i () throw (asl::DAQException);

  /**
   * Handle fatal error.
   */
  void handle_daq_exception_i (const asl::DAQException&);

  /**
   * Handle hardware calibration.
   */
  void handle_hardware_calibration_i ()
      throw (asl::DAQException);

  /**
   * The DAQ configuration.
   */
  ContinuousAOConfig config_;

  /**
   * The underlying DAQ hardware.
   */
  adl::ADContinuousAOBoard * daq_hw_;

  /**
   * Identifier of thread executing the user hook (recurs. handling).
   */
  ACE_thread_t thread_executing_user_hook_;
  
#if defined(_SIMULATION_)

  /**
   * The DAQ simulator.
   */
  CallbackCallerAO * cb_caller_;

#endif // _SIMULATION_

#if !defined (DAQ_CALLBACK_ACCEPT_ARG)
  static ContinuousAO * instance;
#endif

  // = Disallow these operations.
  //--------------------------------------------
  ACE_UNIMPLEMENTED_FUNC(ContinuousAO& operator= (const ContinuousAO&))
  ACE_UNIMPLEMENTED_FUNC(ContinuousAO(const ContinuousAO&))
};

} // namespace asl

#if defined (__ASL_INLINE__)
# include "asl/ContinuousAO.i"
#endif // __ASL_INLINE__

#endif // _CONTINUOUS_AO_H_

