// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    Data_T.h
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

#ifndef _DATA_T_H_
#define _DATA_T_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/DAQException.h>
#include <asl/Data.h>

// ============================================================================
// FORWARD DECLARATIONS
// ============================================================================
class ACE_Allocator;

namespace asl {

// ============================================================================
//! The ASL generic container.  
// ============================================================================
//!  
//! This class provides a way to encapsulate any type of data into a Message.
//! 
// ============================================================================
template <class T> 
class ASL_EXPORT Container : public Data
{
public:

  /**
   * Constructor.
   * @param _data the data to encapsulate.
   */
  Container (T& _data);
  
  /**
   * Destructor.
   */
  virtual ~Container();

  /**
   * Return the size of the encapsulated data in bytes.
   * @return size of the encapsulated data in bytes
   */
  virtual size_t size() const;

  /**
   * Return the container's content.
   * @return a reference to the container's content
   */
  T& content();

private:

  /**
   * the encapsulated data.
   */
  T data_;
};

// ============================================================================
//! The ASL buffer abstraction base class.  
// ============================================================================
//!  
//! Just a base class for template class Buffer (see asl::Buffer). 
//! 
// ============================================================================
class ASL_EXPORT BaseBuffer : public Data
{
public:
  /**
   * Constructor. 
   */
  BaseBuffer () : Data() 
    {/*noop*/};

  /**
   * Destructor. 
   */
  virtual ~BaseBuffer ()
    { /*noop*/};
		
	/**
   * Returns the maximum number of element that can be stored into 
   * the buffer. 
   * @return the buffer depth. 
   */
  virtual unsigned long depth () const = 0;
};

// ============================================================================
//! The ASL buffer abstraction.  
// ============================================================================
//!  
//! This template class provides a buffer abstraction. 
//! <operator=> must be defined for template parameter T.
//! 
// ============================================================================
template <class T> 
class ASL_EXPORT Buffer : public BaseBuffer
{
public:

  /**
   * Constructor. 
   * @param  depth the maximum number of element of type T 
   *         that can be stored into the buffer 
   * @param  alloc an ACE allocator, use default ACE_Allocator::instance 
   *        otherwise
   */
  Buffer (unsigned long depth, ACE_Allocator *alloc = 0);
 
  /**
   * Memory copy constructor. Memory is copied from _base to _base + _depth.
   * @param  depth the maximum number of element of type T 
   *         that can be stored into the buffer. 
   * @param  base address of the block to copy.
   * @param  alloc an ACE allocator, use default ACE_Allocator::instance. 
   *         otherwise
   */
  Buffer (unsigned long depth, T *base, ACE_Allocator *alloc = 0);

  /**
   * Copy constructor. Use allocator associated with the source buffer.
   * @param  buf the source buffer.
   */
  Buffer (const Buffer<T> &buf);

  /**
   * Destructor. Release resources.
   */
  virtual ~Buffer ();

  /**
   * operator= 
   */
  Buffer<T>& operator= (const Buffer<T> &src);

  /**
   * operator=. Memory is copied from base to base + Buffer::depth_. 
   * @param base address of the block to copy.
   */
  Buffer<T>& operator= (const T *base);

  /**
   * operator=. Fill the buffer with a specified value.
   * @param val the value.
   */
  Buffer<T>& operator= (const T &val);
   
  /**
   * Fills the buffer with a specified value.
   * @param val the value.
   */
  void fill (const T& val);
  
  /**
   * Clears buffer's content. This is a low level clear: set memory
   * from Buffer::base_ to Buffer::base_ + Buffer::depth_ to 0.
   */
  virtual void clear ();

  /**
   * Returns a reference to the _ith element. No bound error checking.
   * @param i index of the element to return.
   * @return a reference to the ith element.
   */
  T& operator[] (unsigned long i);

  /**
   * Returns a copy of the _ith element. No bound error checking.
   * @param i index of the element to return.
   * @return a copy of to the _ith element.
   */
  T operator[] (unsigned long i) const;

  /**
   * Returns the size of each element in bytes.
   * @return sizeof(T).
   */
  size_t elem_size () const;

  /**
   * Returns the actual size of the buffer in bytes. 
   * @return the buffer size in bytes.
   */
  virtual size_t size () const;

  /**
   * Returns the maximum number of element that can be stored into 
   * the buffer. 
   * @return the buffer depth. 
   */
  virtual unsigned long depth () const;
  
  /**
   * Returns the buffer base address. 
   * @return the buffer base address. 
   */
  T * base () const;
  
  /**
   * Returns the buffer allocator. 
   * @return the buffer allocator. 
   */
  ACE_Allocator * allocator () const;

protected:

  /**
   * the buffer base address. 
   */
  T * base_;

  /**
   * maximum number of element of type T.
   */
  unsigned long depth_;

private:
  /**
   * underlying allocator. 
   */
  ACE_Allocator * alloc_;
};

// ============================================================================
//! The ASL ciruclar buffer abstraction.  
// ============================================================================
//!  
//! This template class provides a  (write only) circular buffer abstraction. 
//! <operator=> must be defined for template parameter T.
//! 
// ============================================================================
template <class T> 
class ASL_EXPORT CircularBuffer : public Buffer<T>
{
public:
 /**
   * Constructor. 
   * @param  depth the maximum number of element of type T 
   *         that can be stored into the buffer 
   * @param  alloc an ACE allocator, use default ACE_Allocator::instance 
   *        otherwise
   */
  CircularBuffer (unsigned long modulo, unsigned long factor, ACE_Allocator *alloc = 0);

  /**
   * Destructor. Release resources.
   */
  virtual ~CircularBuffer ();

  /**
   * Clears buffer's content.
   */
  virtual void clear ();

  /**
   * Pushes the specified data into the circular buffer.
   * The data buffer must have 
   */
  void push (Buffer<T> * data)
	  throw (asl::DAQException);

  /**
   * Freezes the buffer. 
   * Any data pushed into a frozen circular buffer is silently ignored.  
   */
  void freeze ();

  /**
   * Unfreeze 
   * Data pushed into a frozen circular buffer is silently ignored (see CircularBuffer::freeze).  
   */
  void unfreeze ();

  /**
   * Returns the "chronologically ordered" circular buffer's content.
   * Caller gets ownership of the returned buffer (i.e. should delete it)  
   */
  asl::Buffer<T> * ordered_data ()
	   throw (asl::DAQException);

  /**
   * Returns the modulo  
   */
  unsigned long modulo () const;


   /**
   * Returns the factor  
   */
  unsigned long factor () const;

private:
  /**
   * The write pointer
   */
  T * wp_; 

  /**
   * Frozen flag
   */
  bool frozen_;

  /**
   * Modulo
   */
  unsigned long modulo_;

  /**
   * Factor
   */
  unsigned long factor_;

  /**
   * A lock to protect the circular buffer against race conditions.
   */
  ACE_Thread_Mutex lock_;

  // = Disallow these operations.
  //--------------------------------------------
  ACE_UNIMPLEMENTED_FUNC(CircularBuffer& operator= (const CircularBuffer&))
  ACE_UNIMPLEMENTED_FUNC(CircularBuffer(const CircularBuffer&))
};

} // namespace asl

#if defined (__ASL_INLINE__)
# include <asl/Data_T.i>
#endif // __ASL_INLINE__

#if defined (ACE_TEMPLATES_REQUIRE_SOURCE)
#if defined (ASL_BUILD) || defined (ASL_BUILD_TEST)
# include <asl/Data_T.hpp>
#else
# include <asl/Data_T.hpp>
#endif
#endif // ACE_TEMPLATES_REQUIRE_SOURCE

#if defined (ACE_TEMPLATES_REQUIRE_PRAGMA)
#if defined (ASL_BUILD) || defined (ASL_BUILD_TEST)
# pragma implementation (<asl/Data_T.hpp>)
#else
# pragma implementation (<asl/Data_T.hpp>)
#endif
#endif // ACE_TEMPLATES_REQUIRE_PRAGMA

#endif // _DATA_T_H_

