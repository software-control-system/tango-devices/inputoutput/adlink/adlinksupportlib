// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ContinuousDIConfig.i
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

namespace asl {

// ============================================================================
// ContinuousDIConfig::set_sampling_rate
// ============================================================================
ASL_INLINE void 
ContinuousDIConfig::set_sampling_rate (double _sr)
{
  this->sampling_rate_ = _sr;
}

// ============================================================================
// ContinuousDIConfig::get_sampling_rate
// ============================================================================
ASL_INLINE double 
ContinuousDIConfig::get_sampling_rate () const
{
  return this->sampling_rate_;
}

// ============================================================================
// ContinuousDIConfig::set_port_width
// ============================================================================
ASL_INLINE void 
ContinuousDIConfig::set_port_width (adl::DIOPortWidth _port)
{
  this->port_width_ = _port;
}

// ============================================================================
// ContinuousDIConfig::get_port_width
// ============================================================================
ASL_INLINE  adl::DIOPortWidth 
ContinuousDIConfig::get_port_width () const
{
  return this->port_width_;
}

// ============================================================================
// ContinuousDIConfig::set_trigger_source
// ============================================================================
ASL_INLINE  void 
ContinuousDIConfig::set_trigger_source (adl::DIOTriggerSource _trig_src)
{
  this->trigger_source_ = _trig_src;
}

// ============================================================================
// ContinuousDIConfig::get_trigger_source
// ============================================================================
ASL_INLINE  adl::DIOTriggerSource 
ContinuousDIConfig::get_trigger_source () const
{
  return this->trigger_source_;
}

// ============================================================================
// ContinuousDIConfig::set_start_mode
// ============================================================================
ASL_INLINE  void 
ContinuousDIConfig::set_start_mode (adl::StartMode _mode)
{
  this->start_mode_ = _mode;
}

// ============================================================================
// ContinuousDIConfig::get_start_mode
// ============================================================================
ASL_INLINE  adl::StartMode 
ContinuousDIConfig::get_start_mode () const
{
  return this->start_mode_;
}

// ============================================================================
// ContinuousDIConfig::set_terminator
// ============================================================================
ASL_INLINE  void 
ContinuousDIConfig::set_terminator (adl::TerminatorMode _mode)
{
  this->terminator_ = _mode;
}

// ============================================================================
// ContinuousDIConfig::get_terminator
// ============================================================================
ASL_INLINE  adl::TerminatorMode 
ContinuousDIConfig::get_terminator () const
{
  return this->terminator_;
}

// ============================================================================
// ContinuousDIConfig::set_request_polarity
// ============================================================================
ASL_INLINE  void 
ContinuousDIConfig::set_request_polarity (adl::RequestPolarity _pol)
{
  this->request_polarity_ = _pol;
}

// ============================================================================
// ContinuousDIConfig::get_request_polarity
// ============================================================================
ASL_INLINE  adl::RequestPolarity 
ContinuousDIConfig::get_request_polarity () const
{
  return this->request_polarity_;
}

// ============================================================================
// ContinuousDIConfig::set_ack_polarity
// ============================================================================
ASL_INLINE  void 
ContinuousDIConfig::set_ack_polarity (adl::AcknowledgementPolarity _pol)
{
  this->ack_polarity_ = _pol;
}

// ============================================================================
// ContinuousDIConfig::get_ack_polarity
// ============================================================================
ASL_INLINE  adl::AcknowledgementPolarity 
ContinuousDIConfig::get_ack_polarity () const
{
  return this->ack_polarity_;
}

// ============================================================================
// ContinuousDIConfig::set_trigger_polarity
// ============================================================================
ASL_INLINE  void 
ContinuousDIConfig::set_trigger_polarity (adl::TriggerPolarity _pol)
{
  this->trigger_polarity_ = _pol;
}

// ============================================================================
// ContinuousDIConfig::get_trigger_polarity
// ============================================================================
ASL_INLINE  adl::TriggerPolarity 
ContinuousDIConfig::get_trigger_polarity () const
{
  return this->trigger_polarity_;
}
// ============================================================================
// ContinuousDIConfig::set_data_lost_strategy 
// ============================================================================
ASL_INLINE void 
ContinuousDIConfig::set_data_lost_strategy (adl::DataLostStrategy _strategy)
{
  this->data_lost_strategy_ = _strategy;
}

// ============================================================================
// ContinuousDIConfig::get_data_lost_strategy
// ============================================================================
ASL_INLINE adl::DataLostStrategy 
ContinuousDIConfig::get_data_lost_strategy () const
{
  return data_lost_strategy_;
}

// ============================================================================
// ContinuousDIConfig::set_buffer_depth
// ============================================================================
ASL_INLINE void 
ContinuousDIConfig::set_buffer_depth (unsigned long _depth)
{
  this->buffer_depth_ = _depth;
}

// ============================================================================
// ContinuousDIConfig::get_buffer_depth
// ============================================================================
ASL_INLINE unsigned long 
ContinuousDIConfig::get_buffer_depth () const
{
  return this->buffer_depth_;
}

// ============================================================================
// ContinuousDIConfig::set_db_event_callback
// ============================================================================
ASL_INLINE void
ContinuousDIConfig::set_db_event_callback (DAQCallback _cb)
{
  this->db_event_callback_ = _cb;
}

// ============================================================================
// ContinuousDIConfig::get_db_event_callback
// ============================================================================
ASL_INLINE DAQCallback 
ContinuousDIConfig::get_db_event_callback () const
{
  return this->db_event_callback_;
}

// ============================================================================
// ContinuousDIConfig::set_di_end_event_callback
// ============================================================================
ASL_INLINE void
ContinuousDIConfig::set_di_end_event_callback (DAQCallback _cb)
{
	this->di_end_event_callback_ = _cb;
}

// ============================================================================
// ContinuousDIConfig::get_di_end_event_callback
// ============================================================================
ASL_INLINE DAQCallback 
ContinuousDIConfig::get_di_end_event_callback () const
{
	return this->di_end_event_callback_;
}

// ============================================================================
// ContinuousDIConfig::set_timeout
// ============================================================================
ASL_INLINE void 
ContinuousDIConfig::set_timeout (long _tmo_sec)
{
  this->timeout_ = (_tmo_sec >= 1) ? _tmo_sec : 1;
}

// ============================================================================
// ContinuousDIConfig::get_timeout
// ============================================================================
ASL_INLINE long
ContinuousDIConfig::get_timeout () const
{
  return this->timeout_;
}

} // namespace asl







