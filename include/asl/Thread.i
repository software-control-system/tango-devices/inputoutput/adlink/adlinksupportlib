// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    Thread.i
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

namespace asl {

// ============================================================================
// Thread::quit_requested
// ============================================================================
ASL_INLINE bool 
Thread::quit_requested () const
{
	return this->go_on_ ? false : true;
}

// ============================================================================
// Thread::id
// ============================================================================
ASL_INLINE ACE_thread_t 
Thread::id () const
{
	return this->self_idt_;
}

} // namespace asl

