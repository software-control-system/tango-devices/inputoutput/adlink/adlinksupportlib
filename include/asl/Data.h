// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    Data.h
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

#ifndef _DATA_H_
#define _DATA_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/ASLConfig.h>
#if defined(ASL_DEBUG)
# include <asl/Mld.h>
#endif

namespace asl {

// ============================================================================
//! The ASL data abstraction base class.  
// ============================================================================
//!  
//! Base class of any ASL or user defined data encapsulated in a Message.
//! 
// ============================================================================
class ASL_EXPORT Data
{
public:

  /**
   * Initialization. 
   */
  Data ();
  
  /**
   * Release resources.
   */
  virtual ~Data ();

  /**
   * Return next data in the continuation field, 0 otherwise. 
   * The current implementation is not thread safe.
   * @return next Data in the continuation field. 
   */
  Data* cont () const;

  /**
   * Set next data in the continuation field. 
   * The current implementation is not thread safe.
   * @param cont next Data in the continuation field.
   */
  void cont (Data *cont);

  /**
   * Return the size (in bytes) of the actual data. 
   * Any derivated class must implement this pure virtual method. 
   * @return size of underlying data in bytes.
   */
  virtual size_t size () const = 0;

  /**
   * Return the total number of bytes in the continuation field.
   * @return number of bytes in the continuation field.
   */
  size_t total_size () const;

#if defined(ASL_DEBUG)
  /**
   * Return the total number of objects currently instanciated.
   * @return current number of Data instances
   */
  static u_long instance_counter();
#endif
  
private:

  /**
   * Continuation field : chains together composite data.
   */
  Data * cont_;

#if defined(ASL_DEBUG)
  /**
   * Instances counter.
   */
  static AtomicOp instance_counter_;
#endif

#if defined(ASL_DEBUG)
  /**
   * Memory leak detector.
   */
  MLD; 
#endif

  // = Disallow these operations.
  //--------------------------------------------
  ACE_UNIMPLEMENTED_FUNC(Data& operator= (const Data&))
  ACE_UNIMPLEMENTED_FUNC(Data(const Data&))
};

} // namespace asl

#if defined (__ASL_INLINE__)
# include "asl/Data.i"
#endif // __ASL_INLINE__

#endif // _DATA_H_

