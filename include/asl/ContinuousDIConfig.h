// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ContinuousDI.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _CONTINUOUS_DI_CONFIG_H_
#define _CONTINUOUS_DI_CONFIG_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/ASLConfig.h>
#include <asl/adlink/ADLink.h>
#include <asl/DAQException.h>

namespace asl { 

// ============================================================================
// Create a dedicated type for DAQ events callback
// ============================================================================
#if defined(WIN32)
  typedef void (*DAQCallback) ();
#else
  typedef void (*DAQCallback) (int);
#endif
// ============================================================================
//! The ASL continuous analog input configuration class.  
// ============================================================================
//! Configure all the parameters needed by the hardware in continuous digital input. \n
//! The user must be aware that knowing the hardware is essential to have the desired input.
//! All the parameters have got values by default.
// ============================================================================
class ASL_EXPORT ContinuousDIConfig  
{
public:

  /**
   * Constructor.
   */
  ContinuousDIConfig ();

  /**
   * Copy constructor.
   * @param src The config to copy.
   */
  ContinuousDIConfig (const ContinuousDIConfig& src);

  /**
   * Redefinition of the operator =.
   */
  ContinuousDIConfig& operator= (const ContinuousDIConfig& src);

  /**
   * Destructor.
   */
  virtual ~ContinuousDIConfig();

  /**
   * Configure the sampling rate in Hz.
   * @param _sr The sampling rate in hertz.
   */
  void set_sampling_rate (double _sr);
  /**
  * Get sampling rate.
  * @return The sampling rate in hertz.
  */
  double get_sampling_rate () const;

  /**
   * Configure the data lost strategy.
   * @param _strategy The strategy applied if overrun occurs.
   */
  void set_data_lost_strategy (adl::DataLostStrategy _strategy);
  /**
  * Get the data lost strategy.
  * @return The strategy.
  */
  adl::DataLostStrategy get_data_lost_strategy () const;

  /**
   * Configure the width of the output port (16 bits by default).
   * @param _port The width of the port in bits.
   */
  void set_port_width(adl::DIOPortWidth _port);
  /**
  * Get port width
  * @return the current width
  */
  adl::DIOPortWidth get_port_width() const;

  /**
   * Configure the trigger source (on board programmable pacer timer by default).
   * @param _trig The trigger source.
   */
  void set_trigger_source(adl::DIOTriggerSource _trig);
  /**
  * Get the trigger source.
  * @return The trigger source.
  */
  adl::DIOTriggerSource get_trigger_source() const;
 
  /**
   * Configure start mode (the input sampling starts immediatly by default).
   * @param _mode Start mode.
   */
  void set_start_mode(adl::StartMode _mode);
  /**
  * Get start mode.
  * @return Start mode.
  */
  adl::StartMode get_start_mode() const;

  /**
   * Configure terminator mode (on by default).
   * @param _term The terminator mode. 
   */
  void set_terminator(adl::TerminatorMode _term);
  /**
  * Get terminator mode.
  * @return terminator mode.
  */
  adl::TerminatorMode get_terminator() const;

  /**
   * Configure the request signal polarity (rising edge by default).
   * @param _pol The polarity.
   */
  void set_request_polarity (adl::RequestPolarity _pol);
  /**
  * Get the request signal polarity.
  * @return The polarity.
  */
  adl::RequestPolarity get_request_polarity () const;

  /**
   * Configure the acknowledgment signal polarity. (rising edge by default).
   * @param _pol The polarity.
   */
  void set_ack_polarity (adl::AcknowledgementPolarity _pol);
  /**
  * Get the acknowledgment signal polarity.
  * @return The polarity.
  */
  adl::AcknowledgementPolarity get_ack_polarity () const;
  /**
   * Configure the trigger polarity (rising edge by default).
   * @param _pol The polarity.
   */
  void set_trigger_polarity (adl::TriggerPolarity _pol);
  /**
  * Get the trigger polarity.
  * @return The polarity.
  */
  adl::TriggerPolarity get_trigger_polarity () const;
  /**
   * Configure the DAQ buffer depth.
   * @param _depth DAQ buffer depth in samples.
   */
  void set_buffer_depth (unsigned long _depth);
  /**
   * Get the buffer depth
   * @return The depth
   */
  unsigned long get_buffer_depth () const;
  /**
   * Configure double-buffering event call back.
   * @param _cb The call back address
   */
  void set_db_event_callback (DAQCallback _cb);
  /**
   * Get double buffering event call back address.
   * @return The call back address.
   */
  DAQCallback get_db_event_callback () const;
  /**
   * Configure end-of-daq event call back.
   * @param _cb The call back address
   */
  void set_di_end_event_callback (DAQCallback _cb);
  /**
   * Get end-of-daq callback address.
   * @return The call back address.
   */
  DAQCallback get_di_end_event_callback () const;
  /**
   * Configure timeout 
   * @param _tmo_sec The timeout in seconds. 
   */
  void set_timeout (long _tmo_sec);
  /**
   * Get timeout 
   * @return The timeout in seconds
   */
  long get_timeout () const;
  /**
   * Print out the configuration
   */
  void dump (); 

private:

  //- internal sampling rate
  double sampling_rate_;

  //-port width
  adl::DIOPortWidth port_width_;

  //- trigger source
  adl::DIOTriggerSource trigger_source_;
    
  //- start mode
  adl::StartMode start_mode_;

  //- terminator
  adl::TerminatorMode terminator_;

  //- REQ polarity
  adl::RequestPolarity request_polarity_;

  //- ACK polarity
  adl::AcknowledgementPolarity ack_polarity_;

  //- trigger polarity
  adl::TriggerPolarity trigger_polarity_;

  //- data_lost strategy
  adl::DataLostStrategy data_lost_strategy_;

  //- db-buffer depth in samples
  unsigned long buffer_depth_;

  //- db-event callback address
  DAQCallback db_event_callback_;

  //- di-end-event callback address
  DAQCallback di_end_event_callback_;

  //- daq timeout in seconds 
  long timeout_;
};

} // namespace asl

#if defined (__ASL_INLINE__)
# include "asl/ContinuousDIConfig.i"
#endif // __ASL_INLINE__

#endif // _CONTINUOUS_DI_CONFIG_H_

