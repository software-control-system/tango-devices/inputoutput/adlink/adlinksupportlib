// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    PulsedTask.h
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

#ifndef _PULSED_TASK_H_
#define _PULSED_TASK_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <ace/Event_Handler.h>
#include <asl/ASLConfig.h>
#if defined(ASL_DEBUG)
# include <asl/Mld.h>
#endif

namespace asl {

// ============================================================================
//! The ASL pulsed task abstraction class [ABSTRACT].  
// ============================================================================
//!  
//! detailed description to be defined
//! 
// ============================================================================
class ASL_EXPORT PulsedTask : public ACE_Event_Handler 
{
   typedef ACE_Event_Handler inherited;

   friend class PulsedTaskManager;

public:
  /**
   * Initialization. 
   */
  PulsedTask ();

  /**
   * Release resources.
   */
  virtual ~PulsedTask ();

  /**
   * User defined behaviour. Executed when Task is pulsed.
   *
   * \param arg The PulsedTask argument (pointer to user data)
   */
  virtual int pulsed (void* arg) = 0;

  /**
   * Start the PulsedTask.
   *
   * \param arg The task generic argument (passed to the user defined 
   *  PulsedTask::pulsed).
   *
   * \param pulse_interval The pulse period in milliseconds (defaults to 
   *  1 second).
   *
   * \param pulse_count The number of times the Task should be executed. 
   *  Pass 0 for infinite (re)trigger (the default).
   *
   * \return -1 on failure, 0 otherwise.
   */
  int start (void *arg, 
             unsigned long pulse_interval = 1000,
             unsigned long pulse_count = 0);

  /**
   * Stop the PulsedTask.  
   *
   * \return -1 on failure, 0 otherwise.
   */
  int stop ();

#if defined(ASL_DEBUG)
  /**
   * Returns the current instance counter value
   */
  static unsigned long instance_counter();
#endif

  //- ASL internal cooking. 
  //- Consider this member as private and do not call it directly.
  int handle_timeout (const ACE_Time_Value&, const void* = 0);

private:

  /**
   * The number of times the task should be executed.
   */
   unsigned long max_count_;

  /**
   * The number of times the task has been be executed.
   */
   unsigned long cur_count_;

  /**
   * The timer identifier.
   */
   long timer_;

  /**
   * Protect the task against race consition
   */
   ACE_Thread_Mutex lock_;

#if defined(ASL_DEBUG)
  /**
   * Instances counter.
   */
  static AtomicOp instance_counter_;

  /**
   * Memory leak detector.
   */
  MLD; 
#endif

  // = Disallow these operations.
  //--------------------------------------------
  ACE_UNIMPLEMENTED_FUNC(PulsedTask& operator= (const PulsedTask&))
  ACE_UNIMPLEMENTED_FUNC(PulsedTask(const PulsedTask&))
};

} // namespace asl

#if defined (__ASL_INLINE__)
# include "asl/PulsedTask.i"
#endif // __ASL_INLINE__

#endif // _PULSED_TASK_H_

