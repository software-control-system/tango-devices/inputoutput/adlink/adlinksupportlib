// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    Message.i
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

namespace asl {

// ============================================================================
// DataBlock::data
// ============================================================================
ASL_INLINE Data *
DataBlock::data () const
{
  return this->data_;
}

// ============================================================================
// DataBlock::data_size
// ============================================================================
ASL_INLINE size_t
DataBlock::data_size () const
{
  return this->data_ ? this->data_->total_size() : 1; 
}

// ============================================================================
// DataBlock::detach_data
// ============================================================================
ASL_INLINE void
DataBlock::detach_data ()
{
  this->flags_ = DataBlock::MSG_DELETE_NONE;
}

// ============================================================================
// Message::duplicate
// ============================================================================
ASL_INLINE Message *
Message::duplicate ()
{
  Message *dup = ACE_reinterpret_cast(Message*, inherited::duplicate());
  //- WARNING: the message is copied, but the data block is not. 
  //- Consequently, all the <Message> data member should be copied
  //- into the new instance in order to reflect the exact internal
  //- state of <this> instance. For instance...  
  if (dup) {
    dup->force_processing_ = this->force_processing_;
  }
  return dup;
}

// ============================================================================
// Message::cont
// ============================================================================
ASL_INLINE void 
Message::cont (Message * _cont)
{
  inherited::cont(_cont);
}

// ============================================================================
// Message::cont
// ============================================================================
ASL_INLINE Message *
Message::cont () const
{
  return ACE_reinterpret_cast(Message*, inherited::cont());
}

// ============================================================================
// Message::is_a
// ============================================================================
ASL_INLINE bool
Message::is_a (int _type) const
{
  return (this->msg_type() == _type) ? true : false;
}

// ============================================================================
// Message::force_processing
// ============================================================================
ASL_INLINE bool
Message::force_processing ()
{
 return this->force_processing_;
}

// ============================================================================
// Message::data_size
// ============================================================================
ASL_INLINE size_t 
Message::data_size () const
{
  return this->data_block() 
          ? 
         (ACE_reinterpret_cast(DataBlock*, this->data_block()))->data_size()
          :
         sizeof(Message); 
}

// ============================================================================
// Message::detach_data
// ============================================================================
ASL_INLINE void
Message::detach_data ()
{
  DataBlock * db = ACE_reinterpret_cast(DataBlock *, this->data_block());
  if (db) {
    db->detach_data();
  }
}

} // namespace asl
