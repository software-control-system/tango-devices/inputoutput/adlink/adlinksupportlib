// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ContinuousDAQ.i
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

namespace asl {

// ============================================================================
// ContinuousDAQ::lock
// ============================================================================
ASL_INLINE int 
ContinuousDAQ::lock ()
{
  return this->lock_.acquire();
}

// ============================================================================
// ContinuousDAQ::unlock
// ============================================================================
ASL_INLINE int 
ContinuousDAQ::unlock ()
{
  return this->lock_.release();
}

// ============================================================================
// ContinuousDAQ::state
// ============================================================================
ASL_INLINE int 
ContinuousDAQ::state () const
{
  if (exception_status_ != HANDLING_NONE)
  {
	  switch (exception_status_)
    {
      case HANDLING_ERROR:
      case HANDLING_OVERRUN:
        return RUNNING;
        break;
      case HANDLING_CALIBRATION:
        return CALIBRATING_HW;
        break;
      default:
        break;
    }
  }
  return this->state_;
}

// ============================================================================
// ContinuousDAQ::action_allowed
// ============================================================================
ASL_INLINE void
ContinuousDAQ::action_allowed (const char * origin) const 
  throw (asl::DeviceBusyException, asl::DAQException)
{
  this->abort_request_if_busy(origin);
  this->abort_request_if_bad_state(origin);
}

// ============================================================================
// ContinuousDAQ::abort_request_if_busy
// ============================================================================
ASL_INLINE void
ContinuousDAQ::abort_request_if_busy (const char * origin) const 
  throw (asl::DeviceBusyException)
{
  switch (exception_status_)
  {
    case HANDLING_ERROR:
    case HANDLING_OVERRUN:
      {
        throw asl::DeviceBusyException("device busy (handling exception)",
                                       "could not execute requested action - device is busy (handling exception)",
                                       origin);
      }
      break;
    case HANDLING_CALIBRATION:
      {
        throw asl::DeviceBusyException("device busy (calibrating DAQ hardware)",
                                       "could not execute requested action - device is busy (calibrating DAQ hardware)",
                                       origin);
      }
      break;
    case HANDLING_NONE:
    default:
      break;
  }
}

// ============================================================================
// ContinuousDAQ::abort_request_if_bad_state
// ============================================================================
ASL_INLINE void
ContinuousDAQ::abort_request_if_bad_state (const char * origin) const 
  throw (asl::DAQException)
{
  switch (this->state_)
  {
    case FAULT:
      {
        throw asl::DAQException("bad state",
                                "could not execute requested action - a fatal error occured",
                                origin);
      }
      break;
    case UNKNOWN:
      {
        throw asl::DAQException("bad state",
                                "could not execute requested action - DAQ is not properly initialized",
                                origin);
      }
      break;
    default:
      break;
  }
}

} // namespace asl


