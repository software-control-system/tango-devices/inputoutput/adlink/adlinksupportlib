// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    SingleShotDAQ.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _SINGLE_SHOT_DAQ_H_
#define _SINGLE_SHOT_DAQ_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/DAQException.h>

namespace asl {

// ============================================================================
//! The ASL SingleShotDAQ abstraction base class.  
// ============================================================================
//! Base class for all classes that perform continuous aquisition: 
//! - Single Shot AI
//! - Single Shot AO
//! - Single Shot DI
//! - Single Shot DO
// ============================================================================
class ASL_EXPORT SingleShotDAQ
{
public:

  /**
   * Initialization. 
   */
  SingleShotDAQ ();
  
  /**
   * Release resources.
   */
  virtual ~SingleShotDAQ ();

  /**
   * Initialize continuous DAQ.
   */
  virtual void init (unsigned short hw_type, unsigned short hw_id) 
    throw (DAQException) = 0;

#if defined(ASL_DEBUG)
  /**
   * Return the total number of objects currently instanciated.
   *
   * \return current number of SingleShotDAQ instances
   */
  static u_long instance_counter();
#endif
  
private:

#if defined(ASL_DEBUG)
  /**
   * Instances counter.
   */
  static AtomicOp instance_counter_;

  /**
   * Memory leak detector.
   */
  MLD; 
#endif

  // = Disallow these operations.
  //--------------------------------------------
  ACE_UNIMPLEMENTED_FUNC(SingleShotDAQ& operator= (const SingleShotDAQ&))
  ACE_UNIMPLEMENTED_FUNC(SingleShotDAQ(const SingleShotDAQ&))
};

} // namespace asl

#if defined (__ASL_INLINE__)
# include "asl/SingleShotDAQ.i"
#endif // __ASL_INLINE__

#endif // _SINGLE_SHOT_DAQ_H_

