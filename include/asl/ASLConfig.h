// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    Config.h
//
// = AUTHOR
//    Nicolas Leclercq
//
// ===========================================================================
//-----------------------------------------------------------------------------
// WINDOWS PRAGMA
//-----------------------------------------------------------------------------
#if defined (WIN32)
# pragma warning( disable : 4786 )
# pragma warning( disable : 4290 )
# pragma warning( disable : 4996 )
#endif

//-----------------------------------------------------------------------------
// WIN32 DLL EXPORT STUFFS
//-----------------------------------------------------------------------------
#include <asl/Export.h>

//-----------------------------------------------------------------------------
// COMMON ACE INCLUDES
//-----------------------------------------------------------------------------
#include <ace/Synch.h>
#include <ace/Atomic_Op_T.h>
#include <ace/OS_NS_sys_time.h> 

//-----------------------------------------------------------------------------
// TYPEDEFS
//-----------------------------------------------------------------------------
typedef ACE_Time_Value Timeout;
typedef ACE_Atomic_Op<ACE_Thread_Mutex, size_t> AtomicOp;


//-----------------------------------------------------------------------------
// DEBUG
//-----------------------------------------------------------------------------
#if defined(WIN32) 
# if defined(_DEBUG)
#   define ASL_DEBUG
# else
#   undef ASL_DEBUG
# endif
#else
# if defined(DEBUG)
#   define ASL_DEBUG
# else
#   undef ASL_DEBUG
# endif
#endif

//-----------------------------------------------------------------------------
// INLINE
//-----------------------------------------------------------------------------
#if !defined(ASL_DEBUG)
# define __ASL_INLINE__
# define ASL_INLINE inline
#else
# undef __ASL_INLINE__
# define ASL_INLINE
#endif

//-----------------------------------------------------------------------------
// NEW MACRO: DEALS WITH EXCEPTION THROWING ON ALLOCATION FAILURE
//-----------------------------------------------------------------------------
#define ASL_NEW ACE_NEW_NORETURN






