// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    AD7300.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _ADLINK_7300_H_
#define _ADLINK_7300_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/ASLConfig.h>
#include <map>
#include <asl/adlink/ADSingleShotDIBoard.h>
#include <asl/adlink/ADSingleShotDOBoard.h>
#include <asl/adlink/ADContinuousDIBoard.h>
#include <asl/DIData.h>
#include <asl/ContinuousDIConfig.h>

namespace adl {

// ============================================================================
//! The ADLink 7300 general purpose DAQ card abstraction.  
// ============================================================================
//!  This board is able to perform the following features:
//! - SingleShot DI/DO.
//! - Continous DI.
// ============================================================================
class ASL_EXPORT AD7300 : public ADSingleShotDIBoard,
                          public ADSingleShotDOBoard,
                          public ADContinuousDIBoard 
{
  typedef std::map<unsigned short, AD7300*> Repository;

  typedef Repository::iterator RepositoryIterator;

  typedef Repository::value_type RepositoryValue;

public:

  /**
   * Instanciate an AD7300 board.
   * @param _id The number of the card in the cPCI crate.
   * @param _exclusive_access Allows or not the exclusive access (no exclusive access by default).
   * @return A pointer to the card.
   */
  static AD7300* instanciate (unsigned short _id, bool _exclusive_access = false) 
    throw (asl::DAQException);

  /**
   * Duplicate this shared object.
   */
   AD7300 * duplicate ();
  
   /**
   * Continuous DI configuration.
   * @param config The configuration.
   */
  virtual void configure_continuous_di (const asl::ContinuousDIConfig& config) 
    throw (asl::DAQException);

  /**
   * Configure a DIO port for input operation.
   * @param _port The port to configure.
   * @see adl::DIOPort enumeration.
   */
	virtual void configure_di_port (adl::DIOPort _port) 
    throw (asl::DAQException);

  /**
   * Configure a DIO port for output operation.
   * @param _port The port to configure.
   * @see adl::DIOPort enumeration.
   */
	virtual void configure_do_port (adl::DIOPort _port) 
    throw (asl::DAQException);

  /**
   * Read a single DI line.
   * @param _port The line's port.
   * @see adl::DIOPort enumeration.
   * @param _line The line identifier.
   * @return The read value.
   */
	virtual bool read_line (adl::DIOPort _port, int _line) 
    throw (asl::DAQException);

  /**
   * Read a DI port.
   * @param _port The port identifier.
   * @see adl::DIOPort enumeration.
   * @return The read value.
   */
	virtual unsigned long read_port (adl::DIOPort _port) 
    throw (asl::DAQException);

  /**
   * Write on a single DI line.
   * @param _port The line's port.
   * @see adl::DIOPort enumeration.
   * @param _line The line identifier.
   * @param _state The new line's state.
   */
	virtual void write_line (adl::DIOPort _port, int _line, bool _state) 
    throw (asl::DAQException);

  /**
   * Write on DI port.
   * @param _port The port identifier.
   * @see adl::DIOPort enumeration.
   * @param _state The output value.
   */
	virtual void write_port (adl::DIOPort _port, unsigned long _state) 
    throw (asl::DAQException);

  /**
   * Dump hardware info.
   */
  virtual void dump () const;

private:

  /**
   * Constructor.
   */
	AD7300 (unsigned short _id);

  /**
   * Destructor.
   */
  virtual ~AD7300();
  
  /**
   * Register the board with the driver.
   * @return The id of the card defined by the driver.
   */
	virtual int register_hardware ();

  /**
   * Unregister the board with the driver.
   */
	virtual int release_hardware ();

  /**
   * Validate DI port identifier
   */
  virtual void check_di_port (adl::DIOPort _port)
    throw (asl::DAQException);

  /**
   * Validate DI line identifier
   */
  virtual void check_di_line (int line)
    throw (asl::DAQException);

  /**
   * Validate DO port identifier
   */
  virtual void check_do_port (adl::DIOPort _port)
    throw (asl::DAQException);

  /**
   * Validate DO line identifier
   */
  virtual void check_do_line (int line)
    throw (asl::DAQException);

  /**
   * AD7300 boards repository.
   */
  static Repository repository;
	
  /**
   * Boards repository' lock.
   */
  static ACE_Recursive_Thread_Mutex repository_lock;
};

} // namespace adl

#endif // _ADLINK_7300_H_
