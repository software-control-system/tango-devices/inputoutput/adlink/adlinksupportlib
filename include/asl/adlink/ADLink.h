// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ADD2kDask.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _ADLINK_H_
#define _ADLINK_H_

// ============================================================================
// WIN32 SPECIFIC DECLARATION
// ============================================================================
#if defined(WIN32)
  typedef void *HANDLE;
  typedef unsigned char BOOLEAN;
#endif

// ============================================================================
// DEPENDENCIES
// ============================================================================
//- include ASL stuffs
#include <asl/ASLConfig.h>

//- include pci-dask stuffs
#include <dask.h>

//- avoid define conflict
#undef GPTC_GATESRC_EXT

//- include d2k-dask stuffs
#include <d2kdask.h>

#define kAD6208_OFFSET_ID 100

namespace adl {

// ============================================================================
// ADLink boards
// ============================================================================
/**
* The ADLink boards identifiers.
*/
typedef enum 
{
//- PCIS-DASK boards --------------
//---------------------------------
  PCI6208 = PCI_6208V + kAD6208_OFFSET_ID,
  PCI6216 = PCI_6208V + kAD6208_OFFSET_ID + 1,
	PCI7300 = PCI_7300A_RevB,
	PCI7432 = PCI_7432,
	PCI7248 = PCI_7248,

//- D2K-DASK boards ---------------
//---------------------------------
  DAQ2010 = DAQ_2010,
  DAQ2205 = DAQ_2205,
  DAQ2206 = DAQ_2206,
  DAQ2005 = DAQ_2005,
  DAQ2204 = DAQ_2204,
  DAQ2006 = DAQ_2006,
  DAQ2501 = DAQ_2501,
  DAQ2502 = DAQ_2502,
  DAQ2208 = DAQ_2208
} 
ADBoards;

// ============================================================================
// ADLink: AIO channels configuration: range (bp_: bipolar, up_: unipolar)
// ============================================================================ 
 /**
  * AIO channel ranges (bp_: bipolar, up_: unipolar).
  * The non-commented ranges are not valid for DAQ-2010/2205/2206/2205/2005/2006.
  */
typedef enum 
{
  /**
  * Valid for DAQ-2010/2205/2206/2205/2005/2006.
  */
  bp_10     = AD_B_10_V, 
  /**
  * Valid for DAQ-2010/2205/2206/2205/2005/2006.
  */
  bp_5	    = AD_B_5_V,
  /**
  * Valid for DAQ-2010/2205/2206/2205/2005/2006.
  */
  bp_2_5    = AD_B_2_5_V, 
  /**
  * Valid for DAQ-2010/2205/2206/2205/2005/2006.
  */
  bp_1_25   = AD_B_1_25_V, 
  bp_0_625  = AD_B_0_625_V, 
  bp_0_3125 = AD_B_0_3125_V, 
  bp_0_5    = AD_B_0_5_V, 
  bp_0_05   = AD_B_0_05_V, 
  bp_0_005  = AD_B_0_005_V, 
  bp_1	    = AD_B_1_V, 
  bp_0_1    = AD_B_0_1_V, 
  bp_0_01   = AD_B_0_01_V, 
  bp_0_001  = AD_B_0_001_V, 
  up_20     = AD_U_20_V, 
  /**
  * Valid for DAQ-2010/2205/2206/2205/2005/2006.
  */
  up_10     = AD_U_10_V, 
  /**
  * Valid for DAQ-2010/2205/2206/2205/2005/2006.
  */
  up_5	    = AD_U_5_V,
  /**
  * Valid for DAQ-2010/2205/2206/2205/2005/2006.
  */
  up_2_5    = AD_U_2_5_V,
  /**
  * Valid for DAQ-2010/2205/2206/2205/2005/2006.
  */
  up_1_25   = AD_U_1_25_V, 
  up_1	    = AD_U_1_V, 
  up_0_1    = AD_U_0_1_V, 
  up_0_01   = AD_U_0_01_V, 
  up_0_001  = AD_U_0_001_V, 
  bp_2	    = AD_B_2_V, 
  bp_0_25   = AD_B_0_25_V, 
  bp_0_2    = AD_B_0_2_V,
  up_4	    = AD_U_4_V, 
  up_2	    = AD_U_2_V, 
  up_0_5    = AD_U_0_5_V, 
  up_0_4    = AD_U_0_4_V    
} 
Range;

// ============================================================================
// ADLink: AIO calibration banks
// ============================================================================
/**
* AIO calibration banks.
*/
typedef enum
{
  bank_0 = 0,
  bank_1,    
  bank_2,   
  bank_3,    
} 
AIOCalibrationBank;

// ============================================================================ 
// ADLink: default bank to store calibration data
// ============================================================================ 
#define kDEFAULT_CALIBRATION_BANK adl::bank_0

// ============================================================================
// ADLink: AI channels configuration: ground reference
// ============================================================================
/**
* AI channel ground reference. Only valuable for DAQ-2204/2205/2206/2208.
*/
typedef enum
{
  /**
  * Referenced single ended mode.
  */
  ref_single_ended     = AI_RSE,
  /**
  * differential mode.
  */
  differential	       = AI_DIFF, 
  /**
  * Non-referenced single ended mode.
  */
  non_ref_single_ended = AI_NRSE
} 
GroundRef;

// ============================================================================
// ADLink: AO channels configuration: output polarity
// ============================================================================
/**
* AO channel polarity.
*/
typedef enum
{
  bipolar  = DAQ2K_DA_BiPolar,
  unipolar = DAQ2K_DA_UniPolar
} OutputPolarity;

// ============================================================================
// ADLink: AO channels configuration: voltage reference source
// ============================================================================
/**
* AO channel voltage reference.
*/
typedef enum
{
  /**
  * The voltage reference is internal.
  */
  internal_reference = DAQ2K_DA_Int_REF,
  /**
  * The voltage reference is external
  */
  external_reference = DAQ2K_DA_Ext_REF
} 
VoltageReferenceSource;

// ============================================================================
// ADLink: AI conversion source (internal timer or ASL signal on pin AFI0)
// ============================================================================ 
/**
* AI conversion source.
*/
typedef enum 
{
  /**
  * The conversion is timed by the internal clock.
  */
  ai_internal_timer = DAQ2K_AI_ADCONVSRC_Int,
  /**
  * The conversion is timed by the line AFI0.
  */
  ai_external_afio  = DAQ2K_AI_ADCONVSRC_AFI0
} 
AIConversionSource;

// ============================================================================
// ADLink: AO conversion source (internal timer or ASL signal on pin AFI0)
// ============================================================================ 
/**
* AO conversion source.
*/
typedef enum 
{
  /**
  * The conversion is timed by the internal clock.
  */
  ao_internal_timer = DAQ2K_DA_WRSRC_Int,
  /**
  * The conversion is timed by the line AFI0.
  */
  ao_external_afio  = DAQ2K_DA_WRSRC_AFI0
} 
AOConversionSource;

// ============================================================================
// ADLink:  AO - groups
// ============================================================================ 
/**
* AO groups (no need for the user setup it).
*/
typedef enum 
{
  /**
  * channel 0 to 3.
  */
  group_a  = DA_Group_A,
  /**
  * channel 4 to 7
  */
  group_b  = DA_Group_B,
  /**
  * channel 0 to 7.
  */
  group_ab = DA_Group_AB,
} 
AOGroup;

// ============================================================================
// ADLink: AO configuration: delay counter source selection 
// ============================================================================
/**
* AO delay counter source.
*/
typedef enum
{
  /**
  * The source is internal. 
  */
  ao_delay_internal_timer = DAQ2K_DA_TDSRC_Int,
  /**
  * The source is from line AFI0
  */
  ao_delay_external_afi0  = DAQ2K_DA_TDSRC_AFI0,
  /**
  * The source is from line GPTC0
  */
  ao_delay_external_gptc0 = DAQ2K_DA_TDSRC_GPTC0,
   /**
  * The source is from line GPTC1
  */
  ao_delay_external_gptc1 = DAQ2K_DA_TDSRC_GPTC1
} 
AODelaySource;

// ============================================================================
// ADLink: AO configuration: delay break counter source selection 
// ============================================================================
/**
* AO delay break counter source.
*/
typedef enum
{
  /**
  * The source is internal. 
  */
  ao_delay_brk_internal_timer = DAQ2K_DA_BDSRC_Int,
   /**
  * The source is from line AFI0
  */
  ao_delay_brk_external_afi0  = DAQ2K_DA_BDSRC_AFI0,
  /**
  * The source is from line GPTC0
  */
  ao_delay_brk_external_gptc0 = DAQ2K_DA_BDSRC_GPTC0,
   /**
  * The source is from line GPTC1
  */
  ao_delay_brk_external_gptc1 = DAQ2K_DA_BDSRC_GPTC1
} 
AODelayBreakSource;

// ============================================================================
// ADLink: trigger source
// ============================================================================
/**
  * AIO trigger source.
  */
typedef enum 
{
  /**
  * The trigger source is internal.
  */
  internal_software = DAQ2K_DA_TRGSRC_SOFT,
  /**
  * The trigger source is from an external analog channel.
  */
  external_analog   = DAQ2K_DA_TRGSRC_ANA,
  /**
  * The trigger source is from an external digital line.
  */
  external_digital  = DAQ2K_DA_TRGSRC_ExtD
} 
AIOTriggerSource;

// ============================================================================
// ADLink: analog trigger input channel
// ============================================================================   
/**
* AIO analog trigger source.
*/
typedef enum 
{ 
  /**
  * The analog trigger channel is channel 0.
  */
  analog_trigger_chan0 = CH0ATRIG, 
  /**
  * The analog trigger channel is channel 1.
  */
  analog_trigger_chan1 = CH1ATRIG,
  /**
  * The analog trigger channel is channel 2.
  */
  analog_trigger_chan2 = CH2ATRIG,
  /**
  * The analog trigger channel is channel 3.
  */
  analog_trigger_chan3 = CH3ATRIG,
  /**
  * The analog trigger channel is EXTATRIG pin.
  */
  analog_trigger_ext = EXTATRIG
} 
AnalogTriggerChannel;

// ============================================================================
// ADLink: analog trigger condition
// ============================================================================ 
/**
* AI analog trigger condition.
*/
typedef enum 
{
  /**
  * The trigger is generated when input analog signal is less than 'low_level'.
  */
  below_low_level  = Below_Low_level, 
  /**
  * The trigger is generated when input analog signal is more than 'high_level'.
  */
  above_high_level = Above_High_Level, 
  /**
  * The trigger is generated when input analog signal falls in the range between 'low_level' and 'high_level'.
  */
  inside_region    = Inside_Region, 
  /**
  * The trigger is generated when input analog signal is more than 'high_level'.
  * The trigger stops when input analog signal is less than 'low_level'.
  */
  high_hysteresis  = High_Hysteresis, 
   /**
  * The trigger is generated when input analog signal is less than 'low_level'.
  * The trigger stops when input analog signal is more than 'high_level'.
  */
  low_hysteresis   = Low_Hysteresis
} 
AnalogTriggerCondition;

// ============================================================================
// ADLink: AI trigger mode
// ============================================================================
/**
* AI trigger mode.
*/
typedef enum 
{
  /**
  * Post trigger mode.
  */
  ai_post   = DAQ2K_AI_TRGMOD_POST,
  /**
  * Delay trigger mode.
  */
  ai_delay  = DAQ2K_AI_TRGMOD_DELAY, 
  /**
  * Pre trigger mode.
  */
  ai_pre    = DAQ2K_AI_TRGMOD_PRE, 
  /**
  * Middle trigger mode.
  */
  ai_middle = DAQ2K_AI_TRGMOD_MIDL
} 
AITriggerMode;

// ============================================================================
// ADLink: AO trigger mode
// ============================================================================
/**
* AO trigger mode.
*/
typedef enum 
{
  /**
  * Post trigger mode.
  */
  ao_post   = DAQ2K_DA_TRGMOD_POST,
   /**
  * Delay trigger mode.
  */
  ao_delay  = DAQ2K_DA_TRGMOD_DELAY, 
} 
AOTriggerMode;

// ============================================================================
// ADLink: delay unit (for delay trigger mode)
// ============================================================================
/**
* AIO trigger delay unit.
*/
typedef enum 
{
  /**
  * The delay is defined with a number of clock ticks.
  */
  clock_ticks = DAQ2K_AI_Dly1InTimebase,
  /**
  * The delay is defined with a number of samples.
  */
  samples     = DAQ2K_AI_Dly1InSamples
} 
AIODelayUnit;

// ============================================================================
// ADLink: trigger polarity
// ============================================================================
/**
* AI digital trigger polarity.
*/
typedef enum 
{
  /**
  * A digital trigger occurs when a rising edge is detected.
  */
  ai_rising_edge  = DAQ2K_AI_TrgPositive,
  /**
  * A digital trigger occurs when a falling edge is detected.
  */
  ai_falling_edge = DAQ2K_AI_TrgNegative
} 
AITriggerPolarity;

// ============================================================================
// ADLink: AO trigger polarity
// ============================================================================
/**
* AO digital trigger polarity.
*/
typedef enum 
{
  /**
  * A digital trigger occurs when a rising edge is detected.
  */
  ao_rising_edge  = DAQ2K_DA_TrgPositive,
  /**
  * A digital trigger occurs when a falling edge is detected.
  */
  ao_falling_edge = DAQ2K_DA_TrgNegative
} 
AOTriggerPolarity;

// ============================================================================
// ADLink: AIO time base
// ============================================================================
/**
* AIO time base.
*/
typedef enum 
{
  /**
  * The time base is internal.
  */
  int_time_base = DAQ2K_IntTimeBase,
  /**
  * The time base is external.
  */
  ext_time_base = DAQ2K_ExtTimeBase,
  /**
  * The time base is from the SSI.
  */
  ssi_time_base = DAQ2K_SSITimeBase
} 
AIOTimeBase;

// ============================================================================
// ADLink: AI retrigger on/off
// ============================================================================ 
/**
* AI re-trigger control.
*/
typedef enum 
{
  /**
  * Disable infinite retrigger.
  */
  ai_rt_disabled = 0x00,
  /**
  * Enable infinite retrigger.
  */
  ai_rt_enabled  = DAQ2K_AI_ReTrigEn 
} 
AIRetriggerStatus;

// ============================================================================
// ADLink: AO retrigger on/off
// ============================================================================ 
/**
* AO re-trigger control.
*/
typedef enum 
{
  /**
  * Disable infinite retrigger.
  */
  ao_rt_disabled = 0x00,
  /**
  * Enable infinite retrigger.
  */
  ao_rt_enabled  = DAQ2K_DA_ReTrigEn 
} 
AORetriggerStatus;

// ============================================================================
// ADLink: AI m-counter on/off
// ============================================================================ 
/**
* AI m-counter control. Only useful for pre and middle trigger mode.
* m-counter is the number of samples saved in the input buffer.
* It is dangerous to disable it. 
*/
typedef enum 
{
  /**
  * All the triggers are accepted. The values acquired can be invalid.
  */
  ai_mc_disabled = 0x00,
  /**
  * The triggers occured before the first time m-counter end was reached are ignored.
  */
  ai_mc_enabled  = DAQ2K_AI_MCounterEn
} 
AIMCounterStatus;

// ============================================================================
// ADLink: AO delay break on/off
// ============================================================================ 
/**
* AO delay break on/off.
* The delay break is the delay between to consecutives waveforms.
*/
typedef enum 
{
  /**
  * The delay break is disabled.
  */
  ao_dbrk_disabled = 0x00,
  /**
  * The delay break is enabled.
  */
  ao_dbrk_enabled  = DAQ2K_DA_DLY2En
} 
AODelayBreakMode;
  
// ============================================================================
// ADLink: double-buffer overrun strategy
// ============================================================================
/**
*  Double-buffer overrun strategy.
*/
typedef enum 
{
 /**
  * Ignore data lost and pass overwritten data to user defined asl::ContinuousAI::handle_input
  */
  ignore, 
 /**
  * Ignore data lost but do not pass overwritten data to user defined asl::ContinuousAI::handle_input
  */
  trash, 
 /**
  * Notify user by calling user defined asl::ContinuousAI::handle_data_lost (overwritten data is trashed)
  */
  notify,
 /**
  * Auto-restart DAQ then call user defined asl::ContinuousAI::handle_data_lost (overwritten data is trashed)
  */
  restart, 
 /**
  * Abort DAQ then call user defined asl::ContinuousAI::handle_data_lost (overwritten data is trashed)
  */
  abort
} 
DataLostStrategy;

// ============================================================================
// ADLink: asynchronous events
// ============================================================================ 
/**
* Asynchronous events.
*/
typedef enum 
{
 /**
  * Continuous DAQ: end of daq event.
  */
  daq_end  = DAQEnd,
 /**
  * Double-buffered DAQ: data ready event.
  */
  db_evt   = DBEvent,
 /**
  * Re-triggered DAQ: next trigger ready event. Only in analog mode.
  */
  trig_evt = TrigEvent
} 
Event;

// ============================================================================
// ADLink: asynchronous events
// ============================================================================ 
/**
* Asynchronous events.
*/
typedef enum 
{
 /**
  * Synchronous operation.
  */
  sync_op  = SYNCH_OP,
 /**
  * Asynchronous operation.
  */
  async_op = ASYNCH_OP
} 
SynchMode;

// ============================================================================
// ADLinkPcisDask: digital ports 
// ============================================================================ 
/**
* Possible digital input/output ports.
*/
typedef enum {
 /**
  * PCI-7248, DAQ-2005, DAQ-2010, DAQ-2204, DAQ-2205, DAQ-2502 - port 1A - programmable I/O port.
  */
	port_1a = Channel_P1A, 
 /**
	* PCI-7248, DAQ-2005, DAQ-2010, DAQ-2204, DAQ-2205, DAQ-2502 - port 1B - programmable I/O port.
	*/
	port_1b = Channel_P1B,
 /**
  * PCI-7248, DAQ-2005, DAQ-2010, DAQ-2204, DAQ-2205, DAQ-2502 - port 1C - programmable I/O port.
  */
	port_1c = Channel_P1C,
 /**
  * PCI-7248, DAQ-2005, DAQ-2010, DAQ-2204, DAQ-2205, DAQ-2502 - port 1C-LOW - programmable I/O port.
  */
	port_1cl = Channel_P1CL,
 /**
  * PCI-7248, DAQ-2005, DAQ-2010, DAQ-2204, DAQ-2205, DAQ-2502 - port 1C-HIGH - programmable I/O port.
  */
	port_1ch = Channel_P1CH,
 /**
  * PCI-7248 - port 2A - programmable I/O port.
  */
	port_2a = Channel_P2A,
 /**
  * PCI-7248 - port 2B - programmable I/O port.
  */
	port_2b = Channel_P2B,
 /**
  * PCI-7248 - port 2C - programmable I/O port.
  */
	port_2c = Channel_P2C,
 /**
  * PCI-7248 - port 2C-LOW - programmable I/O port.
  */
	port_2cl = Channel_P2CL,
 /**
  * PCI-7248 - port 2C-HIGH - programmable I/O port.
  */
	port_2ch = Channel_P2CH,
 /**
  * PCI-7300 - AUX-DI - input port.
  */
	aux_di, 
 /**
  * PCI-7300 - AUX-D0 - output port.
  */
	aux_do, 
 /**
  * PCI-7432, PCI-6208V - port A - input port.
  */
	port_a, 
 /**
  * PCI-7432, PCI-6208V - port B - output port.
  */
	port_b
} DIOPort;

// ============================================================================
// ADLinkPcisDask: digital ports direction
// ============================================================================ 
/**
* Digital port directions.
*/
typedef enum 
{
   /**
	* DIO port configuration: unknown direction.
	*/
  unknown = -1,
 /**
	* DIO port configuration: input direction.
	*/
	input = INPUT_PORT,
 /**
	* DIO port configuration: output direction.
	*/
	output = OUTPUT_PORT
 
} 
DIODirection;

// ============================================================================
// ADLinkPcisDask: digital ports width
// ============================================================================ 
/**
* digital port width (only avaible for 7300).
*/
typedef enum 
{
	/**
	* The port is 8-bit width.
	*/
	width_8 = 8,
	/**
	* The port is 16-bit width.
	*/
	width_16 = 16,
	/**
	* The port is 32-bit width.
	*/
	width_32 = 32
} 
DIOPortWidth;

// ============================================================================
// ADLinkPcisDask: trigger source
// ============================================================================
/**
* The trigger source in digital mode.
*/
typedef enum 
{
	/**
	* Software trigger source
	*/
	software = TRIG_SOFTWARE,
	/**
	* On-board programmable pacer timer.
	*/
	internal = TRIG_INT_PACER,
	/**
	* The trigger source is an external signal.
	*/
	external  = TRIG_EXT_STROBE, 
	/**
	* The trigger source is in handshaking mode (with request and a acknowledgment signal).
	*/
	handshaking  = TRIG_HANDSHAKE,
	/**
	* A 10MHz clock.
	*/
	trigger_clock_10Mhz = TRIG_CLK_10MHZ,
	/**
	* A 20MHz clock.
	*/
	trigger_clock_20Mhz =  TRIG_CLK_20MHZ,
	/**
	* In output only.
	*/
	trigger_do_handshaking_timer = TRIG_DO_CLK_TIMER_ACK,
	/**
	* Burst handshaking mode by using a 10MHz clock as output clock. In output only.
	*/
	trigger_do_handshaking_10M = TRIG_DO_CLK_10M_ACK,
	/**
	*  Burst handshaking mode by using a 20MHz clock as output clock. In output only.
	*/
	trigger_do_handshaking_20M = TRIG_DO_CLK_20M_ACK
} 
DIOTriggerSource;
// ============================================================================
// ADLinkPcisDask:data mode
// ============================================================================
/**
* Data format gave by user
*/
typedef enum 
{
  /**
  * The data is non-scaled. The valuable values depend on the resolution of the card.
  */
	raw_data,
  /**
  * The data is scaled. The valuable values depend on the range of the card.
  */
	scaled_data
}
AODataFormat;
// ============================================================================
// ADLinkPcisDask:start mode
// ============================================================================
/**
* Start mode for the PCI-7300 (continuous digital acquisition).
*/
typedef enum 
{
 /**
	* Digital operation starts immediatly.
	*/
	start_immediatly = P7300_WAIT_NO,
 /**
	* Digital operation waits for rising edge or falling edge of the trigger.
	*/
	wait_trigger = P7300_WAIT_TRG,
 /**
	* Digital operation starts when some outputs have loaded in the FIFO (only in output).
	*/
	wait_fifo = P7300_WAIT_FIFO,
 /**
	* Digital operation starts when some outputs have loaded in the FIFO and when the trigger is active (only in ouput).
	*/
	wait_trigger_fifo =P7300_WAIT_BOTH
} 
StartMode;

// ============================================================================
// ADLinkPcisDask:teminator mode
// ============================================================================
/**
* Terminator mode for the PCI-7300 (continuous digital acquisition).
*/
typedef enum 
{
 /**
	* PCI-7300 - terminator on.
	*/
	terminator_on = P7300_TERM_ON,
 /**
	* PCI-7300 - terminator off.
	*/
	terminator_off = P7300_TERM_OFF
} 
TerminatorMode;

// ============================================================================
// ADLinkPcisDask:request polarity
// ============================================================================
/**
* The polarity for the request line for the PCI-7300.
*/
typedef enum 
{
 /**
	* Digital input. The request signal is rising edge active.
	*/
	request_di_rising_edge = P7300_DIREQ_POS,
 /**
	* Digital input. The request signal is falling edge active.
	*/
	request_di_falling_edge = P7300_DIREQ_NEG,
 /**
	* Digital output. The request signal is rising edge active.
	*/
	request_do_rising_edge = P7300_DOREQ_POS,
 /**
	* Digital output. The request signal is falling edge active.
	*/
	request_do_falling_edge = P7300_DOREQ_NEG
} 
RequestPolarity;

// ============================================================================
// ADLinkPcisDask:acknowledgement polarity
// ============================================================================
/**
*  The polarity for the acknowledgment line for the PCI-7300.
*/
typedef enum 
{
 /**
	* Digital input. The acknowledgment signal is rising edge active.
	*/
	ack_di_rising_edge = P7300_DIACK_POS,
 /**
	* Digital input. The acknowledgment signal is falling edge active.
	*/
	ack_di_falling_edge = P7300_DIACK_NEG,
 /**
	* Digital output. The acknowledgment signal is rising edge active.
	*/
	ack_do_rising_edge = P7300_DOACK_POS,
 /**
	* Digital output. The acknowledgment signal is falling edge active.
	*/
	ack_do_falling_edge = P7300_DOACK_NEG
} 
AcknowledgementPolarity;

// ============================================================================
// ADLinkPcisDask:trigger polarity
// ============================================================================
/**
* The polarity for the trigger line for the PCI-7300.
*/
typedef enum 
{
	/**
	* Digital input. The trigger signal is rising edge active.
	*/
	trigger_di_rising_edge = P7300_DITRIG_POS,
	/**
	* Digital input. The trigger signal is falling edge active.
	*/
	trigger_di_falling_edge = P7300_DITRIG_NEG,
	/**
	* Digital output. The trigger signal is rising edge active.
	*/
	trigger_do_rising_edge = P7300_DITRIG_POS,
	/**
	* Digital output. The trigger signal is falling edge active.
	*/
	trigger_do_falling_edge = P7300_DITRIG_NEG
} 
TriggerPolarity;
// ============================================================================
// ADLinkPcisDask:stop source
// ============================================================================
/**
* The stop source for continuous AO (the signal that stops outputing data).
*/
typedef enum 
{
	/**
	* The signal is software.
	*/
	software_termination = DAQ2K_DA_STOPSRC_SOFT,
  /**
  * The signal is from line AFI0.
  */
	afi0_termination = DAQ2K_DA_STOPSRC_AFI0,
  /**
  * The signal is from the external signal of analog trigger.
  */
	analog_trigger_termination = DAQ2K_DA_STOPSRC_ATrig,
  /**
  *  The signal is from line AFI1.
  */
	afi1_termination = DAQ2K_DA_STOPSRC_AFI1
}
StopSource;
// ============================================================================
// ADLinkPcisDask:stop mode
// ============================================================================
/**
* The stop mode for continuous AO.
*/
typedef enum 
{
	/**
	* The waveform generation stops immediatly when acquisition is stopped by user.
	*/
	stop_immediatly = DAQ2K_DA_TerminateImmediate,
	/**
	* The waveform generation stops at the end of the current buffer when acquisition is stopped by user.
	*/
	wait_end_waveform = DAQ2K_DA_TerminateUC,
	/**
	* This parameter is only useful when retrigger is enable.
	* The waveform generation stops after n waveforms (n is configured in ContinuousDIConfig).
	*/
	wait_end_n_waveforms = DAQ2K_DA_TerminateIC
}
StopMode;

// ============================================================================
// ADLink: channel id
// ============================================================================ 
typedef unsigned short ChanId;

// ============================================================================
// ADLink: analog input data type
// ============================================================================ 
typedef unsigned short AIDataType;

// ============================================================================
// ADLink: boards internal clock frequency and sampling rate range
// ============================================================================ 
#define DEFAULT_CLOCK_FREQ   40000000
//- Number of nanoseconds per second.
#define NANOSEC_PER_SEC      1000000000UL

//- DAQ 2005 ----------------------
#define AD2005_CLOCK_FREQ   40000000
#define AD2005_MAX_SRATE    500000
#define AD2005_MIN_SRATE    3
#define AD2005_NSEC_TICK    25.0
#define AD2005_NUM_CHANNELS 4
//- DAQ 2010 ----------------------
#define AD2010_CLOCK_FREQ   40000000
#define AD2010_MAX_SRATE    2000000
#define AD2010_MIN_SRATE    3
#define AD2010_NSEC_TICK    25.0
#define AD2010_NUM_CHANNELS 4
//- DAQ 2502 ----------------------
#define AD2502_CLOCK_FREQ   40000000 
#define AD2502_MAX_SRATE    1000000
#define AD2502_MIN_SRATE    3
#define AD2502_NSEC_TICK    25.0
#define AD2502_NUM_CHANNELS 4
//- DAQ 2204 ----------------------
#define AD2204_NUM_CHANNELS 64
//- DAQ 2205 ----------------------
#define AD2205_CLOCK_FREQ   40000000
#define AD2205_MAX_SRATE    500000
#define AD2205_MIN_SRATE    3
#define AD2205_NSEC_TICK    25.0
#define AD2205_NUM_CHANNELS 64
//- PCI 6208 ----------------------
#define AD6208_NUM_CHANNELS  8
#define AD6208_MIN_VOLTAGE  -10.0
#define AD6208_MAX_VOLTAGE   10.0
//- PCI 6208 ----------------------
#define AD6216_NUM_CHANNELS  16
#define AD6216_MIN_VOLTAGE   AD6208_MIN_VOLTAGE
#define AD6216_MAX_VOLTAGE   AD6208_MAX_VOLTAGE

// ============================================================================ 
// DIO ports: board specific values
// ============================================================================ 
#define AD7248_MIN_PORT     port_1a
#define AD7248_MAX_PORT     port_2ch
#define AD7248_MIN_LINE     0
#define AD7248_MAX_4BP_LINE 3
#define AD7248_MAX_8BP_LINE 7
// ============================================================================ 
#define AD7300_MIN_PORT   aux_di
#define AD7300_MAX_PORT   aux_do
#define AD7300_MIN_LINE   0
#define AD7300_MAX_LINE   3
#define AD7300_READ_PORT  1
#define AD7300_WRITE_PORT AD7300_READ_PORT 
// ============================================================================ 
#define AD7432_MIN_PORT port_a //- DI port
#define AD7432_MAX_PORT port_b //- DO port
#define AD7432_MIN_LINE 0
#define AD7432_MAX_LINE 31
// ============================================================================ 
#define AD2005_MIN_PORT     port_1a
#define AD2005_MAX_PORT     port_1ch
#define AD2005_MIN_LINE     0
#define AD2005_MAX_4BP_LINE 3
#define AD2005_MAX_8BP_LINE 7
// ============================================================================ 
#define AD2010_MIN_PORT     port_1a
#define AD2010_MAX_PORT     port_1ch
#define AD2010_MIN_LINE     0
#define AD2010_MAX_4BP_LINE 3
#define AD2010_MAX_8BP_LINE 7
// ============================================================================ 
#define AD2204_MIN_PORT     port_1a
#define AD2204_MAX_PORT     port_1ch
#define AD2204_MIN_LINE     0
#define AD2204_MAX_4BP_LINE 3
#define AD2204_MAX_8BP_LINE 7
// ============================================================================ 
#define AD2205_MIN_PORT     port_1a
#define AD2205_MAX_PORT     port_1ch
#define AD2205_MIN_LINE     0
#define AD2205_MAX_4BP_LINE 3
#define AD2205_MAX_8BP_LINE 7
// ============================================================================ 
#define AD2502_MIN_PORT     port_1a
#define AD2502_MAX_PORT     port_1ch
#define AD2502_MIN_LINE     0
#define AD2502_MAX_4BP_LINE 3
#define AD2502_MAX_8BP_LINE 7
// ============================================================================ 
#define AD6208_MIN_PORT port_a //- DI port
#define AD6208_MAX_PORT port_b //- DO port
#define AD6208_MIN_LINE 0
#define AD6208_MAX_LINE 3
// ============================================================================ 
#define AD6216_MIN_PORT AD6208_MIN_PORT
#define AD6216_MAX_PORT AD6208_MAX_PORT
#define AD6216_MIN_LINE AD6208_MIN_LINE
#define AD6216_MAX_LINE AD6208_MIN_LINE

// ============================================================================ 
// ADLink: dummy parameters 
// ============================================================================ 
#define kIGNORED_PARAM	0
#define kIGNORED_BUFFER BufferNotUsed 

// ============================================================================ 
// ADLink: D2K-DASK error code to error text
// ============================================================================ 
/**
* D2K-DASK error code to error text.
*/
const char* d2kdask_error_text (int _err_code);

// ============================================================================ 
// ADLink: PCIS-DASK error code to error text
// ============================================================================ 
/**
* PCIS-DASK error code to error text.
*/
const char* pcisdask_error_text (int _err_code);

} // namespace adl


namespace asl {

// ============================================================================
/// ADLink: active analog input channel configuration
// ============================================================================

class ASL_EXPORT ActiveAIChannel 
{
public:
  /**
   * Channel identifier
   */
  adl::ChanId id;

  /**
   * Channel voltage range
   */
  adl::Range range;
  
  /**
   * Channel ground reference
   */
  adl::GroundRef grd_ref;

  /**
   * Channel user data gain
   */
  double user_data_gain;

  /**
   * Channel user data offset1
   */
  double user_data_offset1;

  /**
   * Channel user data offset2
   */
  double user_data_offset2;

  /**
   * Channel user data enabled?
   */
  bool user_data_enabled;


  /**
   * Default constructor.
   */
  ActiveAIChannel () 
    : id (0), 
      range (adl::bp_10), 
      grd_ref (adl::ref_single_ended),
      user_data_gain(1.0), 
      user_data_offset1(0.0),
      user_data_offset2(0.0),
      user_data_enabled(false)
  {/*noop*/};

  /**
   * Constructor with default values.
   * @param _c The channel id.
   * @param _r The input range.
   * @param _g The input reference.
   * @param _udg  The user data gain.
   * @param _udo1 The user data offset1.
   * @param _udo2 The user data offset2.
   * @param _ude  User data enabled.
   */
  ActiveAIChannel (adl::ChanId _c, 
		   adl::Range _r = adl::bp_10,
		   adl::GroundRef _g = adl::ref_single_ended,
       double _udg = 1.0, 
       double _udo1 = 0.0,
       double _udo2 = 0.0,
       bool _ude = false)
    : id(_c), 
      range(_r), 
      grd_ref(_g),
      user_data_gain(_udg), 
      user_data_offset1(_udo1),
      user_data_offset2(_udo2),
      user_data_enabled(_ude)
  {/*noop*/};

  /**
   * Copy constructor.
   * @param _src The ActiveAIChannel to copy
   */
  ActiveAIChannel (const ActiveAIChannel& _src)
    : id(_src.id),
      range(_src.range),
      grd_ref(_src.grd_ref),
      user_data_gain(_src.user_data_gain), 
      user_data_offset1(_src.user_data_offset1),
      user_data_offset2(_src.user_data_offset2),
      user_data_enabled(_src.user_data_enabled)
  {/*noop*/};

  /**
   * operator =.
   */
  ActiveAIChannel& operator= (const ActiveAIChannel& _src)
  {
    if (&_src == this) {
       return *this;
    }
    id = _src.id;
    range = _src.range;
    grd_ref = _src.grd_ref;
    user_data_gain = _src.user_data_gain;
    user_data_offset1 = _src.user_data_offset1;
    user_data_offset2 = _src.user_data_offset2;
    user_data_enabled = _src.user_data_enabled;
    return *this;
  }

  /**
   * Destructor.
   */
  virtual ~ActiveAIChannel () {/*noop*/};
};

// ============================================================================
/// ADLink: active analog output channel configuration
// ============================================================================
class ASL_EXPORT ActiveAOChannel
{
public:
  /**
   * Channel identifier.
   */
  adl::ChanId id;

  /**
   * Channel output polarity (unipolar or bipolar).
   * @see ADLink.h
   */
  adl::OutputPolarity polarity;
  
  /**
   * Channel voltage reference source (internal or external).
   * @see ADLink.h
   */
   adl::VoltageReferenceSource volt_ref_src;
  /**
   * Channel voltage reference in Volt.
   * If the reference voltage source is internal volt_ref must be 10.0
   * If the reference voltage source is external volt_ref must be between -10 and +10.
   */
  double volt_ref;

  /**
   * Default constructor.
   */
  ActiveAOChannel () 
    : id(0), 
      polarity(adl::bipolar), 
      volt_ref_src(adl::internal_reference), 
      volt_ref(10.0) 
    {/*noop*/};

  /**
   * Constructor with default values.
   * @param _c The channel id.
   * @param _p The ouput polarity (bipolar by default).
   * @param _v The voltage reference source (internal reference by default). 
   * @param _vr The voltage reference (10.0 by default)
   */
  ActiveAOChannel (adl::ChanId _c, 
		   adl::OutputPolarity _p = adl::bipolar,
		   adl::VoltageReferenceSource _v = adl::internal_reference, 
		   double  _vr = 10.0)
    : id(_c), 
      polarity(_p), 
      volt_ref_src(_v), 
      volt_ref(_vr) 
   {/*noop*/};

  /**
   * Copy constructor.
   */
  ActiveAOChannel (const ActiveAOChannel& src)
    : id(src.id), 
      polarity(src.polarity),
      volt_ref_src(src.volt_ref_src),
      volt_ref(src.volt_ref)
    {/*noop*/};

  /**
   * operator=.
   */
  ActiveAOChannel& operator= (const ActiveAOChannel& _src)
  {
    if (&_src == this) {
       return *this;
    }
    id = _src.id;
    polarity = _src.polarity;
    volt_ref_src = _src.volt_ref_src;
    volt_ref = _src.volt_ref;
    return *this;
  }

  /**
   * Destructor.
   */

  virtual ~ActiveAOChannel () {/*noop*/};
};

} // namespace asl

#endif // _ADLINK_H_
