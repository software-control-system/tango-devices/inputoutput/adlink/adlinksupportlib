// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    AD6216.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _ADLINK_6216_H_
#define _ADLINK_6216_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/ASLConfig.h>
#include <map>
#include <asl/adlink/ADSingleShotAOBoard.h>
#include <asl/adlink/ADSingleShotDIBoard.h>
#include <asl/adlink/ADSingleShotDOBoard.h>

namespace adl {

// ============================================================================
//! The ADLink 6216 general purpose DAQ card abstraction.
// ============================================================================
//!  This board is able to perform the following features:
//! - SingleShot AO.
//! - SingleShot DI/DO.
// ============================================================================
class ASL_EXPORT AD6216 : public ADSingleShotAOBoard,
			  public ADSingleShotDIBoard,
			  public ADSingleShotDOBoard
{
  typedef std::map<unsigned short, AD6216*> Repository;

  typedef Repository::iterator RepositoryIterator;

  typedef Repository::value_type RepositoryValue;

public:

  /**
   * Instanciate an AD6216 board.
   * @param _id The number of the card in the cPCI crate.
   * @param _exclusive_access Allows or not the exclusive access (no exclusive access by default).
   * @return A pointer to the card.
   * @throw asl::DAQException 
   */
  static AD6216* instanciate (unsigned short _id, bool _exclusive_access = false)
    throw (asl::DAQException);

  /**
   * Duplicate this shared object.
   */
   AD6216 * duplicate ();

  /**
   * Since the PCI-6216V AO channels are not configurable, this method always
   * throws an exception. It is there for adl::SingleShotAO interface homogeneity reasons.  
   */
  virtual void configure_ao_channel (adl::ChanId _chan_id, 
				     adl::OutputPolarity _polarity, 
				     adl::VoltageReferenceSource _ref_source, 
				     double _ref_volt)
    throw (asl::DAQException);

  /**
   * Write on specified AO channel (numeric version)
   * @param _chan_id The channel write.
   * @param _value The ouput numeric value (non scaled).
   */
  virtual void write_channel (adl::ChanId _chan_id, unsigned short _value)
    throw (asl::DAQException);

  /**
   * Write on specified AO channel (voltage version).
   * @param _chan_id The channel to write.
   * @param _value The ouput voltage value (in Volts).
   */
  virtual void write_scaled_channel (adl::ChanId _chan_id, double _value)
    throw (asl::DAQException);

  /**
   * Since the PCI-6216V DI port is not configurable, this method always
   * throws an exception. It is there for adl::SingleShotDI interface homogeneity reasons.  
   */
  virtual void configure_di_port (adl::DIOPort _port) 
    throw (asl::DAQException);

  /**
   * Since the PCI-6216V DO port is not configurable, this method always
   * throws an exception. It is there for adl::SingleShotDO interface homogeneity reasons.  
   */
  virtual void configure_do_port (adl::DIOPort _port) 
    throw (asl::DAQException);

  /**
   * Read a single DI line.
   * @param _port The line's port - always use adl::port_a as _port parameter value.
   * @see adl::DIOPort enumeration.
   * @param _line The line identifier.
   * @return The read value.
   * @throw asl::DAQException 
   */
  virtual bool read_line (adl::DIOPort _port, int _line) 
    throw (asl::DAQException);

  /**
   * Read a DI port.
   * @param _port The port identifier - always use adl::port_a as _port parameter value.
   * @see adl::DIOPort enumeration.
   * @return The read value.
   * @throw asl::DAQException 
   */
  virtual unsigned long read_port (adl::DIOPort _port) 
    throw (asl::DAQException);

  /**
   * Write on a single DI line.
   * @param _port The line's port - always use adl::port_b as _port parameter value.
   * @see adl::DIOPort enumeration.
   * @param _line The line identifier.
   * @param _state The new line's state.
   * @throw asl::DAQException 
   */
  virtual void write_line (adl::DIOPort _port, int _line, bool _state) 
    throw (asl::DAQException);

  /**
   * Write on DI port.
   * @param _port The port identifier - always use adl::port_b as _port parameter value.
   * @see adl::DIOPort enumeration.
   * @param _state The output value.
   * @throw asl::DAQException 
   */
  virtual void write_port (adl::DIOPort _port, unsigned long _state) 
    throw (asl::DAQException);

  /**
   * Since the PCI-6216V does not support auto-calibration, this method always
   * throws an exception. It is there for adl::ADCalibrableAIOBoard interface 
   * homogeneity reasons. 
   */
  virtual void auto_calibrate (bool _save_data = true) 
    throw (asl::DAQException);

  /**
   * Since the PCI-6216V does not support auto-calibration, this method always
   * throws an exception. It is there for adl::ADCalibrableAIOBoard interface 
   * homogeneity reasons. 
   */
  virtual void save_calibration (adl::AIOCalibrationBank _bank) 
    throw (asl::DAQException);

  /**
   * Since the PCI-6216V does not support auto-calibration, this method always
   * throws an exception. It is there for adl::ADCalibrableAIOBoard interface 
   * homogeneity reasons. 
   */
  virtual void load_calibration (adl::AIOCalibrationBank _bank) 
    throw (asl::DAQException);

  /**
   * Dump hardware info.
   */
  virtual void dump () const;

protected:

  /**
   * Constructor.
   */
  AD6216 (unsigned short _id);

  /**
   * Destructor.
   */
  virtual ~AD6216();
  
  /**
   * Register the board with the driver.
   */
  virtual int register_hardware ();

  /**
   * Unregister the board with the driver.
   */
  virtual int release_hardware ();

private:
  /**
   * Validate DI port identifier
   */
  virtual void check_di_port (adl::DIOPort _port)
    throw (asl::DAQException);

  /**
   * Validate DI line identifier
   */
  virtual void check_di_line (int line)
    throw (asl::DAQException);

  /**
   * Validate DO port identifier
   */
  virtual void check_do_port (adl::DIOPort _port)
    throw (asl::DAQException);

  /**
   * Validate DO line identifier
   */
  virtual void check_do_line (int line)
    throw (asl::DAQException);

  /**
   * Boards repository.
   */
  static Repository repository;
	
  /**
   * Boards repository' lock.
   */
  static ACE_Recursive_Thread_Mutex repository_lock;
};

} // namespace adl

#endif // _ADLINK_6216_H_
