// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ADSingleShotAOBoard.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _ADLINK_SINGLE_SHOT_AO_BOARD_H_
#define _ADLINK_SINGLE_SHOT_AO_BOARD_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/ASLConfig.h>
#include <vector>
#include <asl/adlink/ADCalibrableAIOBoard.h>
#include <asl/AOData.h>
#include <asl/DAQException.h>

namespace adl {

// ============================================================================
//! The ADLink single shot AO capable boards base class. 
// ============================================================================
//!  Base class for boards that perform single shot analog output. \n
//!  Current supported boards are: 
//!  - AD2005
//!  - AD2010
//!  - AD2204
//!  - AD2205
//!  - AD6208
// ============================================================================
class ASL_EXPORT ADSingleShotAOBoard : virtual public ADCalibrableAIOBoard
{
public:

  /**
   * ADSingleShotAOBoard factory.
   */
  static ADSingleShotAOBoard* instanciate (unsigned short _type, 
			                                     unsigned short _id, 
			                                     bool _exclusive_access)
    throw (asl::DAQException);

  /**
   * Configure one output channel for single-shot or continuous AO operation.
   * @param _chan_id The id of the channel to configure. 
   * @param _polarity The ouput polarity.
   * @param  _ref_source The voltage reference source.
   * @param _ref_volt The reference voltage.
   */
  virtual void configure_ao_channel (adl::ChanId _chan_id, 
						                         adl::OutputPolarity _polarity, 
						                         adl::VoltageReferenceSource _ref_source, 
						                         double _ref_volt)
    throw (asl::DAQException);

  /**
   * Write on specified AO channel (numeric version)
   * @param _chan_id The channel write.
   * @param _value The ouput numeric value (non scaled).
   */
  virtual void write_channel (adl::ChanId _chan_id, unsigned short _value)
    throw (asl::DAQException);

  /**
   * Write on specified AO channel (voltage version).
   * @param _chan_id The channel to write.
   * @param _value The ouput voltage value (in Volts).
   */
  virtual void write_scaled_channel (adl::ChanId _chan_id, double _value)
    throw (asl::DAQException);


protected:

  /**
   * Constructor.
   */
	ADSingleShotAOBoard (unsigned short _type, 
                       unsigned short _id,
                       unsigned short nchannels);

  /**
   * Destructor.
   */
  virtual ~ADSingleShotAOBoard();
};

} // namespace adl

#endif // _ADLINK_SINGLE_SHOT_AO_BOARD_H_
