// ============================================================================
//
// = CONTEXT
//    TANGO Project - Tango Tools Library
//
// = FILENAME
//    ADContinuousAOBoard.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _ADLINK_CONTINUOUS_AO_BOARD_H_
#define _ADLINK_CONTINUOUS_AO_BOARD_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/adlink/ADCalibrableAIOBoard.h>
#include <asl/AOData.h>
#include <asl/DAQException.h>
#include <asl/ContinuousAOConfig.h>

namespace adl {

// ============================================================================
//! The ADLink continuous AO capable boards base class.  
// ============================================================================
//!  Base class for boards that perform continuous analog output. \n
//!  Current supported boards are: 
//!  - AD2502
// ============================================================================
class ASL_EXPORT ADContinuousAOBoard : virtual public ADCalibrableAIOBoard
{
public:

  /**
   * Instanciate an ADContinuousAOBoard board.
   * @param _type The type of the card.
   * @param _id The number of the card in the cPCI crate.
   * @param _exclusive_access Allows or not the exclusive access (no exclusive access by default).
   * @return A pointer to the card.
   */
  static ADContinuousAOBoard* instanciate (unsigned short _type,
                                           unsigned short _id,
                                           bool _exclusive_access = false) 
    throw (asl::DAQException);

  /**
   * Duplicate this shared object.
   * @return The duplicated object.
   */
   ADContinuousAOBoard * duplicate ();

  /**
   * Continuous AO configuration.
   * @param config The configuration for analog output.
   */
  void configure_continuous_ao (asl::ContinuousAOConfig& config)
    throw (asl::DAQException);

  /**
   * Start continous AO
   */
  void start_continuous_ao () 
    throw (asl::DAQException);

  /**
   * Stop continous AO
   */
  void stop_continuous_ao () 
    throw (asl::DAQException);

  /**
   * Double-buffering event callback.
   * @param ao_data The output buffer.
   * @param delete_data Controls ao_data ownership. 
   *  Passing true allows ao_db_event_callback to delete ao_data.
   * @param arg The user defined generic argument.
   */
  void ao_db_event_callback (asl::BaseBuffer* ao_data, 
                             bool delete_data, 
                             void* arg = 0)
    throw (asl::DAQException);

  /**
   * @return true if data have been loaded in FIFO. 
   * The information is available after calling start()
   */
  bool use_board_fifo () const
  {
    return this->use_board_fifo_;
  };

protected:

  /**
   * Constructor.
   */
  ADContinuousAOBoard (unsigned short _type, unsigned short _id);

  /**
   * Destructor.
   */
  virtual ~ADContinuousAOBoard();
  
  /**
   * DAQ buffers initialization.
   */
  void init_daq_buffers ()
       throw (asl::DAQException);

  /**
   * DAQ buffers release.
   */
  void release_daq_buffers ();

  /**
   * Load periodic data into the board FIFO.
   */
  void load_periodic_data ()
       throw (asl::DAQException);

   /**
   * AO data unscaling (convert voltage data to binary values).
   * @param _scaled_data The scaled data to convert.
   * @return The unscaled data (i.e. AO raw data).
   */
  asl::AORawData * unscale_data (const asl::AOScaledData * _scaled_data) const
    throw (asl::DAQException);

   /**
   * AO data unscaling for one channel(convert voltage data to binary values).
   * @param _chan The channel to convert.
   * @param _scaled_data The scaled data to convert.
   * @return The unscaled data (i.e. AO raw data).
   */
  asl::AORawData * unscale_data_channel (unsigned long _chan, const asl::AOScaledData * _scaled_data) const
    throw (asl::DAQException);

  /**
   * Returns the internal clock frequency in Hz.
   */
	virtual double clock_frequency () const = 0;

  /**
   * Returns the number of nanoseconds per clock-tick.
   */
	virtual double nsec_per_clock_tick () const = 0;

  /**
   * Configuration for continuous AO.
   */
  asl::ContinuousAOConfig& ao_config_;
  
  /**
   * Buffer index (use to identify "current" buffer).
   */
  int daq_buffer_idx_;

  /**
   * DAQ buffers (for double buffering)
   */
  asl::AORawData * daq_buffer_0_;
  asl::AORawData * daq_buffer_1_;

  /**
   * DAQ buffers identifiers
   */
  unsigned short daq_buffer_0_id_;
  unsigned short daq_buffer_1_id_ ;

  /**
   * AO group setup array (ADLink API arg)
   */
  unsigned short* ao_group_setup_;

  /**
   * True if periodic data loaded into board's FIFO
   */
  bool use_board_fifo_;
};

} // namespace adl

#endif // _ADLINK_CONTINUOUS_AO_BOARD_H_
