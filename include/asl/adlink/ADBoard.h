// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ADBoard.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _ADLINK_BOARD_H_
#define _ADLINK_BOARD_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <iostream>
#include <ace/Recursive_Thread_Mutex.h>
#include <asl/SharedObject.h>
#include <asl/DAQException.h>

namespace adl {

// ============================================================================
// ADLink driver can't register more than 32 boards. The following is garantee
// to be an invalid ID returned by D2K_Register_Card.
// ============================================================================
#define kIMPOSSIBLE_IID 0xffff

// ============================================================================
//! A base class for all ADLink daq board.  
// ============================================================================
//!  
//! detailed description to be written
//! 
// ============================================================================
class ASL_EXPORT ADBoard : public asl::SharedObject
{
public:

  /**
   * Dump hardware info.
   */
  virtual void dump () const;

protected:

  /**
   * Constructor.
   */
  ADBoard (unsigned short _type, unsigned short _id);
  
  /**
   * Destructor.
   */
  virtual ~ADBoard();
 
  /**
   * Register the board with the driver.
   */
  virtual int register_hardware () = 0;

  /**
   * Unregister the board with the driver.
   */
  virtual int release_hardware () = 0;

  /**
   * Check board registration. 
	 * Throws a DAQException if the board has an invalid registration id. Does
	 * nothing otherwise.
   */
	void check_registration () 
    throw (asl::DAQException);
	
  /**
   * The board type
   */
	unsigned short type_;

  /**
   * The board identifier
   */
	unsigned short id_;

  /**
   * The board internal driver identifier
   */
	unsigned short idid_;

  /**
   * True if board can't be shared
   */
  bool exclusive_access_;
};

} // namespace adl

#if defined (__ASL_INLINE__)
# include "asl/adlink/ADBoard.i"
#endif // __ASL_INLINE__

#endif // _ADLINK_BOARD_H_
