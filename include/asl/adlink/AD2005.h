// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    AD2005.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _ADLINK_2005_H_
#define _ADLINK_2005_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/ASLConfig.h>
#include <map>
#include <asl/adlink/ADSingleShotAIBoard.h>
#include <asl/adlink/ADSingleShotAOBoard.h>
#include <asl/adlink/ADSingleShotDIBoard.h>
#include <asl/adlink/ADSingleShotDOBoard.h>
#include <asl/adlink/ADContinuousAIBoard.h>

namespace adl {

// ============================================================================
//! The ADLink 2005 general purpose DAQ card abstraction.  
// ============================================================================
//!  This board is able to perform the following features:
//! - SingleShot AI/AO.
//! - SingleShot DI/DO.
//! - Continuous AI.
//! - Auto-calibration.
// ============================================================================
class ASL_EXPORT AD2005 : public ADSingleShotAIBoard,
                          public ADSingleShotAOBoard,
                          public ADSingleShotDIBoard,
                          public ADSingleShotDOBoard,
                          public ADContinuousAIBoard
{
  typedef std::map<unsigned short, AD2005*> Repository;

  typedef Repository::iterator RepositoryIterator;

  typedef Repository::value_type RepositoryValue;

  typedef std::map<adl::DIOPort, adl::DIODirection> DIOPortDir;

  typedef DIOPortDir::iterator DIOPortDirIterator;

  typedef DIOPortDir::value_type DIOPortDirValue;

public:

  /**
   * Instanciate an AD2005 board.
   * @param _id The number of the card in the cPCI crate.
   * @param _exclusive_access Allows or not the exclusive access (no exclusive access by default).
   * @return A pointer to the card.
   * @throw asl::DAQException 
   */
  static AD2005* instanciate (unsigned short _id, bool _exclusive_access = false)
    throw (asl::DAQException);

  /**
   * Duplicate this shared object.
   */
   AD2005 * duplicate ();

   /**
   * Read one AI channel (numeric version)
   * @param _chan_id The channel to read.
   * @return The value (non scaled).
   */
  virtual unsigned short read_channel (adl::ChanId _chan_id) 
    throw (asl::DAQException);

  /**
   * Read several channels (numeric version)
   * @param _max_chan_id All the channels from channel 0 to _max_channel_id will be read.
   * @return A table containing the inputs values.
   * @throw asl::DAQException 
   */
  virtual asl::AIRawData* read_channels (adl::ChanId _max_chan_id) 
    throw (asl::DAQException);

  /**
   * Read several channels (voltage version)
   * @param _max_chan_id All the channels from channel 0 to _max_channel_id will be read.
   * @return A table containing the inputs values in volts.
   * @throw asl::DAQException 
   */
  virtual asl::AIScaledData* read_scaled_channels (adl::ChanId _max_chan_id) 
    throw (asl::DAQException);

  /**
   * Configure a DIO port for input operation.
   * @param _port The port to configure.
   * @see adl::DIOPort enumeration.
   * @throw asl::DAQException 
   */
	virtual void configure_di_port (adl::DIOPort _port) 
    throw (asl::DAQException);

  /**
   * Configure a DIO port for output operation.
   * @param _port The port to configure.
   * @see adl::DIOPort enumeration.
   * @throw asl::DAQException 
   */
	virtual void configure_do_port (adl::DIOPort _port) 
    throw (asl::DAQException);

  /**
   * Read a single DI line.
   * @param _port The line's port.
   * @see adl::DIOPort enumeration.
   * @param _line The line identifier.
   * @return The read value.
   * @throw asl::DAQException 
   */
	virtual bool read_line (adl::DIOPort _port, int _line) 
    throw (asl::DAQException);

  /**
   * Read a DI port.
   * @param _port The port identifier.
   * @see adl::DIOPort enumeration.
   * @return The read value.
   * @throw asl::DAQException 
   */
	virtual unsigned long read_port (adl::DIOPort _port) 
    throw (asl::DAQException);

  /**
   * Write on a single DI line.
   * @param _port The line's port.
   * @see adl::DIOPort enumeration.
   * @param _line The line identifier.
   * @param _state The new line's state.
   * @throw asl::DAQException 
   */
	virtual void write_line (adl::DIOPort _port, int _line, bool _state) 
    throw (asl::DAQException);

  /**
   * Write on DI port.
   * @param _port The port identifier 
   * @see adl::DIOPort enumeration.
   * @param _state The output value.
   * @throw asl::DAQException 
   */
	virtual void write_port (adl::DIOPort _port, unsigned long _state) 
    throw (asl::DAQException);

  /**
   * Dump hardware info.
   */
  virtual void dump () const;

protected:

  /**
   * Constructor.
   */
	AD2005 (unsigned short _id);

  /**
   * Destructor.
   */
  virtual ~AD2005();
  
  /**
   * Register the board with the driver.
   */
  virtual int register_hardware ();

  /**
   * Unregister the board with the driver.
   */
  virtual int release_hardware ();

  /**
   * Returns the internal clock frequency in Hz.
   */
	virtual double clock_frequency () const;

  /**
   * Returns the number of nanoseconds per clock-tick.
   */
	virtual double nsec_per_clock_tick () const;

private:
  /**
   * Validate DI port identifier
   */
  virtual void check_di_port (adl::DIOPort _port)
    throw (asl::DAQException);

  /**
   * Validate DI line identifier
   */
  virtual void check_di_line (int line)
    throw (asl::DAQException);

  /**
   * Validate DO port identifier
   */
  virtual void check_do_port (adl::DIOPort _port)
    throw (asl::DAQException);

  /**
   * Validate DO line identifier
   */
  virtual void check_do_line (int line)
    throw (asl::DAQException);

  /**
   * Check the input port configuration
   */
  virtual void check_di_port_config (adl::DIOPort _port)
    throw (asl::DAQException);

  /**
   * Check the output port configuration
   */
  virtual void check_do_port_config (adl::DIOPort _port)
    throw (asl::DAQException);

  /**
   * Boards repository.
   */
  static Repository repository;
  
  /**
   * Boards repository' lock.
   */
  static ACE_Recursive_Thread_Mutex repository_lock;
	
  /**
   * DI port.
   */
  adl::DIOPort read_line_port_;

  /**
   * DO port.
   */
  adl::DIOPort write_line_port_;
  
  /**
   * The configuration
   */
  DIOPortDir dio_port_dir_;
};

} // namespace adl

#endif // _ADLINK_2005_H_
