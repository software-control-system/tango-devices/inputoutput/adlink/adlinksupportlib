// ============================================================================
//
// = CONTEXT
//    TANGO Project - Tango Tools Library
//
// = FILENAME
//    AD7432.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _ADLINK_7432_H_
#define _ADLINK_7432_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/ASLConfig.h>
#include <map>
#include <asl/adlink/ADSingleShotDIBoard.h>
#include <asl/adlink/ADSingleShotDOBoard.h>

namespace adl {

// ============================================================================
//! The ADLink 7432 general purpose DAQ card abstraction.  
// ============================================================================
//!  This board is able to perform the following features:
//! - SingleShot DI/DO.
// ============================================================================
class ASL_EXPORT AD7432 : public ADSingleShotDIBoard,
                          public ADSingleShotDOBoard 
{
  typedef std::map<unsigned short, AD7432*> Repository;

  typedef Repository::iterator RepositoryIterator;

  typedef Repository::value_type RepositoryValue;

public:

  /**
   * Instanciate an AD7432 board.
   * @param _id The number of the card in the cPCI crate.
   * @param _exclusive_access Allows or not the exclusive access (no exclusive access by default).
   * @return A pointer to the card.
   */
  static AD7432* instanciate (unsigned short _id, bool _exclusive_access = false) 
    throw (asl::DAQException);

  /**
   * Duplicate this shared object.
   */
   AD7432 * duplicate ();
 
  /**
   * Since the PCI-7432 DI port is not configurable, this method always 
   * throws an exception. It is there for adl::SingleShotDI interface homogeneity 
   * reasons.  
   */
	virtual void configure_di_port (adl::DIOPort _port) 
    throw (asl::DAQException);

  /**
   * Since the PCI-7432 DO port is not configurable, this method always 
   * throws an exception. It is there for adl::SingleShotDO interface homogeneity 
   * reasons.  
   */
	virtual void configure_do_port (adl::DIOPort _port) 
    throw (asl::DAQException);

  /**
   * Read a single DI line.
   * @param _port The line's port - always use adl::port_a as _port parameter value.
   * @param _line The line identifier.
   * @return The read value.
   */
	virtual bool read_line (adl::DIOPort _port, int _line) 
    throw (asl::DAQException);

  /**
   * Read a DI port.
   * @param _port The port identifier - always use adl::port_a as _port parameter value.
   * @return The read value.
   */
	virtual unsigned long read_port (adl::DIOPort _port) 
    throw (asl::DAQException);

  /**
   * Write on a single DO line.
   * @param _port The line's port - always use adl::port_b as _port parameter value.
   * @param _line The line identifier.
   * @param _state The new line's state.
   */
	virtual void write_line (adl::DIOPort _port, int _line, bool _state) 
    throw (asl::DAQException);

  /**
   * Write on DO port.
   * @param _port The port identifier - always use adl::port_b as _port parameter value.
   * @param _state The output value.
   */
	virtual void write_port (adl::DIOPort _port, unsigned long _state) 
    throw (asl::DAQException);

  /**
   * Dump hardware info.
   */
  virtual void dump () const;

protected:

  /**
   * Constructor.
   */
	AD7432 (unsigned short _id);

  /**
   * Destructor.
   */
  virtual ~AD7432();
  
  /**
   * Register the board with the driver.
   */
  virtual int register_hardware ();

  /**
   * Unregister the board with the driver.
   */
  virtual int release_hardware ();

  /**
   * Validate DI port identifier
   */
  virtual void check_di_port (adl::DIOPort _port)
    throw (asl::DAQException);

  /**
   * Validate DI line identifier
   */
  virtual void check_di_line (int line)
    throw (asl::DAQException);

  /**
   * Validate DO port identifier
   */
  virtual void check_do_port (adl::DIOPort _port)
    throw (asl::DAQException);

  /**
   * Validate DO line identifier
   */
  virtual void check_do_line (int line)
    throw (asl::DAQException);

  /**
   * AD7432 boards repository.
   */
  static Repository repository;
	
  /**
   * Boards repository' lock.
   */
  static ACE_Recursive_Thread_Mutex repository_lock;
};

} // namespace adl

#endif // _ADLINK_7432_H_

