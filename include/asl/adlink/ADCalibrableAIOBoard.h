// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ADCalibrableAIOBoard.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _ADLINK_CALIBRABLE_AIO_BOARD_H_
#define _ADLINK_CALIBRABLE_AIO_BOARD_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/adlink/ADLink.h>
#include <asl/adlink/ADBoard.h>
#include <asl/DAQException.h>

namespace adl {

// ============================================================================
//! The ADLink analog-I/O calibrable boards base class.  
// ============================================================================
//!  Base class for boards of which analog channels are calibrable. \n
//!  Current supported boards are: 
//!  - AD2005
//!  - AD2010
//!  - AD2204
//!  - AD2205
// ============================================================================
class ASL_EXPORT ADCalibrableAIOBoard : virtual public ADBoard
{
public:

  /**
   * Performs hardware auto-calibration.
   */
  virtual void auto_calibrate (bool _save_data = false) 
    throw (asl::DAQException);

  /**
   * Saves calibration data into one of the board's banks.
   * @param _bank The bank in which to save the calibration.
   */
  virtual void save_calibration (adl::AIOCalibrationBank _bank) 
    throw (asl::DAQException);

  /**
   * Load calibration data from one of the board's banks.
   * @param _bank The bank The bank from which to load the calibration
   */
  virtual void load_calibration (adl::AIOCalibrationBank _bank) 
    throw (asl::DAQException);

protected:

  /**
   * Constructor.
   */
  ADCalibrableAIOBoard (unsigned short _type, unsigned short _id);

  /**
   * Destructor.
   */
  virtual ~ADCalibrableAIOBoard ();
};

} // namespace adl

#endif // _ADLINK_CALIBRABLE_AIO_BOARD_H_




