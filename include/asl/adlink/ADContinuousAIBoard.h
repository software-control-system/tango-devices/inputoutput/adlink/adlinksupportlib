// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ADContinuousAIBoard.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _ADLINK_CONTINUOUS_AI_BOARD_H_
#define _ADLINK_CONTINUOUS_AI_BOARD_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/adlink/ADCalibrableAIOBoard.h>
#include <asl/AIData.h>
#include <asl/DAQException.h>
#include <asl/ContinuousAIConfig.h>

namespace adl {

// ============================================================================
//! The ADLink continuous AI capable boards base class.  
// ============================================================================
//!  Base class for boards that perform continuous analog input.\n 
//!  Current supported boards are: 
//!  - AD2005
//!  - AD2010
// ============================================================================
class ASL_EXPORT ADContinuousAIBoard : virtual public ADCalibrableAIOBoard
{
public:

  /**
   * Continuous AI factory.
   * @param _type The type of the card.
   * @param _id The number The number of the card in the cPCI crate.
   * @param _exclusive_access The access to this card is exclusive if true.
   * @return The instanciate card.
   */
  static ADContinuousAIBoard* instanciate (unsigned short _type, 
			                                     unsigned short _id, 
			                                     bool _exclusive_access)
    throw (asl::DAQException);

  /**
   * Continuous AI configuration.
   * @param config The configuration.
   */
  virtual void configure_continuous_ai (const asl::ContinuousAIConfig& config) 
    throw (asl::DAQException);

  /**
   * Start continous AI.
   */
  virtual void start_continuous_ai () 
    throw (asl::DAQException);

  /**
   * Stop continous AI.
   */
  virtual void stop_continuous_ai () 
    throw (asl::DAQException);

  /**
   * Double-buffering event callback.
   * @param arg The user defined generic argument.
   * @return A buffer containing the last outputs.
   */
  asl::AIRawData * ai_db_event_callback (void* arg = 0) 
    throw (asl::DataLostException, asl::DAQException);

  /**
   * (re)Trigger event callback.
   * @param arg The user defined generic argument.
   * @return A buffer containing the last outputs.
   */
  asl::AIRawData * ai_trigger_event_callback (void* arg = 0) 
    throw (asl::DataLostException, asl::DAQException);

  /**
   * AI data scaling (convert ADC raw data to volts)
   * @param _raw_data The non-scaled data to convert
   * @param scaled_data_ Destination buffer of the scaled data
   */
  void scale_data (asl::AIRawData * _raw_data, asl::AIScaledData *& scaled_data_) const
    throw (asl::DAQException);

  /**
   * AI data scaling (convert ADC raw data to volts)
   * @param _raw_data The non-scaled data to convert
   * @return The scaled data.
   */
  asl::AIScaledData * scale_data (asl::AIRawData * _raw_data) const
    throw (asl::DAQException);

  /**
   * Returns last sample index in last buffer (pre or middle trigger mode only)
   * @return last sample index in last buffer.
   */
  unsigned long last_sample_index_in_last_daq_buffer () const;


  /**
   * Returns the samples remaining into the DAQ buffer after pre-trigger condition 
   * Returns 0 in case there is no data available
   * @return The remaining samples.
   */
  asl::AIRawData* remaining_samples ()
    throw (asl::DAQException);

   /**
   * Returns the two DAQ buffers - This method is supposed to be used for pre and middle trigger test purpose
   * Should be called once the DAQ_END event is received.
   * @param buffer_0 The pointed buffer contains the data contanined into the first DAQ buffer after the trigger is fired
    * @param buffer_1 The pointed buffer contains the data contanined into the second DAQ buffer after the trigger is fired
   */
  void daq_buffers (asl::AIRawData*& buffer_0, asl::AIRawData*& buffer_1)
    throw (asl::DAQException);
    
protected:

  /**
   * Constructor.
   */
  ADContinuousAIBoard (unsigned short _type, 
                       unsigned short _id, 
                       unsigned short nchannels);

  /**
   * Destructor.
   */
  virtual ~ADContinuousAIBoard();

  /**
   * DAQ buffers initialization.
   */
  int init_daq_buffers ();

  /**
   * DAQ buffers release.
   */
  void release_daq_buffers ();

  /**
   * Returns the default number of nanoseconds per clock-tick.
   */
  virtual double nsec_per_clock_tick () const = 0;

  /**
   * Returns the default internal clock frequency in Hz.
   */
  virtual double clock_frequency () const = 0;

  /**
   * Configuration for continuous AI.
   */
  asl::ContinuousAIConfig ai_config_;

  /**
   * Configuration optimization flag.
   */
  bool first_config_;

  /**
   * Buffer index (use to identify "current" buffer).
   */
  int daq_buffer_idx_;

  /**
   * Buffer identifier (use to identify "first" buffer).
   */
  unsigned short daq_buffer_id_;

  /**
   * DAQ buffers (for double buffering)
   */
  asl::AIRawData * daq_buffer_0_;
  asl::AIRawData * daq_buffer_1_;

private:
  asl::Buffer<unsigned short> channels_arg_;

  bool has_been_started_at_least_once_;

  unsigned long last_sample_index_in_last_daq_buffer_;

  // last buffer indexes (updated at stop command)
  unsigned long stop_pos_;
  unsigned long stop_count_;
};

} // namespace adl

#endif // _ADLINK_CONTINUOUS_AI_BOARD_H_
