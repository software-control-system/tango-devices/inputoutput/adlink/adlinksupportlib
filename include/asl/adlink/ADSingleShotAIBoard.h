// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ADSingleShotAIBoard.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _ADLINK_SINGLE_SHOT_AI_BOARD_H_
#define _ADLINK_SINGLE_SHOT_AI_BOARD_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/ASLConfig.h>
#include <vector>
#include <asl/adlink/ADCalibrableAIOBoard.h>
#include <asl/AIData.h>
#include <asl/DAQException.h>

namespace adl {

// ============================================================================
// AI channels 
// ============================================================================
typedef std::vector<asl::ActiveAIChannel> AIChannels;

// ============================================================================
//! The ADLink single shot AI capable boards base class.  
// ============================================================================
//!  Base class for boards that perform single shot analog input. \n
//!  Current supported boards are: 
//!  - AD2005
//!  - AD2010
//!  - AD2204
//!  - AD2205
// ============================================================================
class ASL_EXPORT ADSingleShotAIBoard : virtual public ADCalibrableAIOBoard
{
public:

  /**
   * Continuous AI factory.
   */
  static ADSingleShotAIBoard* instanciate (unsigned short _type, 
			                                     unsigned short _id, 
			                                     bool _exclusive_access)
    throw (asl::DAQException);

  /**
   * Configure one input channel for single-shot or continuous AI operation.
   * @param _chan_id The id of the channel to configure. 
   * @param _range The range for this channel.
   * @param _ref The ground reference for this channel (only available for boards 2204 and 2205).
   */
  virtual void configure_ai_channel (adl::ChanId _chan_id, 
                                     adl::Range _range, 
                                     adl::GroundRef _ref)
    throw (asl::DAQException);

  /**
   * Read one AI channel (numeric version)
   * @param _chan_id The channel to read.
   * @return The value (non scaled).
   */
  virtual unsigned short read_channel (adl::ChanId _chan_id) 
    throw (asl::DAQException) = 0;

  /**
   * Read one AI channel (voltage version)
   * @param _chan_id The channel to read.
   * @return The value in Volts.
   */
  virtual double read_scaled_channel (adl::ChanId _chan_id) 
    throw (asl::DAQException);

  /**
   * Read several channels (numeric version)
   * @param _max_chan_id The last channel to read.
   * @return A table of the read values. 
   */
  virtual asl::AIRawData* read_channels (adl::ChanId _max_chan_id) 
    throw (asl::DAQException) = 0;

  /**
   * Read several channels (voltage version)
   * @param _max_chan_id The last channel to read.
   * @return A table of the read values in volts.
   */
  virtual asl::AIScaledData* read_scaled_channels (adl::ChanId _max_chan_id) 
    throw (asl::DAQException) = 0;

protected:

  /**
   * Constructor.
   */
	ADSingleShotAIBoard (unsigned short _type, 
                       unsigned short _id,
                       unsigned short nchannels);
  /**
   * Destructor.
   */
  virtual ~ADSingleShotAIBoard();

  /**
   * AI channels configuration
   */
  AIChannels ai_channels;
};

} // namespace adl

#endif // _ADLINK_SINGLE_SHOT_AI_BOARD_H_
