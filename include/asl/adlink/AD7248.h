// ============================================================================
//
// = CONTEXT
//    TANGO Project - Tango Tools Library
//
// = FILENAME
//    AD7248.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _ADLINK_7248_H_
#define _ADLINK_7248_H_
// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/ASLConfig.h>
#include <map>
#include <asl/adlink/ADCalibrableAIOBoard.h>
#include <asl/adlink/ADSingleShotDIBoard.h>
#include <asl/adlink/ADSingleShotDOBoard.h>

namespace adl {

// ============================================================================
//! The ADLink 7248 general purpose DAQ card abstraction.  
// ============================================================================
//!  This board is able to perform the following features:
//! - SingleShot DI/DO.
// ============================================================================
class ASL_EXPORT AD7248 : public ADSingleShotDIBoard,
                          public ADSingleShotDOBoard 
{
  typedef std::map<unsigned short, AD7248*> Repository;

  typedef Repository::iterator RepositoryIterator;

  typedef Repository::value_type RepositoryValue;

  typedef std::map<adl::DIOPort, adl::DIODirection> DIOPortDir;

  typedef DIOPortDir::iterator DIOPortDirIterator;

  typedef DIOPortDir::value_type DIOPortDirValue;

public:

  /**
   * Instanciate an AD7248 board.
   * @param _id The number of the card in the cPCI crate.
   * @param _exclusive_access Allows or not the exclusive access (no exclusive access by default).
   * @return A pointer to the card.
   */
  static AD7248* instanciate (unsigned short _id, bool _exclusive_access = false) 
    throw (asl::DAQException);

  /**
   * Duplicate this shared object.
   * @return A pointer to the card.
   */
   AD7248 * duplicate ();

  /**
   * Configure a DIO port for input or output operation.
   * @param _port The port to configure.
   * @see adl::DIOPort enumeration.
   */
	virtual void configure_di_port (adl::DIOPort _port) 
    throw (asl::DAQException);

  /**
   * Configure a DIO port for input or output operation.
   * @param _port The port to configure.
   * @see adl::DIOPort enumeration.
   */
	virtual void configure_do_port (adl::DIOPort _port) 
    throw (asl::DAQException);

  /**
   * Read a single DI line.
   * @param _port The line's port.
   * @see adl::DIOPort enumeration.
   * @param _line The line identifier.
   * @return The read value.
   */
	virtual bool read_line (adl::DIOPort _port, int _line) 
    throw (asl::DAQException);

  /**
   * Read a DI port.
   * @param _port The port identifier.
   * @see adl::DIOPort enumeration.
   * @return The read value.
   */
	virtual unsigned long read_port (adl::DIOPort _port) 
    throw (asl::DAQException);

  /**
   * Write on a single DI line.
   * @param _port The line's port.
   * @see adl::DIOPort enumeration.
   * @param _line The line identifier.
   * @param _state The new line's state.
   */
	virtual void write_line (adl::DIOPort _port, int _line, bool _state) 
    throw (asl::DAQException);

  /**
   * Write on DI port.
   * @param _port The port identifier.
   * @see adl::DIOPort enumeration.
   * @param _state The output value.
   */
	virtual void write_port (adl::DIOPort _port, unsigned long _state) 
    throw (asl::DAQException);

  /**
   * Dump hardware info.
   */
  virtual void dump () const;

protected:

  /**
   * Constructor.
   */
	AD7248 (unsigned short _id);

  /**
   * Destructor.
   */
  virtual ~AD7248();
  
  /**
   * Register the board with the driver.
   */
  virtual int register_hardware ();

  /**
   * Unregister the board with the driver.
   */
  virtual int release_hardware ();

  /**
   * Validates DI port identifier
   */
  virtual void check_di_port (adl::DIOPort port)
    throw (asl::DAQException);

  /**
   * Validates DI line identifier
   */
  virtual void check_di_line (int line)
    throw (asl::DAQException);

  /**
   * Validates DO port identifier
   */
  virtual void check_do_port (adl::DIOPort port)
    throw (asl::DAQException);

  /**
   * Validates DO line identifier
   */
  virtual void check_do_line (int line)
    throw (asl::DAQException);

  /**
   * Check the input port configuration
   */
  virtual void check_di_port_config (adl::DIOPort _port)
    throw (asl::DAQException);

  /**
   * Check the output port configuration
   */
  virtual void check_do_port_config (adl::DIOPort _port)
    throw (asl::DAQException);

  /**
   * AD7248 boards repository.
   */
  static Repository repository;

  /**
   * Boards repository' lock.
   */
  static ACE_Recursive_Thread_Mutex repository_lock;
	
  adl::DIOPort read_line_port_;
  adl::DIOPort write_line_port_;
/**
  * The configuration
  */
  DIOPortDir dio_port_dir_;
};

} // namespace adl

#endif // _ADLINK_7248_H_



