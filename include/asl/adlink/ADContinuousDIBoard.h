// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ADContinuousDIBoard.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _ADLINK_CONTINUOUS_DI_BOARD_H_
#define _ADLINK_CONTINUOUS_DI_BOARD_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/adlink/ADLink.h>
#include <asl/adlink/ADBoard.h>
#include <asl/DIData.h>
#include <asl/ContinuousDIConfig.h>

namespace adl {

// ============================================================================
//! The ADLink continuous DI capable boards base class.   
// ============================================================================
//!  Base class for boards that perform continuous digital input. \n
//!  Current supported boards are: 
//!  - AD7300
// ============================================================================
class ASL_EXPORT ADContinuousDIBoard : virtual public ADBoard
{
public:

  /**
   * Instanciate an ADContinuousDIBoard board.
   * @param _type The type of the card.
   * @param _id The number of the card in the cPCI crate.
   * @param _exclusive_access Allows or not the exclusive access (no exclusive access by default).
   * @return A pointer to the card.
   */
  static ADContinuousDIBoard* instanciate (unsigned short _type, 
                                           unsigned short _id, 
                                           bool _exclusive_access = false) 
    throw (asl::DAQException);

  /**
   * Duplicate this shared object.
   */
  ADContinuousDIBoard * duplicate ();

  /**
   * Continuous DI configuration.
   * @param config The configuration.
   */
  virtual void configure_continuous_di (const asl::ContinuousDIConfig& config) 
    throw (asl::DAQException);

  /**
   * Start continous DI.
   */
  void start_continuous_di () 
    throw (asl::DAQException);

  /**
   * Stop continous DI.
   */
  void stop_continuous_di () 
    throw (asl::DAQException);

  /**
   * Double-buffering event callback.
   * @param arg The user defined generic argument.
   * @return A pointer to the buffer of input data.
   */
  asl::BaseBuffer * di_db_event_callback (void* arg = 0) 
    throw (asl::DataLostException, asl::DAQException);

protected:

  /**
   * Constructor.
   */
	ADContinuousDIBoard (unsigned short _type, 
                       unsigned short _id);

  /**
   * Destructor.
   */
  virtual ~ADContinuousDIBoard ();

  /**
   * DAQ buffers initialization.
   */
	int init_daq_buffers ();

  /**
   * DAQ buffers release.
   */
	void release_daq_buffers ();

  /**
   * Configuration for continuous AI.
   */
  asl::ContinuousDIConfig di_config_;

  /**
   * Buffer index (use to identify "current" buffer).
   */
  unsigned short daq_buffer_idx_;

  /**
   * DAQ buffers (for double buffering)
   */
  asl::BaseBuffer* daq_buffer_0_;
  asl::BaseBuffer* daq_buffer_1_;
};

} // namespace adl

#endif // _ADLINK_CONTINUOUS_DI_BOARD_H_
