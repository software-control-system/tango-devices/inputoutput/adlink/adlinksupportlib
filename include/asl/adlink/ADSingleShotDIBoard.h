// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ADSingleShotDIBoard.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _ADLINK_SINGLE_SHOT_DI_BOARD_H_
#define _ADLINK_SINGLE_SHOT_DI_BOARD_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/adlink/ADLink.h>
#include <asl/adlink/ADBoard.h>
#include <asl/DAQException.h>

namespace adl {

// ============================================================================
//! The ADLink single shot DI capable boards base class.   
// ============================================================================
//!  Base class for boards that perform single shot digital input. \n
//!  Current supported boards are: 
//!  - AD2005
//!  - AD2010
//!  - AD2204
//!  - AD2205
//!  - AD2505
//!  - AD7248
//!  - AD7300
//!  - AD7432
// ============================================================================
class ASL_EXPORT ADSingleShotDIBoard : virtual public ADBoard
{
public:

  /**
   * ADSingleShotDIBoard factory.
   * @param _type The board's type (see adl::PCI_Boards enumeration).
   * @param _id The board's identifier in the cPCI crate.
   * @param _exclusive_access Pass true to obtain exclusive access to the hardware, false otherwise.
   * @return A reference to the newly instanciated board.
   */
  static ADSingleShotDIBoard * instanciate (unsigned short _type, 
                                            unsigned short _id, 
                                            bool _exclusive_access)
    throw (asl::DAQException);

  /**
   * Configure a DIO port for input or output operation.
   * @param _port The port to configure (see adl::DIOPort enumeration).
   */
	virtual void configure_di_port (adl::DIOPort _port) 
    throw (asl::DAQException) = 0;

  /**
   * Read a single DI line.
   * @param _port The line's port (see adl::DIOPort enumeration).
   * @param _line The line identifier.
   * @return The read value.
   */
	virtual bool read_line (adl::DIOPort _port, int _line) 
    throw (asl::DAQException) = 0;

  /**
   * Read a DI port.
   * @param _port The port identifier (see adl::DIOPort enumeration).
   * @return The read value.
   */
	virtual unsigned long read_port (adl::DIOPort _port) 
    throw (asl::DAQException) = 0;

protected:

  /**
   * Protected constructor.
   * @param _type The board's type (see adl::PCI_Boards enumeration).
   * @param _id The board's identifier in the cPCI crate.
   */
	ADSingleShotDIBoard (unsigned short _type, unsigned short _id);

  /**
   * Protected destructor.
   */
  virtual ~ADSingleShotDIBoard ();

  /**
   * Validate DI port identifier
   */
  virtual void check_di_port (adl::DIOPort _port)
    throw (asl::DAQException) = 0;

  /**
   * Validate DI line identifier
   */
  virtual void check_di_line (int line)
    throw (asl::DAQException) = 0;
};

} // namespace adl

#endif // _ADLINK_SINGLE_SHOT_DI_BOARD_H_


