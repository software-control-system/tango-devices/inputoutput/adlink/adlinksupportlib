// ============================================================================
//
// = CONTEXT
//    TANGO Project - Tango Tools Library
//
// = FILENAME
//    ADBoard.i
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

namespace adl {	
	
// ============================================================================
// ADBoard::check_registration
// ============================================================================
ASL_INLINE void 
ADBoard::check_registration () throw (asl::DAQException)
{
  if (this->idid_ ==  kIMPOSSIBLE_IID) 
  {
	throw asl::DAQException(static_cast<const char*>("invalid registration identifier"),
							static_cast<const char*>("board initialization failed"),
							static_cast<const char*>("ADBoard::check_registration"));
  }
}

} // namespace adl


