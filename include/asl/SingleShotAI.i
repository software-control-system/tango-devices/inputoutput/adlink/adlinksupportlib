// ============================================================================
//
// = CONTEXT
//    TANGO Project - Tango Tools Library
//
// = FILENAME
//    SingleShotAI.i
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

namespace asl {

// ============================================================================
// SingleShotAI::initialized
// ============================================================================
ASL_INLINE bool
SingleShotAI::initialized () const
{
  return (this->daq_hw_) ? true : false;
}

} // namespace asl


