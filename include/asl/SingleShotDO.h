// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    SingleShotDO.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _SINGLE_SHOT_DO_H_
#define _SINGLE_SHOT_DO_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/adlink/ADSingleShotDOBoard.h>
#include <asl/SingleShotDAQ.h>

namespace asl {

// ============================================================================
//! The ASL single shot digital output DAQ class.   
// ============================================================================
//! Provides functions to perform single shot digital output on ADLink boards that
//! support this functions.\n
//! The main fonctionnalities are write one line or one port.
// ============================================================================
class ASL_EXPORT SingleShotDO : public SingleShotDAQ
{
public:

  /**
   * Initialization. 
   */
  SingleShotDO  ();
  
  /**
   * Release resources.
   */
  virtual ~SingleShotDO  ();
  
  /**
   * Initialize the single shot digital output DAQ.
   * @param hw_type The type of the card.
   * @param hw_id The number of the card in the cPCI crate.
   */
  virtual void init (unsigned short hw_type, unsigned short hw_id) 
    throw (DAQException);
 
  /**
   * Configure the ports in output.
   * @param _port The port to configure.
   */
  void configure_port (adl::DIOPort _port) 
    throw (DAQException);
 
  /**
   * write on one line in output.
   * @param _port The port.
   * @param _line The line.
   * @param _state The output value.
   */
  void write_line (adl::DIOPort _port, int _line, bool _state) 
    throw (DAQException);

  /**
   * write a 32-bit value in output.
   * @param _port The port.
   * @param _state The output value.
   */
  void write_port (adl::DIOPort _port, unsigned long _state) 
    throw (DAQException);

private:

	/**
   * The underlying hardware.
   */
   adl::ADSingleShotDOBoard * daq_hw_;

  /**
   * Return true if initialized, false otherwise.
   */
   bool initialized () const;
};

} // namespace asl
	
#if defined (__ASL_INLINE__)
# include <asl/SingleShotDO.i>
#endif // __ASL_INLINE__

#endif // _SINGLE_SHOT_DO_H_

