// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ContinuousAI.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _CONTINUOUS_AI_CONFIG_H_
#define _CONTINUOUS_AI_CONFIG_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/ASLConfig.h>
#include <vector>
#if defined(ASL_DEBUG)
# include <asl/Mld.h>
#endif
#include <asl/adlink/ADLink.h>
#include <asl/Data_T.h>
#include <asl/DAQException.h>

// ============================================================================
// Forward declaration
// ============================================================================
namespace adl {
  class ADContinuousAIBoard;
}

namespace asl { 

// ============================================================================
// Create a dedicated type for DAQ events callback
// ============================================================================
#if defined(WIN32)
  typedef void (*DAQCallback) ();
#else
  typedef void (*DAQCallback) (int);
#endif

// ============================================================================
// Create a dedicated type for std::vector of asl::ActiveAIChannel
// ============================================================================
typedef std::vector<asl::ActiveAIChannel> ActiveAIChannels;
typedef asl::Buffer<double> DataMask;

// ============================================================================
//! The ASL continuous analog input configuration class.  
// ============================================================================
//!  
//! This class encapsulates the ContinuousAI configuration parameters.
//! Most of this parameters are defined in ADLink.h.
//! 
// ============================================================================
class ASL_EXPORT ContinuousAIConfig  
{
  friend class ContinuousAI;
  friend class adl::ADContinuousAIBoard;

public:

  /**
   * ContinousAIConfig config ctor.
   */
  ContinuousAIConfig ();
  
  /**
   * ContinousAIConfig copy ctor.
   * @param src The source configuration.
   */
  ContinuousAIConfig (const ContinuousAIConfig& src);

  /**
   * ContinousAIConfig operator=.
   * @param src The source configuration.
   */
  ContinuousAIConfig& operator= (const ContinuousAIConfig& src);

  /**
   * ContinousAIConfig dtor.
   */
  virtual ~ContinuousAIConfig ();

  /**
   * Adds an AI channels to the ActiveAIChannel list.
   * See class ActiveAIChannel in ADLink.h for details.
   * @param ac The channel to be activated/enabled.
   */
  void add_active_channel (const ActiveAIChannel& ac);

  /**
   * Returns the <ActiveAIChannel> list.
   * @return The active channels list.
   */
  const ActiveAIChannels& get_active_channels () const;

  /**
   * Returns the current number of active channels.
   * @return The number of active channels.
   */
  int num_active_channels () const;

  /**
   * Returns true if the active channels are ordered increasingly, 
   * returns false otherwise.
   * @return true if channels are ordered increasingly, false otherwise.
   */
  bool active_channels_ordered_from_zero () const;

  /**
   * Returns true if the active channels have the same input range,
   * returns false otherwise.
   * @return true have the same input range, false otherwise.
   */
  bool active_channels_have_same_range (adl::Range& common_range_) const;

  /**
   * Set the AI conversion signal source (i.e. DAC sampling source).
   * Use one of the following: adl::ai_internal_timer, adl::ai_external_afio.
   * Defaults to adl::ai_internal_timer.
   * @param src DAC sampling source.
   */
  void set_conversion_source (adl::AIConversionSource src);

  /**
   * Returns the AI conversion signal source (i.e. DAC sampling source).
   * @return The current AI conversion signal source.
   */
  adl::AIConversionSource get_conversion_source () const;

  /**
   * Set the DAQ trigger source.
   * Use one of the following: adl::internal_software, adl::external_analog, adl::external_digital
   * Defaults to adl::internal_software.
   * @param src DAQ trigger source.
   */
  void set_trigger_source (adl::AIOTriggerSource src);

  /**
   * Returns the DAQ trigger source.
   * @return The DAQ trigger source.
   */
  adl::AIOTriggerSource get_trigger_source () const;

  /**
   * Set the DAQ trigger mode.
   * Use one of the following: adl::ai_post, adl::ai_delay, adl::ai_pre, adl::ai_middle.
   * Defaults to adl::ai_post.
   * @param mode The DAQ trigger mode.
   */
  void set_trigger_mode (adl::AITriggerMode mode);

  /**
   * Returns the DAQ trigger mode.
   * @return The DAQ trigger mode.
   */
  adl::AITriggerMode get_trigger_mode () const;

  /**
   * Set the DAQ trigger polarity.
   * Use one of the following: adl::ai_rising_edge, adl::ai_falling_edge.
   * Defaults to adl::ai_rising_edge.
   * @param tp The DAQ trigger polarity.
   */
  void set_trigger_polarity (adl::AITriggerPolarity tp);

  /**
   * Returns the DAQ trigger polarity.
   * @return The DAQ trigger polarity.
   */
  adl::AITriggerPolarity get_trigger_polarity () const;

  /**
   * Set the DAQ trigger delay unit (i.e. time between trigger and actual DAQ start).
   * Use one of the following: adl::clock_ticks, adl::samples.
   * Defaults to adl::clock_ticks.
   * @param du The DAQ trigger delay unit.
   */
  void set_delay_unit (adl::AIODelayUnit du);

  /**
   * Returns the DAQ trigger delay unit.
   * @return The DAQ trigger delay unit.
   */
  adl::AIODelayUnit get_delay_unit () const;

  /**
   * This configuration parameter is only valid for middle trigger and delay trigger modes.
   * For middle trigger, it indicates the number of samples will be accessed after a specific 
   * trigger event. For delay trigger, indicates the number of samples or nanoseconds will
   * be ignored after a specific trigger event (see ContinuousAIConfig::set_delay_unit).
   * Defaults to 0.
   */
  void set_middle_or_delay_scans (double num_scans_or_nsec);

  /**
   * The returned configuration parameter is only valid for middle trigger and delay trigger 
   * mode. For middle trigger, it indicates the number of samples will be accessed after a 
   * specific trigger event. For delay trigger, indicates the number of samples or nanoseconds 
   * will be ignored after a specific trigger event (see ContinuousAIConfig::set_delay_unit).
   */
  double get_middle_or_delay_scans () const;

  /**
   * Enables infinite re-trigger. AI operation will be triggered infinitely. 
   * This is only valid for delay and post trigger mode.
   * @param nts The total number of "trigger sequences" to acquire. 
   * Set nts to 0 (the default) for infinite retrig mode or specify the number of "trigger 
   * sequences" to acquire before stopping the data acquisition. In this case the data 
   * acquisition is automatically stopped by the driver and a DAQEnd notification is sent
   * to the user. The nts valid range is [0, 65535] (driver limitation).
   */
  void enable_retrigger (unsigned long nts = 0);

  /**
   * Returns the total num. of trig. sequences to acquire (retrigger mode only)
   */
  unsigned long num_trigger_sequences () const;

  /**
   * Disables infinite re-trigger. AI operation will NOT be triggered infinitely 
   * (one shot DAQ). This is only valid for delay and post trigger mode.
   */
  void disable_retrigger ();

  /**
   * Returns true if infinite re-trigger is enabled, false otherwise.
   */
  bool retrigger_enabled () const;

  /**
   * Enables intermediate buffers sending (for finite RETRIG mode only).
   * @param ibn Intermediate buffer number, i.e. the client will receive intermediate buffers every
   *            ibn triggs.
   * Modus operandi:
   *  - enable_retrigger(val); // val != 0 => finite RETRIG mode
   *  - enable_intermediate_buffers(ibn);  => the client will receive a
   *                                          callback every ibn trigger signals +
   *                                          one callback at the end of acquisition
   */
  void enable_intermediate_buffers (unsigned long ibn);

  /**
   * Disables intermediate buffers sending (for finite RETRIG mode only).
   * Modus operandi:
   *  - enable_retrigger(val); // val != 0 => finite RETRIG mode
   *  - disable_intermediate_buffers();    => the client will receive a unique
   *                                       callback at the end of acquisition.
   * This is the default behaviour.
   */
  void disable_intermediate_buffers ();

  /**
   * Returns true if intermediate buffers sending is enabled, false otherwise.
   */
  bool intermediate_buffers_enabled () const;

  /**
   * Returns the intermediate number of triggers (retrigger + intermediate mode only)
   */
  unsigned long num_intermediate_trigger () const;

  /**
   * Enables the MCounter . This is only valid for pre-trigger and middle trigger 
   * mode. 
   *
   * ### WARNING: NEVER TESTED FEATURE ### 
   */
  void enable_mcounter ();

  /**
   * Disables the MCounter . This is only valid for pre-trigger and middle trigger 
   * mode. 
   *
   * ### WARNING: NEVER TESTED FEATURE ### 
   */
  void disable_mcounter ();

  /**
   * Returns true if MCounter is enabled, false otherwise.
   *
   * ### WARNING: NEVER TESTED FEATURE ### 
   */
  bool mcounter_enabled () const;

  /**
   * Set the counter value of MCounter. This argument is only valid for pre-trigger and
   * middle trigger mode. 
   * Defaults to 0.
   *
   * ### WARNING: NEVER TESTED FEATURE ### 
   *
   * @param mcounter the MCounter value of MCounter.
   */
  void set_mcounter_value (unsigned short mcounter);

  /**
   * Returns the MCounter value.
   *
   * ### WARNING: NEVER TESTED FEATURE ### 
   *
   * @return The MCounter value.
   */
  unsigned short get_mcounter_value () const;

  /**
   * Set the DAQ analog trigger source. This configuration parameter is only 
   * valid if DAQ trigger source has been set to adl::external_analog.
   * Use one of the following: adl::analog_trigger_chan0, adl::analog_trigger_chan1
   * adl::analog_trigger_chan2, adl::analog_trigger_chan3, adl::analog_trigger_ext.
   * Defaults to adl::analog_trigger_chan0.
   * @param ats The DAQ analog trigger source.
   */
  void set_analog_trigger_source (adl::AnalogTriggerChannel ats);

  /**
   * Returns the DAQ analog trigger source. This configuration parameter is only 
   * valid if DAQ trigger source has been set to adl::external_analog.
   * @return The DAQ analog trigger source.
   */
  adl::AnalogTriggerChannel get_analog_trigger_source () const;

  /**
   * Set the DAQ analog trigger condition (i.e. criteria). This configuration parameter is
   * only valid if DAQ trigger source has been set to adl::external_analog.
   * Use one of the following: adl::below_low_level,  adl::above_high_level, 
   * adl::inside_region, adl::high_hysteresis. See adl::AnalogTriggerCondition for details.
   * Defaults to adl::above_high_level.
   * @param atc The DAQ analog trigger condition.
   */
  void set_analog_trigger_condition (adl::AnalogTriggerCondition atc);

  /**
   * Returns the DAQ analog trigger condition. This configuration parameter is only 
   * valid if DAQ trigger source has been set to adl::external_analog.
   * @return The DAQ analog trigger condition.
   */
  adl::AnalogTriggerCondition get_analog_trigger_condition () const;

  /**
   * Set the analog trigger low-level condition. This configuration parameter is only 
   * valid if DAQ trigger source has been set to adl::external_analog.
   * Defaults to 0.
   * @param at_llc The analog trigger low-level condition in numeric format.
   */
  void set_analog_low_level_condition (unsigned short at_llc);

  /**
   * Returns the DAQ analog trigger low level condition. This configuration parameter 
   * is only valid if DAQ trigger source has been set to adl::external_analog.
   * @return The DAQ analog trigger low level condition in numeric format.
   */
  unsigned short get_analog_low_level_condition () const;

  /**
   * Set the analog trigger high-level condition. This configuration parameter is only 
   * valid if DAQ trigger source has been set to adl::external_analog.
   * Defaults to 0.
   * @param at_hlc The analog trigger high-level condition in numeric format.
   */
  void set_analog_high_level_condition (unsigned short at_hlc);

  /**
   * Returns the DAQ analog trigger high-level condition. This configuration parameter 
   * is only valid if DAQ trigger source has been set to adl::external_analog.
   * @return The DAQ analog trigger high-level condition in numeric format.
   */
  unsigned short get_analog_high_level_condition () const;

  /**
   * Set the DAQ sampling rate. This configuration parameter is only valid if the AI 
   * conversion signal source (i.e. DAC sampling source) has been set to 
   * adl::ai_internal_timer. Valid range is board dependent.
   * Defaults to 1000.
   * @param sr The DAQ sampling rate in Hertz (board dependent value).
   */
  void set_sampling_rate (double sr);

  /**
   * Returns the DAQ sampling rate. This configuration parameter is only valid if the 
   * AI conversion signal source (i.e. DAC sampling source) has been set to 
   * adl::ai_internal_timer.
   * @return The DAQ sampling rate in Hertz (board dependent value).
   */
  double get_sampling_rate () const;

  /**
   * Specifies the strategy to aplly in case of buffer overrun (i.e. data lost).
   * Use one of the following: adl::ignore, adl::trash, adl::notify, adl::restart, 
   * adl::abort. See adl::DataLostStrategy for details.
   * Defaults to adl::ignore.
   * @param The data lost strategy.
   */
  void set_data_lost_strategy (adl::DataLostStrategy _strategy);

  /**
   * Returns the data lost strategy.
   * @return The data lost strategy.
   */
  adl::DataLostStrategy get_data_lost_strategy () const;

  /**
   * Set the DAQ buffer depth. The user defined callback ContinuousAI::handle_input
   * will be called each time "depth" samples have been acquired on each active channel.
   * The specified value must be even (otherwise an exception will be thrown at DAQ startup).
   * Defaults to: 500.
   * @param db_depth The DAQ buffer depth in samples per active channel.
   */
  void set_buffer_depth (unsigned long db_depth);

  /**
   * Returns the DAQ buffer depth.
   * @return The DAQ buffer depth in samples per active channel.
   */
  unsigned long get_buffer_depth () const;

  /**
   * Set the DAQ timeout. The user defined callback ContinuousAI::handle_timeout
   * will be called each time the specified timeout elapsed and no data could been 
   * acquired. This provides a way to detect DAQ problems (such as DAQ buffer depth 
   * too small or no trigger activity). 
   * @param tmo_sec The DAQ timeout in milliseconds.
   */
  void set_timeout (long tmo_sec);

  /**
   * Returns the DAQ timeout.
   * @return The DAQ timeout in milliseconds.
   */
  long get_timeout () const;

  /**
   * Enables the DAQ history buffer. The history buffer will contain the last hlen_msec
   * milliseconds of acqusition. Can act as a post-mortem buffer if DAQ trigger mode has 
   * been set to adl::pre.
   * @param hlen_msec The DAQ history buffer length in milliseconds.
   */
  void enable_history (unsigned long hlen_msec = 1000);

  /**
   * Disables the DAQ history buffer.
   */
  void disable_history ();

  /**
   * Returns true if the DAQ history buffer is enabled, false otherwise.
   */
  bool history_enabled () const;

  /**
   * Returns the DAQ history buffer length.
   * @return The DAQ history buffer in milliseconds.
   */
  unsigned long get_history_length () const;

  /**
   * Set the DAQ history buffer length. The history buffer will contain the last
   * hlen_msec milliseconds of acqusition. 
   * @param hlen_msec The DAQ history buffer length in milliseconds.
   */
  void set_history_length (unsigned long hlen_msec);

  /**
   * Enables the recovery of the last acquisition buffer (partially filled). 
   * The history buffer will contain this last buffer.
   * No action on PRE & MIDDLE modes (always enabled).
   */
  void enable_last_buffer_recovery();

  /**
   * Disables the recovery of the last acquisition buffer (partially filled). 
   * The history buffer will contain this last buffer.
   * No action on PRE & MIDDLE modes (always enabled).
   */
  void disable_last_buffer_recovery();

  /**
   * Returns true if the recovery of the last acquisition buffer is enabled, 
   * false otherwise.
   */
  bool last_buffer_recovery_enabled () const;

  /**
   * Enables the user data conversion for all channels (scaled to user data)
   */
  void enable_user_data ();

  /**
   * Disables the user data conversion for all channels (scaled to user data)
   */
  void disable_user_data ();

  /**
   * Enables the user data conversion for the specified channel (scaled to user data)
   */
  void enable_user_data (adl::ChanId id);

  /**
   * Disables the user data conversion for the specified channel (scaled to user data)
   */
  void disable_user_data (adl::ChanId id);

  /**
   * Returns true if the user data conversion is currently enabled for the 
   * specified channel, returns false otherwise.
   */
  bool user_data_enabled (adl::ChanId id) const;

  /**
   * Returns true if the user data conversion is currently enabled for at least one of 
   * the active channels, returns false otherwise.
   */
  bool user_data_enabled () const;

  /**
   * Set the user data parameters to the specified values, for the specified channel
   */
  void set_user_data_parameters (adl::ChanId id, double gain, 
                                 double offset1, double offset2);

  /**
   * Returns the user data parameters of the specified channel
   */
  void get_user_data_parameters (adl::ChanId id, double& gain, 
                                 double& offset1, double& offset2) const; 

  /**
   * Enables the (user defined) data mask
   */
  void enable_data_mask ();

  /**
   * Disables the (user defined) data mask
   */
  void disable_data_mask ();

  /**
   * Returns true if the (user defined) data mask is currently enabled, returns false otherwise.
   */
  bool data_mask_enabled () const;

  /**
   * Set the (user defined) data mask
   */
  void set_data_mask (const std::vector<bool> & dm);

  /**
   * Returns (user defined) data mask
   */
  void get_data_mask (std::vector<bool> & dm);

  /**
   * Returns (user defined) data mask buffer length
   */
  size_t get_data_mask_buffer_length () const;

  /**
   * Sets the DAQ timebase type.
   * @param tb The DAQ timebase type.
   */
  void set_timebase_type(adl::AIOTimeBase tb);

  /**
   * Returns the DAQ timebase type.
   * @return The DAQ timebase type.
   */
  adl::AIOTimeBase get_timebase_type() const;

  /**
   * Sets the DAQ external timebase value in Hz.
   * @param freq The DAQ external timebase value in Hz.
   */
  void set_ext_timebase_val(double freq);

  /**
   * Returns the DAQ external timebase value in Hz.
   * @return The DAQ external timebase value in Hz.
   */
  double get_ext_timebase_val() const;

  /**
   * Checks the configuration (might throw an exception)
   */
  void check () const
    throw (asl::DAQException);
  
  /**
   * Dump this configuration.
   */
  void dump (); 

private:
  //- private - not documented method
  void set_db_event_callback (DAQCallback _cb);

  //- private - not documented method
  DAQCallback get_db_event_callback () const;

  //- private - not documented method
  void set_trigger_event_callback (DAQCallback _cb);

  //- private - not documented method
  DAQCallback get_trigger_event_callback () const;

  //- private - not documented method
  void set_ai_end_event_callback (DAQCallback _cb);

  //- private - not documented method
  DAQCallback get_ai_end_event_callback () const;

  //- analog input active channels
  ActiveAIChannels channels_;

  //- conversion source
  adl::AIConversionSource conversion_source_;

  //- trigger source
  adl::AIOTriggerSource trigger_source_;

  //- trigger mode
  adl::AITriggerMode trigger_mode_;

  //- trigger mode
  adl::AITriggerPolarity trigger_polarity_;

  //- delay unit (internal-clock-ticks or samples)
  adl::AIODelayUnit delay_unit_;

  //- # scans for middle or delay trigger mode
  double middle_or_delay_scans_;

  //- retrigger on/off option
  adl::AIRetriggerStatus retrigger_status_;

  //- mCounter on/off option
  adl::AIMCounterStatus mcounter_status_;

  //- mCounter value 
  unsigned short mcounter_value_;

  //- analog trigger source
  adl::AnalogTriggerChannel analog_trigger_channel_;

  //- analog trigger condition
  adl::AnalogTriggerCondition analog_trigger_condition_;

  //- analog trigger mode low level condition
  unsigned short analog_trigger_lo_level_;

  //- analog trigger mode high level condition
  unsigned short analog_trigger_hi_level_;

  //- internal sampling rate
  double sampling_rate_;

  //- data_lost strategy
  adl::DataLostStrategy data_lost_strategy_;

  //- half-db-buffer depth in samples
  unsigned long buffer_depth_;

  //- daq event callback address
  DAQCallback db_event_callback_;

  //- daq event callback address
  DAQCallback trigger_event_callback_;

  //- daq event callback address
  DAQCallback ai_end_event_callback_;

  //- daq timeout in seconds 
  long timeout_;

  //- depth of the circular biuffer will be: cb_factor_ * buffer_depth_
  unsigned long history_len_msec_;

  //- circular buffer on/off option
  bool history_enabled_;

  //- last buffer recovery on/off option
  bool last_buffer_recovery_enabled_;

  //- num total of trig. seqs. to acquire
  unsigned long num_retrig_sequences_;

  //- user defined data mask 
  bool data_mask_enabled_;
  DataMask * data_mask_;

  //- intermediate buffer sending flag
  bool intermediate_buffer_send_enabled_;

  //- intermediate number of triggers
  unsigned long intermediate_num_triggers_;

 //- timebase type
  adl::AIOTimeBase timebase_;

  //- external timebase value in Hz
  double ext_timebase_val_;

};

} // nemespace asl

#if defined (__ASL_INLINE__)
# include "asl/ContinuousAIConfig.i"
#endif // __ASL_INLINE__

#endif // _CONTINUOUS_AI_CONFIG_H_
