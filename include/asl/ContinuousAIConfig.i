// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ContinuousAIConfig.i
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

namespace asl {

// ============================================================================
// ContinuousAIConfig::add_active_channel
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::add_active_channel (const ActiveAIChannel& _ac) 
{
  this->channels_.push_back(_ac);
}

// ============================================================================
// ContinuousAIConfig::get_active_channels
// ============================================================================
ASL_INLINE const ActiveAIChannels& 
ContinuousAIConfig::get_active_channels () const
{
  return this->channels_;
}

// ============================================================================
// ContinuousAIConfig::num_active_channels
// ============================================================================
ASL_INLINE int 
ContinuousAIConfig::num_active_channels () const
{
  return this->channels_.size();
}

// ============================================================================
// ContinuousAIConfig::active_channels_have_same_range
// ============================================================================
ASL_INLINE bool 
ContinuousAIConfig::active_channels_have_same_range (adl::Range& common_range_) const
{
  switch (this->channels_.size()) 
  {
    case 0:
      return false;
      break;
    case 1:
      common_range_ = this->channels_[0].range;
      return true;
      break;
    default:
      {
        common_range_ = this->channels_[0].range;
        for (unsigned int i = 1; i < this->channels_.size(); i++) 
        {
          if (common_range_ != this->channels_[i].range) 
          {
            return false;
          }
        } 
      }
      break;
  }
  return true;
}

// ============================================================================
// ContinuousAIConfig::set_conversion_source
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::set_conversion_source (adl::AIConversionSource _src)
{
  this->conversion_source_ = _src;
}

// ============================================================================
// ContinuousAIConfig::get_conversion_source
// ============================================================================
ASL_INLINE adl::AIConversionSource 
ContinuousAIConfig::get_conversion_source () const
{
	return this->conversion_source_;
}

// ============================================================================
// ContinuousAIConfig::set_trigger_source
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::set_trigger_source (adl::AIOTriggerSource _src)
{
	this->trigger_source_ = _src;
}

// ============================================================================
// ContinuousAIConfig::get_trigger_source
// ============================================================================
ASL_INLINE adl::AIOTriggerSource
ContinuousAIConfig::get_trigger_source () const
{
	return this->trigger_source_;
}

// ============================================================================
// ContinuousAIConfig::set_trigger_mode 
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::set_trigger_mode (adl::AITriggerMode _mode)
{
	this->trigger_mode_ = _mode;
}

// ============================================================================
// ContinuousAIConfig::get_trigger_mode
// ============================================================================
ASL_INLINE adl::AITriggerMode 
ContinuousAIConfig::get_trigger_mode () const
{
	return this->trigger_mode_;
}

// ============================================================================
// ContinuousAIConfig::set_trigger_polarity
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::set_trigger_polarity (adl::AITriggerPolarity _polarity)
{
	this->trigger_polarity_ = _polarity;
}

// ============================================================================
// ContinuousAIConfig::get_trigger_polarity
// ============================================================================
ASL_INLINE adl::AITriggerPolarity 
ContinuousAIConfig::get_trigger_polarity () const
{
	return this->trigger_polarity_;
}

// ============================================================================
// ContinuousAIConfig::set_delay_unit
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::set_delay_unit (adl::AIODelayUnit _unit)
{
	this->delay_unit_ = _unit;
}

// ============================================================================
// ContinuousAIConfig::get_delay_unit
// ============================================================================
ASL_INLINE adl::AIODelayUnit 
ContinuousAIConfig::get_delay_unit () const
{
	return this->delay_unit_;
}

// ============================================================================
// ContinuousAIConfig::set_middle_or_delay_scans
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::set_middle_or_delay_scans (double _n_scans_or_nsec)
{														
	this->middle_or_delay_scans_ = _n_scans_or_nsec;
}

// ============================================================================
// ContinuousAIConfig::get_middle_or_delay_scans
// ============================================================================
ASL_INLINE double  
ContinuousAIConfig::get_middle_or_delay_scans () const
{
	return this->middle_or_delay_scans_;
}

// ============================================================================
// ContinuousAIConfig::enable_retrigger
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::enable_retrigger (unsigned long _nts)
{
  this->retrigger_status_ = adl::ai_rt_enabled;
  
  if (_nts > 65535)
    _nts = 65535;
    
  this->num_retrig_sequences_ = _nts;
}

// ============================================================================
// ContinuousAIConfig::num_trigger_sequences
// ============================================================================
ASL_INLINE unsigned long 
ContinuousAIConfig::num_trigger_sequences () const
{
  return this->num_retrig_sequences_;
}

// ============================================================================
// ContinuousAIConfig::disable_retrigger
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::disable_retrigger ()
{
  this->retrigger_status_ = adl::ai_rt_disabled;
}

// ============================================================================
// ContinuousAIConfig::retrigger_enabled
// ============================================================================
ASL_INLINE bool 
ContinuousAIConfig::retrigger_enabled () const
{
  return (this->retrigger_status_ == adl::ai_rt_enabled) ? true : false;
}

// ============================================================================
// ContinuousAIConfig::enable_intermediate_buffers
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::enable_intermediate_buffers (unsigned long ibn)
{
  this->intermediate_buffer_send_enabled_ = true;
  this->intermediate_num_triggers_ = ibn;
}

// ============================================================================
// ContinuousAIConfig::disable_intermediate_buffers
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::disable_intermediate_buffers ()
{
  this->intermediate_buffer_send_enabled_ = false;
}

// ============================================================================
// ContinuousAIConfig::intermediate_buffers_enabled
// ============================================================================
ASL_INLINE bool 
ContinuousAIConfig::intermediate_buffers_enabled () const
{
  return this->intermediate_buffer_send_enabled_;
}

// ============================================================================
// ContinuousAIConfig::num_intermediate_trigger
// ============================================================================
ASL_INLINE unsigned long 
ContinuousAIConfig::num_intermediate_trigger () const
{
  return this->intermediate_num_triggers_;
}

// ============================================================================
// ContinuousAIConfig::enable_mcounter
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::enable_mcounter ()
{
  this->mcounter_status_ = adl::ai_mc_enabled;
}

// ============================================================================
// ContinuousAIConfig::disable_mcounter
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::disable_mcounter ()
{
  this->mcounter_status_ = adl::ai_mc_disabled;
}

// ============================================================================
// ContinuousAIConfig::mcounter_enabled
// ============================================================================
ASL_INLINE bool 
ContinuousAIConfig::mcounter_enabled () const
{
  return (this->mcounter_status_ == adl::ai_mc_enabled) ? true : false;
}

// ============================================================================
// ContinuousAIConfig::set_mcounter_value
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::set_mcounter_value (unsigned short _value)
{
  this->mcounter_value_ = _value;
}

// ============================================================================
// ContinuousAIConfig::get_mcounter_value
// ============================================================================
ASL_INLINE unsigned short 
ContinuousAIConfig::get_mcounter_value () const
{
  return this->mcounter_value_;
}

// ============================================================================
// ContinuousAIConfig::set_analog_trigger_source
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::set_analog_trigger_source (adl::AnalogTriggerChannel _src)
{
	this->analog_trigger_channel_ = _src;
}

// ============================================================================
// ContinuousAIConfig::get_analog_trigger_source
// ============================================================================
ASL_INLINE adl::AnalogTriggerChannel 
ContinuousAIConfig::get_analog_trigger_source () const
{
	return this->analog_trigger_channel_;
}

// ============================================================================
// ContinuousAIConfig::set_analog_trigger_condition
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::set_analog_trigger_condition (adl::AnalogTriggerCondition _cond)
{
	this->analog_trigger_condition_ = _cond;
}

// ============================================================================
// ContinuousAIConfig::get_analog_trigger_condition
// ============================================================================
ASL_INLINE adl::AnalogTriggerCondition 
ContinuousAIConfig::get_analog_trigger_condition () const
{
	return this->analog_trigger_condition_;
}

// ============================================================================
// ContinuousAIConfig::set_analog_low_level_condition
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::set_analog_low_level_condition (unsigned short _lc)
{
  this->analog_trigger_lo_level_ = _lc;
}

// ============================================================================
// ContinuousAIConfig::get_analog_low_level_condition
// ============================================================================
ASL_INLINE unsigned short 
ContinuousAIConfig::get_analog_low_level_condition () const
{
	return this->analog_trigger_lo_level_;
}

// ============================================================================
// ContinuousAIConfig::set_analog_high_level_condition
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::set_analog_high_level_condition (unsigned short _hc)
{
	this->analog_trigger_hi_level_ = _hc;
}

// ============================================================================
// ContinuousAIConfig::get_analog_high_level_condition
// ============================================================================
ASL_INLINE unsigned short 
ContinuousAIConfig::get_analog_high_level_condition () const
{
	return this->analog_trigger_hi_level_;
}

// ============================================================================
// ContinuousAIConfig::set_sampling_rate
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::set_sampling_rate (double _sr)
{
	this->sampling_rate_ = _sr;
}

// ============================================================================
// ContinuousAIConfig::get_sampling_rate
// ============================================================================
ASL_INLINE double 
ContinuousAIConfig::get_sampling_rate () const
{
	return this->sampling_rate_;
}

// ============================================================================
// ContinuousAIConfig::set_data_lost_strategy
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::set_data_lost_strategy (adl::DataLostStrategy _strategy)
{
	this->data_lost_strategy_ = _strategy;
}

// ============================================================================
// ContinuousAIConfig::get_data_lost_strategy
// ============================================================================
ASL_INLINE adl::DataLostStrategy 
ContinuousAIConfig::get_data_lost_strategy () const
{
	return data_lost_strategy_;
}

// ============================================================================
// ContinuousAIConfig::set_buffer_depth
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::set_buffer_depth (unsigned long _depth)
{
    if (_depth % 2)
      _depth = _depth + 1;
	this->buffer_depth_ = _depth;
}

// ============================================================================
// ContinuousAIConfig::get_buffer_depth
// ============================================================================
ASL_INLINE unsigned long 
ContinuousAIConfig::get_buffer_depth () const
{
	return this->buffer_depth_;
}

// ============================================================================
// ContinuousAIConfig::set_db_event_callback
// ============================================================================
ASL_INLINE void
ContinuousAIConfig::set_db_event_callback (DAQCallback _cb)
{
	this->db_event_callback_ = _cb;
}

// ============================================================================
// ContinuousAIConfig::get_db_event_callback
// ============================================================================
ASL_INLINE DAQCallback 
ContinuousAIConfig::get_db_event_callback () const
{
	return this->db_event_callback_;
}

// ============================================================================
// ContinuousAIConfig::set_trigger_event_callback
// ============================================================================
ASL_INLINE void
ContinuousAIConfig::set_trigger_event_callback (DAQCallback _cb)
{
	this->trigger_event_callback_ = _cb;
}

// ============================================================================
// ContinuousAIConfig::get_trigger_event_callback
// ============================================================================
ASL_INLINE DAQCallback 
ContinuousAIConfig::get_trigger_event_callback () const
{
	return this->trigger_event_callback_;
}

// ============================================================================
// ContinuousAIConfig::set_ai_end_event_callback
// ============================================================================
ASL_INLINE void
ContinuousAIConfig::set_ai_end_event_callback (DAQCallback _cb)
{
	this->ai_end_event_callback_ = _cb;
}

// ============================================================================
// ContinuousAIConfig::get_ai_end_event_callback
// ============================================================================
ASL_INLINE DAQCallback 
ContinuousAIConfig::get_ai_end_event_callback () const
{
	return this->ai_end_event_callback_;
}

// ============================================================================
// ContinuousAIConfig::set_timeout
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::set_timeout (long _tmo_msec)
{
	this->timeout_ = _tmo_msec;
}

// ============================================================================
// ContinuousAIConfig::get_timeout
// ============================================================================
ASL_INLINE long
ContinuousAIConfig::get_timeout () const
{
	return this->timeout_;
}

// ============================================================================
// ContinuousAIConfig::enable_history
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::enable_history (unsigned long _hlen_msec)
{
  this->history_len_msec_ = _hlen_msec;
  this->history_enabled_ = true;
}

// ============================================================================
// ContinuousAIConfig::disable_history
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::disable_history ()
{
  this->history_enabled_ = false;
}

// ============================================================================
// ContinuousAIConfig::history_enabled
// ============================================================================
ASL_INLINE bool 
ContinuousAIConfig::history_enabled () const
{
  return this->history_enabled_;
}

// ============================================================================
// ContinuousAIConfig::set_history_length
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::set_history_length (unsigned long _hlen_msec)
{
  this->history_len_msec_ = _hlen_msec;
}

// ============================================================================
// ContinuousAIConfig::get_history_length
// ============================================================================
ASL_INLINE unsigned long
ContinuousAIConfig::get_history_length () const
{
  return this->history_len_msec_;
}

// ============================================================================
// ContinuousAIConfig::enable_last_buffer_recovery
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::enable_last_buffer_recovery ()
{
  this->last_buffer_recovery_enabled_ = true;
}

// ============================================================================
// ContinuousAIConfig::disable_last_buffer_recovery
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::disable_last_buffer_recovery ()
{
  this->last_buffer_recovery_enabled_ = false;
}

// ============================================================================
// ContinuousAIConfig::last_buffer_recovery_enabled
// ============================================================================
ASL_INLINE bool 
ContinuousAIConfig::last_buffer_recovery_enabled () const
{
  return this->last_buffer_recovery_enabled_;
}

// ============================================================================
// ContinuousAIConfig::enable_user_data
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::enable_user_data ()
{
  // enable user data for all defined channels
  for (unsigned int i = 0; i < this->channels_.size(); i++) 
  {
    this->channels_[i].user_data_enabled = true;
  } 
}

// ============================================================================
// ContinuousAIConfig::disable_user_data
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::disable_user_data ()
{
  // disable user data for all defined channels
  for (unsigned int i = 0; i < this->channels_.size(); i++) 
  {
    this->channels_[i].user_data_enabled = false;
  } 
}

// ============================================================================
// ContinuousAIConfig::enable_user_data
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::enable_user_data (adl::ChanId id)
{
  for (unsigned int i = 0; i < this->channels_.size(); i++) 
  {
    if (this->channels_[i].id == id)
    {
      this->channels_[i].user_data_enabled = true;
      break;
    }
  } 
}

// ============================================================================
// ContinuousAIConfig::disable_user_data
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::disable_user_data (adl::ChanId id)
{
  for (unsigned int i = 0; i < this->channels_.size(); i++) 
  {
    if (this->channels_[i].id == id)
    {
      this->channels_[i].user_data_enabled = false;
      break;
    }
  }
}

// ============================================================================
// ContinuousAIConfig::user_data_enabled
// ============================================================================
ASL_INLINE bool 
ContinuousAIConfig::user_data_enabled (adl::ChanId id) const
{
  for (unsigned int i = 0; i < this->channels_.size(); i++) 
  {
    if (this->channels_[i].id == id)
    {
      return this->channels_[i].user_data_enabled;
    }
  }
}

// ============================================================================
// ContinuousAIConfig::user_data_enabled
// ============================================================================
ASL_INLINE bool 
ContinuousAIConfig::user_data_enabled () const
{
  bool enabled = false;
  
  for (unsigned int i = 0; i < this->channels_.size(); i++) 
  {
    enabled = enabled || this->channels_[i].user_data_enabled;
  }
  return enabled;
}

// ============================================================================
// ContinuousAIConfig::set_user_data_parameters
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::set_user_data_parameters (adl::ChanId id, double g, 
                                              double o1, double o2)
{
  for (unsigned int i = 0; i < this->channels_.size(); i++) 
  {
    if (this->channels_[i].id == id)
    {
      this->channels_[i].user_data_gain = g;
      this->channels_[i].user_data_offset1 = o1;
      this->channels_[i].user_data_offset2 = o2;
      break;
    }
  }
}

// ============================================================================
// ContinuousAIConfig::get_user_data_parameters
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::get_user_data_parameters (adl::ChanId id, double& g, 
                                              double& o1, double& o2) const
{
  for (unsigned int i = 0; i < this->channels_.size(); i++) 
  {
    if (this->channels_[i].id == id)
    {
      g = this->channels_[i].user_data_gain;
      o1 = this->channels_[i].user_data_offset1;
      o2 = this->channels_[i].user_data_offset2;
      break;
    }
  }
}

// ============================================================================
// ContinuousAIConfig::enable_data_mask
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::enable_data_mask ()
{
  this->data_mask_enabled_ = true;
}

// ============================================================================
// ContinuousAIConfig::disable_data_mask
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::disable_data_mask ()
{
  this->data_mask_enabled_ = false;
}

// ============================================================================
// ContinuousAIConfig::data_mask_enabled
// ============================================================================
ASL_INLINE bool 
ContinuousAIConfig::data_mask_enabled () const
{
  return this->data_mask_enabled_;
}

// ============================================================================
// ContinuousAIConfig::get_data_mask_buffer_length
// ============================================================================
ASL_INLINE size_t 
ContinuousAIConfig::get_data_mask_buffer_length () const
{
  return this->data_mask_ ? this->data_mask_->depth() : 0;
}

// ============================================================================
// ContinuousAIConfig::set_timebase_type
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::set_timebase_type (adl::AIOTimeBase tb)
{
	this->timebase_ = tb;
}

// ============================================================================
// ContinuousAIConfig::get_timebase_type
// ============================================================================
ASL_INLINE adl::AIOTimeBase 
ContinuousAIConfig::get_timebase_type () const
{
	return this->timebase_;
}

// ============================================================================
// ContinuousAIConfig::set_ext_timebase_val
// ============================================================================
ASL_INLINE void 
ContinuousAIConfig::set_ext_timebase_val (double freq)
{
	this->ext_timebase_val_ = freq;
}

// ============================================================================
// ContinuousAIConfig::get_ext_timebase_val
// ============================================================================
ASL_INLINE double 
ContinuousAIConfig::get_ext_timebase_val () const
{
	return this->ext_timebase_val_;
}
} // namespace asl

