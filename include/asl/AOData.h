// ============================================================================
//
// = CONTEXT
//    TANGO Project - Tango Tools Library
//
// = FILENAME
//    AOData.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _AO_DATA_H_
#define _AO_DATA_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/Data_T.h>

namespace asl {

// ============================================================================
//! Create a dedicated type for AO raw data   
// ============================================================================
typedef Buffer<unsigned short> AORawData;
// ============================================================================
//! Create a dedicated type for AI scaled data (i.e. expressed in volts)  
// ============================================================================
typedef Buffer<double> AOScaledData;
} // namespace asl

#endif // _AO_DATA_H_

