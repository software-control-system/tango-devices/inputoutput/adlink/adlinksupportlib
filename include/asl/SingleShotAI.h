// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    SingleShotAI.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _SINGLE_SHOT_AI_H_
#define _SINGLE_SHOT_AI_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/SingleShotDAQ.h>
#include <asl/adlink/ADSingleShotAIBoard.h>

namespace asl {

// ============================================================================
//! The ASL single shot analog input DAQ class.   
// ============================================================================
//! Provides functions to perform single shot analog input on ADLink boards that
//! support this functions.\n
//! The main fonctionnalities are read one or several channels.
// ============================================================================
class ASL_EXPORT SingleShotAI : public SingleShotDAQ
{
public:

  /**
   * Initialization. 
   */
  SingleShotAI  ();
  
  /**
   * Release resources.
   */
  virtual ~SingleShotAI  ();
  
  /**
   * Initialize the single shot digital input DAQ.
   * @param hw_type The type of the card.
   * @param hw_id The number of the card in the cPCI crate.
   */
  virtual void init (unsigned short hw_type, unsigned short hw_id) 
    throw (DAQException);
 
  /**
   * Performs hardware auto-calibration.
   */
  void calibrate_hardware ()
          throw (asl::DAQException);

  /**
   * Configure the ports in input.
   * @param _chan_id The channel number.
   * @param _range The input voltage range (default value is -10 to +10V)
   * @param _ref The ground reference (only available for 2204 and 2205).
   */
  void configure_channel (adl::ChanId _chan_id, 
                          adl::Range _range, 
                          adl::GroundRef _ref = adl::ref_single_ended)
    throw (DAQException);
  
  /**
   * Read one channel (numeric version).
   * @param _chan_id The channel to read.
   * @return The read value.
   */
  unsigned short read_channel (adl::ChanId _chan_id) 
    throw (DAQException);
		
  /**
   * Read several channels (numeric version).
   * @param _max_chan_id All the channels from channel 0 to _max_channel_id will be read.
   * @return A table of the read values.
   */
  asl::AIRawData * read_channels (adl::ChanId _max_chan_id) 
    throw (DAQException);

  /**
   * Read one channel (voltage version).
   * @param _chan_id The channel to read.
   * @return The read value in volts.
   */
  double read_scaled_channel (adl::ChanId _chan_id) 
    throw (DAQException);

  /**
   * Read several channels (voltage version).
   * @param _max_chan_id All the channels from channel 0 to _max_channel_id will be read.
   * @return A table of the read values in volts. 
   */
  asl::AIScaledData* read_scaled_channels (adl::ChanId _max_chan_id)
    throw (DAQException);

private:

	/**
   * The underlying hardware.
   */
   adl::ADSingleShotAIBoard * daq_hw_;

  /**
   * Return true if initialized, false otherwise.
   */
   bool initialized () const;
};

} // namespace asl
	
#if defined (__ASL_INLINE__)
# include <asl/SingleShotAI.i>
#endif // __ASL_INLINE__

#endif // _SINGLE_SHOT_AI_H_
