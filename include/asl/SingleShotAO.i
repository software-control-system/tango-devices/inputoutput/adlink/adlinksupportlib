// ============================================================================
//
// = CONTEXT
//    TANGO Project - Tango Tools Library
//
// = FILENAME
//    SingleShotAO.i
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

namespace asl {

// ============================================================================
// SingleShotAO::initialized
// ============================================================================
ASL_INLINE bool
SingleShotAO::initialized () const
{
  return (this->daq_hw_) ? true : false;
}

} // namespace asl



