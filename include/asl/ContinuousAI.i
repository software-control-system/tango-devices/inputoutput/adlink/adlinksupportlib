// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ContinuousAI.i
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

namespace asl {

// ============================================================================
// ContinuousAI::configure
// ============================================================================
ASL_INLINE void 
ContinuousAI::configure (ContinuousAIConfig* _config) 
  throw (asl::DAQException, asl::DeviceBusyException)
{
  if (_config == 0) {
    throw asl::DAQException("invalid configuration specified",
                            "invalid configuration specified (null pointer)",
                            "ContinuousAI::configure");
  }
  this->configure(*_config);
}

// ============================================================================
// ContinuousAI::configuration
// ============================================================================
ASL_INLINE const ContinuousAIConfig& 
ContinuousAI::configuration () const
{
  return this->config_;
}

// ============================================================================
// ContinuousAI::initialized
// ============================================================================
ASL_INLINE bool
ContinuousAI::initialized () const
{
  return (this->work_ && this->worker_ && this->daq_hw_) ? true : false;
}

// ============================================================================
// ContinuousAI::handle_data_lost
// ============================================================================
ASL_INLINE void 
ContinuousAI::handle_data_lost ()
{
  //-noop default implementation
} 

// ============================================================================
// ContinuousAI::handle_timeout
// ============================================================================
ASL_INLINE void 
ContinuousAI::handle_timeout ()
{
  //-noop default implementation
} 

// ============================================================================
// ContinuousAI::handle_daq_end
// ============================================================================
ASL_INLINE void 
ContinuousAI::handle_daq_end (ContinuousDAQ::DaqEndReason reason_why)
{
  ACE_UNUSED_ARG(reason_why);
  //-noop default implementation
} 

// ============================================================================
// ContinuousAI::last_sample_index_in_history_buffer
// ============================================================================
ASL_INLINE void 
ContinuousAI::last_sample_index_in_history_buffer (unsigned long lsi)
{
  this->last_sample_index_in_history_buffer_ = lsi;
}

// ============================================================================
// ContinuousAI::last_sample_index_in_last_daq_buffer
// ============================================================================
ASL_INLINE unsigned long 
ContinuousAI::last_sample_index_in_last_daq_buffer () const
{
  return this->daq_hw_ 
       ? this->daq_hw_->last_sample_index_in_last_daq_buffer()
       : 0;
}

// ============================================================================
// ContinuousAI::last_sample_index_in_history_buffer
// ============================================================================
ASL_INLINE unsigned long 
ContinuousAI::last_sample_index_in_history_buffer () const
{
  return this->last_sample_index_in_history_buffer_;
}

} // namespace asl


