// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    Worker.i
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

namespace asl {

// ============================================================================
// Worker::timeout
// ============================================================================
ASL_INLINE void 
Worker::timeout (long _tmo_msec)
{
  this->timeout_.set(0, 1000 * _tmo_msec);
}

// ============================================================================
// Worker::timeout
// ============================================================================
ASL_INLINE long 
Worker::timeout () const
{
  return 1000 * this->timeout_.usec();
}

// ============================================================================
// Worker::state
// ============================================================================
ASL_INLINE void 
Worker::state (int _state)
{
  ACE_GUARD(ACE_Thread_Mutex, ace_mon, this->lock_);
  this->state_ = _state;
}

// ============================================================================
// Worker::state
// ============================================================================
ASL_INLINE int 
Worker::state () const
{
  return this->state_;
}

// ============================================================================
// Worker::name
// ============================================================================
ASL_INLINE const std::string&
Worker::name () const
{
  return this->name_;
}

// ============================================================================
// Worker::priority_level
// ============================================================================
ASL_INLINE long
Worker::priority_level () const
{
  return this->prio_;
}

// ============================================================================
// Worker::post
// ============================================================================
ASL_INLINE int 
Worker::post (Message * _mb, Timeout * _tv)
{
  return this->msg_queue_->enqueue_prio (_mb, _tv);
}

//=============================================================================
// Worker::overloaded
//=============================================================================
#ifdef __linux
ASL_INLINE bool
Worker::overloaded () const 
{
  return this->msg_queue_->is_full();
}
#endif

// ============================================================================
// Worker::low_water_mark
// ============================================================================
ASL_INLINE void 
Worker::low_water_mark (size_t _lwm)
{
  this->msg_queue_->low_water_mark(_lwm);
}

// ============================================================================
// Worker::low_water_mark
// ============================================================================
ASL_INLINE size_t 
Worker::low_water_mark () const
{
  return this->msg_queue_->low_water_mark();
}

// ============================================================================
// Worker::high_water_mark
// ============================================================================
ASL_INLINE void 
Worker::high_water_mark (size_t _hwm)
{
  this->msg_queue_->high_water_mark(_hwm);
}

// ============================================================================
// Worker::high_water_mark
// ============================================================================
ASL_INLINE size_t 
Worker::high_water_mark () const
{
  return this->msg_queue_->high_water_mark();
}

#if defined(ASL_DEBUG)
// ============================================================================
// Worker::just_a_call_in_svc_thread_context
// ============================================================================
ASL_INLINE void
Worker::just_a_call_in_svc_thread_context ()
{
  this->thread_id_ = ACE_Thread::self();
}
#endif

// ============================================================================
// Worker::has_error
// ============================================================================
ASL_INLINE bool
Worker::has_error () const
{
  ACE_GUARD_RETURN(ACE_SYNCH_MUTEX, ace_mon, 
                   ACE_const_cast(ACE_Thread_Mutex&, this->lock_), 
                   false);
  return this->last_error_ ? true : false; 
}

// ============================================================================
// Worker::last_error
// ============================================================================
ASL_INLINE WorkerErrorPtr
Worker::last_error ()
{
  ACE_GUARD_RETURN(ACE_SYNCH_MUTEX, ace_mon, 
                   ACE_const_cast(ACE_Thread_Mutex&, this->lock_), 
                   WorkerErrorPtr(0));
  if (last_error_ == 0) {
    return WorkerErrorPtr(new WorkerError("no error", WorkerError::ERR_NOERROR));
  }
  WorkerError* tmp = last_error_;
  last_error_ = 0;
  return WorkerErrorPtr(tmp);
}

// ============================================================================
// Worker::getq
// ============================================================================
ACE_INLINE int 
Worker::getq (Message * &_mb, Timeout * _tv)
{
  return this->msg_queue_->dequeue_head(ACE_reinterpret_cast(ACE_Message_Block*&, _mb), _tv);
}

} // namespace asl
