// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    WorkerError.h
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

#ifndef _WORKER_ERROR_H_
#define _WORKER_ERROR_H_

// ============================================================================
// DEPENDENCIEs
// ============================================================================
#include <string>
#include <ace/Auto_Ptr.h>
#include <asl/ASLConfig.h>
#if defined(ASL_DEBUG)
# include <asl/Mld.h>
#endif


namespace asl {

// ============================================================================
// FORWARD DECLARATION
// ============================================================================
class WorkerError;

// ============================================================================
// TYPEDEF
// ============================================================================
//! Define what is an WorkerError auto pointer - just use this as a 
//! "standard pointer". No need to call delete (or whatever) on it. 
//! Its destructor will the underlying WorkerError.
typedef auto_ptr<WorkerError> WorkerErrorPtr;

// ============================================================================
//! A Worker error abstraction.
// ============================================================================
//!  
//! detailed description to be written
//! 
// ============================================================================
class ASL_EXPORT WorkerError
{
public:
  
  //- Basic error codes
  enum {
    //- no error 
    ERR_NOERROR = 0,
    //- generic/unknown error (default)
    ERR_UNKNOWN,
    //- an error occured while trying to start the Worker
    ERR_START, 
    //- an error occured in Worker's processing routine
    ERR_PROCESS,
    //- an error occured while trying to reset the Worker
    ERR_RESET,
    //- an error occured while trying to stop the Worker
    ERR_STOP,
    //- an error occured while trying to abort the Worker
    ERR_ABORT,
    //- an error occured while Worker tried to quit
    ERR_QUIT,
    //- user defined error code start here 
    ERR_FISRT_USER_DEFINED
  };

  /**
   *
   */
  WorkerError (const std::string& _txt, long _code = ERR_UNKNOWN, bool _fatal = false);

  /**
   *
   */
  WorkerError (const char *_txt, long _code = ERR_UNKNOWN, bool _fatal = false);

  /**
   *
   */
  virtual ~WorkerError ();

  /**
   *
   */
  const std::string& text () const;

  /**
   *
   */
  long code () const;

  /**
   *
   */
  bool is_fatal () const;

  /**
   *
   */
  WorkerError * cont () const; 

  /**
   *
   */
  void cont (WorkerError * _cont); 

#if defined(ASL_DEBUG)
  /**
   * Returns the current instance counter value
   */
  static unsigned long instance_counter();
#endif

private:

  //- error text
  std::string txt_;

  //- error code
  long code_;

  //- true for fatal error, false otherwise
  bool fatal_;

  //- continuation field (for chained/stacked errors)
  WorkerError * cont_;

#if defined(ASL_DEBUG)
  //- Instances counter.
  static AtomicOp instance_counter_;

  //- Memory leak detector
  MLD;
#endif

  // = Disallow these operations.
  //--------------------------------------------
  ACE_UNIMPLEMENTED_FUNC(WorkerError (const WorkerError&))
  ACE_UNIMPLEMENTED_FUNC(WorkerError& operator= (const WorkerError&))
};

} // namespace asl

#if defined (__ASL_INLINE__)
# include <asl/WorkerError.i>
#endif /* __ASL_INLINE__ */

#endif // _WORKER_ERROR_H_

