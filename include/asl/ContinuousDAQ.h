// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ContinuousDAQ.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _CONTINUOUS_DAQ_H_
#define _CONTINUOUS_DAQ_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/DAQException.h>

// ============================================================================
// CONSTS
// ============================================================================
#define kMAX_MESSAGES   32      //- 32 messages = full msgQ   
#define kMAX_MSGQ_BYTES 1677216 //- msgQ bytes limit = 16Mb (32 msgs or 16Mb)
#define kPOST_TIMEOUT   250000  //- 250 ms
#define kWSC_TIMEOUT    2       //- 2 seconds timeout on <lock>.tryacquire

// ============================================================================
// IMPL OPTIONS
// ============================================================================
#define USE_ASYNC_CALIBRATION

// ============================================================================
// LOCKING MACROS
// ============================================================================
#define TIMEOUT_GUARD(_LOCK, _CALLER) \
ACE_Guard<ACE_Thread_Mutex> ace_mon(_LOCK, 0); \
if (ace_mon.locked() == 0) \
{ \
  Timeout tmo = ACE_OS::gettimeofday() + Timeout(kWSC_TIMEOUT, 0); \
  do { \
    ace_mon.tryacquire(); \
    if (ace_mon.locked() == 0) { \
      if (errno != EBUSY) { \
		      throw asl::DAQException("asl internal error", \
														      "could not acquire mutex", \
														      _CALLER); \
      } \
      if (ACE_OS::gettimeofday() > tmo) { \
        break; \
      } \
      ACE_Thread::yield(); \
    } \
    else break; \
  } while (1);\
} 
//-----------------------------------------------------------------------------
#define CHECK_GUARD_LOCKED(_CALLER) \
if (ace_mon.locked() == 0) \
{ \
  std::string desc("this is a bug in ASL"); \
  desc += " [one of the ContinuousDAQ internal states is properly handled]"; \
  throw asl::DAQException(std::string("asl internal error"), \
												  desc, \
												  std::string(_CALLER)); \
}
//-----------------------------------------------------------------------------
#define CHECK_GUARD_LOCKED_ACTION(_CALLER, _ACTION) \
if (ace_mon.locked() == 0) \
{ \
  _ACTION; \
  std::string desc("this is a bug in ASL"); \
  desc += " [one of the ContinuousDAQ internal states is properly handled]"; \
  throw asl::DAQException(std::string("asl internal error"), \
												  desc, \
												  std::string(_CALLER)); \
} \
else \
{ \
  _ACTION; \
}


namespace asl {

// ============================================================================
//! The ASL ContinuousDAQ abstraction base class.  
// ============================================================================
//! Base class for all classes that perform continuous aquisition: 
//! - ContinuousAI
//! - ContinuousAO
//! - ContinuousDI
// ============================================================================
class ASL_EXPORT ContinuousDAQ
{
  friend class ContinuousDAQHandler;

public:

  //- Continuous DAQ states
  //------------------------
  enum 
  {
    STANDBY,
    RUNNING,
    ABORTING,
    CALIBRATING_HW,
    FAULT,
    UNKNOWN
  };

  //- Continuous DAQ-end reasons
  //----------------------------
	typedef enum 
  {
    DAQEND_ON_UNKNOWN_EVT,
    DAQEND_ON_USER_REQUEST,
    DAQEND_ON_EXTERNAL_TRIGGER,
    DAQEND_ON_FINITE_RETRIGGER_SEQUENCE,
    DAQEND_ON_ERROR,
    DAQEND_ON_OVERRUN,
		DAQEND_ON_CALIB_REQUEST
  } DaqEndReason;
	
  /**
   * Initialization. 
   */
  ContinuousDAQ ();
  
  /**
   * Release resources.
   */
  virtual ~ContinuousDAQ ();

  /**
   * Initialize continuous DAQ.
   */
  virtual void init (unsigned short hw_type, unsigned short hw_id) 
    throw (asl::DAQException, asl::DeviceBusyException) = 0; 

  /**
   * Start continuous DAQ. 
   */
  virtual void start () 
    throw (asl::DAQException, asl::DeviceBusyException) = 0; 

  /**
   * Stop continuous DAQ. 
   */
  virtual void stop () 
    throw (asl::DAQException, asl::DeviceBusyException) = 0; 

  /**
   * Abort continuous DAQ. 
   */
  virtual void abort () 
    throw (asl::DAQException, asl::DeviceBusyException) = 0; 

  /**
   * Returns current DAQ state.
   */
   int state () const;

  /**
   * Performs hardware auto-calibration asynchronously.
   * This default implementation does nothing
   */
  virtual void calibrate_hardware ()
    throw (asl::DAQException, asl::DeviceBusyException);

#if defined (USE_ASYNC_CALIBRATION)
  /**
   * Does nothing if the last hardware calibration completed successfully. 
   * Otherwise, rethrows the exception caught while trying to calibrate the hardware.
   */
  virtual void check_hardware_calibration ()
    throw (asl::DAQException);

  /**
   * Returns true if the calibration process completed, false otherwise. 
   * Doesn't say anything about acalibration success.
   */
  bool calibration_completed ();
#endif

#if defined(ASL_DEBUG)
  /**
   * Return the total number of objects currently instanciated.
   *
   * \return current number of ContinuousDAQ instances
   */
  static u_long instance_counter();
#endif
  
  /**
   * Protect DAQ against race conditions.
   */
  int lock ();

  /**
   * Release DAQ protection against race conditions.
   */
  int unlock ();

protected:

  /**
   * ExceptionStatus
   */
  typedef enum {
	  HANDLING_NONE,
	  HANDLING_ERROR,
	  HANDLING_OVERRUN,
	  HANDLING_CALIBRATION
  } ExceptionStatus;
	
	ExceptionStatus exception_status_;
	
  /**
   * Throws the appriopriate exception if DAQ can't respond to external requests. 
   * Calls both abort_request_if_busy and abort_request_if_bad_state.
   */
  virtual void action_allowed (const char * caller) const 
      throw (asl::DeviceBusyException, asl::DAQException);

  /**
   * Throws the appriopriate asl::DeviceBusyException if DAQ
   * can't respond to external requests
   */
  virtual void abort_request_if_busy (const char * caller) const 
      throw (asl::DeviceBusyException);

  /**
   * Throws the appriopriate asl::DAQException if DAQ
   * can't respond to external requests
   */
  virtual void  abort_request_if_bad_state (const char * caller) const 
      throw (asl::DAQException);
 
  /**
   * Returns the reason why the DAQ stopped
   * This default implementation returns DAQEND_ON_UNKNOWN_EVT
   */
  virtual ContinuousDAQ::DaqEndReason daq_end_reason ();

  /**
   * DAQ exception handler. 
   */
  virtual void handle_daq_exception () 
    throw (DAQException) = 0; 

  /**
   * Overrun exception handler. 
   * This default implementation does nothing
   */
  virtual void handle_data_lost_exception () 
    throw (DAQException); 

#if defined (USE_ASYNC_CALIBRATION)
  /**
   * Calibration handler. 
   * This default implementation does nothing
   */
  virtual void handle_hardware_calibration () 
    throw (DAQException); 
#endif

  /**
   * A lock to protect the DAQ against race conditions.
   */
  ACE_Thread_Mutex lock_;

  /**
   * Current DAQ state
   */
  int state_;
	
#if defined (USE_ASYNC_CALIBRATION)
  /**
   * Calibration flag
   */
  bool hw_calibration_successfull_;

  /**
   * Calibration error
   */
  DAQException hw_calibration_exception_;
#endif

private:

#if defined(ASL_DEBUG)
  /**
   * Instances counter.
   */
  static AtomicOp instance_counter_;

  /**
   * Memory leak detector.
   */
  MLD; 
#endif

  // = Disallow these operations.
  //--------------------------------------------
  ACE_UNIMPLEMENTED_FUNC(ContinuousDAQ& operator= (const ContinuousDAQ&))
  ACE_UNIMPLEMENTED_FUNC(ContinuousDAQ(const ContinuousDAQ&))
};

} // namespace asl

#if defined (__ASL_INLINE__)
# include "asl/ContinuousDAQ.i"
#endif // __ASL_INLINE__

#endif // _CONTINUOUS_DAQ_H_

