// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    SharedObject.h
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

#ifndef _SHARED_OBJECT_H_
#define _SHARED_OBJECT_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/ASLConfig.h>
#if defined(ASL_DEBUG)
# include <asl/Mld.h>
#endif

namespace asl {

// ============================================================================
//! The ASL reference counted object abstraction.
// ============================================================================
//!  
//! Base class for any reference counted object (i.e. shared) object.
//! 
// ============================================================================
class ASL_EXPORT SharedObject
{
public:

  SharedObject ();
  // Constructor.
  
  virtual ~SharedObject ();
  // Destructor.

  SharedObject * duplicate ();
  // Return a "shallow" copy. Increment the reference count by 1
  // to avoid deep copies.
  
  void release ();
  // Decrease the shared reference count by 1.  If the reference count
  // equals 0, then delete <this> and return 0. Behavior is undefined 
  // if reference count < 0. 
 
  int reference_count () const;
  // Returns the current reference count.
  
  void lock();
  // Gets exclusive access to the data.

  void unlock();
  // Release exclusive access to the data.

#if defined(ASL_DEBUG)
  /**
   * Returns the current instance counter value
   */
  static unsigned long instance_counter();
#endif
  
protected:
  // a mutex to protect the data against race conditions
  ACE_Thread_Mutex lock_; private:

private:
  // internal release implementation
  SharedObject * release_i ();

  //- reference count for (used to avoid deep copies)
  int reference_count_;

#if defined(ASL_DEBUG)
  //- instances counter
  static AtomicOp instance_counter_;
#endif

#if defined(ASL_DEBUG)
  //- memory leak detector
  MLD; 
#endif

  // = Disallow these operations.
  //--------------------------------------------
  ACE_UNIMPLEMENTED_FUNC(SharedObject& operator= (const SharedObject&))
  ACE_UNIMPLEMENTED_FUNC(SharedObject (const SharedObject&))
};

} // namespace asl

#if defined (__ASL_INLINE__)
# include <asl/SharedObject.i>
#endif // __ASL_INLINE__

#endif // _SHARED_OBJECT_H_
