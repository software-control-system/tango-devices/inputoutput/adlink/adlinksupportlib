// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    SingleShotDI.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _SINGLE_SHOT_DI_H_
#define _SINGLE_SHOT_DI_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/adlink/ADSingleShotDIBoard.h>
#include <asl/SingleShotDAQ.h>

namespace asl {
// ============================================================================
//! The ASL single shot digital input DAQ class.   
// ============================================================================
//! Provides functions to perform single shot digital input on ADLink boards that
//! support this functions.\n
//! The main fonctionnalities are read one line or one port.
// ============================================================================
class ASL_EXPORT SingleShotDI : public SingleShotDAQ
{
public:

  /**
   * Initialization. 
   */
  SingleShotDI  ();
  
  /**
   * Release resources.
   */
  virtual ~SingleShotDI  ();
  
  /**
   * Initialize the single shot digital input DAQ.
   * @param hw_type The type of the card.
   * @param hw_id The number of the card in the cPCI crate.
   */
  virtual void init (unsigned short hw_type, unsigned short hw_id) 
    throw (DAQException);
 
  /**
   * Configure the ports in input.
   * @param _port The port to configure.
   */
  void configure_port (adl::DIOPort _port) 
    throw (DAQException);
  
  /**
   * Read one line in input.
   * @param _port The port.
   * @param _line The line.
   * @return The input value.
   */
  bool read_line (adl::DIOPort _port, int _line) 
    throw (DAQException);
		
  /**
   * Read a 32-bit input.
   * @param _port The port.
   * @return The input value.
   */
  unsigned long read_port (adl::DIOPort _port) 
    throw (DAQException);

private:

	/**
   * The underlying hardware.
   */
   adl::ADSingleShotDIBoard * daq_hw_;

  /**
   * Return true if initialized, false otherwise.
   */
   bool initialized () const;
};
} // namespace asl
	
#if defined (__ASL_INLINE__)
# include <asl/SingleShotDI.i>
#endif // __ASL_INLINE__

#endif // _SINGLE_SHOT_DI_H_
