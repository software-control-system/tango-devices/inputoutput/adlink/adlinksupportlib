// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    Data_T.i
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

namespace asl {

// ============================================================================
// Class : Container
// ============================================================================
// ============================================================================
// Container::content
// ============================================================================
template <class T> ASL_INLINE T&  
Container<T>::content()
{
  return this->data_;
}

// ============================================================================
// Class : Buffer
// ============================================================================
// ============================================================================
// Buffer::clear
// ============================================================================
template <class T> ASL_INLINE void
Buffer<T>::clear()
{
  ACE_OS::memset(this->base(), 0, this->size());
}

// ============================================================================
// Buffer::elem_size
// ============================================================================
template <class T> ASL_INLINE size_t 
Buffer<T>::elem_size() const
{
  return sizeof(T);
}

// ============================================================================
// Buffer::depth
// ============================================================================
template <class T> ASL_INLINE unsigned long 
Buffer<T>::depth() const
{
  return this->depth_;
}

// ============================================================================
// Buffer::base
// ============================================================================
template <class T> ASL_INLINE T * 
Buffer<T>::base() const
{
  return this->base_;
}

// ============================================================================
// Buffer::allocator
// ============================================================================
template <class T> ASL_INLINE ACE_Allocator * 
Buffer<T>::allocator() const
{
  return this->alloc_;
}

// ============================================================================
// Buffer::operator[]
// ============================================================================
template <class T> ASL_INLINE T& 
Buffer<T>::operator[](unsigned long _indx)
{
  /* !! no bound error check !!*/
  return this->base_[_indx];
}

// ============================================================================
// Buffer::operator[]
// ============================================================================
template <class T> ASL_INLINE T
Buffer<T>::operator[](unsigned long _indx) const
{
  /* !! no bound error check !!*/
  return this->base_[_indx];
}

// ============================================================================
// Buffer::size
// ============================================================================
template <class T> ASL_INLINE size_t 
Buffer<T>::size() const
{
	return this->depth_ * sizeof(T);
}

// ============================================================================
// Buffer::fill
// ============================================================================
template <class T> ASL_INLINE void
Buffer<T>::fill(const T& _val)
{
  *this = _val;
}

// ============================================================================
// Buffer::operator=
// ============================================================================
template <class T> ASL_INLINE Buffer<T>& 
Buffer<T>::operator=(const Buffer<T>& src)
{
  if (&src == this)
    return *this;

  u_long cpy_depth = (src.depth() < this->depth_) ? src.depth() : this->depth_; 
  
  ACE_OS::memcpy(this->base_, src.base(), cpy_depth * sizeof(T));
     
  return *this;
}

// ============================================================================
// Buffer::operator=
// ============================================================================
template <class T> Buffer<T>& 
Buffer<T>::operator=(const T* _src)
{
  if (_src == this->base_)
    return *this;

  ACE_OS::memcpy(this->base_, _src, this->depth_ * sizeof(T));

  return *this;
}

// ============================================================================
// Buffer::operator=
// ============================================================================
template <class T> Buffer<T>& 
Buffer<T>::operator=(const T& _val)
{
  for (int i=0; i < this->depth_; ++i) {
     *(this->base_ + i) = _val;
  }

  return *this;
}

// ============================================================================
// CircularBuffer::modulo
// ============================================================================
template <class T> unsigned long 
CircularBuffer<T>::modulo () const
{
  return this->modulo_;
}

// ============================================================================
// CircularBuffer::factor
// ============================================================================
template <class T> unsigned long 
CircularBuffer<T>::factor () const
{
  return this->factor_;
}
  
} // namespace asl
