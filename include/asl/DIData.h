// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    DIData.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _DI_DATA_H_
#define _DI_DATA_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/Data_T.h>

namespace asl {

  // ============================================================================
//! Create a dedicated type for DI raw data (i.e numeric values)
// ============================================================================
typedef unsigned char  DIByteDataType;
typedef unsigned short DIShortDataType;
typedef unsigned long  DILongDataType;

// ============================================================================
//! Create a dedicated type for DI raw data (i.e numeric values)
// ============================================================================
typedef Buffer<DIByteDataType>  DIByteData;
typedef Buffer<DIShortDataType> DIShortData;
typedef Buffer<DILongDataType>  DILongData;


} // namespace asl

#endif // _DI_DATA_H_

