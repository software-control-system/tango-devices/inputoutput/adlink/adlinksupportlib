// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    Export.h
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

#ifndef _EXPORT_H_
#define _EXPORT_H_

#if defined(WIN32)
# if defined(ASL_HAS_DLL) 
#   if defined (ASL_BUILD)
#     define ASL_EXPORT __declspec(dllexport)
#   else
#     define ASL_EXPORT __declspec(dllimport)
#   endif
# else
#   define ASL_EXPORT
# endif
#else
# define ASL_EXPORT
#endif

#endif // _EXPORT_H_



