// ============================================================================
//
// = CONTEXT
//    TANGO Project - Tango Tools Library
//
// = FILENAME
//    SingleShotDI.i
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

namespace asl {

// ============================================================================
// SingleShotDI::initialized
// ============================================================================
ASL_INLINE bool
SingleShotDI::initialized () const
{
  return (this->daq_hw_) ? true : false;
}

} // namespace asl



