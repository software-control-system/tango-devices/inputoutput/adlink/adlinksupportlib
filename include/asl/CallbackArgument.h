// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    CallbackArgument.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _CALLBACK_ARG_H_
#define _CALLBACK_ARG_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/ASLConfig.h>
#if defined(ASL_DEBUG)
# include <asl/Mld.h>
#endif
#include <asl/MessageConsumer.h>

namespace asl {

// ============================================================================
//! The ASL abstraction for continuous DAQ callback argument.  
// ============================================================================
//!  
//! detailed description to be written
//! 
// ============================================================================
class ASL_EXPORT CallbackArgument
{
public:

  /**
   * Initialization.
   *
   * \param b The DAQ board identifier
   *
   * \param c The MessageConsumer who consume the data or NULL if none.
   */
  CallbackArgument (unsigned short b, MessageConsumer * c = 0) 
    : board_id_(b), msg_consumer_(c)
  {
    //- noop ctor
  }

  /**
   * Release resources.
   */
  virtual ~CallbackArgument ()
  {
    //- noop dtor
  }

  /**
   * The DAQ board identifier. 
   */
   unsigned short board_id_;
   
  /**
   * The MessageConsumer who consume the data. May be NULL.
   */
   MessageConsumer * msg_consumer_;

private:

#if defined(ASL_DEBUG)
  /**
   * Memory leak detector.
   */
  MLD; 
#endif

  // = Disallow these operations.
  //--------------------------------------------
  ACE_UNIMPLEMENTED_FUNC(CallbackArgument& operator= (const CallbackArgument&))
  ACE_UNIMPLEMENTED_FUNC(CallbackArgument(const CallbackArgument&))
};

} // namespace asl

#endif // _CALLBACK_ARG_H_

