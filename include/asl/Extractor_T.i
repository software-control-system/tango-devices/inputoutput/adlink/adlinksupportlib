// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    DataExtractor_T.i
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

namespace asl {

// ============================================================================
// message_data
// ============================================================================
template <class T> 
ASL_INLINE int message_data(Message * msg, T*& data)
{
  DataExtractor<T> ext;
  return (data = ext(msg)) == 0 ? -1 : 0;
}

// ============================================================================
// DS_Data_Extractor::operator()
// ============================================================================
template <class T>  ASL_INLINE T *
DataExtractor<T>::operator() (Message * msg) const
{
  DataBlock* db;
  db = ACE_reinterpret_cast(DataBlock*, msg->data_block());
  try {
  	return (db != 0) ? ACE_dynamic_cast(T*, db->data()) : 0;
  }
  catch(...) 
  {
    //- ignore
  }
  return 0;
}

} // namespace asl
