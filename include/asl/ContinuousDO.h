// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ContinuousDO.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _CONTINUOUS_DO_H_
#define _CONTINUOUS_DO_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/ContinuousDAQ.h>

namespace asl {

// ============================================================================
//! The ASL continuous digital output DAQ abstraction base class.  
// ============================================================================
//!  
//! detailed description to be written
//! 
// ============================================================================
class ASL_EXPORT ContinuousDO : public ContinuousDAQ
{
public:

  /**
   * Initialization. 
   */
  ContinuousDO ();
  
  /**
   * Release resources.
   */
  virtual ~ContinuousDO ();

  /**
   * Configure the continuous digital output DAQ.
   */
  virtual void configure ()
          throw (asl::DAQException);

  /**
   * Initialize the continuous digital output DAQ.
   */
  virtual void init ()
          throw (asl::DAQException);

  /**
   * Start the continuous digital output DAQ.
   */
  virtual void start ()
          throw (asl::DAQException);

  /**
   * Stop the continuous digital output DAQ.
   */
  virtual void stop ()
          throw (asl::DAQException);

  /**
   * Abort the continuous digital output DAQ.
   */
  virtual void abort ()
          throw (asl::DAQException);

private:

  /**
   * The underlying DAQ board
   */
  //-TODO: daq_hw_;

  // = Disallow these operations.
  //--------------------------------------------
  ACE_UNIMPLEMENTED_FUNC(ContinuousDO& operator= (const ContinuousDO&))
  ACE_UNIMPLEMENTED_FUNC(ContinuousDO(const ContinuousDO&))
};

} // namespace asl

#if defined (__ASL_INLINE__)
# include "asl/ContinuousDO.i"
#endif // __ASL_INLINE__

#endif // _CONTINUOUS_DO_H_

