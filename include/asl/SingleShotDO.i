// ============================================================================
//
// = CONTEXT
//    TANGO Project - Tango Tools Library
//
// = FILENAME
//    SingleShotDO.i
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

namespace asl {

// ============================================================================
// SingleShotDO::initialized
// ============================================================================
ASL_INLINE bool
SingleShotDO::initialized () const
{
  return (this->daq_hw_) ? true : false;
}

} // namespace asl



