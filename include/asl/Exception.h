// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    Exception.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _EXCEPTION_H_
#define _EXCEPTION_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/ASLConfig.h>
#include <string>
#include <vector>
#if defined(ASL_DEBUG)
# include <asl/Mld.h>
#endif

namespace asl {

// ============================================================================
// ASL Errors severities 
// ============================================================================
typedef enum {
  WARN, 
  ERR, 
  PANIC
} ErrorSeverity;

// ============================================================================
//! The ASL exception abstraction base class.  
// ============================================================================
//!  
//! detailed description to be written
//! 
// ============================================================================
class ASL_EXPORT Error
{
public:

  /**
   * Initialization. 
   */
  Error ();

  /**
   * Initialization. 
   */
  Error (const char *reason,
				 const char *desc,
				 const char *origin,
	       int err_code = -1, 
	       int severity = asl::ERR);
  

  /**
   * Initialization. 
   */
  Error (const std::string& reason,
				 const std::string& desc,
				 const std::string& origin, 
	       int err_code = -1, 
	       int severity = asl::ERR);

  /**
   * Copy constructor. 
   */
  Error (const Error& src);

  /**
   * Error details: code 
   */
  virtual ~Error ();

  /**
   * operator= 
   */
  Error& operator= (const Error& _src);

  /**
   * Error details: reason 
   */
  std::string reason;

  /**
   * Error details: description 
   */
  std::string desc;

  /**
   * Error details: origin 
   */
  std::string origin;

  /**
   * Error details: code 
   */
  int code;

  /**
   * Error details: severity 
   */
  int severity;

};

// ============================================================================
// The ASL error list.	
// ============================================================================
typedef std::vector<Error> ErrorList;

// ============================================================================
//! The ASL exception abstraction base class.  
// ============================================================================
//!  
//! detailed description to be written
//! 
// ============================================================================
class ASL_EXPORT Exception
{
public:

  /**
   * Initialization. 
   */
  Exception ();

  /**
   * Initialization. 
   */
  Exception (const char *reason,
					   const char *desc,
					   const char *origin,
	           int err_code = -1, 
	           int severity = asl::ERR);
  
  /**
   * Initialization. 
   */
  Exception (const std::string& reason,
					   const std::string& desc,
					   const std::string& origin, 
	           int err_code = -1, 
	           int severity = asl::ERR);

  /**
   * Initialization. 
   */
  Exception (const Error& error);


  /**
   * Copy constructor. 
   */
  Exception (const Exception& src);

  /**
   * operator=
   */
  Exception& operator= (const Exception& _src); 

  /**
   * Release resources.
   */
  virtual ~Exception ();

  /**
   * Push the specified error into the errors list.
   */
  void push_error (const char *reason,
					         const char *desc,
						       const char *origin, 
		               int err_code = -1, 
		               int severity = asl::ERR);

  /**
   * Push the specified error into the errors list.
   */
  void push_error (const std::string& reason,
                   const std::string& desc,
                   const std::string& origin, 
                   int err_code = -1, 
                   int severity = asl::ERR);

  /**
   * Push the specified error into the errors list.
   */
  void push_error (const Error& error);

  /**
   * The errors list
   */
   ErrorList errors;
  

#if defined(ASL_DEBUG)
  /**
   * Return the total number of objects currently instanciated.
   *
   * \return current number of Exception instances
   */
  static u_long instance_counter();
#endif
  
private:

#if defined(ASL_DEBUG)
  /**
   * Instances counter.
   */
  static AtomicOp instance_counter_;

  /**
   * Memory leak detector.
   */
  MLD; 
#endif
};

} // namespace asl

#if defined (__ASL_INLINE__)
# include "asl/Exception.i"
#endif // __ASL_INLINE__

#endif // _ASL_EXCEPTION_H_

