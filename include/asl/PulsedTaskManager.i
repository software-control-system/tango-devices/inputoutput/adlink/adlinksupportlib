// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    PulsedTaskManager.i
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

namespace asl {

// ============================================================================
// PulsedTaskManager::register_task
// ============================================================================
ASL_INLINE int 
PulsedTaskManager::register_task (PulsedTask * _t, 
                                  void* _arg, 
                                  ACE_Time_Value& _intv)
{
  return this->reactor_->schedule_timer(_t, _arg, _intv, _intv);  
}

// ============================================================================
// PulsedTaskManager::remove_task
// ============================================================================
ASL_INLINE int 
PulsedTaskManager::remove_task (PulsedTask * _t)
{
  this->reactor_->cancel_timer(_t);
  ACE_Reactor_Mask masks = ACE_Event_Handler::ALL_EVENTS_MASK 
                         | ACE_Event_Handler::DONT_CALL;
  this->reactor_->remove_handler(_t, masks); 
  return 0;
}

// ============================================================================
// PulsedTaskManager::dll_name
// ============================================================================
ASL_INLINE const ACE_TCHAR *
PulsedTaskManager::dll_name ()
{
	return ACE_LIB_TEXT ("ASL");
}

// ============================================================================
// PulsedTaskManager::name
// ============================================================================
ASL_INLINE const ACE_TCHAR * 
PulsedTaskManager::name ()
{
	return ACE_LIB_TEXT ("PulsedTaskManager");
}

} // namespace asl

