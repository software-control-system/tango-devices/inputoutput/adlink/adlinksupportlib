// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    Mld.h
//
// = AUTHOR
//    ? (stolen by NL from ACE Tutorials)
//
// ============================================================================

#ifndef _MLD_H_
#define _MLD_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/ASLConfig.h>

namespace asl {

// ============================================================================
//! A cheap memory leak detector
// ============================================================================
//!  
//! Each class you want to watch over contains an Mld object.  
//! The Mld object's ctor increments a global counter while 
//! the dtor decrements it.If the counter is non-zero when 
//! the program is ready to exit then there may be a leak.
//! 
// ============================================================================
class ASL_EXPORT Mld
{
public:
  
  /**
   * Initialization.
   */
  Mld ();

  /**
   * Release resources.
   */
  virtual ~Mld ();

  /**
   * Returns the number of (potential) memory leak.
   * 
   * \return The memory leak counter
   */
  static unsigned long value ();

private:

  /**
   * the actual counter (thread safe) 
   */
  static AtomicOp counter_;

  // = Disallow these operations.
  //--------------------------------------------
  ACE_UNIMPLEMENTED_FUNC(Mld& operator= (const Mld&))
  ACE_UNIMPLEMENTED_FUNC(Mld (const Mld&))
};

// Just drop 'MLD' anywhere in your class definition to get 
// cheap memory leak detection for your class.
#define MLD asl::Mld mld_

// Use 'MLD_COUNTER' to see if things are OK.
#define MLD_COUNTER asl::Mld::value()

} // namespace asl

#endif // _MLD_H_
