// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    Worker.h
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

#ifndef _WORK_H_
#define _WORK_H_

// ============================================================================
// DEPENDENCIEs
// ============================================================================
#include <asl/ASLConfig.h>
#if defined(ASL_DEBUG)
# include <asl/Mld.h>
#endif
#include <asl/Message.h>
#include <asl/WorkerError.h>

namespace asl {

// ============================================================================
// "C++" VIRTUAL METHOD INVOCATION vs "C" CALLBACK BENCHMARK.
// ----------------------------------------------------------------------------
// The few tests I made show that there is no perf. improvement using callback.
// So...use C++ virtual method invocation to process incoming <DATA> messages.
// ============================================================================

// ============================================================================
//! The ASL Worker 's work abstraction.
// ============================================================================
//!  
//! detailed description to be written
//! 
// ============================================================================
class ASL_EXPORT Work 
{
  friend class Worker;

public:

  virtual ~Work ();
  // Destructor.

#if defined(ASL_DEBUG)
  /**
   * Returns the current instance counter value
   */
  static unsigned long instance_counter();
#endif

protected:

  Work ();
  // Constructor.

  // = <Actions>
  //--------------------------------------------

  //- THE FOLLOWING MEMBERS SHOULD BE PURE VIRTUAL
  //- HOWEVER THEY ARE NOT DUE TO A BUG IN GCC-3.2
  
  virtual int on_init (WorkerError *&err);
  // Called when Worker is initialized.
  // Default implementation: does nothing.

  virtual int on_start (WorkerError *&err);
  // Called when the Worker receive a <START> message.
  // Default implementation: does nothing.

  virtual int on_reset (WorkerError *&err);
  // Called when the Worker receive a <RESET> message.
  // Default implementation: does nothing.

  virtual int on_stop (WorkerError *&err);
  // Called when the Worker  receive a <STOP> message.
  // Default implementation: does nothing.

  virtual int on_abort (WorkerError *&err);
  // Called when the Worker receive a <ABORT> message.
  // Default implementation: does nothing.

  virtual void on_quit ();
  // Called when the Worker receive a <QUIT> message.
  // Default implementation: does nothing.

  virtual int process (Message *in, WorkerError *&err);
  // Called when the Worker receive a <DATA> or a user defined message.
  // Default implementation: does nothing.

private:

#if defined(ASL_DEBUG)
  /**
   * Instances counter.
   */
  static AtomicOp instance_counter_;

  /**
   * Memory leak detector.
   */
  MLD; 
#endif

  /**
   * A default asl::Work hook implementation
   */
  int default_hook_impl (asl::WorkerError *&err, Message *in = 0);

  // = Disallow these operations.
  //--------------------------------------------
  ACE_UNIMPLEMENTED_FUNC(Work& operator= (const Work&))
  ACE_UNIMPLEMENTED_FUNC(Work(const Work&))
};

} // namespace asl

#if defined (__ASL_INLINE__)
# include <asl/Work.i>
#endif // __ASL_INLINE__

#endif // _WORK_H_
