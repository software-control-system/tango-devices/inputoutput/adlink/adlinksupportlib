// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    AIData.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _AI_DATA_H_
#define _AI_DATA_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/Data_T.h>

namespace asl {

// ============================================================================
//! Create a dedicated type for AI raw data (i.e numeric values)
// ============================================================================
typedef Buffer<unsigned short> AIRawData;

// ============================================================================
//! Create a dedicated type for AI scaled data (i.e. expressed in volts)  
// ============================================================================
typedef Buffer<double> AIScaledData;

// ============================================================================
//! Create a dedicated type for AI circular buffer (i.e numeric values)
// ============================================================================
typedef CircularBuffer<unsigned short> AICircularBuffer;

} // namespace asl

#endif // _AI_DATA_H_

