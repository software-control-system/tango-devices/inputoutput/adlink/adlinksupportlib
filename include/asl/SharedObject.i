// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    SharedObject.i
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

namespace asl {

// ============================================================================
// SharedObject::lock
// ============================================================================
ASL_INLINE void 
SharedObject::lock ()
{
  this->lock_.acquire();
}

// ============================================================================
// SharedObject::unlock
// ============================================================================
ASL_INLINE void 
SharedObject::unlock ()
{
  this->lock_.release();
}

} // namespace asl



