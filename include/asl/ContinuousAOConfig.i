// ============================================================================
//
// = CONTEXT
//    TANGO Project - Tango Tools Library
//
// = FILENAME
//    ContinuousAOConfig.i
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================
#include <iostream>
namespace asl {

// ============================================================================
// ContinuousAOConfig::get_default_config
// ============================================================================
ASL_INLINE ContinuousAOConfig& 
ContinuousAOConfig::get_default_config ()
{
  return ContinuousAOConfig::default_config;
}

// ============================================================================
// ContinuousAOConfig::add_active_channel
// ============================================================================
ASL_INLINE void
ContinuousAOConfig::add_active_channel (const ActiveAOChannel& _ac) 
{
  this->channels_.push_back(_ac);
}

// ============================================================================
// ContinuousAOConfig::get_active_channels
// ============================================================================
ASL_INLINE const ActiveAOChannels&
ContinuousAOConfig::get_active_channels () const
{
  return this->channels_;
}
// ============================================================================
// ContinuousAOConfig::enable_periodic_mode
// ============================================================================
ASL_INLINE void
ContinuousAOConfig::enable_periodic_mode ()
{
  this->periodic_mode_enabled_ = true;
}

// ============================================================================
// ContinuousAOConfig::disable_periodic_mode
// ============================================================================
ASL_INLINE void
ContinuousAOConfig::disable_periodic_mode ()
{
  this->periodic_mode_enabled_ = false;
}

// ============================================================================
// ContinuousAOConfig::periodic_mode_enabled
// ============================================================================
ASL_INLINE bool
ContinuousAOConfig::periodic_mode_enabled () const
{
  return periodic_mode_enabled_;
}

// ============================================================================
// ContinuousAOConfig::set_conversion_source
// ============================================================================
ASL_INLINE void
ContinuousAOConfig::set_conversion_source (adl::AOConversionSource _src)
{
  this->conversion_source_ = _src;
}

// ============================================================================
// ContinuousAOConfig::get_conversion_source
// ============================================================================
ASL_INLINE adl::AOConversionSource
ContinuousAOConfig::get_conversion_source () const
{
	return this->conversion_source_;
}

// ============================================================================
// ContinuousAOConfig::set_group
// ============================================================================
ASL_INLINE void
ContinuousAOConfig::set_group(adl::AOGroup _group)
{
	this->group_ = _group;
}

// ============================================================================
// ContinuousAOConfig::get_group
// ============================================================================
ASL_INLINE adl::AOGroup
ContinuousAOConfig::get_group() const
{
	return this->group_ ;
}

// ============================================================================
// ContinuousAOConfig::set_delay_break_mode
// ============================================================================
ASL_INLINE void
ContinuousAOConfig::set_delay_break_mode(adl::AODelayBreakMode _mode)
{
	this->delay_break_mode_ = _mode;
}

// ============================================================================
// ContinuousAOConfig::set_delay_break_mode
// ============================================================================
ASL_INLINE adl::AODelayBreakMode
ContinuousAOConfig::get_delay_break_mode() const
{
	return this->delay_break_mode_;
}

// ============================================================================
// ContinuousAOConfig::set_delay_counter_source
// ============================================================================
ASL_INLINE void
ContinuousAOConfig::set_delay_counter_source(adl::AODelaySource _src)
{
	this->delay_counter_source_ = _src;
}

// ============================================================================
// ContinuousAOConfig::get_delay_counter_source
// ============================================================================
ASL_INLINE	adl::AODelaySource
ContinuousAOConfig::get_delay_counter_source() const
{
	return this->delay_counter_source_;
}

// ============================================================================
// ContinuousAOConfig::set_delay_break_counter_source
// ============================================================================
ASL_INLINE void
ContinuousAOConfig::set_delay_break_counter_source(adl::AODelayBreakSource _src)
{
	this->delay_break_counter_source_ = _src;
}

// ============================================================================
// ContinuousAOConfig::get_delay_break_counter_source
// ============================================================================
ASL_INLINE	adl::AODelayBreakSource
ContinuousAOConfig::get_delay_break_counter_source() const
{
	return this->delay_break_counter_source_;
}

// ============================================================================
// ContinuousAOConfig::set_trigger_source
// ============================================================================
ASL_INLINE void
ContinuousAOConfig::set_trigger_source (adl::AIOTriggerSource _src)
{
	this->trigger_source_ = _src;
}

// ============================================================================
// ContinuousAOConfig::get_trigger_source
// ============================================================================
ASL_INLINE adl::AIOTriggerSource
ContinuousAOConfig::get_trigger_source () const
{
	return this->trigger_source_;
}

// ============================================================================
// ContinuousAOConfig::set_trigger_mode 
// ============================================================================
ASL_INLINE void
ContinuousAOConfig::set_trigger_mode (adl::AOTriggerMode _mode)
{
	this->trigger_mode_ = _mode;
}

// ============================================================================
// ContinuousAOConfig::get_trigger_mode
// ============================================================================
ASL_INLINE adl::AOTriggerMode
ContinuousAOConfig::get_trigger_mode () const
{
	return this->trigger_mode_;
}

// ============================================================================
// ContinuousAOConfig::set_trigger_polarity
// ============================================================================
ASL_INLINE void
ContinuousAOConfig::set_trigger_polarity (adl::AOTriggerPolarity _polarity)
{
	this->trigger_polarity_ = _polarity;
}

// ============================================================================
// ContinuousAOConfig::get_trigger_polarity
// ============================================================================
ASL_INLINE adl::AOTriggerPolarity
ContinuousAOConfig::get_trigger_polarity () const
{
	return this->trigger_polarity_;
}

// ============================================================================
// ContinuousAOConfig::enable_retrigger
// ============================================================================
ASL_INLINE void
ContinuousAOConfig::enable_retrigger ()
{
  this->retrigger_status_ = adl::ao_rt_enabled;
}

// ============================================================================
// ContinuousAOConfig::disable_retrigger
// ============================================================================
ASL_INLINE void
ContinuousAOConfig::disable_retrigger ()
{
  this->retrigger_status_ = adl::ao_rt_disabled;
}

// ============================================================================
// ContinuousAOConfig::retrigger_enabled
// ============================================================================
ASL_INLINE bool
ContinuousAOConfig::retrigger_enabled () const
{
  return (this->retrigger_status_ == adl::ao_rt_enabled) ? true : false;
}

// ============================================================================
// ContinuousAOConfig::set_nb_waveforms
// ============================================================================
ASL_INLINE void
ContinuousAOConfig::set_nb_waveforms (unsigned short _w)
{
	this->nb_waveforms_ = _w;
}

// ============================================================================
// ContinuousAOConfig::get_nb_waveforms
// ============================================================================
ASL_INLINE unsigned short
ContinuousAOConfig::get_nb_waveforms () const
{
	return this->nb_waveforms_;
}

// ============================================================================
// ContinuousAOConfig::set_delay_counter
// ============================================================================
ASL_INLINE void
ContinuousAOConfig::set_delay_counter(unsigned short _value)
{
	this->delay_counter_ = _value;
}

// ============================================================================
// ContinuousAOConfig::get_delay_counter
// ============================================================================
ASL_INLINE	unsigned short
ContinuousAOConfig::get_delay_counter() const
{
	return this->delay_counter_;
}

// ============================================================================
// ContinuousAOConfig::set_delay_break_counter
// ============================================================================
ASL_INLINE void
ContinuousAOConfig::set_delay_break_counter(unsigned short _value)
{
	this->delay_break_counter_ = _value;
}

// ============================================================================
// ContinuousAOConfig::get_delay_break_counter
// ============================================================================
ASL_INLINE	unsigned short
ContinuousAOConfig::get_delay_break_counter () const
{
	return this->delay_counter_;
}
// ============================================================================
// ContinuousAOConfig::get_stop_source
// ============================================================================
ASL_INLINE  void
ContinuousAOConfig::set_stop_source (adl::StopSource _ss)
{
  this->stop_source_ = _ss;
}
// ============================================================================
// ContinuousAOConfig::set_stop_source
// ============================================================================
ASL_INLINE  adl::StopSource 
ContinuousAOConfig::get_stop_source () const
{
  return this->stop_source_;
}
// ============================================================================
// ContinuousAOConfig::set_stop_mode
// ============================================================================
ASL_INLINE  void
ContinuousAOConfig::set_stop_mode (adl::StopMode _sm)
{
  this->stop_mode_ = _sm;
}
// ============================================================================
// ContinuousAOConfig::get_stop_source
// ============================================================================
ASL_INLINE  adl::StopMode
ContinuousAOConfig::get_stop_mode () const
{
  return this->stop_mode_;
}
// ============================================================================
// ContinuousAOConfig::set_analog_trigger_source
// ============================================================================
ASL_INLINE void 
ContinuousAOConfig::set_analog_trigger_source (adl::AnalogTriggerChannel _src)
{
	this->analog_trigger_channel_ = _src;
}

// ============================================================================
// ContinuousAIConfig::get_analog_trigger_source
// ============================================================================
ASL_INLINE adl::AnalogTriggerChannel 
ContinuousAOConfig::get_analog_trigger_source () const
{
	return this->analog_trigger_channel_;
}

// ============================================================================
// ContinuousAOConfig::set_analog_trigger_condition
// ============================================================================
ASL_INLINE void 
ContinuousAOConfig::set_analog_trigger_condition (adl::AnalogTriggerCondition _cond)
{
	this->analog_trigger_condition_ = _cond;
}

// ============================================================================
// ContinuousAOConfig::get_analog_trigger_condition
// ============================================================================
ASL_INLINE adl::AnalogTriggerCondition 
ContinuousAOConfig::get_analog_trigger_condition () const
{
	return this->analog_trigger_condition_;
}

// ============================================================================
// ContinuousAOConfig::set_analog_low_level_condition
// ============================================================================
ASL_INLINE void 
ContinuousAOConfig::set_analog_low_level_condition (unsigned short _lc)
{
  this->analog_trigger_lo_level_ = _lc;
}

// ============================================================================
// ContinuousAOConfig::get_analog_low_level_condition
// ============================================================================
ASL_INLINE unsigned short 
ContinuousAOConfig::get_analog_low_level_condition () const
{
	return this->analog_trigger_lo_level_;
}

// ============================================================================
// ContinuousAOConfig::set_analog_high_level_condition
// ============================================================================
ASL_INLINE void 
ContinuousAOConfig::set_analog_high_level_condition (unsigned short _hc)
{
	this->analog_trigger_hi_level_ = _hc;
}

// ============================================================================
// ContinuousAOConfig::get_analog_high_level_condition
// ============================================================================
ASL_INLINE unsigned short 
ContinuousAOConfig::get_analog_high_level_condition () const
{
	return this->analog_trigger_hi_level_;
}
// ============================================================================
// ContinuousAOConfig::set_output_rate
// ============================================================================
ASL_INLINE void
ContinuousAOConfig::set_output_rate (double _sr)
{
	this->output_rate_ = _sr;
}

// ============================================================================
// ContinuousAOConfig::get_output_rate
// ============================================================================
ASL_INLINE double
ContinuousAOConfig::get_output_rate () const
{
	return this->output_rate_;
}
// ============================================================================
// ContinuousAOConfig::set_data_format
// ============================================================================
ASL_INLINE  void 
ContinuousAOConfig::set_data_format (adl::AODataFormat _format)
{
  this->format_ = _format;
}
// ============================================================================
// ContinuousAOConfig::get_data_format
// ============================================================================
ASL_INLINE  adl::AODataFormat
ContinuousAOConfig::get_data_format () const
{
  return this->format_ ;
}
/*
// ============================================================================
// ContinuousAOConfig::set_overrun_strategy
// ============================================================================
ASL_INLINE void
ContinuousAOConfig::set_overrun_strategy (adl::OverrunStrategy _strategy)
{
	this->overrun_strategy_ = _strategy;
}

// ============================================================================
// ContinuousAOConfig::get_overrun_strategy
// ============================================================================
ASL_INLINE adl::OverrunStrategy
ContinuousAOConfig::get_overrun_strategy () const
{
	return overrun_strategy_;
}
*/

// ============================================================================
// ContinuousAOConfig::set_buffer_depth
// ============================================================================
ASL_INLINE void
ContinuousAOConfig::set_buffer_depth (unsigned long _depth)
{
	this->buffer_depth_ = _depth;
}

// ============================================================================
// ContinuousAOConfig::get_buffer_depth
// ============================================================================
ASL_INLINE unsigned long
ContinuousAOConfig::get_buffer_depth () const
{
	return this->buffer_depth_;
}

// ============================================================================
// ContinuousAOConfig::set_db_event_callback
// ============================================================================
ASL_INLINE void
ContinuousAOConfig::set_db_event_callback (DAQCallback _cb)
{
	this->db_event_callback_ = _cb;
}

// ============================================================================
// ContinuousAOConfig::get_db_event_callback
// ============================================================================
ASL_INLINE DAQCallback
ContinuousAOConfig::get_db_event_callback () const
{
	return this->db_event_callback_;
}

} // namespace asl
