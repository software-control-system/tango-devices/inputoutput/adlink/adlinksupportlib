// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    PulsedTaskManager.h
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

#ifndef _PULSED_TASK_MANAGER_H_
#define _PULSED_TASK_MANAGER_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <ace/Reactor.h>
#include <asl/ASLConfig.h>
#if defined(ASL_DEBUG)
# include <asl/Mld.h>
#endif
#include <asl/Thread.h>
#include <asl/PulsedTask.h>

namespace asl {

// ============================================================================
//! The ASL pulsed task manager.  
// ============================================================================
//!  
//! detailed description to be defined
//! 
// ============================================================================
class ASL_EXPORT PulsedTaskManager : public Thread
{
public:

  /**
   * Get unique instance (singleton).
   */
  static PulsedTaskManager * instance ();

  /**
   * Release resources.
   */
  virtual ~PulsedTaskManager ();

  /**
   * Register a PulsedTask.
   */
  int register_task (PulsedTask * t, void * arg, ACE_Time_Value& intv);

  /**
   * Remove a PulsedTask.
   */
  int remove_task (PulsedTask * t);

  /**
   * ACE internal cooking. Consider as private.
   */
  const ACE_TCHAR * dll_name ();

  /**
   * ACE internal cooking. Consider as private.
   */
  const ACE_TCHAR * name ();

  /**
   * ACE internal cooking. Consider as private.
   */
  static void close_singleton ();

protected:

  /**
   *  Entry point implementation.
   */
  virtual ACE_THR_FUNC_RETURN svc (void* arg);

private:

  /**
   * Initialization. 
   */
  PulsedTaskManager ();
  
  /**
   * The underlying ACE Reactor.
   */
  ACE_Reactor * reactor_;

  /**
   * A mutex to protect the singleton against race conditions.
   */
  static ACE_Recursive_Thread_Mutex instance_lock;

  /**
   * The PulsedTaskManager singleton
   */
  static PulsedTaskManager * manager;

  // = Disallow these operations.
  //--------------------------------------------
  ACE_UNIMPLEMENTED_FUNC(PulsedTaskManager& operator= (const PulsedTaskManager&))
  ACE_UNIMPLEMENTED_FUNC(PulsedTaskManager(const PulsedTaskManager&))
};

} // namespace asl

#if defined (__ASL_INLINE__)
# include "asl/PulsedTaskManager.i"
#endif // __ASL_INLINE__

#endif // _PULSED_TASK_MANAGER_H_

