// ============================================================================
//
// = CONTEXT
//   TANGO Project - ADLink Support Library
//
// = FILENAME
//    Worker.h
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

#ifndef _WORKER_H_
#define _WORKER_H_

// ============================================================================
// DEPENDENCIEs
// ============================================================================
#include <string>
#include <ace/Task.h>
#include <asl/ASLConfig.h>
#if defined(ASL_DEBUG)
# include <asl/Mld.h>
#endif
#include <asl/Message.h>
#include <asl/WorkerError.h>
#include <asl/Work.h>

namespace asl {

// ============================================================================
// TYPEDEFS
// ============================================================================
//- define what is a MessageQueue
typedef ACE_Message_Queue<ACE_MT_SYNCH> MessageQueue;

// ============================================================================
//! The ASL Worker abstraction.
// ============================================================================
//!  
//! detailed description to be written
//! 
// ============================================================================
class ASL_EXPORT Worker : private ACE_Task_Base
{
public:

  //- Default water marks and priority
  //--------------------------------------------------------
  enum {
    // Default worker priority.
    DEFAULT_PRIORITY = ACE_DEFAULT_THREAD_PRIORITY,
    // Default worker's message queue low water mark
    DEFAULT_LWM = 32 * ACE_Message_Queue_Base::DEFAULT_LWM,
    // Default worker's message queue high water mark
    DEFAULT_HWM = 64 * ACE_Message_Queue_Base::DEFAULT_HWM
  };

  //- Worker's states
  //--------------------------------------------------------
  enum {
    UP,
    STANDBY,
    WORKING,
    DOWN,
    UNDEFINED
  };

  /**
   * Creates a non initialized Worker instance (see init below)
   */
  Worker (const std::string& _name, Work* _work, bool _delete_work = true);

  /**
   * Initializes the Worker. Must be called after Worker 
   * instanciation. Return -1 on failure, 0 otherwise. Any 
   * initialization error can be retrieved using Worker::last_error.
   * Don't try to use an uninitialized Worker (i.e. a Worker for 
   * which the init method has not been called or a Worker 
   * whose init method returned -1. 
   *
   * \param lwm low water mark (in bytes) of the Worker's message queue
   *
   * \param hwm high water mark (in bytes) of the Worker's message queue
   *
   * \param priority Worker's daemon thread priority
   */
  int init (size_t lwm = DEFAULT_LWM, 
            size_t hwm = DEFAULT_LWM, 
            long priority = DEFAULT_PRIORITY);

  /**
   * Release Worker's resources
   */
  virtual ~Worker ();

  /**
   * Ask the Worker to start working. 
   * Note that _tv uses {absolute} time rather than {relative} 
   * time. If _tv==0, the caller will block until action is possible, 
   * else will wait until the {absolute} time specified in *_tv 
   * elapses). These calls will return, however, when queue is closed, 
   * deactivated, when a signal occurs, or if the time specified in 
   * timeout elapses, (in  which case errno = EWOULDBLOCK).
   *
   * \return -1 on failure, 0 otherwise. 
   */
  int start (bool _wait = true, Timeout * _tv = 0, bool * _tv_expired = 0);

  /**
   * Reset the Worker. 
   * Note that _tv uses {absolute} time rather than {relative} 
   * time. If tv==0, the caller will block until action is possible, 
   * else will wait until the {absolute} time specified in *_tv 
   * elapses). These calls will return, however, when queue is closed, 
   * deactivated, when a signal occurs, or if the time specified in 
   * timeout elapses, (in  which case errno = EWOULDBLOCK).
   *
   * \return -1 on failure, 0 otherwise. 
   */
  int reset (bool _wait = true, Timeout * _tv = 0, bool * _tv_expired = 0);

  /**
   * Ask the Worker to stop working. 
   * Note that _tv uses {absolute} time rather than {relative} 
   * time. If tv==0, the caller will block until action is possible, 
   * else will wait until the {absolute} time specified in *_tv 
   * elapses). These calls will return, however, when queue is closed, 
   * deactivated, when a signal occurs, or if the time specified in 
   * timeout elapses, (in  which case errno = EWOULDBLOCK).
   *
   * \return -1 on failure, 0 otherwise. 
   */
  int stop (bool _wait = true, Timeout * _tv = 0, bool * _tv_expired = 0);

  /**
   * Ask the Worker to abort its activity. 
   * Note that _tv uses {absolute} time rather than {relative} 
   * time. If tv==0, the caller will block until action is possible, 
   * else will wait until the {absolute} time specified in *_tv 
   * elapses). These calls will return, however, when queue is closed, 
   * deactivated, when a signal occurs, or if the time specified in 
   * timeout elapses, (in  which case errno = EWOULDBLOCK).
   *
   * \return -1 on failure, 0 otherwise. 
   */
  int abort (bool _wait = true, Timeout * _tv = 0, bool * _tv_expired = 0);

  /**
   * Ask the Worker to quit (daemon thread exit). 
   * Returns -1 on failure, 0 otherwise.
   */
  int quit ();

  /**
   * Insert message into the message queue.  Note that _tv uses
   * {absolute} time rather than {relative} time. If tv==0, the
   * caller will block until action is possible, else will wait until
   * the {absolute} time specified in *_tv elapses). These calls
   * will return, however, when queue is closed, deactivated, when a
   * signal occurs, or if the time specified in timeout elapses, (in 
   * which case errno = EWOULDBLOCK).
   */
  int post (Message *_msg, Timeout *_tv = 0);

  /**
   * Returns the worker's priority (i.e. daemon thread priority)
   */
  long priority_level () const;

  /**
   * Set worker's priority (i.e. daemon thread priority)
   */
  int priority_level (long _prio);

  /**
   * Returns low water mark.
   */
  size_t low_water_mark () const;

  /**
   * Set low water mark.
   */
  void low_water_mark (size_t _lo);

  /**
   * Returns high water mark.
   */
  size_t high_water_mark () const;

  /**
   * Set high water mark.
   */
  void high_water_mark (size_t _hi);

  /**
   * Returns timeout (in milliseconds).
   */
  long timeout () const;

  /**
   * Set timeout (in milliseconds).
   */
  void timeout (long _tmo_msec);

  /**
   * Returns true if an error occured, false otherwise
   */
  bool has_error () const;

  /**
   * Returns last error (as a smart pointer).
   */
  WorkerErrorPtr last_error ();

  /**
   * Returns current worker's state.
   */
  int state () const;

  /**
   * Returns true if the Worker is overloaded (full msgQ)
   */
#ifdef __linux
  bool overloaded () const;
#endif

  /**
   * Returns worker's name.
   */
  const std::string& name () const;

  /**
   * Dump worker's internal state.
   */
  void dump (int _indent_level = 0) const;

#if defined(ASL_DEBUG)
  /**
   * Returns the current instance counter value
   */
  static unsigned long instance_counter();
#endif

  //- Hook called from <ACE_Thread_Exit> when during thread exit.  
  //- In general, this method shouldn't be called directly by an 
  //- application, Instead, a special message should be passed 
  //- into the <Worker> via the <post> method defined below, and 
  //- the <svc> method should interpret this as a flag to shut 
  //- down the <Worker>.
  virtual int close (u_long _flags = 0);

protected:

  //- run by a daemon thread to handle deferred processing
  virtual int svc ();

  //- run by a daemon thread to handle deferred processing
  virtual int sub_svc();

  //- get next message in the queue
  virtual int next_message(Message *&_msg);

  //- set the underlying thread priority to <_prio>
  int priority_level_i (long _prio);

  //- set task state
  void state (int _state);

#if defined(ASL_DEBUG)
  //- just_a_call_in_svc_thread_context (debugging)
  void just_a_call_in_svc_thread_context(); 
#endif

private:

  //- helper method to extract a Message from the messageQ
  int getq (Message * &_mb, Timeout * _tv = 0);

  //- store <_err> as last error
  void set_error (WorkerError * _err);

  //- the message queue.
  MessageQueue * msg_queue_;

  //- next message in queue (help in chained messages handling)
  Message * next_msg_;

  //- the worker priority.
  long prio_;

  //- worker's work to do.
  Work * work_;

  //- name of Worker
  std::string name_;

  //- Worker's state.
  int state_;

  //- message queue lo warter mark
  size_t lwm_;

  //- message queue hi warter mark
  size_t hwm_;

  //- last error
  WorkerError * last_error_;

  //- work ownership
  bool work_ownership_;

  //- call Work's <process> if no message receive after <timeout_> seconds
  Timeout timeout_;

  //- a an event to sync. Worker clients with internal activity.
  ACE_Manual_Event sync_event_;

  //- a lock to protect internal state against race condition
  ACE_Thread_Mutex ctrl_lock_;

#if defined(ASL_DEBUG)

  int msg_counter_; 
  // Total message counter

  int ctrl_counter_;
  // Ctrl message counter

  int trash_counter_;
  // Thrashed message counter

  int data_counter_;
  // Data message counter

  double proc_data_;
  // Total of data processed (in bytes).

  int thread_id_;
  // Service thread ID.

  size_t max_bytes_;
  // Max of (sum of msg block size) in MQ

  size_t max_len_;
  // Max of (sum of msg block len) in MQ

  size_t max_msgs_;
  // Max # of messages store in MQ

  int has_been_empty_;
  // Set to 1 if MQ has been empty (at least once)

  int has_been_full_;
  // Set to 1 if MQ has been full (at least once)

  //- Instances counter.
  static AtomicOp instance_counter_;

  //- Memory leak detector
  MLD;
#endif

  // = Disallow these operations.
  ACE_UNIMPLEMENTED_FUNC(Worker& operator= (const Worker&))
  ACE_UNIMPLEMENTED_FUNC(Worker (const Worker&))
};

} // namespace asl

#if defined (__ASL_INLINE__)
# include <asl/Worker.i>
#endif // __ASL_INLINE__

#endif // _WORKER_H_

