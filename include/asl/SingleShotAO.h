// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    SingleShotAO.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _SINGLE_SHOT_AO_H_
#define _SINGLE_SHOT_AO_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/SingleShotDAQ.h>
#include <asl/adlink/ADSingleShotAOBoard.h>

namespace asl {

// ============================================================================
//! The ASL single shot analog output DAQ class.   
// ============================================================================
//! Provides functions to perform single shot analog output on ADLink boards that
//! support this functions.\n
//! The main fonctionnalities are write on one or several channels.
// ============================================================================
class ASL_EXPORT SingleShotAO : public SingleShotDAQ
{
public:

  /**
   * Initialization. 
   */
  SingleShotAO ();
  
  /**
   * Release resources.
   */
  virtual ~SingleShotAO ();
  
  /**
   * Initialize the single shot digital output DAQ.
   * @param hw_type The type of the card.
   * @param hw_id The number of the card in the cPCI crate.
   */
  virtual void init (unsigned short hw_type, unsigned short hw_id) 
    throw (DAQException);
 
  /**
   * Performs hardware auto-calibration.
   */
  void calibrate_hardware ()
          throw (asl::DAQException);

  /**
   * Configure one input channel.
   * @param _chan_id The channel number.
   * @param _polarity The output polarity (default value is adl::bipolar).
   * @param _ref_source The voltage source reference (default value is adl::internal_reference).
   * @param _ref_volt The reference voltage (default value is 10.0).
   */
  void configure_channel (adl::ChanId _chan_id, 
                          adl::OutputPolarity _polarity = adl::bipolar, 
                          adl::VoltageReferenceSource _ref_source = adl::internal_reference, 
                          double _ref_volt = 10.0) 
    throw (DAQException);
 
  /**
   * Writes on one channel.
   * @param _chan_id The channel number.
   * @param _value the output value (non-scaled).
   */
  void write_channel (adl::ChanId _chan_id, unsigned short _value) 
    throw (DAQException);

  /**
   * Writes a value in volt in output.
   * @param _chan_id The channel number.
   * @param _value The output value in volt.
   */
  void write_scaled_channel (adl::ChanId _chan_id, double _value) 
    throw (DAQException);

private:

	/**
   * The underlying hardware.
   */
  adl::ADSingleShotAOBoard * daq_hw_;

  /**
   * Return true if initialized, false otherwise.
   */
  bool initialized () const;
};

} // namespace asl
	
#if defined (__ASL_INLINE__)
# include <asl/SingleShotAO.i>
#endif // __ASL_INLINE__

#endif // _SINGLE_SHOT_AO_H_




