// ============================================================================
//
// = CONTEXT
//    TANGO Project - Tango Tools Library
//
// = FILENAME
//    ContinuousAOConfig.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _CONTINUOUS_AO_CONFIG_H_
#define _CONTINUOUS_AO_CONFIG_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/ASLConfig.h>
#include <vector>
#include <map>
#if defined(ASL_DEBUG)
# include <asl/Mld.h>
#endif
#include <asl/adlink/ADLink.h>
#include <asl/AOData.h>
#include <asl/DAQException.h>

namespace asl {

// ============================================================================
// Create a dedicated type for DAQ events callback
// ============================================================================
#if defined(WIN32)
  typedef void (*DAQCallback) ();
#else
  typedef void (*DAQCallback) (int);
#endif

// ============================================================================
// Create a dedicated type for std::vector of asl::ActiveAOChannel
// ============================================================================
typedef std::vector<asl::ActiveAOChannel> ActiveAOChannels;

typedef std::map<unsigned short, asl::AORawData*> PeriodicRawBuffers;
typedef PeriodicRawBuffers::iterator PeriodicRawBuffersIterator;
typedef PeriodicRawBuffers::const_iterator ConstPeriodicRawBuffersIterator;
typedef PeriodicRawBuffers::value_type PeriodicRawBuffersValue;

typedef std::map<unsigned short, asl::AOScaledData*> PeriodicScaledBuffers;
typedef PeriodicScaledBuffers::iterator PeriodicScaledBuffersIterator;
typedef PeriodicScaledBuffers::const_iterator ConstPeriodicScaledBuffersIterator;
typedef PeriodicScaledBuffers::value_type PeriodicScaledBuffersValue;
// ============================================================================
// Num of channels per AO group 
// ============================================================================
#define kNUM_CHAN_PER_AO_GROUP 4

// ============================================================================
//! The ASL continuous analog input configuration class.
// ============================================================================ 
//! Configure all the parameters needed by the hardware in continuous analog output.\n
//! The user must be aware that knowing the hardware is essential to have the desired output.
//! All the parameters have got values by default.
// ============================================================================
class ASL_EXPORT ContinuousAOConfig
{
public:

  /**
   * Constructor.
   */
  ContinuousAOConfig ();

  /**
   * Copy constructor.
   * @param src The config to copy.
   */
  ContinuousAOConfig (const ContinuousAOConfig& src);

  /**
   * Redefinition of the operator =.
   */
  ContinuousAOConfig& operator= (const ContinuousAOConfig& src);

  /**
   * Destructor.
   */
  virtual ~ContinuousAOConfig();

  /**
   * Add one active channel to the configuration.
   * @param ac The active channel to add.
   */
  void add_active_channel (const ActiveAOChannel& ac);

  /**
   * Get all the active channels of the configuration.
   * @return A vector containing the active channels.
   */
  const ActiveAOChannels& get_active_channels () const;

  /**
   * Enable the periodic mode to output a periodic signal (disable by default).
   */
  void enable_periodic_mode ();

  /**
   * Disable the periodic mode.
   */
  void disable_periodic_mode ();

  /**
   * Check if periodic mode is enabled.
   * @return true if periodic mode is enabled.
   */
  bool periodic_mode_enabled () const;

  /**
  * Give one period a data for one channel in scaled format(volts). 
  * The data must be given in the same format for all channels. Don't give scaled and raw data!
  * @param _channel The channel identifier.
  * @param _pdata The output data in volts.
  */
  void set_channel_periodic_data (unsigned int _channel, const asl::AOScaledData& _pdata);

   /**
  * Give one period a data for one channel in raw format(digital). 
  * The data must be given in the same format for all channels. Don't give scaled and raw data!
  * @param _channel The channel identifier.
  * @param _pdata The output data in volts in digital format.
  */
  void set_channel_periodic_data (unsigned int _channel, const asl::AORawData& _pdata);
    
  /**
  * @param _channel The channel to get.
  * @return The buffer of periodic data as an asl::BaseBuffer for the specified channel.
  */
  const asl::BaseBuffer * get_channel_periodic_data (unsigned int _channel);
	
  /**
   * Configure the conversion source (internal timer by default). 
   * @param _src The conversion source
   */
  void set_conversion_source (adl::AOConversionSource _src);

  /**
   * Get the conversion source. 
   * @return the conversion source.
   */
  adl::AOConversionSource get_conversion_source () const;

  /**
   * Configure a output group of ports (group_a by default).
   * @param _grp The group.
   */
  void set_group(adl::AOGroup _grp);

  /**
   * Get the group.
   * @return The group.
   */
  adl::AOGroup get_group() const;

  /**
   * Configure the counter source for the delay mode (internal timer by default).
   * @param _src The delay source.
   */
  void set_delay_counter_source(adl::AODelaySource _src);

  /**
   * Get the counter source for the delay mode.
   * @return The delay source.
   */
  adl::AODelaySource get_delay_counter_source() const;

  /**
   * Configure the counter source for delay break (internal timer by default).
   * The delay between two consecutive waveforms.
   * @param _src The source.
   */
  void set_delay_break_counter_source(adl::AODelayBreakSource _src);

  /**
   * Get the counter source for delay break.
   * @return The source.
   */
  adl::AODelayBreakSource get_delay_break_counter_source() const;

  /**
   * Configure the trigger source (software by default).
   * @param _src The trigger source.
   */
  void set_trigger_source (adl::AIOTriggerSource _src);
  
  /**
   * Get the trigger source.
   * @return The trigger source.
   */
  adl::AIOTriggerSource get_trigger_source () const;

  /**
   * Configure the trigger mode (post trigger mode by default)
   * @param _mode The trigger mode. 
   */
  void set_trigger_mode (adl::AOTriggerMode _mode);

  /**
   * Get the trigger mode.
   * @return The trigger mode. 
   */
  adl::AOTriggerMode get_trigger_mode () const;
  /**
   * Configure the trigger polarity (rising edge by default).
   * @param _polarity The polarity.
   */
  void set_trigger_polarity (adl::AOTriggerPolarity _polarity);

  /**
   * Get the trigger polarity.
   * @return The polarity.
   */
  adl::AOTriggerPolarity get_trigger_polarity () const;
  
  /**
   * Enable retrigger mode (disable by default).
   */
  void enable_retrigger ();
  
  /**
   * Disable retrigger mode (disable by default).
   */
  void disable_retrigger ();
	
  /**
   * Returns true if infinite retrigger mode is enabled, false otherwise.
   */
  bool retrigger_enabled () const;
  
  /**
  * Set the number of waveforms outputed after a trigger signal (0 by default). Only used in trigger mode.
  * @param _w The number of waveforms.
  */
  void set_nb_waveforms (unsigned short _w);
  
  /**
   * Get the number of waveforms outputed after a trigger signal.
   * @return The number of waveforms.
   */
  unsigned short get_nb_waveforms () const;
  
  /**
   * Enable delay break mode (disable by default).
   * It is the delay between two consecutive waveform generations.
   * @param _mode The delay break mode.
   */
  void set_delay_break_mode (adl::AODelayBreakMode _mode);
 
  /**
   * Get delay break mode.
   * @return The delay break mode.
   */
  adl::AODelayBreakMode get_delay_break_mode () const;
  
  /**
   * Configure delay counter (0 by default).
   * @param _value The delay counter value.
   */
  void set_delay_counter (unsigned short _value);

  /**
   * Get delay counter.
   * @return The delay counter value.
   */
  unsigned short get_delay_counter () const;

  /**
   * Configure delay break counter (0 by default).
   * @param _value The delay break counter value.
   */
  void set_delay_break_counter (unsigned short _value);

  /**
   * Get delay break counter.
   * @return The delay break counter value.
   */
  unsigned short get_delay_break_counter () const;
	/**
  * Configure the stop source (software by default).
  * @param _ss The stop source.
  */
  void set_stop_source (adl::StopSource _ss);
  /**
  * Get the stop source.
  * @return The stop source.
  */
  adl::StopSource get_stop_source () const;
  /**
  * Configure the stop mode (stop immedialty by default).
  * @param _sm The stop mode.
  */
  void set_stop_mode (adl::StopMode _sm);
  /**
  * Get the stop mode.
  * @return The stop mode.
  */
  adl::StopMode get_stop_mode () const;
  /**
   * Configure the channel source for analog trscaled_igger (analog trigger chan0 by default).
   * @param _src The analog trigger source.
   */
  void set_analog_trigger_source (adl::AnalogTriggerChannel _src);

  /**
   * Get the channel source for analog trigger. 
   * @return The analog trigger source.
   */
  adl::AnalogTriggerChannel get_analog_trigger_source () const;

  /**
   * Configure the analog trigger condition (below low level by default).
   * @param _cond The analg trigger condition.
   */
  void set_analog_trigger_condition (adl::AnalogTriggerCondition _cond);

  /**
   * Get the analog trigger condition.
   * @return The analg trigger condition.
   */
  adl::AnalogTriggerCondition get_analog_trigger_condition () const;

  /**
   * Configure the analog low level condition.
   * The trigger is generated when input analog signal is less than 'low_level'. 
   * @param _lc The condition. Must be between 0 an 255.
   */
  void set_analog_low_level_condition (unsigned short _lc);

  /**
   * Get the analog low level condition.
   * @return The condition.
   */
  unsigned short get_analog_low_level_condition () const;
  /**
   * Configure the analog high level condition.
   * The trigger is generated when input analog signal is more than 'high_level'. 
   * @param _hc The condition. Must be between 0 and 255.
   */
  void set_analog_high_level_condition (unsigned short _hc);

  /**
   * Get the analog high level condition.
   * @return The condition.
   */
  unsigned short get_analog_high_level_condition () const;
  /**
   * Configure the output frequency (1000 Hz by default).
   * @param _or The output frequency in hetz.
   */
  void set_output_rate (double _or);

  /**
   * Get the output frequency.
   * @return The output frequency in hertz.
   */
  double get_output_rate () const;

  /**
  * Configure the ouput data to scaled or not non-scaled by default).
  * @param _format The mode (scaled or not).
  */
  void set_data_format (adl::AODataFormat _format);

   /**
  * Configure the ouput data to scaled or not.
  * @return The format (scaled or not).
  */
  adl::AODataFormat get_data_format () const;

  /**
   * Configure the depth of the output buffer (500 by default).
   * @param _depth The depth of the buffer in samples.
   */
  void set_buffer_depth (unsigned long _depth);

  /**
   * Get the depth of the output buffer.
   * @return The depth of the buffer in samples.
   */
  unsigned long get_buffer_depth () const;

  /**
   * Configure the event call back (0 by default).
   * @param _cb The event call back. 
   */
  void set_db_event_callback (DAQCallback _cb);

  /**
   * Get the event call back.
   * @return The event call back.
   */
  DAQCallback get_db_event_callback () const;

  /**
   * Returns a default configuration
   * @return The default configuration singleton
   */
  static ContinuousAOConfig& get_default_config ();

private:
  
  //- analog output active channels
  ActiveAOChannels channels_;
  
  //- map of all periodic data
  PeriodicRawBuffers periodic_raw_buffers;
  
  //- map of all periodic data
  PeriodicScaledBuffers periodic_scaled_buffers;

  //- periodic mode status
  bool periodic_mode_enabled_;

  //- conversion source
  adl::AOConversionSource conversion_source_;

  //-
  adl::AOGroup group_;

  //- delay counter source selection
  adl::AODelaySource delay_counter_source_;

  //-delay break counter source selection
  adl::AODelayBreakSource delay_break_counter_source_;

  //- trigger source
  adl::AIOTriggerSource trigger_source_;

  //- trigger mode
  adl::AOTriggerMode trigger_mode_;

 //- retrigger on/off option
  adl::AORetriggerStatus retrigger_status_;

  //- number of waveforms after a trigger
  unsigned short nb_waveforms_;

  //-delay break mode enable
  adl::AODelayBreakMode delay_break_mode_;

  //- trigger polarity
  adl::AOTriggerPolarity trigger_polarity_;

  //- delay counter value
  //-TODO delay in seconds
  unsigned short delay_counter_;

  //- delay break counter value
  unsigned short delay_break_counter_;  

  //- stop source
  adl::StopSource stop_source_;
  
  //-stop mode
  adl::StopMode stop_mode_;
   
  //- analog trigger source
  adl::AnalogTriggerChannel analog_trigger_channel_;

  //- analog trigger condition
  adl::AnalogTriggerCondition analog_trigger_condition_;

  //- analog trigger mode low level condition
  unsigned short analog_trigger_lo_level_;

  //- analog trigger mode high level condition
  unsigned short analog_trigger_hi_level_;

  //- internal sampling rate
  double output_rate_;

  //-data mode
  adl::AODataFormat format_;

  //- half-db-buffer depth in samples
  unsigned long buffer_depth_;

  //- daq event callback address
  DAQCallback db_event_callback_;

  //- the default configuration singleton
  static ContinuousAOConfig default_config;
};

} // namespace asl

#if defined (__ASL_INLINE__)
# include "asl/ContinuousAOConfig.i"
#endif // __ASL_INLINE__

#endif // _CONTINUOUS_AO_CONFIG_H_
