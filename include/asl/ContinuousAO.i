// ============================================================================
//
// = CONTEXT
//    TANGO Project - Tango Tools Library
//
// = FILENAME
//    ContinuousAO.i
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

namespace asl {

// ============================================================================
// ContinuousAO::configure
// ============================================================================
ASL_INLINE void
ContinuousAO::configure (ContinuousAOConfig* _config) 
  throw (asl::DAQException, asl::DeviceBusyException)
{
  if (_config == 0) {
    throw asl::DAQException("invalid configuration specified",
                            "invalid configuration specified (null pointer)",
                            "ContinuousAO::configure");
  }
  this->configure(*_config);
}

// ============================================================================
// ContinuousAO::configuration
// ============================================================================
ASL_INLINE const ContinuousAOConfig& 
ContinuousAO::configuration () const
{
  return this->config_;
}

// ============================================================================
// ContinuousAO::initialized
// ============================================================================
ASL_INLINE bool
ContinuousAO::initialized () const
{
  return (this->daq_hw_) ? true : false;
}
// ============================================================================
// ContinuousAO::use_board_fifo
// ============================================================================
ASL_INLINE bool 
ContinuousAO::use_board_fifo() const
{
    return this->daq_hw_->use_board_fifo();
}
} // namespace asl
