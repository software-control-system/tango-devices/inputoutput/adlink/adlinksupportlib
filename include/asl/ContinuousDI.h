// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ContinuousDI.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _CONTINUOUS_DI_H_
#define _CONTINUOUS_DI_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/Worker.h>
#include <asl/ContinuousDAQ.h>
#include <asl/ContinuousDIConfig.h>
#include <asl/adlink/ADContinuousDIBoard.h>

namespace asl {

// ============================================================================
// FORWARD DECLARATION
// ============================================================================
//- daq simulation 
#if defined(_SIMULATION_)
class CallbackCaller;
#endif
class ContinuousDIWork;

// ============================================================================
//! The ASL continuous analog input DAQ class.  
// ============================================================================
//! Provides functions to perform continuous analog output on ADLink boards that
//! support this functions.\n
//! The main functionnalities are:  
//! - Configurable input port to 8, 16 and 32 bits.
//! - Internal clock, external clock or handshaking mode.
// ============================================================================ 
class ASL_EXPORT ContinuousDI : public ContinuousDAQ
{
public:

  /**
   * Initialization. 
   */
  ContinuousDI ();
  
  /**
   * Release resources.
   */
  virtual ~ContinuousDI ();

  //
  // Continuous DAQ control methods
  //-----------------------------------------------------------

  /**
   * Configure the continuous analog input DAQ.
   */
  void configure (const ContinuousDIConfig& config)
      throw (asl::DAQException, asl::DeviceBusyException);

  /**
   * Configure the continuous analog input DAQ.
   */
  void configure (ContinuousDIConfig * config)
      throw (asl::DAQException, asl::DeviceBusyException);

  /**
   * Returns the current configuration.
   */
  const ContinuousDIConfig& configuration () const;

  /**
   * Initialize the continuous analog input DAQ.
   */
  virtual void init (unsigned short hw_type, unsigned short hw_id)
      throw (asl::DAQException);

  /**
   * Start the continuous analog input DAQ.
   */
  virtual void start ()
      throw (asl::DAQException, asl::DeviceBusyException);

  /**
   * Stop the continuous analog input DAQ.
   */
  virtual void stop ()
      throw (asl::DAQException, asl::DeviceBusyException);

  /**
   * Abort the continuous analog input DAQ.
   */
  virtual void abort ()
      throw (asl::DAQException, asl::DeviceBusyException);

  //
  // User hooks
  //-----------------------------------------------------------

  /**
   * Data processing user hook for 8 bits DI data.
   * It is the user responsability to delete the passed DIByteData.
   */
  virtual void handle_byte_input (asl::DIByteData* data);
  
  /**
   * Data processing user hook for 16 bits DI data.
   * It is the user responsability to delete the passed DIShortData.
   */
  virtual void handle_short_input (asl::DIShortData* data); 
  
  /**
   * Data processing user hook for 32 bits DI data.
   * It is the user responsability to delete the passed DILongData.
   */
  virtual void handle_long_input (asl::DILongData* data);

  /**
   * DAQ exception user hook.
   * It is the user responsability to delete the passed DAQException.
   */
  virtual void handle_error (const asl::DAQException& ex) = 0;  

  /**
   * Data lost user hook.
   * This default implementation does nothing.
   */
  virtual void handle_data_lost ();  

  /**
   * DAQ end user hook.
	 * This default implementation does nothing.
   */
  virtual void handle_daq_end (ContinuousDAQ::DaqEndReason why);  

  /**
   * Timeout user hook.
   * This default implementation does nothing.
   */
  virtual void handle_timeout (); 

  //
  // Misc. 
  //-----------------------------------------------------------

  /**
   * Dump DAQ info. 
   */
  virtual void dump () const;

protected:

  /**
   * Handle data_lost exception (called by external handler). 
   */
  virtual void handle_data_lost_exception () 
    throw (DAQException); 

  /**
   * Handle DAQ exception (called by external handler). 
   */
  virtual void handle_daq_exception () 
    throw (DAQException); 

  /**
   * Returns the reason why the DAQ stopped
   */
  virtual ContinuousDAQ::DaqEndReason daq_end_reason ();

  /**
   * Class wide event callbacks.
   */
#if defined (WIN32)
   static void db_event_callback ();
   static void di_end_event_callback ();
#else
   static void db_event_callback (int sig_num);
   static void di_end_event_callback (int sig_num);
#endif

private:

  /**
   * Return true if initialized, false otherwise.
   */
   bool initialized () const;

  /**
   * Internal implementation of the configure the member.
   */
  void configure_i (const ContinuousDIConfig& config)
       throw (asl::DAQException);

  /**
   * Internal implementation of the start member.
   */
  void start_i () throw (asl::DAQException);

  /**
   * Internal implementation of the stop member.
   */
  void stop_i () throw (asl::DAQException);

  /**
   * Internal implementation of the abort member.
   */
  void abort_i () throw (asl::DAQException);

  /**
   * Handle fatal error.
   */
  void handle_daq_exception_i (const DAQException&);

  /**
   * Handle data lost exception.
   */
  void handle_data_lost_exception_i ();

  /**
   * The DAQ configuration.
   */
  ContinuousDIConfig config_;

  /**
   * The underlying Worker (handle defered processing).
   */
  Worker * worker_;

  /**
   * The underlying Worker's work
   */
  ContinuousDIWork * work_;

  /**
   * The underlying DAQ hardware.
   */
  adl::ADContinuousDIBoard * daq_hw_;

  /**
   * A lock to protect the DAQ against race conditions on ctor execution
   */
  static ACE_Thread_Mutex delete_lock;

#if defined(_SIMULATION_)
  /**
   * The DAQ simulator.
   */
  CallbackCaller * cb_caller_;
#endif // _SIMULATION_

#if !defined (DAQ_CALLBACK_ACCEPT_ARG)
  static ContinuousDI * instance;
#endif

  // = Disallow these operations.
  //--------------------------------------------
  ACE_UNIMPLEMENTED_FUNC(ContinuousDI& operator= (const ContinuousDI&))
  ACE_UNIMPLEMENTED_FUNC(ContinuousDI(const ContinuousDI&))
};

} // namespace asl

#if defined (__ASL_INLINE__)
# include "asl/ContinuousDI.i"
#endif // __ASL_INLINE__

#endif // _CONTINUOUS_DI_H_

