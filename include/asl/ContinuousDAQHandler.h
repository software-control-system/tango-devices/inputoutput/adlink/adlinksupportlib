// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ContinuousDAQHandler.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _CONTINUOUS_DAQ_EXT_HDL_H_
#define _CONTINUOUS_DAQ_EXT_HDL_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/Thread.h>
#include <asl/DAQException.h>

namespace asl {

// ============================================================================
// FORWARD DECLARATION
// ============================================================================
class ContinuousDAQ;

// ============================================================================
//! The ASL ContinuousDAQHandler abstraction base class.  
// ============================================================================
//!  
//! detailed description to be written
//! 
// ============================================================================
class ASL_EXPORT ContinuousDAQHandler : private Thread
{
  enum {
    HANDLE_OVERRUN,
    HANDLE_DAQ_ERROR,
    HANDLE_CALIBRATION
  } Action;

public:

  /**
   * Initialization. 
   */
  ContinuousDAQHandler ();

  /**
   * Release resources.
   */
  virtual ~ContinuousDAQHandler ();

  /**
   * Call the "handle_overrun" member of the specified ContinuousDAQ.
   */
  void call_handle_data_lost_exception (ContinuousDAQ * _daq);

  /**
   * Call the "abort" member of the specified ContinuousDAQ.
   */
  void call_handle_daq_exception (ContinuousDAQ * _daq);

#if defined (USE_ASYNC_CALIBRATION)
  /**
   * Call the "hardware_calibration" member of the specified ContinuousDAQ.
   */
  void call_handle_hardware_calibration (ContinuousDAQ * _daq);
#endif

protected:

  /**
   * The action to perform.
   */
  int action_;

  /**
   * Thread entry point.
   */
  virtual ACE_THR_FUNC_RETURN svc (void* arg);

private:

  // = Disallow these operations.
  //--------------------------------------------
  ACE_UNIMPLEMENTED_FUNC(ContinuousDAQHandler& operator= (const ContinuousDAQHandler&))
  ACE_UNIMPLEMENTED_FUNC(ContinuousDAQHandler(const ContinuousDAQHandler&))
};

} // namespace asl

#endif // _CONTINUOUS_DAQ_EXT_HDL_H_

