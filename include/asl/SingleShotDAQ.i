// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    SingleShotDAQ.i
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

namespace asl {

#if defined(ASL_DEBUG)
// ============================================================================
// SingleShotDAQ::instance_counter
// ============================================================================
u_long SingleShotDAQ::instance_counter () 
{
  return instance_counter_.value();
}
#endif  

} // namespace asl


