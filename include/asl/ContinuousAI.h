// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ContinuousAI.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _CONTINUOUS_AI_H_
#define _CONTINUOUS_AI_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/Worker.h>
#include <asl/adlink/ADContinuousAIBoard.h>
#include <asl/ContinuousDAQ.h>
#include <asl/ContinuousAIConfig.h>

namespace asl {

// ============================================================================
// FORWARD DECLARATION
// ============================================================================
//- daq simulation 
#if defined(_SIMULATION_)
class CallbackCaller;
#endif


// ============================================================================
//! The ASL continuous analog input DAQ class.  
// ============================================================================
//! Provides functions to perform continuous analog input on ADLink boards that
//! support this functions.
// ============================================================================ 
class ASL_EXPORT ContinuousAI : public ContinuousDAQ
{
  friend class ContinuousAIWork;

public:

  /**
   * Initialization. 
   */
  ContinuousAI ();
  
  /**
   * Release resources.
   */
  virtual ~ContinuousAI ();

  //
  // Continuous DAQ control methods
  //-----------------------------------------------------------

  /**
   * Configure the continuous analog input DAQ.
   */
  void configure (const ContinuousAIConfig& config)
    throw (asl::DAQException, asl::DeviceBusyException);

  /**
   * Configure the continuous analog input DAQ.
   */
  void configure (ContinuousAIConfig * config)
      throw (asl::DAQException, asl::DeviceBusyException);
 
  /**
   * (re)Configure the user data parameters for the specified channel
   */
  void set_user_data_parameters (adl::ChanId id, double gain, double offset1, double offset2)
    throw (asl::DAQException);
    
  /**
   * Returns the current configuration.
   */
  const ContinuousAIConfig& configuration () const;

  /**
   * Initialize the continuous analog input DAQ.
   */
  virtual void init (unsigned short hw_type, unsigned short hw_id)
      throw (asl::DAQException);

  /**
   * Performs hardware auto-calibration asynchronously (i.e launch
   * calibration process then return immediately). See also ContinuousDAQ
   * calibration_completed and check_hardware_calibration.
   */
  virtual void calibrate_hardware ()
      throw (asl::DAQException, asl::DeviceBusyException);

  /**
   * Start the continuous analog input DAQ.
   */
  virtual void start ()
      throw (asl::DAQException, asl::DeviceBusyException);

  /**
   * Stop the continuous analog input DAQ.
   */
  virtual void stop ()
      throw (asl::DAQException, asl::DeviceBusyException);

  /**
   * Abort the continuous analog input DAQ.
   */
  virtual void abort ()
      throw (asl::DAQException, asl::DeviceBusyException);

  //
  // User hooks
  //-----------------------------------------------------------

  /**
   * Data processing user hook.
   * It is the user responsability to delete the passed AIBuffer.
   */
  virtual void handle_input (asl::AIRawData* data) = 0;  

  /**
   * DAQ exception user hook.
   */
  virtual void handle_error (const asl::DAQException& ex) = 0;
	
  /**
   * Data lost user hook.
   * This default implementation does nothing.
   */
  virtual void handle_data_lost ();  

  /**
   * DAQ end user hook.
   * If enabled, the history buffer is frozen before calling this hook.
	 * This default implementation does nothing.
   */
  virtual void handle_daq_end (ContinuousDAQ::DaqEndReason why);  

  /**
   * Timeout user hook.
   * This default implementation does nothing.
   */
  virtual void handle_timeout ();

  //
  // Data & messages processing 
  //-----------------------------------------------------------

  /**
   * AI data scaling (converts ADC raw data to volts)
   */
  virtual AIScaledData * scale_data (AIRawData * raw_data) const
          throw (asl::DAQException);

  /**
   * AI data scaling (converts ADC raw data to volts)
   */
  virtual void scale_data (AIRawData * raw_data, AIScaledData *& scaled_data)
          throw (asl::DAQException);

  /**
   * User data conversion (converts scaled data to user data)
   */
  virtual  AIScaledData * convert_to_user_data (AIScaledData * scaled_data) const
          throw (asl::DAQException);

  /**
   * User data conversion (converts scaled data to user data)
   */
  virtual void convert_to_user_data (AIScaledData * scaled_data, AIScaledData *& user_data) 
          throw (asl::DAQException);

  /**
   * User defined data mask (applies the user defined data mask on the specifed data)
   */
  virtual void apply_user_data_mask (AIScaledData * scaled_or_user_data) const
          throw (asl::DAQException);

  /**
   * User defined data mask (applies the user defined data mask on the specifed data)
   */
  virtual void apply_user_data_mask (AIScaledData * scaled_or_user_data, AIScaledData *& masked_data) 
          throw (asl::DAQException);

  //
  // History (i.e. circular buffer)
  //-----------------------------------------------------------

  /**
   * Clears the history buffer 
   * NB: history buffer is cleared on each DAQ start.
   */
  void clear_history ();

  /**
   * Freezes the history buffer 
   * NB: history buffer is unfrozen on each DAQ start.
   */
  void freeze_history ();

  /**
   * Returns the "scaled" history buffer 
   */
  AIScaledData * scaled_history () const
          throw (asl::DAQException);

  /**
   * Returns the "raw" history buffer 
   */
  AIRawData * raw_history () const
    throw (asl::DAQException);

  /**
   * Returns the last sample index in the history buffer (pre or middle trigger only)
   */
  unsigned long last_sample_index_in_history_buffer () const;

   /**
   * Returns the two DAQ buffers - This method is supposed to be used for pre and middle trigger test/debugging purpose
   * Should be called once the DAQ_END event is received.
   * @param buffer_0 The pointed buffer contains the data contanined into the first DAQ buffer after the trigger is fired
   * @param buffer_1 The pointed buffer contains the data contanined into the second DAQ buffer after the trigger is fired
   */
  void daq_buffers (asl::AIRawData*& buffer_0, asl::AIRawData*& buffer_1)
    throw (asl::DAQException);
    
  //
  // Misc. 
  //-----------------------------------------------------------

  /**
   * Dump DAQ info. 
   */
  virtual void dump () const;

protected:

  /**
   * Handle data_lost exception (called by external handler). 
   */
  virtual void handle_data_lost_exception () 
    throw (DAQException); 

  /**
   * Handle DAQ exception (called by external handler). 
   */
  virtual void handle_daq_exception () 
    throw (DAQException); 

#if defined (USE_ASYNC_CALIBRATION)
  /**
   * Handle calibration (called by external handler). 
   */
  virtual void handle_hardware_calibration () 
    throw (DAQException);
#endif

  /**
   * Returns the reason why the DAQ stopped
   */
  virtual ContinuousDAQ::DaqEndReason daq_end_reason ();

  /**
   * Class wide event callbacks.
   */
#if defined (WIN32)
   static void db_event_callback ();
   static void trig_event_callback ();
   static void ai_end_event_callback ();
#else
   static void db_event_callback (int sig_num);
   static void trig_event_callback (int sig_num);
   static void ai_end_event_callback (int sig_num);
#endif
   static void db_or_trig_event_callback (adl::Event _evt);

  /**
   * Applies (user defined) data mask on raw data
   */
  virtual void apply_data_mask (asl::AIRawData * raw_data) const
    throw (asl::DAQException);

  /**
   * Applies (user defined) data mask on raw data
   */
  virtual void apply_data_mask (unsigned short * addr, unsigned long depth) const
    throw (asl::DAQException);

  /**
   * Applies (user defined) data mask on scaled data
   */
  virtual void apply_data_mask (asl::AIScaledData * data) const
    throw (asl::DAQException);

  /**
   * Applies (user defined) data mask on scaled data
   */
  virtual void apply_data_mask (double * addr, unsigned long depth) const
    throw (asl::DAQException);

  /**
   * Converts scaled data to user data 
   */
  virtual void scaled_to_user_data (asl::AIScaledData * data) const
    throw (asl::DAQException);

  /**
   * Converts scaled data to user data 
   */
  virtual void scaled_to_user_data (double * addr, unsigned long depth) const
    throw (asl::DAQException);

private:
	
  /**
   * Return true if initialized, false otherwise.
   */
   bool initialized () const;

  /**
   * Internal implementation of the configure the member.
   */
  void configure_i (const ContinuousAIConfig& config)
       throw (asl::DAQException);

  /**
   * Internal implementation of the start member.
   */
  void start_i ()
       throw (asl::DAQException);

  /**
   * Internal implementation of the stop member.
   */
  void stop_i ()
       throw (asl::DAQException);

  /**
   * Internal implementation of the abort member.
   */
  void abort_i ()
       throw (asl::DAQException);

  /**
   * Handle fatal error.
   */
  void handle_daq_exception_i (const asl::DAQException&);

  /**
   * Handle data lost exception.
   */
  void handle_data_lost_exception_i ();
	
  /**
   * Handle hardware calibration.
   */
  void handle_hardware_calibration_i ()
      throw (asl::DAQException);

  /**
   * Returns the last sample index in the history buffer (pre or middle trigger only)
   */
  unsigned long last_sample_index_in_last_daq_buffer () const;

  /**
   * Returns the last sample index in the history buffer (pre or middle trigger only)
   */
  void last_sample_index_in_history_buffer (unsigned long lsi);

  /**
   * Returns the samples remaining into the DAQ buffer after a
   * pre-trigger condition. Returns 0 (i.e. NIL) in case there is no data available.
   * This is ugly (should be private)
   */
  AIRawData * remaining_samples()
      throw (asl::DAQException);

  /**
   * The DAQ configuration.
   */
  ContinuousAIConfig config_;

  /**
   * The underlying Worker (handle defered processing).
   */
  Worker * worker_;

  /**
   * The underlying Worker's work
   */
  ContinuousAIWork * work_;

  /**
   * The underlying DAQ hardware.
   */
  adl::ADContinuousAIBoard * daq_hw_;

  /**
   * The circular buffer;
   */
  AICircularBuffer * cb_;

  /**
   * Last sample index (pre or middle trigger mode only)
   */
  unsigned long last_sample_index_in_history_buffer_;

  /**
   * A lock to protect the DAQ against race conditions on ctor execution
   */
  static ACE_Thread_Mutex delete_lock;

#if defined(_SIMULATION_)
  /**
   * The DAQ simulator.
   */
  CallbackCaller * cb_caller_;
#endif // _SIMULATION_

#if !defined (DAQ_CALLBACK_ACCEPT_ARG)
  static ContinuousAI * instance;
#endif

  // = Disallow these operations.
  //--------------------------------------------
  ACE_UNIMPLEMENTED_FUNC(ContinuousAI& operator= (const ContinuousAI&))
  ACE_UNIMPLEMENTED_FUNC(ContinuousAI(const ContinuousAI&))
};

} // namespace asl

#if defined (__ASL_INLINE__)
# include "asl/ContinuousAI.i"
#endif // __ASL_INLINE__

#endif // _CONTINUOUS_AI_H_

