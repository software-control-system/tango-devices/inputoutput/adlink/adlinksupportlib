// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    DAQException.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _DAQ_EXCEPTION_H_
#define _DAQ_EXCEPTION_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <asl/Exception.h>

namespace asl {

// ============================================================================
//! DAQ exception class.  
// ============================================================================
//!  
//! detailed description to be written
//! 
// ============================================================================
class ASL_EXPORT DAQException : public asl::Exception
{
public:

  /**
   * Initialization (unknown error). 
   */
  DAQException ();

  /**
   * Initialization. 
   */
  DAQException (const char *reason,
					      const char *desc,
					      const char *origin,
                int err_code = -1, 
                int severity = asl::ERR);

  /**
   * Initialization. 
   */
  DAQException (const std::string& reason,
					      const std::string& desc,
					      const std::string& origin,
                int err_code = -1, 
                int severity = asl::ERR);
  
  /**
   * Copy constructor 
   */
  DAQException(const DAQException& src);

  /**
   * Release resources.
   */
  virtual ~DAQException ();

  /**
   * operator=
   */
  DAQException& operator= (const DAQException& _src); 
};


// ============================================================================
//! Data lost exception class.  
// ============================================================================
//!  
//! detailed description to be written
//! 
// ============================================================================
class ASL_EXPORT DataLostException : public DAQException
{
public:

  /**
   * Possible reasons to lost data.
   */
  enum {
    DAQ_BUFFER_OVERRUN,
    PROC_TASK_OVERLOAD
  };

  /**
   * Initialization. 
   */
  DataLostException (int why);
  
  /**
   * Release resources.
   */
  virtual ~DataLostException ();

  /**
   * Reasons why we lost data.
   */
  int why () const {
    return why_; 
  };

private:
  /**
   * Reasons why we lost data.
   */
  int why_;
};

// ============================================================================
//! Device busy exception class.  
// ============================================================================
//!  
//! detailed description to be written
//! 
// ============================================================================
class ASL_EXPORT DeviceBusyException : public DAQException
{
public:

  /**
   * Initialization. 
   */
  DeviceBusyException (const char *reason,
					             const char *desc,
					             const char *origin,
                       int err_code = -1, 
                       int severity = asl::ERR);

  /**
   * Initialization. 
   */
  DeviceBusyException (const std::string& reason,
					             const std::string& desc,
					             const std::string& origin,
                       int err_code = -1, 
                       int severity = asl::ERR);
  
  /**
   * Release resources.
   */
  virtual ~DeviceBusyException ();
};

} // namespace asl

#if defined (__ASL_INLINE__)
# include "asl/DAQException.i"
#endif // __ASL_INLINE__

#endif // _DAQ_EXCEPTION_H_

