// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    ContinuousDI.i
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

namespace asl {

// ============================================================================
// ContinuousDI::configure
// ============================================================================
ASL_INLINE void 
ContinuousDI::configure (ContinuousDIConfig* _config)
  throw (asl::DAQException, asl::DeviceBusyException)
{
  if (_config == 0) {
    throw asl::DAQException("invalid configuration specified",
                            "invalid configuration specified (null pointer)",
                            "ContinuousDI::configure");
  }
  this->configure(*_config);
}

// ============================================================================
// ContinuousDI::configuration
// ============================================================================
ASL_INLINE const ContinuousDIConfig& 
ContinuousDI::configuration () const
{
  return this->config_;
}

// ============================================================================
// ContinuousDI::initialized
// ============================================================================
ASL_INLINE bool
ContinuousDI::initialized () const
{
  //-TODO: 
  return (this->work_ && this->worker_ /* && this->daq_hw_*/) ? true : false;
}

// ============================================================================
// ContinuousDI::handle_data_lost
// ============================================================================
ASL_INLINE void 
ContinuousDI::handle_data_lost ()
{
  //-noop default implementation
} 

// ============================================================================
// ContinuousDI::handle_timeout
// ============================================================================
ASL_INLINE void 
ContinuousDI::handle_timeout ()
{
  //-noop default implementation
} 

// ============================================================================
// ContinuousDI::handle_daq_end
// ============================================================================
ASL_INLINE void 
ContinuousDI::handle_daq_end (ContinuousDAQ::DaqEndReason reason_why)
{
  ACE_UNUSED_ARG(reason_why);
  //-noop default implementation
} 

} // namespace asl





