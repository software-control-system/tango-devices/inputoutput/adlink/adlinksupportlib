// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    Data_T.hpp
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

#ifndef _DATA_T_HPP_
#define _DATA_T_HPP_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <ace/OS_NS_string.h>
#include <ace/Malloc_Allocator.h>
#include "asl/Data_T.h"

#if !defined (__ASL_INLINE__)
# include "asl/Data_T.i"
#endif // __ASL_INLINE__

namespace asl {

// ============================================================================
// Class : Container
// ============================================================================
// ============================================================================
// Container::Container
// ============================================================================
template <class T> 
Container<T>::Container(T& _data)
 : data_ (_data)
{

}

// ============================================================================
// Container::~Container
// ============================================================================
template <class T> 
Container<T>::~Container(void)
{

}

// ============================================================================
// Container::size
// ============================================================================
template <class T> 
size_t Container<T>::size(void) const
{
  return sizeof(T);
}

// ============================================================================
// Class : Buffer
// ============================================================================
// ============================================================================
// Buffer::Buffer
// ============================================================================
template <class T>
Buffer<T>::Buffer(unsigned long _depth, ACE_Allocator *_alloc) 
  : BaseBuffer ()
{
  // Check input.
  ACE_ASSERT(_depth > 0);

  // Use the user specified allocator or the default singleton one.
  this->alloc_ = (_alloc == 0) ? ACE_Allocator::instance() : _alloc;

  // Set buffer depth to 0 in case of ENOMEM error during ACE_ALLOCATOR call.
  this->depth_ = 0;

  // Allocate memory (will return on ENOMEM error).
  ACE_ALLOCATOR(this->base_, (T*)this->alloc_->malloc(_depth * sizeof(T)));

  // Set buffer depth.
  this->depth_ = _depth;
}

// ============================================================================
// Buffer::Buffer
// ============================================================================
template <class T>
Buffer<T>::Buffer(unsigned long _depth, T* _base, ACE_Allocator *_alloc)
  : BaseBuffer ()
{
  // Check input.
  ACE_ASSERT(_depth > 0);

  // Use the user specified allocator or the default singleton one.
  this->alloc_ = (_alloc == 0) ? ACE_Allocator::instance() : _alloc;

  // Set buffer depth to 0 in case of ENOMEM error during ACE_ALLOCATOR call.
  this->depth_ = 0;

  // Allocate memory (will return on ENOMEM error).
  ACE_ALLOCATOR(this->base_, (T*)this->alloc_->malloc(_depth * sizeof(T)));

  // Set buffer depth.
  this->depth_ = _depth;

  // Copy from source to destination using <Buffer::operator=>.
  *this = _base;
}

// ============================================================================
// Buffer::Buffer
// ============================================================================
template <class T> 
Buffer<T>::Buffer(const Buffer<T>& _src)
  : BaseBuffer ()
{
  // Get src allocator.
  this->alloc_ = _src.allocator();
  
  // Set buffer depth to 0 in case of ENOMEM error during ACE_ALLOCATOR call.
  this->depth_ = 0;

  // Allocate memory (will return on ENOMEM error).
  ACE_ALLOCATOR(this->base_, (T*)this->alloc_->malloc(_src.size()));

  // Set buffer depth.
  this->depth_ = _src.depth();

  // Copy from source to destination using <Buffer::operator=>.
  *this = _src;
}

// ============================================================================
// Buffer::~Buffer
// ============================================================================
template <class T> 
Buffer<T>::~Buffer(void)
{
  // Release memory (it is safe to free a null base addr).
  this->alloc_->free(this->base_);
}

// ============================================================================
// Class : CircularBuffer
// ============================================================================
// ============================================================================
// Buffer::CircularBuffer
// ============================================================================
template <class T>
CircularBuffer<T>::CircularBuffer(unsigned long _modulo, unsigned long _factor, ACE_Allocator *_alloc) 
  : Buffer<T>(_modulo * _factor, _alloc), 
    wp_(0), 
    frozen_(false), 
    modulo_(_modulo), 
    factor_(_factor)
{
  this->clear();
}

// ============================================================================
// CircularBuffer::~CircularBuffer
// ============================================================================
template <class T> 
CircularBuffer<T>::~CircularBuffer(void)
{

}

// ============================================================================
// CircularBuffer::freeze
// ============================================================================
template <class T> 
void CircularBuffer<T>::freeze (void)
{
  ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());

  this->frozen_ = true;
}

// ============================================================================
// CircularBuffer::unfreeze
// ============================================================================
template <class T> 
void CircularBuffer<T>::unfreeze (void)
{
  ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());

  this->frozen_ = false;
}

// ============================================================================
// CircularBuffer::clear
// ============================================================================
template <class T> 
void CircularBuffer<T>::clear (void)
{
  ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());

  this->wp_ = this->base_;
  this->Buffer<T>::clear();
}

// ============================================================================
// CircularBuffer::push
// ============================================================================
template <class T> 
void CircularBuffer<T>::push(Buffer<T> * _data)
  throw (asl::DAQException)
{

  ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());

  //- std::cout << "CircularBuffer<T>::push <-" << std::endl;
  //- std::cout << "CircularBuffer<T>::push::before copy::wp-@" << this->wp_ << std::endl;
  //- std::cout << "CircularBuffer<T>::push::before copy::wp-i" << ((long)this->wp_ - (long)this->base_) / sizeof(T) << std::endl;

  //- if frozen then ignore the data
  if (this->frozen_) { 
    //- std::cout << "CircularBuffer<T>::push::buffer is frozen ->" << std::endl;
    return;
  }

  if (_data) 
  {
    //- be sure <_data> has the right size
    if (_data->depth() != this->modulo_) {
      //- out of memory
      throw asl::DAQException("invalid buffer size",
                            "invalid buffer size",
                            "CircularBuffer<T>::push");
    }
    //- copy data
    //- std::cout << "CircularBuffer<T>::push::copying data" << std::endl;
    ACE_OS::memcpy(this->wp_, _data->base(), _data->depth() * sizeof(T)); 
  }
  else {
    //- std::cout << "CircularBuffer<T>::push::pushing dummy values" << std::endl;
    ACE_OS::memset(this->wp_, 0, this->modulo_ * sizeof(T)); 
  }

  //- update write pointer 
  this->wp_ += this->modulo_;
  if (this->wp_ >= (this->base_ + this->depth_)) {
    this->wp_ = this->base_;
  }

  //- std::cout << "CircularBuffer<T>::after copy::push::wp-@" << this->wp_ << std::endl;
  //- std::cout << "CircularBuffer<T>::after copy::push::wp-i" << ((long)this->wp_ - (long)this->base_) / sizeof(T) << std::endl;
  //- std::cout << "CircularBuffer<T>::push ->" << std::endl;
}

// ============================================================================
// CircularBuffer::ordered_data
// ============================================================================
template <class T> 
asl::Buffer<T> * CircularBuffer<T>::ordered_data (void)
  throw (asl::DAQException)
{
  ACE_GUARD_REACTION(ACE_Thread_Mutex, 
                     ace_mon, 
                     this->lock_, 
                     throw asl::DAQException());

  asl::Buffer<T> * tmp = new asl::Buffer<T>(this->depth_);
  if (tmp == 0) {
    //- out of memory
    throw asl::DAQException("out of memory",
                            "buffer allocation failed",
                            "CircularBuffer<T>::ordered_data");
  } 

  //- clear tmp buffer
  tmp->clear();
 
  //- source and destination pointers
  T * src_p = this->wp_;
  T * max_src_addr = this->base_ + this->depth_;
  T * dest_p = tmp->base();

  //- reorder the data
  for (unsigned long i = 0; i < this->factor_; i++) 
  {
    ACE_OS::memcpy(dest_p, src_p,  this->modulo_ * sizeof(T)); 
    src_p += this->modulo_;
    if (src_p >= max_src_addr) {
      src_p = this->base_;
    }
    dest_p += this->modulo_;
  }

  return tmp;
}

} // namespace asl 

#endif // _DATA_T_HPP_

