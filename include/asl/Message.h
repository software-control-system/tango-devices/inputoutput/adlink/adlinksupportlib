// ============================================================================
//
// = CONTEXT
//    TANGO Project - ADLink Support Library
//
// = FILENAME
//    Message.h
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

#ifndef _MESSAGE_H_
#define _MESSAGE_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <string>
#include <ace/Message_Block.h>
#include <asl/ASLConfig.h>
#if defined(ASL_DEBUG)
# include <asl/Mld.h>
#endif
# include <asl/Data.h>

// ============================================================================
// DEFINE
// ============================================================================
//- sizeof empty message (pure semantic - no data)
#define kDEFAULT_MSG_SIZE 1

namespace asl {

// ============================================================================
//! Message types
// ============================================================================
//! 
//! detailed description to be written
//! 
// ============================================================================
class ASL_EXPORT MessageType
{
public:
  enum {
    //-- CONTROL MESSAGES : (fowarded to clients) ------------------
      CONTROL_FLAG = ACE_Message_Block::MB_USER, 
      //- Regular priority control messages (go to tail of queue)
      MSG_START, 
      MSG_TIMEOUT,
      MSG_RESET, 
      MSG_STOP,  
      //- High priority control messages (go to head of queue)
      MSG_TASK_PRIORITY,
      MSG_ABORT, 
      MSG_QUIT,  
    //-- REGULAR MESSAGES ------------------------------------------
      MSG_REGULAR_FLAG = 0x400, 
      //-Regular priority messages
      MSG_DATA, 
      MSG_OVERRUN, 
      MSG_OVERLOAD, 
      MSG_DAQEND, 
    //-- ERROR MESSAGES --------------------------------------------
      //- Regular priority error messages (go to tail of queue)
      MSG_ERROR,       
      //- High priority error messages (go to head of queue)
      MSG_FATAL_ERROR,
    //-- USER DEFINED MESSAGES -------------------------------------
      MSG_FIRST_USER_MSG = 0x800
  };
};

// ============================================================================
//! Message priorities
// ============================================================================
//! 
//! detailed description to be written
//! 
// ============================================================================
class ASL_EXPORT MessagePriority
{
public:

  enum {
    //- Regular priority messages (go to tail of queue)
    MSG_PRIO_DEFAULT      = 0,
    MSG_PRIO_INIT         = 0,
    MSG_PRIO_START        = 0,
    MSG_PRIO_DATA         = 0,
    MSG_PRIO_RESET        = 0,
    MSG_PRIO_TIMEOUT      = 0,
    MSG_PRIO_STOP         = 0,
    MSG_PRIO_ERROR        = 0,
    //- High priority messages (go to head of queue)
    MSG_PRIO_TASK_PRIO    = 1,
    MSG_PRIO_ABORT        = 2,
    MSG_PRIO_FATAL_ERROR  = 2,
    MSG_PRIO_QUIT         = 3
  };
};

// ============================================================================
// Class: DataBlock
// ============================================================================
class ASL_EXPORT DataBlock : public ACE_Data_Block
{
  friend class Message;

  typedef ACE_Data_Block inherited;

public:
  
  enum {
    MSG_DELETE_NONE = 0,
    // Assume ownership and do NOT delete underlying data_ when 
    // the DataBlock is destroyed.
    MSG_DELETE_DATA = 1
    // Delete data_ when the DataBlock is destroyed (default).
  };

  DataBlock (Data *_data = 0, int _flags = MSG_DELETE_DATA);

  virtual ~DataBlock();

  Data * data () const;

  void data (Data *_new_data);

  void detach_data ();

#if defined(ASL_DEBUG)
  static u_long instance_counter() { 
    return instance_counter_.value();
  }
#endif

protected:

  size_t data_size () const;

private:

  //- internal cooking: set priority in accordance to type
  void set_priority_i ();

  //- ownership flag
  int flags_;

  //- the data. 
  Data * data_;

#if defined(ASL_DEBUG)
  //- instances counter
  static AtomicOp instance_counter_;
#endif

#if defined(ASL_DEBUG)
  //- memory leak detector
  MLD; 
#endif

  // = Disallow these operations.
  //--------------------------------------------
  ACE_UNIMPLEMENTED_FUNC(DataBlock& operator= (const DataBlock&))
  ACE_UNIMPLEMENTED_FUNC(DataBlock (const DataBlock&))
};

// ============================================================================
//! The ASL message abstraction base class.  
// ============================================================================
//! 
//! detailed description to be written
//! 
// ============================================================================
class ASL_EXPORT Message : public ACE_Message_Block
{
  friend class MessageAllocator;

  typedef ACE_Message_Block inherited;

public:

  //- Data ownership flag values
  enum {
   // Assume ownership and do NOT delete data when the DataBlock is destroyed.
   MSG_DELETE_NONE = 0,
   // Delete data when the DataBlock is destroyed.
   MSG_DELETE_DATA = 1
  };

  /**
   * Create an empty message of type msg_type. Such a message does
   * not contain any data and can be considered as a control message.
   */
  Message (int msg_type, bool force_processing = false);

  /**
   * Create a message containing _data. By default, the msg_type
   * is set to MessageType::MSG_DATA and the ownership flag to
   * MSG_DELETE_DATA.
   */
  Message (Data *_data = 0,
           int msg_type = MessageType::MSG_DATA,
           bool force_processing = false,
           int _flags = MSG_DELETE_DATA);

  /**
   * Destroy a message. If the ownership flag is set to MSG_DELETE_NONE 
   * then assume ownership and do NOT delete the underlying data_ when 
   * the DataBlock is destroyed. Otherwise, delete the data_.  
   */
  virtual ~Message ();

  /**
   * 
   */
  Message * duplicate ();

  /**
   * Return the continuation field.
   */
  Message * cont () const;

  /**
   * Set the continuation field.
   */
  void cont (Message *_cont);

  /**
   * Returns true if this message is a type message. false otherwise. 
   */
  bool is_a (int type) const;

  /**
   * Returns message size in bytes.
   */
  size_t data_size () const;

  /**
   * Set data ownership flag to MSG_DELETE_NONE
   */
  void detach_data ();

  /**
   * Return true if the Mesage should be passed to the Work's
   * process method even if the Worker is not RUNNING
   */
  bool force_processing ();

#if defined(ASL_DEBUG)
  /**
   * Returns the current instance counter value
   */
  static unsigned long instance_counter();
#endif

private:
  
  //- internal cooking: change set priority in accordance to type
  void set_priority_i ();

  // Since we use a dedicated allocator, we can have instance 
  // data member in the Message class. Remember that duplicate 
  // deals with ACE_Message_Block!
  bool force_processing_;

#if defined(ASL_DEBUG)
  /**
   * Instances counter.
   */
  static AtomicOp instance_counter_;

  /**
   * Memory leak detector.
   */
  MLD; 
#endif

  // = Disallow these operations.
  //--------------------------------------------
  ACE_UNIMPLEMENTED_FUNC(Message& operator= (const Message&))
  ACE_UNIMPLEMENTED_FUNC(Message (const Message&))
};

} // namespace asl

#if defined (__ASL_INLINE__)
# include "asl/Message.i"
#endif /* __ASL_INLINE__ */

#endif // _MESSAGE_H_

