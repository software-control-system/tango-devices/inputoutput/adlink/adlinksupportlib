@ECHO off

:: ======== TANGO HOST 
set TANGO_HOST=localhost:20000

:: ======== INSTALL_ROOT
set INSTALL_ROOT=%SOLEIL_ROOT%

:: ======== ACE PATHS
set ACE_ROOT=%SOLEIL_ROOT%\hw-support\ace
set ACE_INC=%ACE_ROOT%\include
set ACE_LIB=%ACE_ROOT%\lib
set ACE_BIN=%ACE_ROOT%\bin
set PATH=%ACE_BIN%;%PATH%

:: ======== D2K-DASK PATHS
set D2KDASK_ROOT=C:\soleil\drivers\adlink\d2k-dask
set D2KDASK_INC=%D2KDASK_ROOT%\include
set D2KDASK_LIB=%D2KDASK_ROOT%\lib

:: ======== PCIS-DASK PATHS
set PCISDASK_ROOT=C:\soleil\drivers\adlink\pcis-dask
set PCISDASK_INC=%PCISDASK_ROOT%\include
set PCISDASK_LIB=%PCISDASK_ROOT%\lib

:: ======== OMNIORB PATHS
set OMNIORB_INC=%SOLEIL_ROOT%\vc8\omniorb\include
set OMNIORB_LIB=%SOLEIL_ROOT%\vc8\omniorb\lib
set OMNIORB_BIN=%SOLEIL_ROOT%\vc8\omniorb\bin
set PATH=%OMNIORB_BIN%;%PATH%

:: ======== TANGO PATHS
set TANGO_INC=%SOLEIL_ROOT%\vc8\tango\include
set TANGO_LIB=%SOLEIL_ROOT%\vc8\tango\lib\shared
set TANGO_BIN=%SOLEIL_ROOT%\vc8\tango\bin
set PATH=%TANGO_BIN%;%PATH%

echo "env. setup done"

start /B .\asl-projects.sln

