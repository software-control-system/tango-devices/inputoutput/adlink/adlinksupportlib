# Microsoft Developer Studio Project File - Name="asl" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=asl - Win32 Simulation Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "asl.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "asl.mak" CFG="asl - Win32 Simulation Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "asl - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "asl - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "asl - Win32 Simulation Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "asl - Win32 Simulation Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "asl - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "asl_EXPORTS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GR /GX /O2 /I "../../include" /I "../../src" /I "$(ACE_INC)" /I "$(D2KDASK_INC)" /I "$(PCISDASK_INC)" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "ASL_EXPORTS" /D "ASL_HAS_DLL" /D "ASL_BUILD" /FR /FD /c
# SUBTRACT CPP /X /YX
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x40c /d "NDEBUG"
# ADD RSC /l 0x40c /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386
# ADD LINK32 ace.lib D2K-Dask.lib PCI-Dask.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386 /out:"build/asl.dll" /implib:"build/asl.lib" /libpath:"$(D2KDASK_LIB)" /libpath:"$(PCISDASK_LIB)" /libpath:"$(ACE_LIB)"
# SUBTRACT LINK32 /pdb:none /nodefaultlib

!ELSEIF  "$(CFG)" == "asl - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "asl_EXPORTS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GR /GX /ZI /Od /I "../../include" /I "../../src" /I "$(ACE_INC)" /I "$(D2KDASK_INC)" /I "$(PCISDASK_INC)" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "ASL_EXPORTS" /D "ASL_HAS_DLL" /D "ASL_BUILD" /FR /YX /FD /GZ /c
# SUBTRACT CPP /X
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x40c /d "_DEBUG"
# ADD RSC /l 0x40c /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 aced.lib D2K-Dask.lib PCI-Dask.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /out:"build/asld.dll" /implib:"build/asld.lib" /pdbtype:sept /libpath:"$(D2KDASK_LIB)" /libpath:"$(PCISDASK_LIB)" /libpath:"$(ACE_LIB)"
# SUBTRACT LINK32 /pdb:none /nodefaultlib

!ELSEIF  "$(CFG)" == "asl - Win32 Simulation Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "asl_Win32_Simulation_Debug"
# PROP BASE Intermediate_Dir "asl_Win32_Simulation_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Simulation_Debug"
# PROP Intermediate_Dir "Simulation_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GR /GX /ZI /Od /I "../../include" /I "../../src" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "asl_EXPORTS" /D "asl_HAS_DLL" /D "asl_BUILD_DLL" /FR /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GR /GX /ZI /Od /I "../../include" /I "../../src" /I "$(ACE_INC)" /I "$(D2KDASK_INC)" /I "$(PCISDASK_INC)" /D "_SIMULATION_" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "ASL_EXPORTS" /D "ASL_HAS_DLL" /D "ASL_BUILD" /FR /YX /FD /GZ /c
# SUBTRACT CPP /X
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x40c /d "_DEBUG"
# ADD RSC /l 0x40c /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 aced.lib D2K-Dask.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /out:"build/msvc6/asld.dll" /pdbtype:sept
# ADD LINK32 aced.lib D2K-Dask.lib PCI-Dask.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /out:"build/aslsimuld.dll" /implib:"build/aslsimuld.lib" /pdbtype:sept /libpath:"$(D2KDASK_LIB)" /libpath:"$(PCISDASK_LIB)" /libpath:"$(ACE_LIB)"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "asl - Win32 Simulation Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "asl___Win32_Simulation_Release"
# PROP BASE Intermediate_Dir "asl___Win32_Simulation_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Simulation_Release"
# PROP Intermediate_Dir "Simulation_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GR /GX /O2 /I "../../include" /I "../../src" /I "$(D2KDASK_INC)" /I "$(PCISDASK_INC)" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "ASL_EXPORTS" /D "ASL_HAS_DLL" /D "ASL_BUILD" /FR /YX /FD /c
# SUBTRACT BASE CPP /X
# ADD CPP /nologo /MD /W3 /GR /GX /O2 /I "../../include" /I "../../src" /I "$(ACE_INC)" /I "$(D2KDASK_INC)" /I "$(PCISDASK_INC)" /D "_SIMULATION_" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "ASL_EXPORTS" /D "ASL_HAS_DLL" /D "ASL_BUILD" /FR /YX /FD /c
# SUBTRACT CPP /X
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x40c /d "NDEBUG"
# ADD RSC /l 0x40c /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ace.lib D2K-Dask.lib PCI-Dask.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386 /out:"build/asl.dll" /implib:"build/asl.lib" /libpath:"$(D2KDASK_LIB)" /libpath:"$(PCISDASK_LIB)"
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 ace.lib D2K-Dask.lib PCI-Dask.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386 /out:"build/aslsimul.dll" /implib:"build/aslsimul.lib" /libpath:"$(D2KDASK_LIB)" /libpath:"$(PCISDASK_LIB)" /libpath:"$(ACE_LIB)"
# SUBTRACT LINK32 /pdb:none

!ENDIF 

# Begin Target

# Name "asl - Win32 Release"
# Name "asl - Win32 Debug"
# Name "asl - Win32 Simulation Debug"
# Name "asl - Win32 Simulation Release"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\..\src\adlink\AD2005.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\adlink\AD2010.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\adlink\AD2204.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\adlink\AD2205.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\adlink\AD2502.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\adlink\AD6208.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\adlink\AD7248.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\adlink\AD7300.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\adlink\AD7432.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\adlink\ADBoard.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\adlink\ADCalibrableAIOBoard.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\adlink\ADContinuousAIBoard.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\adlink\ADContinuousAOBoard.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\adlink\ADContinuousDIBoard.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\adlink\ADD2kDask.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\adlink\ADPcisDask.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\adlink\ADSingleShotAIBoard.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\adlink\ADSingleShotAOBoard.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\adlink\ADSingleShotDIBoard.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\adlink\ADSingleShotDOBoard.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\ContinuousAI.cpp

!IF  "$(CFG)" == "asl - Win32 Release"

!ELSEIF  "$(CFG)" == "asl - Win32 Debug"

# ADD CPP /W3

!ELSEIF  "$(CFG)" == "asl - Win32 Simulation Debug"

!ELSEIF  "$(CFG)" == "asl - Win32 Simulation Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\src\ContinuousAIConfig.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\ContinuousAO.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\ContinuousAOConfig.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\ContinuousDAQ.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\ContinuousDAQHandler.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\ContinuousDI.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\ContinuousDIConfig.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\ContinuousDO.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\DAQException.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\Data.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\Data_T.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\Exception.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\Extractor_T.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\Message.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\Mld.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\PulsedTask.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\PulsedTaskManager.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\SharedObject.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\SingleShotAI.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\SingleShotAO.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\SingleShotDAQ.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\SingleShotDI.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\SingleShotDO.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\Thread.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\Work.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\Worker.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\WorkerError.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\..\include\asl\adlink\AD2005.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\adlink\AD2010.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\adlink\AD2204.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\adlink\AD2205.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\adlink\AD2502.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\adlink\AD6208.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\adlink\AD7248.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\adlink\AD7300.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\adlink\AD7432.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\adlink\ADBoard.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\adlink\ADCalibrableAIOBoard.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\adlink\ADContinuousAIBoard.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\adlink\ADContinuousAOBoard.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\adlink\ADContinuousDIBoard.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\adlink\ADLink.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\adlink\ADSingleShotAIBoard.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\adlink\ADSingleShotAOBoard.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\adlink\ADSingleShotDIBoard.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\adlink\ADSingleShotDOBoard.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\AIData.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\AOData.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\ASLConfig.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\CallbackArgument.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\ContinuousAI.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\ContinuousAIConfig.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\ContinuousAO.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\ContinuousAOConfig.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\ContinuousDAQ.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\ContinuousDAQHandler.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\ContinuousDI.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\ContinuousDIConfig.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\ContinuousDO.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\DAQException.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\Data.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\Data_T.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\DIData.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\Exception.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\Export.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\Extractor_T.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\Message.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\Mld.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\PulsedTask.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\PulsedTaskManager.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\SharedObject.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\SingleShotAI.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\SingleShotAO.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\SingleShotDAQ.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\SingleShotDI.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\SingleShotDO.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\Thread.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\Work.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\Worker.h
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\WorkerError.h
# End Source File
# End Group
# Begin Group "Inline Files"

# PROP Default_Filter ".i"
# Begin Source File

SOURCE=..\..\include\asl\adlink\ADBoard.i
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\ContinuousAI.i
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\ContinuousAIConfig.i
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\ContinuousAIHandler.i
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\ContinuousAO.i
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\ContinuousAOConfig.i
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\ContinuousDAQ.i
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\ContinuousDI.i
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\ContinuousDIConfig.i
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\ContinuousDO.i
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\DAQException.i
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\Data.i
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\Data_T.i
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\Exception.i
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\Extractor_T.i
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\Message.i
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\PulsedTask.i
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\PulsedTaskManager.i
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\SharedObject.i
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\SingleShotAI.i
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\SingleShotAO.i
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\SingleShotDAQ.i
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\SingleShotDI.i
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\SingleShotDO.i
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\Thread.i
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\Work.i
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\Worker.i
# End Source File
# Begin Source File

SOURCE=..\..\include\asl\WorkerError.i
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
